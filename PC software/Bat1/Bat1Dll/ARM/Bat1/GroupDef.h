//******************************************************************************
//
//   GroupDef.h   Group definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __GroupDef_H__
   #define __GroupDef_H__

#ifndef __DsDef_H__
   #include "../Inc/File/DsDef.h"
#endif

#define GRP_CLASS  2         // file directory group class

typedef TFDataSet TGroup;

#endif
