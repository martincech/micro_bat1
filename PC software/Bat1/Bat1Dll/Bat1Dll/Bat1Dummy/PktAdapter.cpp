//******************************************************************************
//
//   PktAdapter.cpp   Packet adapter
//   Version 0.0      (c) VymOs
//
//******************************************************************************

#include <windows.h>
#include <stdio.h>

#include "../../Library/Unisys/Uni.h"
#include "../../Bat1Dll/Bat1Dll/PktAdapter.h"
#include <typeinfo.h>
#include <string.h>

// Configuration names :

#define USB_DEVICE_NAME "VEIT Bat1 Poultry Scale"        // USB adapter name

//******************************************************************************
// Constructor
//******************************************************************************

TPktAdapter::TPktAdapter()
// Constructor
{
   RxTimeout               = 500;
   RxIntercharacterTimeout = 2;
   RxPacketTimeout         = 500;

   Logger     = 0;

   // default parameters :
   Parameters.BaudRate     = 38400;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUsbUart::NO_PARITY;
   Parameters.Handshake    = TUsbUart::NO_HANDSHAKE;
   strcpy( UsbDevice, USB_DEVICE_NAME);

   FPort       = 0;
} // TPktAdapter

//******************************************************************************
// Destructor
//******************************************************************************

TPktAdapter::~TPktAdapter()
// Destructor
{
} // ~TPktAdapter

//******************************************************************************
// Send
//******************************************************************************

BOOL TPktAdapter::Send( int Command, int Data)
// Send message
{
   return( FALSE);
} // Send

//******************************************************************************
// Send data
//******************************************************************************

BOOL TPktAdapter::SendData( void *Data, int Length)
// Send long data
{
   return( FALSE);
} // SendData

//******************************************************************************
// Receive
//******************************************************************************

BOOL TPktAdapter::Receive( int &Command, int &Data)
// Receive message
{
   return( FALSE);
} // Receive

//******************************************************************************
// Get long message
//******************************************************************************

void TPktAdapter::GetData( int &Length, void *Data)
// Get data of long message
{
} // GetData

//******************************************************************************
// Property IsOpen
//******************************************************************************

BOOL TPktAdapter::GetIsOpen()
// returns true if device opened
{
   return( FALSE);
} // GetIsOpen()

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Flush
//******************************************************************************

void TPktAdapter::FlushRxChars()
// Flush chars up to intercharacter timeout
{
} // FlushRxChars

//******************************************************************************
// Check Connection
//******************************************************************************

BOOL TPktAdapter::CheckConnect()
// Check if adapter is ready
{
   return( FALSE);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TPktAdapter::Disconnect()
// Disconnect port
{
} // Disconnect

//******************************************************************************
// CRC
//******************************************************************************

byte TPktAdapter::CalcCrc( byte *Data, int Size)
// Calculate CRC
{
   return( 0);
} // CalcCrc
