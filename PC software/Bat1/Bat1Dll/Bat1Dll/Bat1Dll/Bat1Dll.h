//******************************************************************************
//
//   Bat1Dll.h    Bat1 DLL library definitions
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef Bat1DllH
   #define Bat1DllH

#ifdef DLL_EXPORT
   #define DLL_IMPORT extern "C" __declspec( dllexport)
#else
   #define DLL_IMPORT extern "C" __declspec( dllimport)
#endif

//-----------------------------------------------------------------------------
//   Helper functions
//-----------------------------------------------------------------------------

DLL_IMPORT unsigned APIENTRY GetDllVersion( void);
// Returns DLL version

DLL_IMPORT BOOL APIENTRY IsUsbLess( void);
// Returns TRUE if library is without USB support

DLL_IMPORT void APIENTRY EnableLogger( BOOL Enable);
// Enable communication logger (at console)

DLL_IMPORT void APIENTRY DecodeTime( int Timestamp, int *Day, int *Month, int *Year, int *Hour, int *Min, int *Sec);
// Decode <Timestamp> to items

DLL_IMPORT int APIENTRY EncodeTime( int Day, int Month, int Year, int Hour, int Min, int Sec);
// Returns timestamp by items

DLL_IMPORT double APIENTRY DecodeWeight( int Weight);
// Decode <Weight> to number

DLL_IMPORT int APIENTRY EncodeWeight( double Weight);
// Encode <Weight> to internal representation

//-----------------------------------------------------------------------------
//   Locales table
//-----------------------------------------------------------------------------

DLL_IMPORT int APIENTRY GetLocaleLanguage( int Country);
// Get language

DLL_IMPORT int APIENTRY GetLocaleCodePage( int Language);
// Get code page

DLL_IMPORT int APIENTRY GetLocaleDateFormat( int Country);
// Get date format

DLL_IMPORT char APIENTRY GetLocaleDateSeparator1( int Country);
// Get first date separator

DLL_IMPORT char APIENTRY GetLocaleDateSeparator2( int Country);
// Get second date separator

DLL_IMPORT int APIENTRY GetLocaleTimeFormat( int Country);
// Get time format

DLL_IMPORT char APIENTRY GetLocaleTimeSeparator( int Country);
// Get time separator

DLL_IMPORT int APIENTRY GetLocaleDaylightSavingType( int Country);
// Get daylight saving time type

//-----------------------------------------------------------------------------
//   Device functions
//-----------------------------------------------------------------------------

DLL_IMPORT BOOL APIENTRY CheckDevice( void);
// Returns TRUE if device present

DLL_IMPORT BOOL APIENTRY DeviceIsOn( BOOL *PowerOn);
// Sets <PowerOn> TRUE on powered device

DLL_IMPORT BOOL APIENTRY DevicePowerOff( void);
// Switch power off

DLL_IMPORT BOOL APIENTRY GetTime( int *Clock);
// Get device clock

DLL_IMPORT BOOL APIENTRY SetTime( int Clock);
// Set device clock

DLL_IMPORT void APIENTRY NewDevice( void);
// Create new empty device

DLL_IMPORT BOOL APIENTRY LoadConfiguration( void);
// Load configuration data only from device

DLL_IMPORT BOOL APIENTRY LoadDevice( void);
// Load data from device

DLL_IMPORT BOOL APIENTRY SaveDevice( void);
// Save data to device

DLL_IMPORT BOOL APIENTRY ReloadConfiguration( void);
// Reload configuration from EEPROM to device

DLL_IMPORT BOOL APIENTRY LoadEstimation( int *Promile);
// Load size estimation [%%]

DLL_IMPORT void APIENTRY SaveEstimation( int *Promile);
// Save size estimation [%%]

DLL_IMPORT BOOL APIENTRY LoadCrashInfo( void);
// Load device crash info

//-----------------------------------------------------------------------------
//   EEPROM access
//-----------------------------------------------------------------------------

DLL_IMPORT BOOL APIENTRY ReadEeprom( int Address, unsigned char *Buffer, int Size);
// Read data from device EEPROM

DLL_IMPORT BOOL APIENTRY WriteEeprom( int Address, unsigned char *Buffer, int Size);
// Write data to device EEPROM

DLL_IMPORT int APIENTRY GetEepromSize( void);
// Returns device EEPROM size

DLL_IMPORT BOOL APIENTRY DeviceByEeprom( unsigned char *Buffer);
// Setup device by EEPROM contents

DLL_IMPORT int APIENTRY GetLogoSize( void);
// Returns logo size

DLL_IMPORT int APIENTRY GetLogoAddress( void);
// Returns logo start address

//-----------------------------------------------------------------------------
//   Configuration data
//-----------------------------------------------------------------------------

//---- get/set version

DLL_IMPORT unsigned APIENTRY GetDeviceVersion( void);
// Get device version

DLL_IMPORT void APIENTRY SetDeviceVersion( unsigned Version);
// Set device version

DLL_IMPORT unsigned APIENTRY GetBuild( void);
// Get device build

DLL_IMPORT void APIENTRY SetBuild( unsigned Build);
// Set device build

DLL_IMPORT unsigned APIENTRY GetHwVersion( void);
// Get hardware version

DLL_IMPORT void APIENTRY SetHwVersion( unsigned Version);
// Set hardware version

//---- get/set scale parameters

DLL_IMPORT void APIENTRY GetScaleName( char *Name);
// Get scale name

DLL_IMPORT void APIENTRY SetScaleName( char *Name);
// Set scale name

DLL_IMPORT void APIENTRY ClearPassword( void);
// Disable password

DLL_IMPORT BOOL APIENTRY ValidPassword( void);
// Returns TRUE if password is in use

DLL_IMPORT void APIENTRY GetPassword( unsigned char *Password);
// Get password

DLL_IMPORT void APIENTRY SetPassword( unsigned char *Password);
// Set password

//---- get/set country data

DLL_IMPORT int APIENTRY GetCountry( void);
// Get country

DLL_IMPORT void APIENTRY SetCountry( int Country);
// Set country

DLL_IMPORT int APIENTRY GetLanguage( void);
// Get language

DLL_IMPORT void APIENTRY SetLanguage( int Language);
// Set language

DLL_IMPORT int APIENTRY GetCodePage( void);
// Get code page

DLL_IMPORT void APIENTRY SetCodePage( int CodePage);
// Set code page

DLL_IMPORT int APIENTRY GetDeviceDateFormat( void);
// Get date format

DLL_IMPORT void APIENTRY SetDeviceDateFormat( int Format);
// Set date format

DLL_IMPORT char APIENTRY GetDateSeparator1( void);
// Get first date separator

DLL_IMPORT void APIENTRY SetDateSeparator1( char Separator);
// Set first date separator

DLL_IMPORT char APIENTRY GetDateSeparator2( void);
// Get second date separator

DLL_IMPORT void APIENTRY SetDateSeparator2( char Separator);
// Set second date separator

DLL_IMPORT int APIENTRY GetDeviceTimeFormat( void);
// Get time format

DLL_IMPORT void APIENTRY SetDeviceTimeFormat( int Format);
// Set time format

DLL_IMPORT char APIENTRY GetTimeSeparator( void);
// Get time separator

DLL_IMPORT void APIENTRY SetTimeSeparator( char Separator);
// Set time separator

DLL_IMPORT int APIENTRY GetDaylightSavingType( void);
// Get daylight saving time type

DLL_IMPORT void APIENTRY SetDaylightSavingType( int DstType);
// Set daylight saving time type

//---- get/set weighing units

DLL_IMPORT int APIENTRY GetWeighingUnits( void);
// Get weighing units

DLL_IMPORT void APIENTRY SetWeighingUnits( int Units);
// Set weighing units

DLL_IMPORT int APIENTRY GetWeighingCapacity( void);
// Get weighing capacity

DLL_IMPORT void APIENTRY SetWeighingCapacity( int Capacity);
// Set weighing capacity

DLL_IMPORT int APIENTRY GetWeighingRange( void);
// Get weighing range

DLL_IMPORT int APIENTRY GetWeighingDecimals( void);
// Get weighing decimals

DLL_IMPORT int APIENTRY GetWeighingMaxDivision( void);
// Get weighing max. division

DLL_IMPORT int APIENTRY GetWeighingDivision( void);
// Get weighing division

DLL_IMPORT void APIENTRY SetWeighingDivision( int Division);
// Set weighing division

//---- get/set sound settings

DLL_IMPORT int APIENTRY GetToneDefault( void);
// Get default beep

DLL_IMPORT void APIENTRY SetToneDefault( int Tone);
// Set default beep

DLL_IMPORT int APIENTRY GetToneLight( void);
// Get light beep

DLL_IMPORT void APIENTRY SetToneLight( int Tone);
// Set light beep

DLL_IMPORT int APIENTRY GetToneOk( void);
// Get OK beep

DLL_IMPORT void APIENTRY SetToneOk( int Tone);
// Set OK beep

DLL_IMPORT int APIENTRY GetToneHeavy( void);
// Get heavy beep

DLL_IMPORT void APIENTRY SetToneHeavy( int Tone);
// Set heavy beep

DLL_IMPORT int APIENTRY GetToneKeyboard( void);
// Get keyboard beep

DLL_IMPORT void APIENTRY SetToneKeyboard( int Tone);
// Set keyboard beep

DLL_IMPORT BOOL APIENTRY GetEnableSpecialSounds( void);
// Get enable special sounds

DLL_IMPORT void APIENTRY SetEnableSpecialSounds( BOOL Enable);
// Set enable special sounds

DLL_IMPORT int APIENTRY GetVolumeSaving( void);
// Get saving volume

DLL_IMPORT void APIENTRY SetVolumeSaving( int Volume);
// Set saving volume

DLL_IMPORT int APIENTRY GetVolumeKeyboard( void);
// Get keyboard volume

DLL_IMPORT void APIENTRY SetVolumeKeyboard( int Volume);
// Set keyboard volume

//---- get/set display settings

DLL_IMPORT int APIENTRY GetDisplayMode( void);
// Get display mode

DLL_IMPORT void APIENTRY SetDisplayMode( int Mode);
// Set display mode

DLL_IMPORT int APIENTRY GetDisplayContrast( void);
// Get display contrast

DLL_IMPORT void APIENTRY SetDisplayContrast( int Contrast);
// Set display contrast

DLL_IMPORT int APIENTRY GetBacklightMode( void);
// Get backlight mode

DLL_IMPORT void APIENTRY SetBacklightMode( int Mode);
// Set backlight mode

DLL_IMPORT int APIENTRY GetBacklightIntensity( void);
// Get backlight intensity

DLL_IMPORT void APIENTRY SetBacklightIntensity( int Intensity);
// Set backlight intensity

DLL_IMPORT int APIENTRY GetBacklightDuration( void);
// Get backlight duration

DLL_IMPORT void APIENTRY SetBacklightDuration( int Duration);
// Set backlight duration

//---- get/set printer settings

DLL_IMPORT int APIENTRY GetPrinterPaperWidth( void);
// Get printer paper width

DLL_IMPORT void APIENTRY SetPrinterPaperWidth( int Width);
// Set printer paper width

DLL_IMPORT int APIENTRY GetPrinterCommunicationFormat( void);
// Get printer communication format

DLL_IMPORT void APIENTRY SetPrinterCommunicationFormat( int Format);
// Set printer communication format

DLL_IMPORT int APIENTRY GetPrinterCommunicationSpeed( void);
// Get printer communication format

DLL_IMPORT void APIENTRY SetPrinterCommunicationSpeed( int Speed);
// Set printer communication format

//---- get/set global parameters

DLL_IMPORT int APIENTRY GetKeyboardTimeout( void);
// Get keyboard timeout

DLL_IMPORT void APIENTRY SetKeyboardTimeout( int Timeout);
// Set keyboard timeout

DLL_IMPORT int APIENTRY GetPowerOffTimeout( void);
// Get power off timeout

DLL_IMPORT void APIENTRY SetPowerOffTimeout( int Timeout);
// Set power off timeout

DLL_IMPORT BOOL APIENTRY GetEnableFileParameters( void);
// Get enable file parameters

DLL_IMPORT void APIENTRY SetEnableFileParameters( BOOL Enable);
// Set enable file parameters

//---- get/set weighing parameters

DLL_IMPORT BOOL APIENTRY GetEnableMoreBirds( void);
// Get enable more birds

DLL_IMPORT void APIENTRY SetEnableMoreBirds( BOOL Enable);
// Set enable more birds

/*
DLL_IMPORT int APIENTRY GetNumberOfBirds( void);
// Get number of birds

DLL_IMPORT void APIENTRY SetNumberOfBirds( int NumberOfBirds);
// Set number of birds
*/

DLL_IMPORT int APIENTRY GetWeightSortingMode( void);
// Get weight sorting mode

DLL_IMPORT void APIENTRY SetWeightSortingMode( int Mode);
// Set weight sorting mode

/*
DLL_IMPORT int APIENTRY GetLowLimit( void);
// Get weight sorting low limit

DLL_IMPORT void APIENTRY SetLowLimit( int LowLimit);
// Set weight sorting low limit

DLL_IMPORT int APIENTRY GetHighLimit( void);
// Get weight sorting high limit

DLL_IMPORT void APIENTRY SetHighLimit( int HighLimit);
// Set weight sorting high limit
*/

DLL_IMPORT int APIENTRY GetSavingMode( void);
// Get saving mode

DLL_IMPORT void APIENTRY SetSavingMode( int Mode);
// Set saving mode

DLL_IMPORT int APIENTRY GetFilter( void);
// Get filter

DLL_IMPORT void APIENTRY SetFilter( int Filter);
// Set filter

DLL_IMPORT int APIENTRY GetStabilisationTime( void);
// Get stabilisation time

DLL_IMPORT void APIENTRY SetStabilisationTime( int StabilisationTime);
// Set stabilisation time

DLL_IMPORT int APIENTRY GetMinimumWeight( void);
// Get minimum weight

DLL_IMPORT void APIENTRY SetMinimumWeight( int Weight);
// Set minimum weight

DLL_IMPORT int APIENTRY GetStabilisationRange( void);
// Get stabilisation range

DLL_IMPORT void APIENTRY SetStabilisationRange( int Range);
// Set stabilisation range

//---- get/set statistics

DLL_IMPORT int APIENTRY GetUniformityRange( void);
// Get uniformity range

DLL_IMPORT void APIENTRY SetUniformityRange( int Range);
// Set uniformity range

DLL_IMPORT int APIENTRY GetHistogramMode( void);
// Get histogram mode

DLL_IMPORT int APIENTRY GetHistogramRange( void);
// Get histogram range

DLL_IMPORT void APIENTRY SetHistogramRange( int Range);
// Set histogram range

DLL_IMPORT int APIENTRY GetHistogramStep( void);
// Get histogram step

DLL_IMPORT void APIENTRY SetHistogramStep( int Step);
// Set histogram step

//-----------------------------------------------------------------------------
//  Crash data
//-----------------------------------------------------------------------------

//---- Exception info

DLL_IMPORT unsigned APIENTRY GetExceptionTimestamp( void);
// Get exception date & time

DLL_IMPORT unsigned APIENTRY GetExceptionAddress( void);
// Get exception address

DLL_IMPORT int APIENTRY GetExceptionType( void);
// Get exception type

DLL_IMPORT unsigned APIENTRY GetExceptionStatus( void);
// Get exception status

//---- WatchDog info

DLL_IMPORT unsigned APIENTRY GetWatchDogTimestamp( void);
// Get watchdog date & time

DLL_IMPORT unsigned APIENTRY GetWatchDogStatus( void);
// Get watchdog status

//-----------------------------------------------------------------------------
//  Data files
//-----------------------------------------------------------------------------

//---- directory maitenance

DLL_IMPORT int APIENTRY GetFilesCount( void);
// Get number of files

DLL_IMPORT void APIENTRY FilesDeleteAll( void);
// Delete all files

DLL_IMPORT int APIENTRY FileCreate( void);
// Create new file, returns index

//---- get/set directory data

DLL_IMPORT void APIENTRY GetFileName( int Index, char *Name);
// Get file name

DLL_IMPORT void APIENTRY SetFileName( int Index, char *Name);
// Set file name

DLL_IMPORT void APIENTRY GetFileNote( int Index, char *Note);
// Get file note

DLL_IMPORT void APIENTRY SetFileNote( int Index, char *Note);
// Set file note

DLL_IMPORT unsigned APIENTRY GetFileCreation( int Index);
// Get file creation date

DLL_IMPORT int APIENTRY GetFileRawSize( int Index);
// Get file size [bytes]

DLL_IMPORT BOOL APIENTRY IsCurrentFile( int Index);
// Returns TRUE on current working file

DLL_IMPORT void APIENTRY SetCurrentFile( int Index);
// Set file on <Index> as current working file

//---- get/set file configuration

DLL_IMPORT BOOL APIENTRY GetFileEnableMoreBirds( int Index);
// Get enable more birds

DLL_IMPORT void APIENTRY SetFileEnableMoreBirds( int Index, BOOL Enable);
// Set enable more birds

DLL_IMPORT int APIENTRY GetFileNumberOfBirds( int Index);
// Get number of birds

DLL_IMPORT void APIENTRY SetFileNumberOfBirds( int Index, int NumberOfBirds);
// Set number of birds

DLL_IMPORT int APIENTRY GetFileWeightSortingMode( int Index);
// Get weight sorting mode

DLL_IMPORT void APIENTRY SetFileWeightSortingMode( int Index, int Mode);
// Set weight sorting mode

DLL_IMPORT int APIENTRY GetFileLowLimit( int Index);
// Get weight sorting low limit

DLL_IMPORT void APIENTRY SetFileLowLimit( int Index, int LowLimit);
// Set weight sorting low limit

DLL_IMPORT int APIENTRY GetFileHighLimit( int Index);
// Get weight sorting high limit

DLL_IMPORT void APIENTRY SetFileHighLimit( int Index, int HighLimit);
// Set weight sorting high limit

DLL_IMPORT int APIENTRY GetFileSavingMode( int Index);
// Get saving mode

DLL_IMPORT void APIENTRY SetFileSavingMode( int Index, int Mode);
// Set saving mode

DLL_IMPORT int APIENTRY GetFileFilter( int Index);
// Get filter

DLL_IMPORT void APIENTRY SetFileFilter( int Index, int Filter);
// Set filter

DLL_IMPORT int APIENTRY GetFileStabilisationTime( int Index);
// Get stabilisation time

DLL_IMPORT void APIENTRY SetFileStabilisationTime( int Index, int StabilisationTime);
// Set stabilisation time

DLL_IMPORT int APIENTRY GetFileMinimumWeight( int Index);
// Get minimum weight

DLL_IMPORT void APIENTRY SetFileMinimumWeight( int Index, int Weight);
// Set minimum weight

DLL_IMPORT int APIENTRY GetFileStabilisationRange( int Index);
// Get stabilisation range

DLL_IMPORT void APIENTRY SetFileStabilisationRange( int Index, int Range);
// Set stabilisation range

//---- get/set samples

DLL_IMPORT int APIENTRY GetFileSamplesCount( int Index);
// Get file samples count

DLL_IMPORT void APIENTRY FileClearSamples( int Index);
// Clear file samples

DLL_IMPORT void APIENTRY FileAllocSamples( int Index, int SamplesCount);
// Allocate file samples

DLL_IMPORT int APIENTRY GetSampleTimestamp( int Index, int SampleIndex);
// Get file sample timestamp

DLL_IMPORT void APIENTRY SetSampleTimestamp( int Index, int SampleIndex, int Timestamp);
// Set file sample timestamp

DLL_IMPORT int APIENTRY GetSampleWeight( int Index, int SampleIndex);
// Get file sample weight

DLL_IMPORT void APIENTRY SetSampleWeight( int Index, int SampleIndex, int Weight);
// Set file sample weight

DLL_IMPORT int APIENTRY GetSampleFlag( int Index, int SampleIndex);
// Get file sample flag

DLL_IMPORT void APIENTRY SetSampleFlag( int Index, int SampleIndex, int Flag);
// Set file sample flag

//-----------------------------------------------------------------------------
//  File Groups
//-----------------------------------------------------------------------------

//---- directory maitenance

DLL_IMPORT int APIENTRY GetGroupsCount( void);
// Get number of groups

DLL_IMPORT void APIENTRY GroupsDeleteAll( void);
// Delete all groups

DLL_IMPORT int APIENTRY GroupCreate( void);
// Create new group, returns index


//---- get/set directory data

DLL_IMPORT void APIENTRY GetGroupName( int Index, char *Name);
// Get group name

DLL_IMPORT void APIENTRY SetGroupName( int Index, char *Name);
// Set group name

DLL_IMPORT void APIENTRY GetGroupNote( int Index, char *Note);
// Get group note

DLL_IMPORT void APIENTRY SetGroupNote( int Index, char *Note);
// Set group note

DLL_IMPORT unsigned APIENTRY GetGroupCreation( int Index);
// Get group creation date

//--- file list

DLL_IMPORT int APIENTRY GetGroupFilesCount( int Index);
// Returns number of files in the group

DLL_IMPORT void APIENTRY GroupClearFiles( int Index);
// Clear group files

DLL_IMPORT int APIENTRY GetGroupFile( int Index, int FileIndex);
// Returns index into file list from the group at position <FileIndex>

DLL_IMPORT void APIENTRY AddGroupFile( int Index, int FileIndex);
// Add <FileIndex> into group

#endif
