﻿using System.Configuration;
using System.Text;

namespace Bat1 {
    public static class AppConfig {
        public static readonly string ApplicationName        = ConfigurationManager.AppSettings["ApplicationName"];
        public static readonly string ApplicationNameSmall   = ConfigurationManager.AppSettings["ApplicationNameSmall"];
        public static readonly string DatabaseFileExtension  = ConfigurationManager.AppSettings["DatabaseFileExtension"];
        public static readonly string BackupFileExtension    = ConfigurationManager.AppSettings["BackupFileExtension"];
        public static readonly string HardcopyFileExtension  = ConfigurationManager.AppSettings["HardcopyFileExtension"];
        public static readonly string ExportFileExtension    = ConfigurationManager.AppSettings["ExportFileExtension"];
        public static readonly string CompanyDirectory       = ConfigurationManager.AppSettings["CompanyDirectory"];
        public static readonly string CompanyName            = ConfigurationManager.AppSettings["CompanyName"];
        public static readonly string CompanyAddress;
        public static readonly string CompanyMail            = ConfigurationManager.AppSettings["CompanyMail"];
        public static readonly string CompanyMailSubject     = ConfigurationManager.AppSettings["CompanyMailSubject"];
        public static readonly string CompanyWeb             = ConfigurationManager.AppSettings["CompanyWeb"];
        public static readonly string CompanyWebAddress      = ConfigurationManager.AppSettings["CompanyWebAddress"];

        static AppConfig() {
            // Vytvorim viceradkovy label s adresou, prvni prazdny radek konci
            var address = new StringBuilder();            
            for (var i = 1; i <= 6; i++) {
                var line = ConfigurationManager.AppSettings["CompanyAddress" + i];
                if (line == "") {
                    break;      // Prvni prazdny radek konci
                }
                if (i >= 2) {
                    address.Append("\n");     // Pridam CRLF
                }
                address.Append(line);
            }

            CompanyAddress = address.ToString();
        }
    }
}
