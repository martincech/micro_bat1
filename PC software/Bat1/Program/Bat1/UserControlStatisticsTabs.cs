﻿using System;
using System.Windows.Forms;
using Bat1.Properties;

namespace Bat1 {
    public partial class UserControlStatisticsTabs : UserControl {
        public UserControlStatisticsTabs() {
            InitializeComponent();

            // Skryju tlacitko vyberu u Last weighings
            userControlStatisticsTopLast.ButtonSelect.Visible = false;

            // Nastavim text tlacitka u Selected weighings
            userControlStatisticsTopSelected.ButtonSelect.Text = Resources.SELECT_WEIGHINGS + "...";
            
            // Zaregistruju eventy
            userControlStatisticsTopLast.ComboBoxFlag.SelectionChangeCommitted += comboBoxLastFlag_SelectionChangeCommitted;
            userControlStatisticsTopSelected.ButtonSelect.Click += buttonSelectWeighings_Click;
            userControlStatisticsTopSelected.ComboBoxFlag.SelectionChangeCommitted += comboBoxSelectedFlag_SelectionChangeCommitted;
        }

        public void ClearData() {
            // Automaticky zobrazim posledni vazeni
            RedrawLastWeighings();

            // Selected weighings ponecham beze zmeny
        }

        public void RedrawLastWeighings() {
            // Zobrazim tabpage s Last weighings
            tabControl.SelectedIndex = 0;

            // Zobrazim posledni vazeni
            userControlStatisticsLast.RedrawLastWeighings();
            
            // Naplnim seznam flagu, default vybere ALL
            userControlStatisticsTopLast.FillFlags(userControlStatisticsLast.WeighingList);
        }

        private void buttonSelectWeighings_Click(object sender, EventArgs e) {
            var form = new FormSelectWeighing();
            form.LoadSelectedWeighings(userControlStatisticsSelected.WeighingList);   // Naplnim dosud vybrana vazeni
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }
            
            // Prekreslim okno, jinak je to hnusne
            ParentForm.Refresh();

            // Nactu vybrana vazeni do pameti, jsou uz setridena podle datumu, vahy a souboru
            userControlStatisticsSelected.RedrawStatistics(form.SelectedWeighingInfoList.WeighingInfoList);

            // Naplnim seznam flagu, default vybere ALL
            userControlStatisticsTopSelected.FillFlags(userControlStatisticsSelected.WeighingList);
        }

        private void comboBoxLastFlag_SelectionChangeCommitted(object sender, EventArgs e) {
            userControlStatisticsLast.SetFlag(userControlStatisticsTopLast.ComboBoxFlag.SelectedIndex);
        }

        private void comboBoxSelectedFlag_SelectionChangeCommitted(object sender, EventArgs e) {
            userControlStatisticsSelected.SetFlag(userControlStatisticsTopSelected.ComboBoxFlag.SelectedIndex);
        }
    }
}
