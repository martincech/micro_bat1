﻿using System;
using System.Windows.Forms;
using Bat1.Properties;

namespace Bat1 {
    public partial class FormManualResults : Form {
        private Weighing editWeighing;

        public FormManualResults() {
            InitializeComponent();
        }

        public FormManualResults(Weighing weighing) {
            InitializeComponent();

            editWeighing = weighing;
            
            userControlManualResults.LoadWeighing(weighing);

            buttonSave.Text = Resources.SAVE_CHANGES;
            AcceptButton = buttonSave;
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            userControlManualResults.Save();

            if (editWeighing != null) {
                DialogResult = DialogResult.OK;     // Pri editaci hned po ulozeni koncim
            }
        }
    }
}
