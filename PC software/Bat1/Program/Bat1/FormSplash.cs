﻿using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormSplash : Form {
        public FormSplash() {
            InitializeComponent();

            // Nahraju splash screen
            try {
               pictureBoxSplash.Load(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Splash.png"));
            } catch {
                // Soubor neexistuje, nic se nedeje
            }

        }
    }
}
