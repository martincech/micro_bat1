﻿using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

// Spusteni prohlizece

namespace Bat1 {
    public partial class UserControlAbout : UserControl {
        public UserControlAbout() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            // Zobrazim jmeno aplikace a verzi
            labelApplicationName.Text = Program.ApplicationName;
            labelVersion.Text         = SwVersion.ToString();

            // Zobrazim informace o firme
            labelCompanyName.Text    = AppConfig.CompanyName;
            labelCompanyAddress.Text = AppConfig.CompanyAddress;

            // Zobrazim mail a web
            linkLabelMail.Text = AppConfig.CompanyMail;
            linkLabelWeb.Text  = AppConfig.CompanyWeb;

            // Nahraju logo
            try {
               pictureBoxLogo.Load(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Logo.png"));
            } catch {
                // Soubor neexistuje, nic se nedeje
            }
        }

        private void linkLabelMail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            // Otevru mailoveho klienta
            Process.Start("mailto:" + AppConfig.CompanyMail + "?subject=" + AppConfig.CompanyMailSubject + " " + SwVersion.ToString());        
        }

        private void linkLabelWeb_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            // Spustim webovy prohlizec
            var startInfo = new ProcessStartInfo(AppConfig.CompanyWebAddress);
            try {
                // Pomoci try-catch osetrim pripad, kdy spusteni nepovoli napr. firewall
                Process.Start(startInfo);
            } catch {
            }
        }
    }
}
