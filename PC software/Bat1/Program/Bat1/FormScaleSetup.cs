﻿using System.Drawing;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormScaleSetup : Form {
        public FormScaleSetup() {         
            InitializeComponent();

            // Vytvorim user control s configem
            var userControlScaleSetup = new UserControlScaleSetup(Text);
            userControlScaleSetup.Location = new Point(0, 0);
            //            userControlScaleSetup.Size     = new Size(699, 367);
            //            userControlScaleSetup.TabIndex = 0;
            userControlScaleSetup.Dock = DockStyle.Fill;
            userControlScaleSetup.SetScaleDefaultConfig();
            Controls.Add(userControlScaleSetup);               
        }
    }
}
