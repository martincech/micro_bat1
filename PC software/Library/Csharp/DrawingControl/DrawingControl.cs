﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;

namespace Veit.DrawingControl {
    /// <summary>
    /// Drawing suspend / resume support for any control
    /// </summary>
    public class DrawingControl {
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);
        
        private const int WM_SETREDRAW = 11;
        
        /// <summary>
        /// Suspend drawing of a control
        /// </summary>
        /// <param name="parent">Control to suspend</param>
        public static void SuspendDrawing(Control parent) {
            SendMessage(parent.Handle, WM_SETREDRAW, false, 0);
        }
        
        /// <summary>
        /// Resume drwawing of a control
        /// </summary>
        /// <param name="parent">Control to resume</param>
        public static void ResumeDrawing(Control parent) {
            SendMessage(parent.Handle, WM_SETREDRAW, true, 0);
            parent.Refresh();
        } 
    }
}
