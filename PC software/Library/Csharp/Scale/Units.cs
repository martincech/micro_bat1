﻿//******************************************************************************
//
//   Scale.cs       Scale definitions
//   Version 1.0
//
//******************************************************************************

namespace Veit.Scale {
    /// <summary>
    /// Weighing units
    /// </summary>
    public enum Units {
        KG,
        G,
        LB
    };
}