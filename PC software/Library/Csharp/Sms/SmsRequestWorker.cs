﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Veit.GsmWorker {
    /// <summary>
    /// Background worker that requests statistics from one modem
    /// </summary>
    public class SmsRequestWorker : SmsSendWorker {
        public SmsRequestWorker(int portNumber, ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted)
            : base (portNumber, progressChanged, runWorkerCompleted) {
        }

        /// <summary>
        /// Start worker to request statistics froma specified day
        /// </summary>
        /// <param name="phoneNumber">Phone number to send the request to</param>
        /// <param name="day">Day number or -1 for today's statistics</param>
        public void Start(string phoneNumber, int day) {
            smsWorker.StartRequest(phoneNumber, day);
        }

        /// <summary>
        /// Start worker to request today's statistics
        /// </summary>
        /// <param name="phoneNumber">Phone number to send the request to</param>
        public void Start(string phoneNumber) {
            Start(phoneNumber, -1);
        }
    }
}
