﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Veit.GsmWorker {
    /// <summary>
    /// Background worker that sends one SMS to multiple phone numbers (using one modem)
    /// </summary>
    public class SmsSendWorker {
        /// <summary>
        /// Workers
        /// </summary>
        protected SmsWorker smsWorker;

        /// <summary>
        /// Returns true if the worker is still running
        /// </summary>
        public bool IsBusy {
            get {
                return smsWorker.IsBusy;
            }
        }

        public SmsSendWorker(int portNumber, ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted) {
            // Vytvorim background worker
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress      = true;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork                    += new DoWorkEventHandler(SmsWorkerRaw.DoWork);
            worker.ProgressChanged           += new ProgressChangedEventHandler(progressChanged);
            worker.RunWorkerCompleted        += new RunWorkerCompletedEventHandler(runWorkerCompleted);

            // Vytvorim SmsWorker
            smsWorker = new SmsWorker(worker, portNumber);
        }

        /// <summary>
        /// Start worker to request statistics froma specified day
        /// </summary>
        /// <param name="phoneNumber">Phone number to send the request to</param>
        /// <param name="day">Day number or -1 for today's statistics</param>
        public void Start(string phoneNumber, int day) {
            smsWorker.StartRequest(phoneNumber, day);
        }

        /// <summary>
        /// Start worker to send SMS
        /// </summary>
        /// <param name="phoneNumberCollection">List of phone numbers to send the SMS to</param>
        /// <param name="text">SMS text</param>
        public void Start(List<string> phoneNumberCollection, string text) {
            smsWorker.StartSend(phoneNumberCollection, text);
        }

        /// <summary>
        /// Stop worker
        /// </summary>
        public void Stop() {
            smsWorker.Stop();
        }
    }
}
