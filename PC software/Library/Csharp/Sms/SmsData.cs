﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Veit.GsmWorker
{
    public enum SmsEvent {
        INIT,               // Inicializace
        DETECT,             // Detekce modemu na zadanem COM portu
        RESET,              // Reset
        CHECK,              // Kontrola pritomnosti modemu
        SIGNAL,             // Cekani na signal
        OPERATOR,           // Nacteni operatora
        STRENGTH,           // Nacteni sily signalu
        READY,              // Cekani, zaroven update sily signalu
        SMS_SENDING,        // Posilani SMS (progress = index tel. cisla v seznamu)
        SMS_SEND_OK,        // Odeslana SMS (progress = index tel. cisla v seznamu)
        SMS_SEND_ERROR,     // Chyba pri odesilani SMS (progress = index tel. cisla v seznamu)
        SMS_READING,        // Cteni SMS na dane pozici
        SMS_READ_OK,        // Nactena SMS na dane pozici
        SMS_READ_ERROR,     // Chyba cteni SMS na dane pozici
        SMS_DELETING,       // Mazani SMS na dane pozici
        SMS_DELETE_OK,      // Smazana SMS na dane pozici
        SMS_DELETE_ERROR,   // Chyba pri mazani SMS na dane pozici
        CANCEL              // Rucni preruseni
    }

    public enum SmsOperationMode {
        STANDBY,            // Modem prihlaseny, opakovane cteni sily signalu
        READ_ALL,           // Nacteni vsech SMS v pameti
        SEND,               // Poslani 1 SMS
        REQUEST             // Request statistiky z urciteho cisla
    }
    
    public class SmsInitData {
        /// <summary>
        /// COM port number
        /// </summary>
        public int PortNumber;

        /// <summary>
        /// Mode of operation
        /// </summary>
        public SmsOperationMode Mode;

        /// <summary>
        /// List of phone numbers for sending
        /// </summary>
        public List<string> SendPhoneNumberCollection;

        /// <summary>
        /// SMS text for sending
        /// </summary>
        public string SendSmsText;

        /// <summary>
        /// Phone number for requests
        /// </summary>
        public string RequestPhoneNumber;

        /// <summary>
        /// Day of request or -1 for today
        /// </summary>
        public int RequestDay;
    }
    
    /// <summary>
    /// Current status
    /// </summary>
    public class SmsStatus {
        /// <summary>
        /// COM port number, so I can recognize the modem
        /// </summary>
        public int PortNumber;

        /// <summary>
        /// Modem name
        /// </summary>
        public string ModemName;

        /// <summary>
        /// Current event of the GSM modem
        /// </summary>
        public SmsEvent Event;

        /// <summary>
        /// Operator name
        /// </summary>
        public string OperatorName;

        /// <summary>
        /// Signal strength in percents
        /// </summary>
        public int SignalStrength;

        /// <summary>
        /// Phone number of received SMS
        /// </summary>
        public string SmsNumber;

        /// <summary>
        /// Text of received SMS
        /// </summary>
        public string SmsText;

        /// <summary>
        /// Constructor
        /// </summary>
        public SmsStatus() {
            ModemName      = "";
            Event          = SmsEvent.INIT;
            OperatorName   = "";
            SignalStrength = 0;
        }
        
        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="source">Source</param>
        public SmsStatus(SmsStatus source) {
            PortNumber     = source.PortNumber;
            ModemName      = source.ModemName;
            Event          = source.Event;
            OperatorName   = source.OperatorName;
            SignalStrength = source.SignalStrength;
            SmsNumber      = source.SmsNumber;
            SmsText        = source.SmsText;
        }
    }
}
