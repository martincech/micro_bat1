﻿using System;
using System.Collections.Generic;
using System.Text;
using FirebirdSql.Data.FirebirdClient;

namespace Veit.Firebird {
    
    /// <summary>
    /// Firebird database maintenance
    /// </summary>
    static class DatabaseMaintenance {

        /// <summary>
        /// Create empty database
        /// </summary>
        static public void CreateEmptyDatabase(string connectionString) {
            FbConnection.CreateDatabase(connectionString);
        }

    }
}
