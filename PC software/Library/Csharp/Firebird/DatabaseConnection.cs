﻿using System;
using System.Collections.Generic;
using System.Text;
using FirebirdSql.Data.FirebirdClient;

namespace Veit.Firebird {
    
    /// <summary>
    /// Firebird database connection
    /// </summary>
    public partial class DatabaseConnection : IDisposable {

        /// <summary>
        /// Name of the database file including path
        /// </summary>
        private string fileName;

        /// <summary>
        /// Login name
        /// </summary>
        private string loginName;

        /// <summary>
        /// Login password
        /// </summary>
        private string loginPassword;

        /// <summary>
        /// Database connection
        /// </summary>
        private FbConnection connection;
        public FbConnection Connection {
            get { return connection; }
        }

        /// <summary>
        /// Current transaction
        /// </summary>
        private FbTransaction transaction;
        public FbTransaction Transaction {
            get { return transaction; }
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">Name of the database file including path</param>
        public DatabaseConnection(string fileName, string loginName, string loginPassword) {
            this.fileName      = fileName;
            this.loginName     = loginName;
            this.loginPassword = loginPassword;
            connection = new FbConnection(GetConnectionString());       // Vytvorim nove spojeni
        }

        /// <summary>
        /// Create database connection string
        /// </summary>
        /// <returns>Connection string</returns>
        public string GetConnectionString() {
            FbConnectionStringBuilder connectionString = new FbConnectionStringBuilder();
            connectionString.Database   = fileName;
            connectionString.UserID     = loginName;
            connectionString.Password   = loginPassword;
            connectionString.Charset    = "UTF8";
            connectionString.Pooling    = true;
            connectionString.ServerType = FbServerType.Embedded;

            return connectionString.ToString();
        }

        /// <summary>
        /// Connect to database
        /// </summary>
        public void Open() {
            connection.Open();
        }

        /// <summary>
        /// Disconnect from database. If there is any transaction pending, it is rolled back.
        /// </summary>
        public void Close() {
            if (transaction != null) {
                // Pokud probiha transakce, pred odpojenim ji zrusim
                RollbackTransaction();
            }
            connection.Close();
        }

        /// <summary>
        /// Clear connection pool
        /// </summary>
        public void ClearPool() {
            FbConnection.ClearAllPools();
        }

        /// <summary>
        /// Create new command using this connection
        /// </summary>
        /// <returns>Command</returns>
        public FbCommand CreateCommand() {
            return connection.CreateCommand();
        }

        /// <summary>
        /// Start transaction
        /// </summary>
        public void StartTransaction() {
            transaction = connection.BeginTransaction();
        }
    
        /// <summary>
        /// Commit transaction
        /// </summary>
        public void CommitTransaction() {
            transaction.Commit();       // Potvrdim transakci
            transaction.Dispose();      // Uvolnim
            transaction = null;         // Oznacim, ze jiz transakce neprobiha
        }
        
        /// <summary>
        /// Roll back transaction
        /// </summary>
        public void RollbackTransaction() {
            transaction.Rollback();     // Zrusim transakci
            transaction.Dispose();      // Uvolnim
            transaction = null;         // Oznacim, ze jiz transakce neprobiha
        }
     }

    // ---------------------------- IDisposable --------------------------------

    public partial class DatabaseConnection : IDisposable {
        // Track whether Dispose has been called
        private bool disposed = false;

        public void Dispose() {
            // Dispose of the managed and unmanaged resources
            Dispose(true);

            // Tell the GC that the Finalize process no longer needs to be run for this object
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposeManagedResources) {
            // Process only if mananged and unmanaged resources have not been disposed of
            if (!this.disposed) {
                if (disposeManagedResources) {
                    // Dispose managed resources:
                    connection.Dispose();
                    if (transaction != null) {
                        transaction.Dispose();
                    }
                }

                // Dispose unmanaged resources:

                // Note disposing has been done
                disposed = true;
            }
        }
    }
}
