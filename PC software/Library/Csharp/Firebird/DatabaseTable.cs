﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using Veit.Database;

namespace Veit.Firebird {
    
    /// <summary>
    /// Firtebird database table
    /// </summary>
    public class DatabaseTable {

        /// <summary>
        /// Table name
        /// </summary>
        public string TableName { get { return tableName; } }
        protected string tableName;
        
        /// <summary>
        /// Database factory used for connection
        /// </summary>
        protected DatabaseFactory factory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tableName">Table name</param>
        public DatabaseTable(string tableName, DatabaseFactory factory) {
            this.tableName = tableName;
            this.factory   = factory;
        }

        /// <summary>
        /// Create new table
        /// </summary>
        /// <param name="commandText">SQL command text</param>
        public void Create(string commandText) {
            using (DbCommand command = factory.CreateCommand(commandText)) {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Get generator name for this table (Firebird)
        /// </summary>
        /// <returns>Generator name</returns>
        private string GetGeneratorName() {
            return "Generator" + tableName;
        }

        /// <summary>
        /// Create generator for an autoincrement use (Firebird)
        /// </summary>
        public void CreateGenerator() {
            using (DbCommand command = factory.CreateCommand("CREATE GENERATOR " + GetGeneratorName())) {
                command.ExecuteNonQuery();
            }
        }

        /// Read next generator value (Firebird)
        /// </summary>
        /// <returns>Next generator value</returns>
        public long GetNextGeneratorValue() {
            using (DbCommand command = factory.CreateCommand("SELECT GEN_ID(" + GetGeneratorName() + ",1) FROM RDB$DATABASE")) {
                return (long)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Get maximum value of numeric field
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <returns>Maximum field value or 0 if not found</returns>
        public int GetMaxValue(string fieldName) {
            int maxValue;
            using (DbCommand command = factory.CreateCommand("SELECT MAX(" + fieldName + ") FROM " + TableName)) {
                try {
                    maxValue = (int)command.ExecuteScalar();
                } catch {
                    // Tabulka je prazdna
                    maxValue = 0;
                }
            }
            return maxValue;
        }

        /// <summary>
        /// Check if the value exists
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Text to look for</param>
        /// <returns>True if exists</returns>
        public bool ValueExists(string fieldName, string value) {
            int count;

            using (DbCommand command = factory.CreateCommand("SELECT COUNT(" + fieldName + ") FROM " + TableName + " "
                                                           + "WHERE UPPER(" + fieldName + ") = @Value")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@Value", value.ToUpper()));
                try {
                    count = (int)(long)command.ExecuteScalar();
                } catch {
                    // Tabulka je prazdna
                    count = 0;
                }
            }
            return (count > 0);
        }

        /// <summary>
        /// Check if the value exists
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Integer value to look for</param>
        /// <returns>True if exists</returns>
        public bool ValueExists(string fieldName, long value) {
            int count;

            using (DbCommand command = factory.CreateCommand("SELECT COUNT(" + fieldName + ") FROM " + TableName + " "
                                                           + "WHERE " + fieldName + " = @Value")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@Value", value));
                try {
                    count = (int)(long)command.ExecuteScalar();
                } catch {
                    // Tabulka je prazdna
                    count = 0;
                }
            }
            return (count > 0);
        }

    }
}
