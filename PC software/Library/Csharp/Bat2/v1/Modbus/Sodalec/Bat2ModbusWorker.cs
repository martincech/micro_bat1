﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Veit.ModbusProtocol;
using Veit.Bat2Modbus;

namespace Veit.Bat2Modbus {
    /// <summary>
    /// One slave connected using Modbus
    /// </summary>
    public struct Bat2Slave {
        /// <summary>
        /// COM port number
        /// </summary>
        public int PortNumber;

        /// <summary>
        /// Scale address
        /// </summary>
        public int Address;
    }

    public enum WorkerEvent {
        INIT,               // Inicializace
        READ_OK,            // Nacteny stav vahy na dane pozici
        READ_ERROR,         // Chyba cteni stavu vahy na dane pozici
        CANCEL              // Rucni preruseni
    }

    /// <summary>
    /// Data passed to the main thread
    /// </summary>
    public struct WorkerData {
        public WorkerEvent Event;
        public CurrentState State;

        /// <summary>
        /// Counter of all sent messages
        /// </summary>
        public int MessageCounter;

        /// <summary>
        /// Counter of wrong replies
        /// </summary>
        public int ErrorCounter;
    }

    public struct WorkerInitData {
        public List<Bat2Slave> ScaleCollection;
        public Modbus Modbus;
        public int NumberOfAttempts;
    }
    
    /// <summary>
    /// Worker reading online status of all scales
    /// </summary>
    public class Bat2ModbusOnlineWorker {
        /// <summary>
        /// Background worker used for the operation
        /// </summary>
        private BackgroundWorker backgroundWorker;

        /// <summary>
        /// Collection of scales connected to the PC
        /// </summary>
        private List<Bat2Slave> scaleCollection;

        /// <summary>
        /// Modbus connection
        /// </summary>
        private Modbus modbus;

        /// <summary>
        /// Max number of attempts in communication
        /// </summary>
        private int numberOfAttempts;

        /// <summary>
        /// Returns true if the worker is still running
        /// </summary>
        public bool IsBusy {
            get {
                return backgroundWorker.IsBusy;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="backgroundWorker"></param>
        /// <param name="scaleCollection"></param>
        public Bat2ModbusOnlineWorker(ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted, int baudRate, Device.Serial.Uart.Parity parity,
                                      int dataBits, int totalTimeout, int intercharacterTimeout, int numberOfAttempts, ModbusPacket.PacketMode mode, List<Bat2Slave> scaleCollection) {
            // Vytvorim background worker
            backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress      = true;
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.DoWork                    += new DoWorkEventHandler(Bat2ModbusOnlineWorkerRaw.DoWork);
            backgroundWorker.ProgressChanged           += new ProgressChangedEventHandler(progressChanged);
            backgroundWorker.RunWorkerCompleted        += new RunWorkerCompletedEventHandler(runWorkerCompleted);

            this.scaleCollection  = scaleCollection;
            this.numberOfAttempts = numberOfAttempts;
            modbus                = new Modbus(baudRate, parity, dataBits, totalTimeout, intercharacterTimeout, mode);

        }

        /// <summary>
        /// Start operation
        /// </summary>
        public void Start() {
            WorkerInitData initData;
            initData.ScaleCollection  = scaleCollection;
            initData.Modbus           = modbus;
            initData.NumberOfAttempts = numberOfAttempts;
            backgroundWorker.RunWorkerAsync(initData);
        }

        /// <summary>
        /// Stop operation
        /// </summary>
        public void Stop() {
            backgroundWorker.CancelAsync();
        }
    }

    /// <summary>
    /// Raw Modbus functions for BackgroundWorker
    /// </summary>
    public class Bat2ModbusOnlineWorkerRaw {
        /// <summary>
        /// Update scale status and report it to the UI
        /// </summary>
        static void UpdateStatus(int scaleIndex, WorkerEvent workerEvent, CurrentState newState, int messageCount, int errorCount, BackgroundWorker worker) {
            // Ulozim si novou udalost
            WorkerData workerData = new WorkerData();
            workerData.Event = workerEvent;
            if (newState != null) {
                workerData.State = new CurrentState(newState);
            }
            workerData.MessageCounter = messageCount;
            workerData.ErrorCounter   = errorCount;
            
            // Pozor, do UI musim predat kopii stavu. Pokud bych predal jen ukazatel, UI by s timto stavem pracovalo a postupne ho zobrazovalo,
            // ale thread by s tim samym stavem pracoval take a zmenil by ho na pozadi.
            worker.ReportProgress(scaleIndex, workerData);
        }

        static void UpdateStatusOk(int scaleIndex, CurrentState newState, int messageCount, int errorCount, BackgroundWorker worker) {
            UpdateStatus(scaleIndex, WorkerEvent.READ_OK, newState, messageCount, errorCount, worker);
        }

        static void UpdateStatusError(int scaleIndex, int messageCount, int errorCount, BackgroundWorker worker) {
            UpdateStatus(scaleIndex, WorkerEvent.READ_ERROR, null, messageCount, errorCount, worker);
            System.Threading.Thread.Sleep(200);     // Pokud zadal u vsech vah neexistujici COM port, worker posila jeden update za druhym a GUI je zahlcene. Musim zpomalit.
        }

        static void UpdateStatus(int scaleIndex, WorkerEvent workerEvent, int messageCount, int errorCount, BackgroundWorker worker) {
            UpdateStatus(scaleIndex, workerEvent, null, messageCount, errorCount, worker);
        }

        public static void DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker worker = (BackgroundWorker)sender;         // Preberu worker, ktery tuto fci vola
            int currentScaleIndex = int.MaxValue - 1;
            WorkerInitData initData = (WorkerInitData)e.Argument;

            // Musi zadat aspon 1 vahu
            if (initData.ScaleCollection.Count == 0) {
                UpdateStatus(currentScaleIndex, WorkerEvent.CANCEL, 0, 0, worker);
                e.Cancel = true;
                return;
            }

            // Vytvorim seznam vah
            List<Bat2Modbus> modbusScaleCollection = new List<Bat2Modbus>();
            foreach (Bat2Slave bat2Slave in initData.ScaleCollection) {
                modbusScaleCollection.Add(new Bat2Modbus(bat2Slave.PortNumber, (byte)bat2Slave.Address, initData.Modbus));
            }

            Bat2Modbus bat2Modbus = null;      // Pracovni

            // Smycka komunikace s modemem, preskocenim na zacatek cyklu muzu kdykoliv udelat inicializaci modemu od zacatku
            while (true) {
                // Prechod na dalsi vahu
                currentScaleIndex++;
                if (currentScaleIndex >= initData.ScaleCollection.Count) {
                    currentScaleIndex = 0;
                }

                bat2Modbus = modbusScaleCollection[currentScaleIndex];
                
                // Otevru port, pokud je otevreny z predchozi vahy, automaticky ho zavre
                bat2Modbus.Open();

                // Nacteni aktualniho stavu
                int attempt;        // Aktualni cislo pokusu
                for (attempt = 0; attempt < initData.NumberOfAttempts; attempt++) {
                    if (worker.CancellationPending) {
                        break;
                    }
                    if (!bat2Modbus.ReadConfig()) {
                        continue;       // Zkusim dalsi pokus
                    }
                    if (worker.CancellationPending) {
                        break;
                    }
                    if (!bat2Modbus.ReadPause()) {
                        continue;       // Zkusim dalsi pokus
                    }
                    if (worker.CancellationPending) {
                        break;
                    }
                    if (!bat2Modbus.ReadDateTime()) {
                        continue;       // Zkusim dalsi pokus
                    }
                    if (worker.CancellationPending) {
                        break;
                    }
                    if (!bat2Modbus.ReadToday()) {
                        continue;       // Zkusim dalsi pokus
                    }
                    if (worker.CancellationPending) {
                        break;
                    }

                    // Nacetl jsem vse, predam nacteny vysledek
                    UpdateStatusOk(currentScaleIndex, bat2Modbus.CurrentState, bat2Modbus.MessageCounter, bat2Modbus.ErrorCounter, worker);
                    break;          // Preskocim na dalsi vahu
                }//for

                // Pokud chce ukoncit, koncim
                if (worker.CancellationPending) {
                    UpdateStatus(currentScaleIndex, WorkerEvent.CANCEL, bat2Modbus.MessageCounter, bat2Modbus.ErrorCounter, worker);
                    e.Cancel = true;
                    break;          // Zaroven zavre port
                }

                // Pokud se nepodarilo nacist ani na vic pokusu, ohlasim neuspech a pokracuju na dalsi vahu
                if (attempt == initData.NumberOfAttempts) {
                    UpdateStatusError(currentScaleIndex, bat2Modbus.MessageCounter, bat2Modbus.ErrorCounter, worker);
                    continue;       // Preskocim na dalsi vahu
                }

                // Podarilo se, pokracuju na dalsi vahu

            }//while
            
            // Ukoncil cinnost workeru, pokud je port otevreny, zavru ho
            if (bat2Modbus != null) {
                bat2Modbus.Close();
            }
        }
    }

}
