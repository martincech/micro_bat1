﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Veit.Bat2 {
    /// <summary>
    /// Decoded SMS
    /// </summary>
    public struct DecodedSms {
        /// <summary>
        /// Phone number of the scale
        /// </summary>
        public string PhoneNumber;

        /// <summary>
        /// Id. number of the scale
        /// </summary>
        public int ScaleNumber;
        
        /// <summary>
        /// Day number
        /// </summary>
        public int DayNumber;
        
        /// <summary>
        /// Date of the day
        /// </summary>
        public DateTime Date;
        
        /// <summary>
        /// Number of birds
        /// </summary>
        public int Count;
        
        /// <summary>
        /// Average weight
        /// </summary>
        public float Average;

        /// <summary>
        /// Daily gain
        /// </summary>
        public float Gain;

        /// <summary>
        /// Standard deviation
        /// </summary>
        public float Sigma;

        /// <summary>
        /// Coeficient of variation
        /// </summary>
        public float Cv;

        /// <summary>
        /// Uniformity
        /// </summary>
        public int Uniformity;
    }
    
    public static class SmsDecode {
        private static void DeleteWord(StringBuilder text, out string word) {
            // Vymaze z <text> prvni slovo zakoncene mezerou a vrati ho v <word>.
            // Pokud se narazi driv na konec stringu nez na mezeru, hodi vyjimku
            int spaceIndex;       // Index nalezene mezery

            spaceIndex = text.ToString().IndexOf(' ');      // Najdu pozici prvni mezery zleva

            if (spaceIndex == 0) {
                word = text.ToString();         // String uz obsahuje pouze 1 slovo bez mezery (zakoncene koncem stringu)
                throw new ArgumentException();  // Narazil jsem na konec stringu
            }
            word = text.ToString().Substring(0, spaceIndex);        // Slovo bez mezery
            text.Remove(0, spaceIndex + 1);                         // Umazu slovo i s mezerou
        }
        
        /// <summary>
        /// Decode SMS text to a struct
        /// </summary>
        /// <param name="phoneNumber">Phone number of the scale</param>
        /// <param name="text">SMS text</param>
        /// <param name="decodedSms">DecodedSms instance</param>
        /// <returns>True if successful</returns>
        public static bool Decode(string phoneNumber, string text, out DecodedSms decodedSms) {
            // Format beru vzdy jen pro 1 pohlavi, tj.:
            // SCALES 00 DAY 000 01.01.2005 Cnt 0000 Avg 00.000 Gain 00.000 Sig 0.000 Cv 000 Uni 000

            decodedSms = new DecodedSms();
            StringBuilder textBuilder =  new StringBuilder(text);
            string str;
            int position;

            decodedSms.PhoneNumber = phoneNumber;

            try {
                // Smazu SCALES
                DeleteWord(textBuilder, out str);            

                // Cislo vah
                DeleteWord(textBuilder, out str);            
                decodedSms.ScaleNumber = int.Parse(str);

                // Smazu DAY
                DeleteWord(textBuilder, out str);            

                // Cislo dne
                DeleteWord(textBuilder, out str);            
                decodedSms.DayNumber = int.Parse(str);

                // Datum dne
                DeleteWord(textBuilder, out str);            
                // Den
                if ((position = str.IndexOf('.')) == 0) {
                    throw new ArgumentException();          // Tecka za dnem chybi
                }
                int day = int.Parse(str.Substring(0, position));
                str = str.Substring(position + 1);          // Smazu i s teckou
                // Mesic
                if ((position = str.IndexOf('.')) == 0) {
                    throw new ArgumentException();          // Tecka za mesicem chybi
                }
                int month = int.Parse(str.Substring(0, position));
                str = str.Substring(position + 1);          // Smazu i s teckou
                // Rok
                int year = int.Parse(str);
                decodedSms.Date = new DateTime(year, month, day);

                // Cnt
                DeleteWord(textBuilder, out str);            
                DeleteWord(textBuilder, out str);            
                decodedSms.Count = int.Parse(str);

                CultureInfo culture = new CultureInfo("en-US");
                
                // Avg
                DeleteWord(textBuilder, out str);            
                DeleteWord(textBuilder, out str);            
                decodedSms.Average = float.Parse(str, culture);

                // Gain
                DeleteWord(textBuilder, out str);            
                DeleteWord(textBuilder, out str);            
                decodedSms.Gain = float.Parse(str, culture);

                // Sigma
                DeleteWord(textBuilder, out str);            
                DeleteWord(textBuilder, out str);            
                decodedSms.Sigma = float.Parse(str, culture);

                // CV vypoctu na desetinu (v SMS je to jen na cele cislo)
                DeleteWord(textBuilder, out str);            
                DeleteWord(textBuilder, out str);            
                if (decodedSms.Average == 0) {
                    decodedSms.Cv = 0;
                } else {
                    decodedSms.Cv = 100.0f * decodedSms.Sigma / decodedSms.Average;
                } 

                // Uni
                DeleteWord(textBuilder, out str);            
                decodedSms.Uniformity = int.Parse(textBuilder.ToString());

                return true;

            } catch {
                // Neco se nepovedlo, neplatny format SMS
                return false;
            }

        }
    }
}
