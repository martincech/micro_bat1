﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Bat2Flash;

namespace Veit.Bat2Stat {
    using TNumber = System.Single;

    /// <summary>
    /// BAT2 statistic functions (rewritten from VymOs Stat.cpp)
    /// </summary>
    public class Bat2Stat {
        private const float MIN_AVERAGE        = 1e-03f;         // mensi hodnota povazovana za 0
        private const float MIN_SIGMA          = 1e-03f;         // mensi hodnota povazovana za 0
        private const int   DEFAULT_UNIFORMITY = 100;           // implicitni uniformita (pri chybe)

        private const float INV_SQRT_2PI = 0.3989422804f;     // 1/(SQRT(2*PI))

        // Konstanty aproximacniho polynomu :
        private const float POLY_P  =  0.231641900f;
        private const float POLY_B1 =  0.319381530f;
        private const float POLY_B2 = -0.356563782f;
        private const float POLY_B3 =  1.781477937f;
        private const float POLY_B4 = -1.821255978f;
        private const float POLY_B5 =  1.330274429f;

        private Bat2Struct.TStatistic statistic;

        // Promenne pro presnou uniformitu
        private TNumber StatRealUniMin;
        private TNumber StatRealUniMax;
        private TNumber StatRealUniAverage;
        private int StatRealUniCountIn;
        private int StatRealUniCountOut;

        public Bat2Stat() {
            statistic.Count  = 0;
            statistic.XSuma  = 0;
            statistic.X2Suma = 0;
        }

        public Bat2Stat(Bat2Struct.TStatistic statistic) {
            this.statistic.Count  = statistic.Count;
            this.statistic.XSuma  = statistic.XSuma;
            this.statistic.X2Suma = statistic.X2Suma;
        }

        public void Add(TNumber x) {
            // prida do statistiky
            statistic.XSuma  += x;
            statistic.X2Suma += x * x;
            statistic.Count++;
        }

        public TNumber Average() {
            // vrati stredni hodnotu
            if (statistic.Count == 0){
                return 0;
            }
            return statistic.XSuma / statistic.Count;
        }

        public TNumber Sigma() {
            // vrati smerodatnou odchylku
            TNumber tmp;

            if (statistic.Count <= 1) {
                return 0;
            }
            tmp = statistic.XSuma * statistic.XSuma / statistic.Count;
            tmp = statistic.X2Suma - tmp;
            if (tmp <= MIN_SIGMA) {
                return 0;
            }
            return (float)Math.Sqrt(1/(TNumber)(statistic.Count - 1) * tmp);
        }

        public TNumber Variation(TNumber average, TNumber sigma) {
            // vrati variaci (procentni smerodatna odchylka)
            if (average < MIN_AVERAGE) {
                return 0;         // "deleni nulou"
            }
            return sigma / average * 100;
        }

        public TNumber StatUniformity(TNumber average, TNumber sigma, byte UniformityWidth) {
            // vrati procentualni pocet vzorku padajicich do rozsahu stredni
            // hodnoty +-UniformityWidth%
            // Sirka <UniformityWidth> se zadava v %, 10 znamena +-10%
            TNumber x, qx;        // normalizovana odchylka, gaussint

            if (sigma < MIN_SIGMA) {
                return DEFAULT_UNIFORMITY;      // "deleni nulou"
            }
            x = (TNumber)UniformityWidth / (TNumber)100 * average / sigma; // normalizace absolutni tolerance pomoci sigma
            if (x > 3) {
                return 100;                     // >3*sigma, vsechny hodnoty v toleranci
            }
            qx  = GaussIntegral( x, Gauss( x));  // plocha krivky nad pozadovanou mez
            qx *= 2;                             // plocha krivky nad a pod pozadovaou mez
            qx  = 1 - qx;                        // plocha uvnitr mezi
            return qx * 100;                   // v procentech
        }

        private TNumber Gauss(TNumber x) {
            // gaussova krivka
            return INV_SQRT_2PI * (float)Math.Exp(-x * x / 2);
        }

        private TNumber GaussIntegral(TNumber x, TNumber gaussx) {
            // gaussova krivka integrovana od <x> do +oo, <gaussx> je hodnota gaussovy
            // krivky pro <x>
            TNumber t;
            TNumber tn;
            TNumber poly;

            t = 1 /(1 + POLY_P * x); // zakladni mocnina
            poly  = POLY_B1 * t;     // prvni aproximace
            tn    = t*t;             // druha mocnina
            poly += POLY_B2 * tn;
            tn   *= t;               // treti mocnina
            poly += POLY_B3 * tn;
            tn   *= t;               // ctvrta mocnina
            poly += POLY_B4 * tn;
            tn   *= t;               // pata mocnina
            poly += POLY_B5 * tn;
            return gaussx * poly;
        }

        // Vyuziva tyto globalni promenne (kvuli rychlosti je nepredavam parametrem):
        //  - TNumber StatRealUniMin: spodni mez okoli prumeru
        //  - TNumber StatRealUniMax: horni mez okoli prumeru
        //  - TNumber StatRealUniAverage: prumer celeho souboru vzorku, ze ktereho se uniformita pocita
        //  - word StatRealUniCountIn: pocet vzorku v pasmu okoli
        //  - word StatRealUniCountOut: pocet vzorku mimo pasmo okoli

        public void RealUniformityInit(TNumber Min, TNumber Max, TNumber Average) {
            // Inicializuje globalni promenne
            StatRealUniMin = Min;
            StatRealUniMax = Max;
            StatRealUniAverage = Average;
            StatRealUniCountOut = 0;
            StatRealUniCountIn = 0;
        }

        public void RealUniformityAdd(TNumber Number) {
            // Prida novy vzorek do uniformity
            if (Number < StatRealUniMin || Number > StatRealUniMax) {
                // Vzorek je mimo pasmo
                StatRealUniCountOut++;
                return;
            }
            // Vzorek je v pasmu
            StatRealUniCountIn++;
        }

        public int RealUniformityGet() {
            // Vypocte uniformitu v celych procentech, vrati cislo 0 - 100
            if (StatRealUniCountIn == 0 && StatRealUniCountOut == 0) {
                return 100;
            } else {
                return 100 * StatRealUniCountIn / (StatRealUniCountIn + StatRealUniCountOut);
            }
        }

    }
}
