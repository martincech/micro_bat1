﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Bat2Flash;

namespace Veit.Bat2Stat {
    using THistogramCount = System.UInt16;
    using THistogramValue = System.Int32;

    /// <summary>
    /// BAT2 histogram (rewritten from VymOs Histogr.cpp)
    /// </summary>
    public class Bat2Histogr {

        private const THistogramValue HISTOGRAM_MIN_STEP = 1;        // Minimalni hodnota kroku v THistogramValue

        private int HISTOGRAM_SLOTS;       // pocet sloupcu - sude i liche cislo (max.254)
        
        Bat2Struct.THistogram histogram;

        public Bat2Histogr(THistogramValue center, THistogramValue step) {
            histogram = new Bat2Struct.THistogram();
            histogram.Center = center;
            histogram.Step = step;
            HISTOGRAM_SLOTS = Bat2Struct.HISTOGRAM_SLOTS;
            histogram.Slot = new THistogramCount[HISTOGRAM_SLOTS];
            for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
                histogram.Slot[i] = 0;
            }
        }

        public Bat2Histogr(Bat2Struct.THistogram histogram) {
            this.histogram.Center = histogram.Center;
            this.histogram.Step = histogram.Step;
            HISTOGRAM_SLOTS = Bat2Struct.HISTOGRAM_SLOTS;
            this.histogram.Slot = new THistogramCount[HISTOGRAM_SLOTS];
            for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
                this.histogram.Slot[i] = histogram.Slot[i];
            }
        }

        public void Add(THistogramValue Value) {
            // Prida hodnotu do histogramu
            int Index;

            Value -= histogram.Center;

            if (HISTOGRAM_SLOTS % 2 == 0) {
                // Sudy pocet
                if( Value >= 0){
                    if( Value >= (HISTOGRAM_SLOTS / 2 * histogram.Step)){
                        histogram.Slot[ HISTOGRAM_SLOTS - 1] += 1;
                        return;                       // nad rozsahem
                    }
                    Index  = Value / histogram.Step; // vzdalenost od stredu
                    Index += HISTOGRAM_SLOTS / 2;
                } else {
                    // Value < 0
                    Value = -Value;
                    if( Value >= (HISTOGRAM_SLOTS / 2 * histogram.Step)){
                        histogram.Slot[ 0] += 1;
                        return;                          // pod rozsahem
                    }
                    Index  = Value / histogram.Step;    // vzdalenost od stredu
                    Index  = HISTOGRAM_SLOTS / 2 - 1 - Index;
                }
            } else {
                // Lichy pocet
                if( Value >= 0){
                    if (Value >= (HISTOGRAM_SLOTS * histogram.Step / 2)) {
                        histogram.Slot[HISTOGRAM_SLOTS - 1] += 1;
                        return;                       // nad rozsahem
                    }
                    Index  = (Value + histogram.Step / 2) / histogram.Step; // vzdalenost od stredu
                    Index += HISTOGRAM_SLOTS / 2;
                } else {
                    Value = -Value;
                    if (Value >= (HISTOGRAM_SLOTS * histogram.Step / 2)) {
                        histogram.Slot[0] += 1;
                        return;                          // pod rozsahem
                    }
                    Index  = (Value + histogram.Step / 2) / histogram.Step; // vzdalenost od stredu
                    Index  = HISTOGRAM_SLOTS / 2 - Index;
                }
            }

            histogram.Slot[ Index] += 1;
        }

        public THistogramValue HistogramGetDenominator(THistogramValue MaxValue) {
          // Vrati cislo, kterym je treba podelit vsechny sloupce, aby se histogram normoval na maximalni hodnotu <MaxValue>
          THistogramValue Max = 0;

          // Zjistim maximalni hodnotu (vysku sloupce) v histogramu
          for (int j = 0; j < HISTOGRAM_SLOTS; j++) {
            if (histogram.Slot[j] > Max) {
              Max = histogram.Slot[j];
            }
          }//for j

          if (Max % MaxValue > 0) {
            Max /= MaxValue;
            Max++;
          } else {
            Max /= MaxValue;
          }//else
          if (Max == 0) {
            return 1;   // Nulu nevracim (bude se tim delit)
          }
          return Max;
        }

        public void SetStep(byte Range) {
          // Vypocte a ulozi do zadaneho histogramu krok podle nastaveneho stredu a rozsahu. Rozsah se zadava v +-%, tj. Range = 10 znamena +-10% okolo stredu.
          // Pokud krok vyjde mensi nez HISTOGRAM_MIN_STEP, nastavi krok na HISTOGRAM_MIN_STEP
          // Pokud minimum histogramu vyjde zaporne, posune stred histogramu tak, aby minimum vychazelo na 0
          histogram.Step = histogram.Center * (THistogramValue)(2 * Range) / (THistogramValue)(100) / (THistogramValue)HISTOGRAM_SLOTS;
          if (histogram.Step < HISTOGRAM_MIN_STEP) {
            // Krok vysel nulovy, nastavim ho na minimalni hodnotu
            histogram.Step = HISTOGRAM_MIN_STEP;
          }
          if (histogram.Center < histogram.Step * (THistogramValue)HISTOGRAM_SLOTS / (THistogramValue)2) {
            // Minimum histogramu by vyslo zaporne - posunu stred tak, aby minimum vyslo na 0
            histogram.Center = histogram.Step * (THistogramValue)HISTOGRAM_SLOTS / (THistogramValue)2;
          }
        }

        bool IsEmpty() {
          // Pokud je histogram prazdny, vrati YES
          for (int j = 0; j < HISTOGRAM_SLOTS; j++) {
            if (histogram.Slot[j] > 0) {
              return false;   // Nasel jsem nejaky prvek
            }
          }
          return true;   // Pokud dosel az sem, nenasel jsem zadny prvek
        }

    }
}
