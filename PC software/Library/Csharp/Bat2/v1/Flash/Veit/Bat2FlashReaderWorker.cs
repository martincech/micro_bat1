﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Veit.Bat2Flash {

    /// <summary>
    /// What the worker should do
    /// </summary>
    public enum WorkerJob {
        READ_CONFIG,    // Read only config section
        READ_ALL        // Read all data
    }
    
    /// <summary>
    /// Result of reading
    /// </summary>
    public class WorkerResult {
        /// <summary>
        /// Reading successful (true = OK, false = error)
        /// </summary>
        public bool IsSuccessful;

        /// <summary>
        /// Raw data read from the memory module
        /// </summary>
        public byte[] Data;

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkerResult() {
            IsSuccessful = false;
            Data         = null;
        }
    }


    /// <summary>
    /// Worker reading data from the BAT2 memory module
    /// </summary>
    public class Bat2FlashReaderWorker {
        /// <summary>
        /// Background worker used for the operation
        /// </summary>
        private BackgroundWorker backgroundWorker;

        /// <summary>
        /// Returns true if the worker is still running
        /// </summary>
        public bool IsBusy {
            get {
                return backgroundWorker.IsBusy;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Bat2FlashReaderWorker(ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted) {
            // Vytvorim background worker
            backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress      = true;
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.DoWork                    += new DoWorkEventHandler(Bat2FlashReaderWorkerRaw.DoWork);
            backgroundWorker.ProgressChanged           += new ProgressChangedEventHandler(progressChanged);
            backgroundWorker.RunWorkerCompleted        += new RunWorkerCompletedEventHandler(runWorkerCompleted);
        }

        /// <summary>
        /// Start reading of config section
        /// </summary>
        public void StartReadConfig() {
            backgroundWorker.RunWorkerAsync(WorkerJob.READ_CONFIG);
        }

        /// <summary>
        /// Start reading of all data
        /// </summary>
        public void StartReadAll() {
            backgroundWorker.RunWorkerAsync(WorkerJob.READ_ALL);
        }

        /// <summary>
        /// Stop reading
        /// </summary>
        public void Stop() {
            backgroundWorker.CancelAsync();
        }
    }

    /// <summary>
    /// Raw functions for BackgroundWorker
    /// </summary>
    public class Bat2FlashReaderWorkerRaw {
        /// <summary>
        /// Max number of attempts in communication
        /// </summary>
        private const int NUMBER_OF_ATTEMPTS = 3;

        /// <summary>
        /// Period of checking of memory module presence during reading of all data
        /// </summary>
        private const int MODULE_CHECK_PERIOD = 50;     // 50 = kontrola cca 1x za sekundu

        public static void DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker worker = (BackgroundWorker)sender;         // Preberu worker, ktery tuto fci vola
            WorkerJob workerJob = (WorkerJob)e.Argument;                // Preberu cinnost, kterou chce provest
            WorkerResult workerResult = new WorkerResult();             // Vysledek cteni, default je neuspesny
            int i;

            Bat2FlashReader flashReader = new Bat2FlashReader();

            // Kontrola pritomnosti modulu
            for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                if (worker.CancellationPending) {
                    e.Cancel = true;
                    return;
                }
                if (flashReader.IsModulePresent()) {
                    break;
                }
            }
            if (i >= NUMBER_OF_ATTEMPTS) {
                flashReader.Close();
                e.Result = workerResult;
                return;
            }

            // Pokud chce nacist jen config, nactu ho a koncim
            if (workerJob == WorkerJob.READ_CONFIG) {
                for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                    if (worker.CancellationPending) {
                        e.Cancel = true;
                        flashReader.Close();
                        return;
                    }
                    if (flashReader.ReadConfigSection()) {
                        break;
                    }
                }
                flashReader.Close();                    // Kazdopadne zavru komunikaci
                if (i >= NUMBER_OF_ATTEMPTS) {
                    e.Result = workerResult;
                } else {
                    workerResult.IsSuccessful = true;
                    workerResult.Data = new byte[flashReader.RawData.Length];
                    flashReader.RawData.CopyTo(workerResult.Data, 0);
                    e.Result = workerResult;
                }
                return;
            }
            
            // Chce nacist celou pamet
            
            // Zahajeni cteni
            for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                if (worker.CancellationPending) {
                    flashReader.Close();
                    e.Cancel = true;
                    return;
                }
                if (flashReader.StartReadData()) {
                    break;
                }
            }
            if (i >= NUMBER_OF_ATTEMPTS) {
                flashReader.Close();
                e.Result = workerResult;
                return;
            }

            // Smycka cteni dat
            bool done = false;
            int checkCounter = 0;
            int oldProgress = 100;
            while (true) {
                for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                    if (worker.CancellationPending) {
                        flashReader.Close();
                        e.Cancel = true;
                        return;
                    }
                    if (checkCounter >= MODULE_CHECK_PERIOD) {
                        if (!flashReader.IsModulePresent()) {
                            continue;
                        }
                        checkCounter = 0;       // Nuluju az po uspesne kontrole
                    }
                    checkCounter++;
                    if (flashReader.ReadPacket(out done)) {
                        break;
                    }
                }
                if (i >= NUMBER_OF_ATTEMPTS) {
                    flashReader.Close();
                    e.Result = workerResult;
                    return;
                }
                
                // Progress posilam jen kazde cele procento
                int progress = (int)flashReader.ReadProgress;
                if (progress != oldProgress) {
                    worker.ReportProgress(progress);
                    oldProgress = progress;
                }

                if (done) {
                    flashReader.Close();
                    workerResult.IsSuccessful = true;
                    workerResult.Data = new byte[flashReader.RawData.Length];
                    flashReader.RawData.CopyTo(workerResult.Data, 0);
                    e.Result = workerResult;
                    return;
                }
            }
        }
    }

}
