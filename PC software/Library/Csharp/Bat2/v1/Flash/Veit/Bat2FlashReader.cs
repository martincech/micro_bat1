﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.UsbReader;

namespace Veit.Bat2Flash {
    /// <summary>
    /// Reading data from the BAT2 memory module (rewritten from VymOs MFlash.cpp)
    /// </summary>
    public class Bat2FlashReader {
        private AtFlash atFlash;

        /// <summary>
        /// Current offset of reading
        /// </summary>
        public int ReadOffset { get { return readOffset; } }
        private int readOffset;            // ofset ctenych dat

        /// <summary>
        /// Data read from the memory module
        /// </summary>
        public byte[] RawData { get { return rawData; } }
        private byte[] rawData;

        private const int MREAD_PACKET = 256;   // velikost paketu pri cteni
        
        // Vraci procenta nactenych dat
        public double ReadProgress { get { return readProgress; } }
        private double readProgress; /*{ get { return ((double)readOffset * 100.0) / (double)AtFlash.FLASH_SIZE; } }*/

        public Bat2FlashReader() {
            atFlash = new AtFlash();
            rawData = new byte[AtFlash.FLASH_SIZE];
            readProgress = 0;
        }

        /// <summary>
        /// Check if the module is present
        /// </summary>
        /// <returns>Tru if present</returns>
        public bool IsModulePresent() {
            // Stavalo se, ze pri opakovanem odpojeni a pripojeni USB ctecky zustal nahozeny flag UsbSpi.IsOpen,
            // tj. fce UsbSpi.CheckConnect() vracela vzdy true. Pomuze nejprve zavrit komunikaci a pak az
            // kontrolovat pritomnost modulu.
            atFlash.Close();
            return atFlash.Connected();
        }

        /// <summary>
        /// Read config section from the module
        /// </summary>
        /// <returns>True if successful</returns>
        public bool ReadConfigSection() {
            // Nacte konfiguracni sekci modulu
            if (!atFlash.Connected()) {
                return false;
            }
            byte[] data;
            if (!atFlash.Read(Bat2Struct.FL_CONFIG_BASE, out data, Bat2Struct.FL_CONFIG_SIZE)) {
                return false;
            }
            for (int i = 0; i < rawData.Length; i++) {
                rawData[i] = 0;
            }
            data.CopyTo(rawData, 0);
            return true;
        }

        /// <summary>
        /// Write config section to the module
        /// </summary>
        /// <param name="data">Raw data of the config section</param>
        /// <returns>True if successful</returns>
        public bool WriteConfigSection(byte[] data) {
            if (!atFlash.Connected()) {
                return false;
            }
            if (!atFlash.Write(Bat2Struct.FL_CONFIG_BASE, data, Bat2Struct.FL_CONFIG_SIZE)) {
                return false;
            }
            byte[] verifyBuffer;
            if (!atFlash.Read(Bat2Struct.FL_CONFIG_BASE, out verifyBuffer, Bat2Struct.FL_CONFIG_SIZE)) {
                return false;
            }
            for (int i = 0; i < Bat2Struct.FL_CONFIG_SIZE; i++) {
                if (verifyBuffer[i] != data[i]) {
                    return false;  // Verifikace neprobehla
                }
            }
            return true;
        }

        /// <summary>
        /// Start reading of marker from the module
        /// </summary>
        /// <returns>True if successful</returns>
        public bool StartReadMarkerPosition() {
            if (!atFlash.Connected()) {
                return false;
            }
            readOffset = Bat2Struct.FL_ARCHIVE_BASE;         // od zacatku archivu
            return true;
        }

        /// <summary>
        /// Read next marker from the memory module
        /// </summary>
        /// <param name="done">True after the marker is reached</param>
        /// <returns>True if successful</returns>
        public bool ReadMarkerPosition(out bool done, out int markerAddress) {
            markerAddress = -1;     // Default marker nenalezen
            if (readOffset >= AtFlash.FLASH_SIZE) {
                // Nenasel jsem
                done = true;
                return true;
            }
            
            done = false;
            
            byte[] data;
            if (!atFlash.Read(readOffset, out data, 1)) {
                return false;
            }
            
            if (data[0] == Bat2Struct.FL_ARCHIVE_EMPTY) {
                markerAddress = readOffset;     // Vratim adresu markeru
                done = true;                    // Hotovo
                return true;
            }

            readOffset += Bat2Struct.FL_ARCHIVE_DAY_SIZE;

            return true;
        }

        /// <summary>
        /// Start reading of data from the module
        /// </summary>
        /// <returns>True if successful</returns>
        public bool StartReadData(bool includingConfig) {
            if (!atFlash.Connected()) {
                return false;
            }
            if (includingConfig) {
                readOffset = 0;                             // od zacatku Flash
            } else {
                readOffset = Bat2Struct.FL_ARCHIVE_BASE;    // od zacatku archivu, config vynecham
            }
            readProgress = 0;
            return true;
        }

        /// <summary>
        /// Start reading of data from the beginning from the module
        /// </summary>
        /// <returns>True if successful</returns>
        public bool StartReadData() {
            return StartReadData(true);    // od zacatku Flash
        }

        /// <summary>
        /// Read next packet from the memory module
        /// </summary>
        /// <param name="done">True after all data is read</param>
        /// <param name="markerAddress">Address of the end marker, which has been found before or -1. If specified, data is being continuously checked for
        /// end marker during reading. If the marker is found, reading is finished sooner without reading of the whole memory</param>
        /// <returns>True if successful</returns>
        public bool ReadPacket(out bool done, int markerAddress) {
            done = false;
            int size = MREAD_PACKET;            // velikost paketu
            int remainder = AtFlash.FLASH_SIZE - readOffset;

            if (remainder == 0) {
                done = true;                     // Uz nic nezbyva, vse je nacteno
                return true;
            } else if (remainder < MREAD_PACKET){
                done = true;                     // posledni cteni
                size = remainder;                // posledni paket muze byt kratsi
            }
            // USB ctecka
            byte[] data;
            if (!atFlash.Read(readOffset, out data, size)) {
                return false;
            }
            data.CopyTo(rawData, readOffset);
            readOffset += size;

            // Vypoctu novy progress
            if (markerAddress < 0) {
                // Cela pamet bez hledani markery
                readProgress = ((double)readOffset * 100.0) / (double)AtFlash.FLASH_SIZE;
            } else {
                // Hledam marker a jeho predem nalezenou adresu mam v markerAddress
                readProgress = ((double)readOffset * 100.0) / (double)markerAddress;
            }
            if (readProgress > 100) {
                readProgress = 100;
            }
            
            // Pokud chce, prubezne kontroluju koncovy marker a pokud ho najdu, muzu skoncit cteni driv
            if (markerAddress >= 0) {
                for (int i = Bat2Struct.FL_ARCHIVE_BASE; i < readOffset; i += Bat2Struct.FL_ARCHIVE_DAY_SIZE) {
                    if (rawData[i] == Bat2Struct.FL_ARCHIVE_EMPTY) {
                        done = true;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Read next packet from the memory module
        /// </summary>
        /// <param name="done">True after all data is read</param>
        /// <returns>True if successful</returns>
        public bool ReadPacket(out bool done) {
            return ReadPacket(out done, -1);     // Ctu celou pamet, bez prubezne kontroly markeru
        }

            /// <summary>
        /// Close communication with the USB reader
        /// </summary>
        public void Close() {
            atFlash.Close();
        }



    }
}
