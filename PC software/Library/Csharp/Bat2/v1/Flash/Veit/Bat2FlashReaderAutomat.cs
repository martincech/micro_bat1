﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Veit.StructConversion;

namespace Veit.Bat2Flash {
    
    public enum WorkerStatus {
        INIT,                   // Inicializace
        DETECT,                 // Detekce modulu ve ctecce
        WAIT_AFTER_DETECT,      // Cekaci procedura po prvni detekci, zda modul ve ctecce opravdu zustane
        CONFIG_READING,         // Cte se config
        WRONG_DATA,             // Neplatna data v modulu
        MARKER_READING_START,   // Config uspesne nacteny, zahajuju hledani markeru
        MARKER_READING,         // Hleda se marker
        DATA_READING_START,     // Zahajuju cteni dat
        DATA_READING,           // Ctou se data
        DATA_READING_FINISHED,  // Data jsou kompletne nactena
        CANCEL                  // Rucni preruseni
    }

    /// <summary>
    /// Worker data sent to the UI
    /// </summary>
    public class WorkerData {
        /// <summary>
        /// Current worker status
        /// </summary>
        public WorkerStatus WorkerStatus;

        /// <summary>
        /// Raw data read from the memory module (or null if not applicable)
        /// </summary>
        public byte[] Data;

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkerData() {
            WorkerStatus = WorkerStatus.INIT;
            Data         = null;
        }
    }


    /// <summary>
    /// Worker reading data from the BAT2 memory module
    /// </summary>
    public class Bat2FlashReaderAutomat {
        /// <summary>
        /// Background worker used for the operation
        /// </summary>
        private BackgroundWorker backgroundWorker;

        /// <summary>
        /// Returns true if the worker is still running
        /// </summary>
        public bool IsBusy {
            get {
                return backgroundWorker.IsBusy;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Bat2FlashReaderAutomat(ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted) {
            // Vytvorim background worker
            backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress      = true;
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.DoWork                    += new DoWorkEventHandler(Bat2FlashReaderWorkerRaw.DoWork);
            backgroundWorker.ProgressChanged           += new ProgressChangedEventHandler(progressChanged);
            backgroundWorker.RunWorkerCompleted        += new RunWorkerCompletedEventHandler(runWorkerCompleted);
        }

        /// <summary>
        /// Start operation of the automat
        /// </summary>
        public void Start() {
            backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Stop operation
        /// </summary>
        public void Stop() {
            backgroundWorker.CancelAsync();
        }
    }

    /// <summary>
    /// Raw functions for BackgroundWorker
    /// </summary>
    public class Bat2FlashReaderWorkerRaw {
        /// <summary>
        /// Max number of attempts in communication
        /// </summary>
        private const int NUMBER_OF_ATTEMPTS = 3;

        /// <summary>
        /// Delay after the memory module was detected in miliseconds
        /// </summary>
        private const int MODULE_DETECT_DELAY = 1600;

        /// <summary>
        /// Step in millisecond during the MODULE_DETECT_DELAY period
        /// </summary>
        private const int MODULE_DETECT_STEP = 400;

        /// <summary>
        /// Period of checking of memory module presence during reading of all data
        /// </summary>
        private const int MODULE_CHECK_PERIOD = 50;     // 50 = kontrola cca 1x za sekundu

        /// <summary>
        /// Update status and report it to the UI
        /// </summary>
        private static void UpdateStatus(ref WorkerStatus workerStatus, WorkerStatus newStatus, byte[] data, int progress, BackgroundWorker worker) {
            // Ulozim si novy stav
            workerStatus = newStatus;

            // Pozor, do UI musim predat kopii stavu. Pokud bych predal jen ukazatel, UI by s timto stavem pracovalo a postupne ho zobrazovalo,
            // ale thread by s tim samym stavem pracoval take a zmenil by ho na pozadi.
            WorkerData workerData = new WorkerData();
            workerData.WorkerStatus = workerStatus;
            if (data != null) {
                // Pokud predavam i data, vytvorim kopii
                workerData.Data = new byte[data.Length];
                data.CopyTo(workerData.Data, 0);
            } 
            worker.ReportProgress(progress, workerData);
        }

        /// <summary>
        /// Update status and report it to the UI
        /// </summary>
        private static void UpdateStatus(ref WorkerStatus workerStatus, WorkerStatus newStatus, int progress, BackgroundWorker worker) {
            UpdateStatus(ref workerStatus, newStatus, null, progress, worker);
        }

        /// <summary>
        /// Update status with progress = 0 and report it to the UI
        /// </summary>
        private static void UpdateStatus(ref WorkerStatus workerStatus, WorkerStatus newStatus, BackgroundWorker worker) {
            UpdateStatus(ref workerStatus, newStatus, null, 0, worker);
        }

        private static byte CalcChecksum(byte[] data) {
            // Spocita a vrati Kontrolni soucet konfigurace zadane v <cfg>
            byte sum = 0;

            // bez koncove sumy :
            for (int i = Bat2Struct.FL_CONFIG_BASE; i < StructConversion.StructConversion.SizeOf(typeof(Bat2Struct.TConfig)) - 1; i++) {
                sum += data[i];
            }
            return sum;
        }

        private static bool IsValidData(byte[] rawData, Bat2Struct.TConfig config) {
            // Zkontroluju, zda modul obsahuje platna data z vazeni
            
            // Kontrola checksum
            if (CalcChecksum(rawData) != config.Checksum) {
                return false;
            }
            
            // Od verze 1.10.x vyse
            if (config.Version < 0x0110) {
                return false;
            }
            
            // Online vazeni neberu
            if (config.WeighingStart.Online > 0) {
                return false;
            }
            
            // Id = 0 je config v modulu, ne data z vahy
            if (config.IdentificationNumber == 0) {
                return false;
            }
            return true;
        }
        
        public static void DoWork(object sender, DoWorkEventArgs e) {
            BackgroundWorker worker = (BackgroundWorker)sender;         // Preberu worker, ktery tuto fci vola
            WorkerStatus workerStatus = new WorkerStatus();             // Aktualni stav automatu
            int i;
            bool readingDone  = false;
            int checkCounter  = 0;
            int oldProgress   = 100;
            int markerAddress = -1;

            Bat2FlashReader flashReader = new Bat2FlashReader();

            UpdateStatus(ref workerStatus, WorkerStatus.INIT, worker);

            // Smycka, preskocenim na zacatek cyklu muzu kdykoliv udelat inicializaci od zacatku
            while(true) {
                // Kontrola ukonceni
                if (worker.CancellationPending) {
                    break;
                }

                try {
                    switch(workerStatus) {
                        case WorkerStatus.INIT:
                            UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                            break;

                        case WorkerStatus.DETECT:
                            // Kontrola pritomnosti modulu
                            while (!worker.CancellationPending && !flashReader.IsModulePresent()) {
                                System.Threading.Thread.Sleep(100);
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            // Modul zasunul
                            UpdateStatus(ref workerStatus, WorkerStatus.WAIT_AFTER_DETECT, worker);
                            break;

                        case WorkerStatus.WAIT_AFTER_DETECT:
                            // Detekoval jsem modul, zkontroluju zakmity po urcitou dobu
                            for (i = 0; i < MODULE_DETECT_DELAY / MODULE_DETECT_STEP; i++) {
                                System.Threading.Thread.Sleep(MODULE_DETECT_STEP);     // Cekam po krocich
                                if (worker.CancellationPending) {
                                    break;
                                }
                                if (!flashReader.IsModulePresent()) {
                                    // Modul nezustal pripojeny, zpet na prvni detekci
                                    UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                                    break;
                                }
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            if (i >= MODULE_DETECT_DELAY / MODULE_DETECT_STEP) {
                                // Modul zustal pripojeny po celou dobu, muzu zacit cteni
                                UpdateStatus(ref workerStatus, WorkerStatus.CONFIG_READING, worker);
                            }// else stav uz je nastaveny na DETECT
                            break;

                        case WorkerStatus.CONFIG_READING:
                            // Modul je bezpecne zasunuty, nactu config
                            for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                                if (worker.CancellationPending) {
                                    break;
                                }
                                if (flashReader.ReadConfigSection()) {
                                    break;
                                }
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            if (i >= NUMBER_OF_ATTEMPTS) {
                                // Config jsem nenacetl, jdu zpet na detekci modulu
                                UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                                break;
                            }

                            // Config je nacteny, zkontroluju identifikacni cislo vahy a typ vazeni
                            Bat2Struct.TFlash flash = new Bat2Struct.TFlash();
                            flash = Veit.StructConversion.StructConversion.ArrayToStruct<Bat2Struct.TFlash>(flashReader.RawData);
                            flash.Swap();

                            // Zkontroluju, zda modul obsahuje platna data z vazeni
                            if (!IsValidData(flashReader.RawData, flash.ConfigSection.Config)) {
                                UpdateStatus(ref workerStatus, WorkerStatus.WRONG_DATA, worker);
                                break;
                            }
                            
                            // Modul obsahuje normalni vazeni, identifikacni cislo vahy predam jako progress
                            UpdateStatus(ref workerStatus, WorkerStatus.MARKER_READING_START, flash.ConfigSection.Config.IdentificationNumber, worker);
                            break;

                        case WorkerStatus.WRONG_DATA:
                            // Zasunuty modul neobsahuje data z vazeni, pockam, az tento modul vytahne a pak jdu zpet na detekci
                            while (!worker.CancellationPending && flashReader.IsModulePresent()) {
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            // Modul vytahnul, jdu zpet na detekci
                            UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                            break;
                        
                        case WorkerStatus.MARKER_READING_START:
                            // Config je v poradku, muzu zacit hledat koncovy marker (abych nemusel cist celou pamet, coz trva 3 minuty)
                            for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                                if (worker.CancellationPending) {
                                    break;
                                }
                                if (flashReader.StartReadMarkerPosition()) {
                                    break;
                                }
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            if (i >= NUMBER_OF_ATTEMPTS) {
                                // Nepodarilo se, jdu zpet na detekci modulu
                                UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                                break;
                            }

                            checkCounter = 0;       // Nuluju pocitadlo kontroly pritomnosti modulu
                            UpdateStatus(ref workerStatus, WorkerStatus.MARKER_READING, 0, worker);
                            break;

                        case WorkerStatus.MARKER_READING:
                            // Nactu dalsi pozici markeru
                            for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                                if (worker.CancellationPending) {
                                    break;
                                }
                                if (checkCounter >= MODULE_CHECK_PERIOD) {
                                    if (!flashReader.IsModulePresent()) {
                                        continue;
                                    }
                                    checkCounter = 0;       // Nuluju az po uspesne kontrole
                                }
                                checkCounter++;
                                if (flashReader.ReadMarkerPosition(out readingDone, out markerAddress)) {
                                    break;
                                }
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            if (i >= NUMBER_OF_ATTEMPTS) {
                                // Nepodarilo se, jdu zpet na detekci modulu
                                UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                                break;
                            }
                            
                            if (readingDone) {
                                // Hotovo, adresu markeru mam ulozenou a jdu na cteni dat
                                UpdateStatus(ref workerStatus, WorkerStatus.DATA_READING_START, worker);
                            }
                            break;

                        case WorkerStatus.DATA_READING_START:
                            // Config je v poradku, muzu zacit nacitat data
                            for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                                if (worker.CancellationPending) {
                                    break;
                                }
                                if (flashReader.StartReadData(false)) {     // Config uz mam nacteny, staci cist bez nej
                                    break;
                                }
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            if (i >= NUMBER_OF_ATTEMPTS) {
                                // Nepodarilo se, jdu zpet na detekci modulu
                                UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                                break;
                            }

                            checkCounter = 0;       // Nuluju pocitadlo kontroly pritomnosti modulu
                            UpdateStatus(ref workerStatus, WorkerStatus.DATA_READING, 0, worker);
                            break;

                        case WorkerStatus.DATA_READING:
                            // Nactu dalsi fragment pameti
                            for (i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
                                if (worker.CancellationPending) {
                                    break;
                                }
                                if (checkCounter >= MODULE_CHECK_PERIOD) {
                                    if (!flashReader.IsModulePresent()) {
                                        continue;
                                    }
                                    checkCounter = 0;       // Nuluju az po uspesne kontrole
                                }
                                checkCounter++;
                                if (flashReader.ReadPacket(out readingDone, markerAddress)) {    // Behem cteni kontroluju koncovy marker a pripadne ukoncim cteni driv
                                    break;
                                }
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            if (i >= NUMBER_OF_ATTEMPTS) {
                                // Nepodarilo se, jdu zpet na detekci modulu
                                UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                                break;
                            }
                            
                            // Progress posilam jen kazde cele procento
                            int progress = (int)flashReader.ReadProgress;
                            if (progress != oldProgress) {
                                if (progress > 0) {
                                    // Progress 0% jsem uz poslal na konci stavu DATA_READING_START, neni treba posilat znovu
                                    UpdateStatus(ref workerStatus, WorkerStatus.DATA_READING, progress, worker);
                                }
                                oldProgress = progress;
                            }

                            if (readingDone) {
                                // Hotovo, predam data
                                UpdateStatus(ref workerStatus, WorkerStatus.DATA_READING_FINISHED, flashReader.RawData, 100, worker);
                            }
                            break;
                            
                        case WorkerStatus.DATA_READING_FINISHED:
                            // Data jsou nactena a predana, pockam, az tento modul vytahne a pak jdu zpet na detekci
                            while (!worker.CancellationPending && flashReader.IsModulePresent()) {
                            }
                            if (worker.CancellationPending) {
                                break;
                            }
                            // Modul vytahnul, jdu zpet na detekci
                            UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                            break;

                    }//switch
                } catch {
                    // Pri vyjimce jdu zpet na detekci
                    UpdateStatus(ref workerStatus, WorkerStatus.DETECT, worker);
                }
            }//while

            // Zavru komunikaci
            flashReader.Close();

            // Konecna kontrola ukonceni
            if (worker.CancellationPending) {
                UpdateStatus(ref workerStatus, WorkerStatus.CANCEL, worker);
                e.Cancel = true;
            }

        }
    }
}
