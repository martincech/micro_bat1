﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.UsbReader;
using System.Runtime.InteropServices;
using Veit.StructConversion;

namespace Veit.Bat2Flash {
    /// <summary>
    /// Results from one day of weighing
    /// </summary>
    public struct WeighingDay {
        public int DayNumber;                     // Cislo dne od pocatku vykrmu
        public DateTime DateTime;                      // datum a cas porizeni


    }
    
    /// <summary>
    /// Decoder of BAT2 flash data (rewritten from VymOs MFlash.cpp)
    /// </summary>
    public class Bat2FlashDecoder {
        private Bat2Struct.TFlash flash;
        private int currentArchive;     // pracovni zaznam archivu
        private int lastArchive;        // marker
        private Bat2Struct.TArchiveDailyInfo archiveDailyInfo;
        private int currentSample;      // pracovni zaznam vzorku
        private int lastSample;         // marker

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rawData">Raw data to decode</param>
        public Bat2FlashDecoder(byte[] rawData) {
            // Prevedu pole bajtu na strukturu
            flash = new Bat2Struct.TFlash();
            flash = Veit.StructConversion.StructConversion.ArrayToStruct<Bat2Struct.TFlash>(rawData);
            flash.Swap();       // Swap na spravny Endian
        }

        /// <summary>
        /// Get config section from the data
        /// </summary>
        /// <returns>TConfigSection instance</returns>
        public Bat2Struct.TConfigSection ConfigSection() {
            return flash.ConfigSection;
        }

        /// <summary>
        /// Set archive in front of the first record
        /// </summary>
        public void FirstDay() {
            // hledani markeru :
            for (int i = 0; i < Bat2Struct.FL_ARCHIVE_DAYS; i++) {
                byte marker = (byte)(flash.ArchiveDailyInfos[i].Archive.DayNumber >> 8);
                if (marker == Bat2Struct.FL_ARCHIVE_EMPTY) {
                    if (i == 0) {
                        currentArchive = -1;           // prazdny archiv
                    } else {
                        currentArchive  = 0;           // od zacatku
                    }
                    lastArchive = i;              // po znacku
                    return;
                }
                if (marker == Bat2Struct.FL_ARCHIVE_FULL){
                    if( i == Bat2Struct.FL_ARCHIVE_DAYS - 1){
                        currentArchive = 0;            // znacka na konci
                    } else {
                        currentArchive = i + 1;        // zacatek za znackou
                    }
                    lastArchive = i;              // po znacku
                    return;
                }
            }
            currentArchive = -1;                    // nenalezena znacka
        }

        /// <summary>
        /// Move archive to next day
        /// </summary>
        /// <param name="archive">Archive of the day</param>
        /// <returns>True if successful or false when the end has been reached</returns>
        public bool NextDay(out Bat2Struct.TArchive archive) {
            if (currentArchive < 0) {
                archive = new Bat2Struct.TArchive();
                return false;                      // konec
            }
            int index = currentArchive;             // pamatuj aktualni pozici
            // vypocti nasledujici polozku :
            currentArchive++;
            if (currentArchive >= Bat2Struct.FL_ARCHIVE_DAYS) {
                // jsme na konci
                currentArchive =  0;                 // pokracuje od zacatku
            }
            if (currentArchive == lastArchive) {
                currentArchive = -1;                 // jsme na znacce, konec
            }
            archiveDailyInfo = flash.ArchiveDailyInfos[index];
            archive = archiveDailyInfo.Archive;
            return true;
        }

        /// <summary>
        /// Set logger in front of the first sample
        /// </summary>
        void FirstSample() {
            // hledani markeru :
            for (int i = 0; i < Bat2Struct.FL_LOGGER_SAMPLES; i++) {
                byte marker = (byte)(flash.ArchiveDailyInfos[i].Samples[i].Flag);

                if (marker == Bat2Struct.LG_SAMPLE_EMPTY) {
                    if( i == 0){
                        currentSample = -1;           // prazdny archiv
                    } else {
                        currentSample  = 0;           // od zacatku
                    }
                    lastSample = i;              // po znacku
                    return;
                }
                if (marker == Bat2Struct.LG_SAMPLE_FULL) {
                    if( i == Bat2Struct.FL_LOGGER_SAMPLES - 1){
                        currentSample = 0;            // znacka na konci
                    } else {
                        currentSample = i + 1;        // zacatek za znackou
                    }
                    lastSample = i;              // po znacku
                    return;
                }
            }
            currentSample = -1;                    // nenalezena znacka
        }

        /// <summary>
        /// Move logger to next sample
        /// </summary>
        /// <param name="sample">Sample</param>
        /// <returns>True if successful or false when the end has been reached</returns>
        bool NextSample(out Bat2Struct.TLoggerSample sample) {
            // Nastavi logger na dalsi vzorek (0=posledni)
            if (currentSample < 0) {
                sample = new Bat2Struct.TLoggerSample();
                return false;                      // konec
            }
            int index = currentSample;             // pamatuj aktualni pozici
            // vypocti nasledujici polozku :
            currentSample++;
            if (currentSample >= Bat2Struct.FL_LOGGER_SAMPLES) {
                // jsme na konci
                currentSample =  0;                 // pokracuje od zacatku
            }
            if (currentSample == lastSample) {
                currentSample = -1;                 // jsme na znacce, konec
            }
            sample = archiveDailyInfo.Samples[index];
            return true;
        }



    }
}
