﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Altairis Web Security Toolkit")]
[assembly: AssemblyDescription("Altairis Web Security Toolkit")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Altairis")]
[assembly: AssemblyProduct("Altairis Web Security Toolkit")]
[assembly: AssemblyCopyright("Copyright © Altairis, 2006-2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// Enable running under medium trust settings
[assembly: System.Security.AllowPartiallyTrustedCallers()]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.5.1.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
