﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Veit.CheckValue {
    /// <summary>
    /// Check entered values
    /// </summary>
    static class CheckValue {
        public static string InvalidValueText;

        public static string ApplicationName;

        /// <summary>
        /// Check if character fits into the specified set of chars
        /// </summary>
        /// <param name="ch">Character to check</param>
        /// <param name="validChars">Array of valid characters</param>
        /// <returns>True if valid</returns>
        public static bool IsCharValid(char ch, char[] validChars) {
            foreach (char validChar in validChars) {
                if (ch == validChar) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Check text
        /// </summary>
        /// <param name="text">Text to check</param>
        /// <param name="validChars">Array of valid characters</param>
        /// <returns>True if text is valid</returns>
        public static bool IsTextValid(string text, char[] validChars) {
            if (text == "") {
                return false;
            }
            foreach (char ch in text) {
                if (!IsCharValid(ch, validChars)) {
                    return false;
                }
            }
            return true;
        }
        
        /// <summary>
        /// Check text
        /// </summary>
        /// <param name="text">Text to check</param>
        /// <param name="validChars">Array of valid characters</param>
        /// <returns>True if text is valid</returns>
        public static bool CheckText(Control control, char[] validChars) {
            if (!IsTextValid(control.Text, validChars)) {
                ShowInvalidValueMessage(control);
                return false;
            }
            return true;
        }

        public static void SelectControl(Control control) {
            control.Focus();
            if (control.GetType().ToString().EndsWith("TextBox")) {
                TextBox textBox = (TextBox)control;
                textBox.SelectAll();
            } else if (control.GetType().ToString().EndsWith("ComboBox")) {
                ComboBox comboBox = (ComboBox)control;
                comboBox.SelectAll();
            }
        }

        /// <summary>
        /// Show invalid value message and focus the control
        /// </summary>
        /// <param name="control">Control to focus or null</param>
        public static void ShowInvalidValueMessage(Control control) {
            MessageBox.Show(InvalidValueText, ApplicationName);
            if (control == null) {
                return;
            }
            SelectControl(control);
        }

        public static bool GetInt(Control control, out int i, int minValue, int maxValue) {
            i = 0;      // Pokud je textbox brazdny, hodnota je 0
            if (!Int32.TryParse(control.Text, out i)) {
                ShowInvalidValueMessage(control);
                return false;
            }
            if (i < minValue || i > maxValue) {
                ShowInvalidValueMessage(control);
                return false;
            }
            return true;
        }

        public static bool IsInt(Control control, int minValue, int maxValue) {
            int i;
            return GetInt(control, out i, minValue, maxValue);
        }

        public static bool GetDouble(Control control, out double d, double minValue, double maxValue) {
            d = 0;      // Pokud je textbox brazdny, hodnota je 0
            if (!Double.TryParse(control.Text, out d)) {
                ShowInvalidValueMessage(control);
                return false;
            }
            if (d < minValue || d > maxValue) {
                ShowInvalidValueMessage(control);
                return false;
            }
            return true;
        }

        public static bool IsDouble(Control control, double minValue, double maxValue) {
            double d;
            return GetDouble(control, out d, minValue, maxValue);
        }

    }
}
