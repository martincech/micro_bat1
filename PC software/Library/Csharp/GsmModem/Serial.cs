﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;     // SerialPort
using System.Runtime.InteropServices;


namespace Device.Serial {
//-----------------------------------------------------------------------------
//   Modem
//-----------------------------------------------------------------------------

class RawModem {
    // PortName property
    public string PortName {
        set {
            mPortName = value;
            Disconnect();
        }
        get {
            return( mPortName);
        }
    }
    // BaudRate property
    public int BaudRate {
        set {
            mBaudRate = value;
            mPort.SetParameters( BaudRate, Uart.Parity.None);
        }
        get {
            return( mBaudRate);
        }
    }
    // RxTimeout property
    public int RxTimeout {
        set {
            mRxTimeout = value;
            mPort.SetRxWait( value);
        }
        get {
            return( mRxTimeout);
        }
    }
    // RxBufferSize property
    public int RxBufferSize {
        set {
            mRxBufferSize = value;
            mRxBuffer     = new byte[ value];
        }
        get {
            return( mRxBufferSize);
        }
    }
    // Constructor
    public RawModem()
    {
    } // RawModem

    // Send method
    public bool Send( string Message)
    {
        if( !CheckConnect()){
            Disconnect();
            return( false);
        }
        mPort.Flush();
        Message = Message + "\r";                // append <CR>
        // convert Message to ASCII :
        Encoding ascii        = Encoding.ASCII;
        Encoding unicode      = Encoding.Unicode;
        byte[]   unicodeBytes = unicode.GetBytes( Message);
        byte[]   Data         = Encoding.Convert( unicode, ascii, unicodeBytes);
        // write to port :
        int      Size         = mPort.Write( Data, Data.Length);
        if( Size != Data.Length){
            Disconnect();                        // Tx timeout, try reopen
            return( false);
        }
        return( true);
    } // Send

    // Receive method
    public bool Receive( out string Message)
    {
        return( Receive( out Message, mRxBufferSize));
    } // Receive

    // Receive method
    public bool Receive( out string Message, int Size)
    {
        Message = "";
        if( !CheckConnect()){
            Disconnect();
            return( false);
        }
        int RxSize = mPort.Read( mRxBuffer, Size);
        if( RxSize == 0){
            return( false);                      // Rx timeout
        }
        Size = RxSize;
        // convert data to string :
        Encoding ascii   = Encoding.ASCII;
        char[] AsciiText = new char[ ascii.GetCharCount( mRxBuffer, 0, RxSize)];
        ascii.GetChars( mRxBuffer, 0, RxSize, AsciiText, 0);
        Message = new string( AsciiText);
        return( true);
    } // Receive

    // Disconnect method
    public void Disconnect()
    {
        if( !mPort.IsOpen){
            return;
        }
        mPort.Close();
    } // Disconnect

//-----------------------------------------------------------------------------
    protected Uart       mPort         = new Uart();
    protected string     mPortName     = "COM1";
    protected int        mBaudRate     = 9600;
    protected int        mRxTimeout    = 1000;
    protected int        mRxBufferSize = 80;
    protected byte[]     mRxBuffer     = new byte[ 80];

    // CheckConnect method
    protected bool CheckConnect()
    {
        if( mPort.IsOpen){
            return( true);
        }
        if( !mPort.Open( mPortName)){
            return( false);
        }
        mPort.SetParameters( mBaudRate, Uart.Parity.None);
        mPort.SetRxWait( mRxTimeout);
        mPort.Flush();
        return( true);
    } // CheckConnect
} // RawModem

//-----------------------------------------------------------------------------
//   Uart
//-----------------------------------------------------------------------------

public class Uart {
    public enum Parity {
        None,
        Odd,
        Even,
        Mark,
        Space
    }
    public enum StopBit {
        One,
        OneHalf,
        Two
    }
    public enum Handshake {
        None,
        Hardware,
        XonXoff
    }
    // IsOpen property
    public bool IsOpen {
        get {
            return( mHandle != INVALID_HANDLE_VALUE);
        }
    }

    // Access method
    public static bool Access( string Name)
    // Check for device <Name> presention
    {
        string DeviceName = ComName( Name);
        int    Handle;
        Handle = CreateFile( DeviceName, GENERIC_READ|GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
        if( Handle == INVALID_HANDLE_VALUE){
            return( false);
        }
        CloseHandle( Handle);
        return( true);
    } // Access

    // Open method
    public bool Open( string Name)
    // Open device by <Name>
    {
        Close();                       // close previous
        string DeviceName = ComName( Name);
        mHandle = CreateFile( DeviceName, GENERIC_READ|GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
        if( mHandle == INVALID_HANDLE_VALUE){
            return( false);
        }
        return( true);
    } // Open

    public void Close()
    // Close device
    {
        if( mHandle == INVALID_HANDLE_VALUE){
            return;
        }
        CloseHandle( mHandle);
        mHandle = INVALID_HANDLE_VALUE;
    } // Close

    public int Write( byte[] Data, int Length)
    // Write <Data> of size <Length>, returns written length (or 0 if error)
    {
        if( !IsOpen){
            throw new System.Exception( "Not open");
        }
        int w;
        if( !WriteFile( mHandle, Data, Length,  out w, 0)){
            return( 0);         // timeout or error
        }
        return( w);
    } // Write

    public int Read( byte[] Data, int Length)
    // Read data <Data> of size <Length>, returns true length (or 0 if error)
    {
        if( !IsOpen){
            throw new System.Exception( "Not open");
        }
        int r = 0;
        if( !ReadFile( mHandle, Data, Length, out r, 0)){
            return( 0);
        }
        return( r);
    } // Read

    public void Flush()
    // Make input/output queue empty
    {
        if( !IsOpen){
            throw new System.Exception( "Not open");
        }
        PurgeComm( mHandle,PURGE_RXCLEAR | PURGE_TXCLEAR);    
    } // Flush

   
    public void SetRxNowait()
    // Set timing of receiver - returns collected data immediately
    {
        COMMTIMEOUTS to;

        if( !IsOpen){
            return;                    // return silently
        }
        // Tx timeout :
        to.WriteTotalTimeoutMultiplier =  1;
        to.WriteTotalTimeoutConstant   =  2000;
        // RxTimeout :
        to.ReadIntervalTimeout         =  0xffffffff;
        to.ReadTotalTimeoutMultiplier  =  0;
        to.ReadTotalTimeoutConstant    =  0;
        SetCommTimeouts( mHandle, ref to);
    } // SetRxNowait

    public void SetRxWait( int Timeout)
    // Set Rx <Timeout> [ms]
    {
        COMMTIMEOUTS to;

        if( !IsOpen){
            return;                    // return silently
        }
        // Tx timeout :
        to.WriteTotalTimeoutMultiplier =  1;
        to.WriteTotalTimeoutConstant   =  2000;
        // RxTimeout :
        to.ReadIntervalTimeout         =  0;
        to.ReadTotalTimeoutMultiplier  =  0;
        to.ReadTotalTimeoutConstant    =  (uint)Timeout;
        SetCommTimeouts( mHandle, ref to);
    } // SetRxWait

    public void SetParameters( int BaudRate, Parity _Parity, int Bits, StopBit StopBits, Handshake _Handshake)
    {
        DCB dcb;

        if( !IsOpen){
            return;                    // return silently
        }
        GetCommState( mHandle, out dcb);
        dcb.BaudRate  = (uint)BaudRate;
        dcb.ByteSize  = (byte)Bits;
        dcb.Parity    = (byte)_Parity;
        dcb.StopBits  = (byte)StopBits;
        switch( _Handshake){
            case Handshake.None :
                dcb.Handshake = (DTR_CONTROL_ENABLE << fDtrControl) | 
                                (RTS_CONTROL_ENABLE << fRtsControl);
                break;

            case Handshake.Hardware :
                dcb.Handshake = (DTR_CONTROL_HANDSHAKE << fDtrControl) | 
                                (RTS_CONTROL_HANDSHAKE << fRtsControl) |
                                (1 << fOutxCtsFlow) |
                                (1 << fOutxDsrFlow) |
                                (1 << fDsrSensitivity);
                break;

            case Handshake.XonXoff :
                dcb.Handshake = (1 << fInX)  |
                                (1 << fOutX) |
                                (DTR_CONTROL_ENABLE << fDtrControl) | 
                                (RTS_CONTROL_ENABLE << fRtsControl);
                break;

            default :
                break;
        }
        SetCommState( mHandle, ref dcb);
    } // SetParameters

    public void SetParameters( int BaudRate, Parity _Parity)
    {
        SetParameters( BaudRate, _Parity, 8, StopBit.One, Handshake.None);
    } // SetParameters

//-----------------------------------------------------------------------------
    protected int  mHandle = INVALID_HANDLE_VALUE;

    static protected string ComName( string Name)
    // Converts <Name> to appropriate OS name
    {
        return( "\\\\.\\" + Name);
    } // ComName

//-----------------------------------------------------------------------------
// Win32 API
//-----------------------------------------------------------------------------
    internal const uint GENERIC_READ  = 0x80000000; 
    internal const uint GENERIC_WRITE = 0x40000000; 

    internal const uint PURGE_TXCLEAR = 0x0004;
    internal const uint PURGE_RXCLEAR = 0x0008;

    internal const int INVALID_HANDLE_VALUE = -1; 

    internal const int OPEN_EXISTING = 3; 
              
    [ DllImport( "kernel32.dll", CharSet=CharSet.Auto, SetLastError=true ) ]
    internal static extern int CreateFile( string lpFileName, uint dwDesiredAccess, int dwShareMode, int lpSecurityAttributes, int dwCreationDisposition, int dwFlagsAndAttributes, int hTemplateFile );

    [ DllImport( "kernel32.dll", SetLastError=true ) ]
    internal static extern void CloseHandle( int hFile);
       
    [ DllImport( "kernel32.dll", SetLastError=true ) ]
    internal static extern Boolean ReadFile( int hFile, byte[] lpBuffer, int nNumberOfBytesToRead, out int lpNumberOfBytesRead, int lpOverlapped );        
            
    [ DllImport( "kernel32.dll", SetLastError=true ) ]
    internal static extern Boolean WriteFile( int hFile, byte[] lpBuffer, int nNumberOfBytesToWrite, out int lpNumberOfBytesWritten, int lpOverlapped);

    [StructLayout(LayoutKind.Sequential, Pack=1)]
    public struct DCB {
        public uint DCBlength;
        public uint BaudRate;
        public uint Handshake;
        public short wReserved;
        public short XonLim;
        public short XoffLim;
        public byte ByteSize;
        public byte Parity;
        public byte StopBits;
        public byte XonChar;
        public byte XoffChar;
        public byte ErrorChar;
        public byte EofChar;
        public byte EvtChar;
        public short wReserved2;
    };

    // Handshake bitfield offsets :
    public const int fBinary           = 0;     /* Binary Mode (skip EOF check)    */
    public const int fParity           = 1;     /* Enable parity checking          */
    public const int fOutxCtsFlow      = 2;     /* CTS handshaking on output       */
    public const int fOutxDsrFlow      = 3;     /* DSR handshaking on output       */
    public const int fDtrControl       = 4;     /* DTR Flow control                */
    public const int fDsrSensitivity   = 6;     /* DSR Sensitivity              */
    public const int fTXContinueOnXoff = 7;     /* Continue TX when Xoff sent */
    public const int fOutX             = 8;     /* Enable output X-ON/X-OFF        */
    public const int fInX              = 9;     /* Enable input X-ON/X-OFF         */
    public const int fErrorChar        = 10;    /* Enable Err Replacement          */
    public const int fNull             = 11;    /* Enable Null stripping           */
    public const int fRtsControl       = 12;    /* Rts Flow control                */
    public const int fAbortOnError     = 14;    /* Abort all reads and writes on Error */
    // DTR handshake :
    public const uint DTR_CONTROL_DISABLE   = 0x00;
    public const uint DTR_CONTROL_ENABLE    = 0x01;
    public const uint DTR_CONTROL_HANDSHAKE = 0x02;
    // RTS handshake
    public const uint RTS_CONTROL_DISABLE   =  0x00;
    public const uint RTS_CONTROL_ENABLE    =  0x01;
    public const uint RTS_CONTROL_HANDSHAKE =  0x02;
    public const uint RTS_CONTROL_TOGGLE    =  0x03;

    [StructLayout(LayoutKind.Sequential, Pack=1)]
    public struct COMMTIMEOUTS {
        public uint ReadIntervalTimeout;
        public uint ReadTotalTimeoutMultiplier;
        public uint ReadTotalTimeoutConstant;
        public uint WriteTotalTimeoutMultiplier;
        public uint WriteTotalTimeoutConstant;
    };

    [ DllImport( "kernel32.dll", SetLastError=true ) ]
    internal static extern int GetCommState( int hFile, out DCB dcb);

    [ DllImport( "kernel32.dll", SetLastError=true ) ]
    internal static extern int SetCommState( int hFile, ref DCB dcb);

    [ DllImport( "kernel32.dll", SetLastError=true ) ]
    internal static extern int SetCommTimeouts( int hFile, ref COMMTIMEOUTS to);

    [ DllImport( "kernel32.dll", SetLastError=true ) ]
    internal static extern int PurgeComm( int hFile, uint Mode);
} // Uart

} // namespace
