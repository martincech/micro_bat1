﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modem;

namespace GsmDemo {
    class Program {
        static void Main( string[] args )
        {
            string  String;
            int     Value;
            string  Phone;
            Gsm     Modem           = new Gsm();
            Modem.PortName          = "COM10";
            Modem.BaudRate          = 9600;
            Modem.CmdTimeout        = 1000;    // [ms]
            Modem.SmsCenterTimeout  = 15;      // [s]
            Modem.SmsReceiveTimeout = 1;       // [s]
            Modem.SmsDeleteTimeout  = 3;       // [s]
            Modem.RxBufferSize      = 512;     // COM Rx buffer
            Modem.MemoryAtSim       = true;    // use SMS memory at SIM card
            while ( true)
            {
                ConsoleKeyInfo ch;
                ch = Console.ReadKey(true);
                switch (ch.Key)
                {
                    case ConsoleKey.D :
                        string Model;
                        /*
                        if( !Gsm.Detect( "COM1", out Model)){
                            Console.WriteLine( "Unable detect COM1");
                        } else {
                            Console.WriteLine( "Detected COM1 : {0}", Model);
                        }
                        */
                        if( !Gsm.Detect( "COM10", out Model)){
                            Console.WriteLine( "Unable detect COM10");
                        } else {
                            Console.WriteLine( "Detected COM10 : '{0}'", Model);
                        }
                        break;

                    case ConsoleKey.R :
                        if( !Modem.Reset()){
                            Console.WriteLine( "Reset failed");
                            break;
                        }
                        Console.WriteLine( "Memory size : {0}", Modem.MemorySize);
                        Console.WriteLine( "Reset OK");
                        break;

                    case ConsoleKey.C :
                        if( !Modem.Check()){
                            Console.WriteLine( "Modem not present");
                            break;
                        }
                        Console.WriteLine( "Modem OK");
                        break;

                    case ConsoleKey.G :
                        if( !Modem.IsRegistered()){
                            Console.WriteLine( "Modem not registered");
                            break;
                        }
                        Console.WriteLine( "Registration OK");
                        break;

                    case ConsoleKey.O :
                        if( !Modem.Operator( out String)){
                            Console.WriteLine( "Unable get operator");
                            break;
                        }
                        Console.WriteLine( "Operator : '{0}'", String);
                        break;

                    case ConsoleKey.E :
                        if( !Modem.SignalStrength( out Value)){
                            Console.WriteLine( "Unable get signal strength");
                            break;
                        }
                        Console.WriteLine( "Signal : '{0}'", Value);
                        break;

                    case ConsoleKey.S :
                        if( !Modem.SmsSend( "+420604967296", "Ahoj ! @@")){
                            Console.WriteLine( "Unable send SMS");
                            break;
                        }
                        Console.WriteLine( "SMS delivered");
                        break;

                    case ConsoleKey.V :
                        for( int i = 0; i < Modem.MemorySize; i++){
                            if( !Modem.SmsReceive( i, out Phone, out String)){
                                Console.WriteLine( "Unable read SMS #{0}", i);
                                continue;
                            }
                            if( Phone != ""){
                               Console.WriteLine( "SMS #{0} from '{1}' : '{2}'", i, Phone, String);
                            } // else empty position
                        }
                        Console.WriteLine( "SMS read done");
                        break;

                    case ConsoleKey.T :
                        if( !Modem.SmsDelete( 0)){
                            Console.WriteLine( "Unable delete SMS");
                            break;
                        }
                        Console.WriteLine( "SMS deleted");
                        break;

                    case ConsoleKey.N :
                        if( !Modem.SmsCount( out Value)){
                            Console.WriteLine( "Unable get SMS count");
                            break;
                        }
                        Console.WriteLine( "SMS count = {0}", Value);
                        break;

                    case ConsoleKey.X :
                        return;

                    default :
                        Console.WriteLine( "D - detect");
                        Console.WriteLine( "R - reset");
                        Console.WriteLine( "C - check");
                        Console.WriteLine( "G - is registered");
                        Console.WriteLine( "O - operator");
                        Console.WriteLine( "E - signal strength");
                        Console.WriteLine( "S - SMS send");
                        Console.WriteLine( "V - SMS read");
                        Console.WriteLine( "T - SMS delete");
                        Console.WriteLine( "N - SMS received count");
                        Console.WriteLine( "X - exit");
                        break;
                }
            }
        }
    }
}
