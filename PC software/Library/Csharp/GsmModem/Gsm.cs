﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Device.Serial;

namespace Modem {

class Gsm : RawModem {
    // modem device model :
    public enum DeviceModel {
        SIEMENS,
        TELTONIKA,
        _LAST,
    }

    // SMS Memory location
    public bool MemoryAtSim {
        get {
            return( mMemoryAtSim);
        }
        set {
            mMemoryAtSim = value;
        }
    }
    // SMS Memory size
    public int MemorySize {
        get {
            return( mMemorySize);
        }
    }
    // Command timeout
    public int CmdTimeout {
        get {
            return( mCmdTimeout);
        }
        set {
            mCmdTimeout = value;
        }
    }
    // SMS Center timeout
    public int SmsCenterTimeout {
        get {
            return( mSmsCenterTimeout);
        }
        set {
            mSmsCenterTimeout = value;
        }
    }
    // SMS receive timeout
    public int SmsReceiveTimeout {
        get {
            return( mSmsReceiveTimeout);
        }
        set {
            mSmsReceiveTimeout = value;
        }
    }
    // SMS delete timeout
    public int SmsDeleteTimeout {
        get {
            return( mSmsDeleteTimeout);
        }
        set {
            mSmsDeleteTimeout = value;
        }
    }
    // device model
    public DeviceModel Model {
        get {
            return( mDeviceModel);
        }
    }

    // Constructor :
    public Gsm()
    {
    } // Gsm

    // Detect method
    public static bool Detect( string Port, out string Model)
    {
        Model = "";
        RawModem Modem  = new RawModem();
        Modem.PortName  = Port;
        Modem.BaudRate  = 9600;
        Modem.RxTimeout = 1000;
        string Reply;
        // echo off :
        if( !Modem.Send( "ATE0")){
            return( false);
        }
        if( !Modem.Receive( out Reply)){
            return( false);
        }
        // get model :
        if( !Modem.Send( "AT+GMM")){
            return( false);
        }
        if( !Modem.Receive( out Reply)){
            return( false);
        }
        if( !ParseDeviceModel( Reply, out Model)){
            return( false);
        }
        Modem.Disconnect();
        return( true);
    } // Detect

    // Reset method
    public bool Reset()
    {
        string Reply;
        // close COM port :
        Disconnect();
        RxTimeout = mCmdTimeout;       // default timeout
        // echo off :
        if( !Send( "ATE0")){
            return( false);
        }
        if( !Receive( out Reply)){
            return( false);
        }
        if( Reply != "ATE0" + StrOk){
            if( Reply != StrOk){
                return( false);
            }
        }
        // SMS text mode :
        if( !Send( "AT+CMGF=1")){
            return( false);
        }
        if( !Receive( out Reply, StrOk.Length)){
            return( false);
        }
        if( Reply != StrOk){
            return( false);
        }
        // SMS receive - short data :
        if( !Send( "AT+CSDH=0")){
            return( false);
        }
        if( !Receive( out Reply, StrOk.Length)){
            return( false);
        }
        if( Reply != StrOk){
            return( false);
        }
        // Long error code :
        if( !Send( "ATV1+CMEE=1")){
            return( false);
        }
        if( !Receive( out Reply, StrOk.Length)){
            return( false);
        }
        if( Reply != StrOk){
            return( false);
        }
        // get device model :
        if( !Send( "AT+GMM")){
            return( false);
        }
        if( !Receive( out Reply)){
            return( false);
        }
        string DeviceModelName;
        if( !ParseDeviceModel( Reply, out DeviceModelName)){
            return( false);
        }
        mDeviceModel = DeviceModel.SIEMENS;
        if( DeviceModelName == "TM3 EDGE"){
            mDeviceModel = DeviceModel.TELTONIKA;
        }
        // Memory to SIM/device :
        mBaseIndex = 0;                 // set standard memory base index
        if( mMemoryAtSim){
            if( !Send( "AT+CPMS=\"SM\",\"SM\",\"SM\"")){
                return( false);
            }
            if( mDeviceModel == DeviceModel.TELTONIKA){
               mBaseIndex = 300;        // change memory base index by Teltonika at SIM
            }
        } else {
            if( !Send( "AT+CPMS=\"MT\",\"MT\",\"MT\"")){
                return( false);
            }
        }
        if( !Receive( out Reply)){
            return( false);
        }
        // check for memory reply header :
        string MemoryHeader = "\r\n+CPMS: "; 
        if( !Reply.Contains( MemoryHeader)){
            return( false);
        }
        Reply = Reply.Substring( MemoryHeader.Length);
        // skip number of messages :
        int Index;
        Index = Reply.IndexOf( ',');
        if( Index < 0){
            return( false);                      // comma not found
        }
        Reply = Reply.Substring( Index + 1);     // cut after comma
        // truncate tail of number :
        Index = Reply.IndexOf( ',');
        if( Index < 0){
            return( false);                      // trailing comma not found
        }
        Reply = Reply.Substring( 0, Index);      // cut trailer after comma
        // read number :
        try {
            mMemorySize = Convert.ToInt32( Reply);
        } catch {
            return( false);
        }
        return( true);
    } // Reset

    // Check method
    public bool Check()
    {
        string Reply;

        if( !Send( "AT")){
            return( false);
        }
        if( !Receive( out Reply, StrOk.Length)){
            return( false);
        }
        if( Reply != StrOk){
            return( false);
        }
        return( true);
    } // Check

    // IsRegistered method
    public bool IsRegistered()
    {
        string Reply;

        if( !Send( "AT+CREG?")){
            return( false);
        }
        string RegHeader = "\r\n+CREG: ";
        if( !Receive( out Reply, RegHeader.Length)){
            return( false);
        }
        if( Reply != RegHeader){
            return( false);
        }
        if( !Receive( out Reply, 11)){
            return( false);
        }
        int Index;
        Index = Reply.IndexOf( ',');
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( Index + 1);     // cut after comma
        // truncate tail of number :
        Index = Reply.IndexOf( '\r');
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( 0, Index);      // cut trailer after CR
        // read number :
        int Registered;
        try {
            Registered = Convert.ToInt32( Reply);
        } catch {
            return( false);
        }
        if( Registered == 1 || Registered == 5){
            return( true);                       // registered or roaming
        }
        return( false);
    } // IsRegistered

    // Operator method
    public bool Operator( out string Name)
    {
        string Reply;

        Name = "";
        if( !Send( "AT+COPS?")){
            return( false);
        }
        string OpsHeader = "\r\n+COPS: ";
        if( !Receive( out Reply, OpsHeader.Length)){
            return( false);
        }
        if( Reply != OpsHeader){
            return( false);
        }
        if( !Receive( out Reply)){
            return( false);
        }
        int Index;
        // skip <mode> field
        Index = Reply.IndexOf( ',');
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( Index + 1);     // cut after comma
        // skip <format> field
        Index = Reply.IndexOf( ',');
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( Index + 1);     // cut after comma
        // leading delimiter
        Index = Reply.IndexOf( '"');
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( Index + 1);     // cut after quotatiton
        // trailing delimiter
        Index = Reply.IndexOf( '"');
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( 0, Index);      // cut trailer after quotation
        Name = Reply;
        return( true);
    } // Operator

    // SignalStrength method
    public bool SignalStrength( out int Strength)
    {
        string Reply;

        Strength = 99;                           // no signal
        if( !Send( "AT+CSQ")){
            return( false);
        }
        string RssiHeader = "\r\n+CSQ: ";
        if( !Receive( out Reply, RssiHeader.Length)){
            return( false);
        }
        if( Reply != RssiHeader){
            return( false);
        }
        if( !Receive( out Reply, 8)){
            return( false);
        }
        int Index;
        Index = Reply.IndexOf( ',');
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( 0, Index);     // cut after comma
        // read number :
        int Rssi;
        try {
            Rssi = Convert.ToInt32( Reply);
        } catch {
            return( false);
        }
        Strength = Rssi;
        return( true);
    } // SignalStrength

    // SmsSend method
    public bool SmsSend( string To, string Message)
    {
        string Reply;

        Message = AsciiToGsm( Message);
        Message = Message + "\x1A";              // append CTRL-Z
        string CmdSend = "AT+CMGS=\"" + To + "\"";
        if( !Send( CmdSend)){
            return( false);
        }
        // wait for prompt :
        Thread.Sleep( 500 );
        string MsgPrompt = "\r\n> ";
        if( !Receive( out Reply, MsgPrompt.Length)){
            return( false);
        }
        if( Reply != MsgPrompt){
            return( false);
        }
        Thread.Sleep( 100 );                     // wait after prompt
        if( !Send( Message)){
            return( false);
        }
        RxTimeout = mSmsCenterTimeout * 1000;    // convert to [ms]
        string SendOk = "\r\n+CMGS: ";
        if( !Receive( out Reply, SendOk.Length)){
            RxTimeout = mCmdTimeout;
            return( false);
        }
        if( Reply != SendOk){
            RxTimeout = mCmdTimeout;
            return( false);
        }
        RxTimeout = mCmdTimeout;
        return( true);
    } // SmsSend

    // SmsReceive method
    public bool SmsReceive( int Address, out string From, out string Message)
    {
        string Reply;

        From    = "";
        Message = "";
        Address++;                               // starts at 1
        Address += mBaseIndex;
        string CmdRead = "AT+CMGR=" + Address.ToString();
        if( !Send( CmdRead)){
            return( false);
        }
        RxTimeout = mSmsReceiveTimeout * 1000;
        if( !Receive( out Reply)){
            RxTimeout = mCmdTimeout;
            return( false);
        }
        RxTimeout = mCmdTimeout;
        // check for empty position :
        if( Reply.Contains( "\r\n+CMGR: 0,,0\r\n") ||
            Reply.Contains( "\r\n+CMS ERROR: 321")){
            Message = "<*EMPTY*>";
            return( true);
        }
        // check for error :
        if( Reply.Contains( StrError)){
            return( false);
        }
        int Index;
        // reply header :
        string RxHeader = "\r\n+CMGR: ";
        Index = Reply.IndexOf( RxHeader);
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( Index + RxHeader.Length);   // cut header
        // check for read status :
        string RecRead   = "\"REC READ\"";
        string RecUnread = "\"REC UNREAD\"";
        int    RecSize   = RecRead.Length;
        Index = Reply.IndexOf( RecRead);
        if( Index < 0){
            Index   = Reply.IndexOf( RecUnread);
            RecSize = RecUnread.Length;
            if( Index < 0){
                return( false);
            }
        }
        Reply = Reply.Substring( Index + RecSize); // cut read status
        // get phone number
        Index = Reply.IndexOf( ",\"");           // comma and quotation after read status
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( Index + 2);     // cut comma and quotation
        Index = Reply.IndexOf( '"');             // terminating quotation
        if( Index < 0){
            return( false);
        }
        From  = Reply.Substring( 0, Index);      // phone number
        Reply = Reply.Substring( Index);         // cut phone number
        // get message
        Index = Reply.IndexOf( "\r\n");          // CR LF at start of message
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( Index + 2);     // start of message
        Index = Reply.IndexOf( '\r');            // CR at end of message
        if( Index < 0){
            return( false);
        }
        Reply = Reply.Substring( 0, Index);      // cut trailing characters
        Message = GsmToAscii( Reply);
        return( true);
    } // SmsReceive

    // SmsDelete method
    public bool SmsDelete( int Address)
    {
        string Reply;
        Address++;                               // starts at 1
        Address += mBaseIndex;
        string CmdDelete = "AT+CMGD=" + Address.ToString();
        if( !Send( CmdDelete)){
            return( false);
        }
        RxTimeout = mSmsDeleteTimeout * 1000;
        if( !Receive( out Reply, StrOk.Length)){
            RxTimeout = mCmdTimeout;
            return( false);
        }
        RxTimeout = mCmdTimeout;
        if( Reply != StrOk){
            return( false);
        }
        return( true);
    } // SmsDelete

    // SmsCount method
    public bool SmsCount( out int Count)
    {
        string Reply;
        Count = 0;

        if( !Send( "AT+CPMS?")){
           return( false);
        }
        if( !Receive( out Reply)){
            return( false);
        }
        // check for memory reply header :
        string MemoryHeader = "\r\n+CPMS: "; 
        if( !Reply.Contains( MemoryHeader)){
            return( false);
        }
        Reply = Reply.Substring( MemoryHeader.Length);
        // skip number of messages :
        int Index;
        Index = Reply.IndexOf( ',');
        if( Index < 0){
            return( false);                      // comma not found
        }
        Reply = Reply.Substring( Index + 1);     // cut after comma
        // truncate tail of number :
        Index = Reply.IndexOf( ',');
        if( Index < 0){
            return( false);                      // trailing comma not found
        }
        Reply = Reply.Substring( 0, Index);      // cut trailer after comma
        // read number :
        try {
            Count = Convert.ToInt32( Reply);
        } catch {
            return( false);
        }
        return( true);
    } // SmsCount

//-----------------------------------------------------------------------------
    protected int    mMemorySize        = 0;
    protected bool   mMemoryAtSim       = true;
    protected int    mCmdTimeout        = 500;
    protected int    mSmsCenterTimeout  = 15;
    protected int    mSmsReceiveTimeout = 1;
    protected int    mSmsDeleteTimeout  = 3;
    protected DeviceModel mDeviceModel  = DeviceModel.SIEMENS;
    protected int    mBaseIndex         = 0;

    protected const string StrOk    = "\r\nOK\r\n";
    protected const string StrError = "\r\n+CMS ERROR: ";

    protected string AsciiToGsm( string Text)
    {
        StringBuilder String = new StringBuilder();
        for( int i = 0; i < Text.Length; i++){
            if( Text[ i] == '@'){
                String.Append( '\0');        // GSM @ == 0
            } else {
                String.Append( Text[ i]);
            }
        }
        return( String.ToString());
    } // AsciiToGsm

    protected string GsmToAscii( string Text)
    {
        StringBuilder String = new StringBuilder();
        for( int i = 0; i < Text.Length; i++){
            if( Text[ i] == '\0'){
                String.Append( '@');        // GSM @ == 0
            } else {
                String.Append( Text[ i]);
            }
        }
        return( String.ToString());
    } // GsmToAscii

    protected static bool ParseDeviceModel( string Reply, out string Model)
    {
        // separate model name :
        Model = "";
        int Index = Reply.IndexOf( "\r\n");
        if( Index != 0){
            return( false);
        }
        Reply = Reply.Substring( 2);
        if( !Reply.Contains( "\r")){
            return( false);
        }
        Model = Reply.Substring( 0, Reply.IndexOf( "\r"));
        return( true);
    } // ParseDeviceModel
} // Gsm

} // Modem
