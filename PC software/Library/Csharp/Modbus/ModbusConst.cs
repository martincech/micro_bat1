﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Veit.ModbusProtocol {
    public class ModbusConst {
        // Rozsahy adres :
        public const byte BROADCAST_ADDRESS = 0;
        public const byte MIN_ADDRESS       = 1;
        public const byte MAX_ADDRESS       = 247;
        // rezervovano :              248..255

        // delkova omezeni :
        public const int  PDU_FUNCTION_SIZE =   1;      // byte
        public const int  PDU_MAX_DATA_SIZE = 252;      // bytes
        public const int  MAX_PDU_SIZE      = (PDU_FUNCTION_SIZE + PDU_MAX_DATA_SIZE);
    }
}
