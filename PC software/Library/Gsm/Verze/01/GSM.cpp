//******************************************************************************
//
//   GSM.cpp      GSM network module
//   Version 1.0  (c) VymOs
//
//  - 15.3.2006: Na WinXP a modemu MC75 neslo odeslat SMS, bylo treba pockat 1 sec mezi Rx a Tx.
//
//  - 18.5.2006: Upravy od Ivose, v nekterych pripadech zustala alokovana pamet, ktera se neuvolnila, na funkci to nemelo vliv
//
//  - 28.6.2006: Upravy od Ivose, nastaveno pouzivani pouze SMS pozic v SIM (pozice v pameti blby u MC39i), nova fce SmsCount()
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>
//#include <string.h>

#include "GSM.h"

#pragma package(smart_init)


//#define DebugText(Str) ShowMessage(Str)
#define DebugText(Str)



#define MAX_TEXT            512        // max raw message length
#define MAX_NUMBER           16        // max Number count
#define MAX_SMS             160        // max SMS length
#define DETECT_BAUD_RATE   9600        // modem detection baud rate
#define DETECT_TIMEOUT      100        // modem detection timeout
#define DEFAULT_BAUD_RATE  9600        // GSM modem rate

#define SMSC_TIMEOUT      30000        // SMS center reply timeout [ms] (pri 15sec to nekdy blblo na ES75 + Eurotel)
#define SMS_RX_TIMEOUT     1000        // SMS Rx timeout [ms]
#define SMS_DELETE_TIMEOUT 2500        // SMS delete timeout [ms]
#define ME_TIMEOUT          100        // ME timeout [ms]

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, (void *)Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, (void *)Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, (void *)Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

// AT strings :

// Basic :
static const char At[]         = "AT\r";                 // AT
static const char Ok[]         = "\r\nOK\r\n";           // ok reply
static const char Error[]      = "\r\n+CMS ERROR: ";     // error reply (+number)

static const char EchoDefault[]= "AT&FE0\r";             // factory default, echo off
static const char EchoOk[]     = "AT&FE0\r\r\nOK\r\n";   // echo off ok reply

static const char ErrorMode[]  = "ATV1+CMEE=1\r";        // Extended error mode
static const char PduMode[]    = "AT+CMGF=0\r";          // SMS PDU mode

// Memory settings :
//static const char MemoryMode[]  = "AT+CPMS=MT,MT,MT\r";  // SMS data in the internal memory
static const char MemoryMode[]  = "AT+CPMS=SM,SM,SM\r";  // SMS data in the SIM card memory
static const char MemoryHeader[]= "\r\n+CPMS: ";         // Header of memory settings

// Operator & signal data :
static const char Registered[] = "AT+CREG?\r";           // Registration
static const char RegHeader[]  = "\r\n+CREG: ";          // Header of registration

static const char GsmOperator[]= "AT+COPS?\r";           // Get operator
static const char OperHeader[] = "\r\n+COPS: ";          // operator header

static const char Rssi[]       = "AT+CSQ\r";             // Get Received Signal Strength
static const char RssiHeader[] = "\r\n+CSQ: ";           // Signal strength header

// Detect :
static const char EchoOff[]     = "ATE0\r";              // echo off
static const char DeviceModel[] = "AT+GMM\r";            // get model

// SMS :
static const char PduSend[]    = "AT+CMGS=%d\r";         // Send SMS + <length><CR>
static const char Prompt[]     = "\r\n> ";               // input prompt
static const char SendOk[]     = "\r\n+CMGS: ";          // Send ok (+message reference, pduack)

static const char PduRead[]    = "AT+CMGR=%d\r";         // Read SMS <index><CR>
static const char ReadHeader[] = "\r\n+CMGR: ";          // Header of read message
static const char EmptyRead[]  = "\r\n+CMGR: 0,,0\r\n";  // Empty read

static const char Delete[]     = "AT+CMGD=%d\r";         // Delete SMS <index><CR>

static const char MemoryCount[]= "AT+CPMS?\r";           // Memory counters

#define CTRL_Z  '\x1A'

#define nibble2hex( n)   ((n) > 9 ? 'A' + (n) - 10 : '0' + (n))
#define char2dec( ch)    ((ch) - '0')

//******************************************************************************
// Constructor
//******************************************************************************

TGsmModem::TGsmModem()
// Constructor
{
   FPort                  = new TComUart;
   FLogger                = 0;
   FBaudRate              = DEFAULT_BAUD_RATE;
   FCmdTimeout            = ME_TIMEOUT;
   FSmsCenterTimeout      = SMSC_TIMEOUT;
   FSmsRxTimeout          = SMS_RX_TIMEOUT;
   FSmsDeleteTimeout      = SMS_DELETE_TIMEOUT;
   FMemorySize            = 0;
} //TGsm

//******************************************************************************
// Destructor
//******************************************************************************

TGsmModem::~TGsmModem()
// Destructor
{
   Disconnect();
} // ~TGsm

//******************************************************************************
// Detect
//******************************************************************************

bool TGsmModem::Detect( TString Port, TString &Model)
// Detect modem device <Model> at <Port>
{
   char       *DeviceName;
   TIdentifier Identifier;
   TComUart   *Uart = new TComUart;
   char        Buffer[ 255];
   // port parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate  = DETECT_BAUD_RATE;
   Parameters.DataBits  = 8;
   Parameters.StopBits  = 10;
   Parameters.Parity    = TUart::NO_PARITY;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   if( !Uart->Locate( Port.c_str(), Identifier)){
      delete Uart;
      return( NO);
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( NO);
   }
   if( !Uart->SetParameters( Parameters)){
      delete Uart;
      return( NO);
   }
   Uart->SetRxWait( DETECT_TIMEOUT, 0);
   Uart->Flush();
   // Echo off :
   if( !Uart->Write( const_cast<char*>( EchoOff), strlen( EchoOff))){
      delete Uart;
      return( NO);
   }
   if( !Uart->Read(  Buffer, sizeof( Buffer))){
      delete Uart;
      return( NO);
   }
   // Device model :
   if( !Uart->Write( const_cast<char*>( DeviceModel), strlen( DeviceModel))){
      delete Uart;
      return( NO);
   }
   if( !Uart->Read(  Buffer, sizeof( Buffer))){
      delete Uart;
      return( NO);
   }
   // Parse device model string :
   DeviceName = Buffer;
   while( *DeviceName == '\r' || *DeviceName == '\n'){
      DeviceName++;                 // skip starting \r \n
   }
   char *p;
   p = strchr( DeviceName, '\r');   // stops  with \r
   if( !p){
      delete Uart;
      return( NO);
   }
   *p = '\0';                       // trim before \r
   Model = DeviceName;              // return model name
   delete Uart;
   return( YES);
} // Detect

//******************************************************************************
// Connect
//******************************************************************************

bool TGsmModem::Connect( TString Port)
// Connect via <Port>
{
   // disconnect old port :
   Disconnect();
   // open new port :
   TComUart *Uart = new TComUart;
   TIdentifier Identifier;
   if( !Uart->Locate( Port, Identifier)){
      delete Uart;
      return( false);
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   // set communication parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate  = FBaudRate;
   Parameters.DataBits  = 8;
   Parameters.StopBits  = 10;
   Parameters.Parity    = TUart::NO_PARITY;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   Uart->SetParameters( Parameters);
   Uart->SetRxWait( FCmdTimeout, 0);
   Uart->Flush();
   FPort = Uart;
   return( true);
} // Connect

//******************************************************************************
// Disconnect
//******************************************************************************

void TGsmModem::Disconnect()
// Disconnect port
{
   if( FPort){
      delete FPort;
      FPort = 0;
   }
} // Disconnect

//******************************************************************************
// Reset
//******************************************************************************

bool TGsmModem::Reset()
// Initialize modem device
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   // Echo off :
   if( !TxString( EchoDefault)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, EchoOk)){
      if( !strequ( RxBuffer, Ok)){
         return( false);
      }
   }
   // PDU mode :
   if( !TxString( PduMode)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   // Long error code :
   if( !TxString( ErrorMode)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   // direct SMS data to internal memory :
   if( !TxString( MemoryMode)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   int HeaderSize = strlen( MemoryHeader);
   if( !strnequ( RxBuffer, MemoryHeader, HeaderSize)){
      return( false);
   }
   char *p = &RxBuffer[ HeaderSize];
   p = strchr( p, ',');                // skip <items> field
   if( !p){
      return( false);
   }
   p++;                                // skip ','
   // Internal memory size :
   FMemorySize = GetDecNumber( p);
   if( !FMemorySize){
      return( false);
   }
   return( true);
} // Reset

//******************************************************************************
// Check
//******************************************************************************

bool TGsmModem::Check()
// Check for modem device
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( At)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   return( true);
} // Check

//******************************************************************************
// IsRegistered
//******************************************************************************

bool TGsmModem::IsRegistered()
// Returns status of network registration
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( Registered)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   // reply header :
   int HeaderSize = strlen( RegHeader);
   if( !strnequ( RxBuffer, RegHeader, HeaderSize)){
      return( false);
   }
   char *p = &RxBuffer[ HeaderSize];
   p = strchr( p, ',');                // skip <n> field
   if( !p){
      return( false);
   }
   p++;                                // skip ','
   int Value = GetDecNumber( p);
   if( Value == 1 || Value == 5){
      return( true);                   // registered or roaming
   }
   return( false);
} // IsRegistered

//******************************************************************************
// Operator
//******************************************************************************

bool TGsmModem::Operator( TString &Name)
// Returns registered operator name
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( GsmOperator)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   int HeaderSize = strlen( OperHeader);
   if( !strnequ( RxBuffer, OperHeader, HeaderSize)){
      return( false);
   }
   char *p = &RxBuffer[ HeaderSize];
   p = strchr( p, ',');                // skip <mode> field
   if( !p){
      return( false);
   }
   p++;                                // skip ','
   p = strchr( p, ',');                // skip <format> field
   if( !p){
      return( false);
   }
   p++;                                // skip ','
   Name = AnsiString( p);
   return( true);
} // Operator

//******************************************************************************
// Sila signalu
//******************************************************************************

bool TGsmModem::SignalStrength( int &Strength)
// Returns relative signal strength 10..31 or 99 if unknown
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( Rssi)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   int HeaderSize = strlen( RssiHeader);
   if( !strnequ( RxBuffer, RssiHeader, HeaderSize)){
      return( false);
   }
   char *p = &RxBuffer[ HeaderSize];
   Strength = GetDecNumber( p);
   return( true);
} // SignalStrength

//******************************************************************************
// Send
//******************************************************************************

bool TGsmModem::SmsSend( TString To, char *Format, ...)
// Send SMS to <To> number (printf format)
{
static char Text[ MAX_TEXT];         // text of message

   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   // expand text :
   va_list Arg;
   va_start( Arg, Format);
   vsprintf( Text, Format, Arg);
   int MessageLength = strlen( Text);
   if( MessageLength > MAX_SMS){
      return( false);
   }
   AsciiToGsm( Text);                // convert into GSM aplhabet ! may contain 0 chars
   // prepare Tx buffer :
   int Index = 0;
   memset( TxBuffer, 0, sizeof( TxBuffer));
   // SMS Center number :
   TxBuffer[ Index++] = 0;           // use default
   // Status :
   TxBuffer[ Index++] = 0x11;        // use expiration, send PDU
   // Message reference number :
   TxBuffer[ Index++] = 0;           // assign by ME
   // Destination number :
   char *p = To.c_str();
   int  NumberLength = To.Length();
   if( NumberLength > MAX_NUMBER){
      IERROR;
   }
   TxBuffer[ Index++] = NumberLength;  // number length
   TxBuffer[ Index++] = 0x91;          // international format
   // convert <To> to lo-endian BCD number :
   byte *Number = &TxBuffer[ Index];
   byte Digit;
   for( int i = 0; i < NumberLength; i++){
      if( !isdigit( *p)){
         IERROR;
      }
      Digit = *p - '0';
      if( i & 1){
         // odd position
         Number[ i/2] |= Digit << 4;
      } else {
         // even position
         Number[ i/2] |= Digit;
      }
      p++;
   }
   if( NumberLength & 1){
      // event count
      Number[ NumberLength / 2] |= 0xF0;    // fill MSB with 0xF0
      NumberLength++;
   }
   Index += NumberLength / 2;
   // protocol flags :
   TxBuffer[ Index++] = 0;             // no hi-level protocol
   TxBuffer[ Index++] = 0;             // default 7-bit GSM aplhabet
   TxBuffer[ Index++] = 0xAA;          // expiration 4 days
   // message :
   TxBuffer[ Index++] = MessageLength; // message length
   // transform into 7-bit character stream :
   int  BitOffset = 0;                 // bit offset to output stream
   int  ByteIndex;                     // byte offset to output stream
   int  Shift;                         // bit shift
   word Twin;                          // character pair
   for( int i = 0; i < MessageLength; i++){
      ByteIndex  = BitOffset / 8;
      ByteIndex += Index;              // start of stream
      Shift      = BitOffset % 8;
      Twin       = Text[ i] & 0x7F;
      Twin     <<= Shift;
      TxBuffer[ ByteIndex]     |= (byte)Twin;        // LSB
      TxBuffer[ ByteIndex + 1] |= (byte)(Twin >> 8); // MSB
      BitOffset += 7;
   }
   Index += (MessageLength * 7 - 1) / 8 + 1;
   // SMS send :
   sprintf( Text, PduSend, Index - 1); // length without SMS Center address
   if( !TxString( Text)){
      return( false);
   }
   if( !RxString( sizeof( Prompt) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Prompt)){
      return( false);
   }
   // Convert binary into ASCII :
   ByteIndex = 0;
   byte Value;
   for( int i = 0; i < Index; i++){
      Value = hnibble( TxBuffer[ i]);             // MSB first
      Text[ ByteIndex++] = nibble2hex( Value);
      Value = lnibble( TxBuffer[ i]);             // LSB next
      Text[ ByteIndex++] = nibble2hex( Value);
   }
   Text[ ByteIndex++] = CTRL_Z;        // Ctrl-Z terminator
   Text[ ByteIndex++] = '\0';          // zero terminator
   // set SMS Center timeout :
   Sleep(1);                            // 15.3.2006: Na WinXP a modemu MC75 neslo odeslat SMS, je treba pockat
   if( !TxString( Text)){
      return( false);
   }
   // wait for reply header :
   FPort->SetRxWait( FSmsCenterTimeout, 0);
   if( !RxString( sizeof( SendOk) - 1)){
      FPort->SetRxWait( FCmdTimeout, 0);
      return( false);
   }
   FPort->SetRxWait( FCmdTimeout, 0);
   if( !strequ( RxBuffer, SendOk)){
      return( false);    // header error
   }
   // skip <message reference>,<pdu ack> :
   if( !RxString()){
      return( false);
   }
   return( true);
} // SmsSend

//******************************************************************************
// Receive
//******************************************************************************

bool TGsmModem::SmsReceive( int Address, TString &From, TString &Message)
// Read received message on position <Address>. Message is empty
// if <From> = ""
{
static char Text[ MAX_TEXT];

   if( !FPort || !FPort->IsOpen){
      DebugText("1");
      return( false);
   }
   if( Address >= FMemorySize){
      DebugText("2");
      IERROR;
   }
   Address++;                         // indexed from 1
   sprintf( Text, PduRead, Address);  // read from memory address <i>
   if( !TxString( Text)){
      DebugText("3");
      return( false);
   }
   FPort->SetRxWait( FSmsRxTimeout, 0);
   if( !RxString()){
      DebugText("4");
      FPort->SetRxWait( FCmdTimeout, 0);
      return( false);
   }
   FPort->SetRxWait( FCmdTimeout, 0);
   if( strnequ( RxBuffer, Error, strlen( Error))){
      DebugText("5");
      return( false);               // there is something wrong
   }
   if( strnequ( RxBuffer, EmptyRead, strlen( EmptyRead))){
      // empty message
      From = "";
      Message = "<*EMPTY*>";
      return( true);
   }
   if( !strnequ( RxBuffer, ReadHeader, strlen( ReadHeader))){
      DebugText("6");
      return( false);
   }
   int Index = strlen( ReadHeader);    // skip Reply header
   if( RxBuffer[ Index] != '0' && RxBuffer[ Index] != '1'){
      DebugText("7");
      return (false);                          // flag other than received message
   }
   Index++;
   Index++;                            // skip comma
   while( RxBuffer[ Index++] != ',');  // skip <alpha> field
   int PduLength = atoi( &RxBuffer[ Index]);
   while( RxBuffer[ Index++] != '\n'); // skip end of line
   // binary data
   byte Value;
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++; // SMSC length
   Index += Value * 2;                 // Skip SMS Center number
   int PduStart  = Index;              // PDU start here
   Index += 2;                         // Skip flag
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++;
   int NumberLength = Value;           // Sender number length
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++;
   if( Value != 0x91 && Value != 0x81 && Value != 0xA1){
      DebugText("8");
      return( false);                  // unknown number format
   }
   // swap number nibbles :
   for( int i = 0; i < NumberLength; i += 2){
      Text[ i]     = RxBuffer[ Index + 1];
      Text[ i + 1] = RxBuffer[ Index];
      Index += 2;
   }
   if( NumberLength & 0x01){
      // odd digits count
      Text[ NumberLength - 1] = RxBuffer[ Index++];
      Index++;                         // skip filler
   }
   Text[ NumberLength] = '\0';         // zero terminator
   From = TString( Text);              // return number
   Index += 2;                         // skip protocol
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++;
   if( Value != 0){     // !!! TADY VYPADAVA AUSTRALIE
      DebugText("9");
      return( false);                  // unknown alphabet
   }
   Index += 12;                        // skip message time
   Index += 2;                         // skip timezone
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++;
   int MessageLength = Value;
   // transform from 7-bit character stream :
   int  BitOffset = 0;                 // bit offset to output stream
   int  ByteIndex;                     // byte offset to output stream
   int  Shift;                         // bit shift
   word Twin;                          // character pair
   for( int i = 0; i < MessageLength; i++){
      ByteIndex  = BitOffset / 8;
      Shift      = BitOffset % 8;
      Twin       =  GetHexNumber( &RxBuffer[ Index + ByteIndex * 2]) |
                   (GetHexNumber( &RxBuffer[ Index + (ByteIndex + 1) * 2]) << 8);
      Twin     >>= Shift;
      Text[ i]   = (char)(Twin & 0x7F);
      BitOffset += 7;
   }
   Index += ((MessageLength * 7 - 1) / 8 + 1) * 2;
   GsmToAscii( Text, MessageLength);
   Message = TString( Text);
   if( PduLength != (Index - PduStart) / 2){
      DebugText("10");
      return( false);                  // message length mismatch
   }
   return( true);
} // SmsReceive

//******************************************************************************
// Delete
//******************************************************************************

bool TGsmModem::SmsDelete( int Address)
// Delete received message on <Address>
{
static char Text[ MAX_TEXT];

   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( Address >= FMemorySize){
      IERROR;
   }
   Address++;                         // indexed from 1
   sprintf( Text, Delete, Address);
   if( !TxString( Text)){
      return( false);
   }
   FPort->SetRxWait( FSmsDeleteTimeout, 0);     // Modry modem MC39i funguje spravne az pri 2000ms, pri 1000ms jeste nekdy zablbne
   if( !RxString()){
      FPort->SetRxWait( FCmdTimeout, 0);
      return( false);
   }
   FPort->SetRxWait( FCmdTimeout, 0);
   if( !strequ( RxBuffer, Ok)){
      return( false);                  // there is something wrong
   }
   return( true);
} // SmsDelete

//******************************************************************************
// SMS count
//******************************************************************************

int TGsmModem::SmsCount()
// Returns count of received messages or -1 if error
{
   if( !TxString( MemoryCount)){
      return( -1);
   }
   if( !RxString()){
      return( -1);
   }
   int HeaderSize = strlen( MemoryHeader);
   if( !strnequ( RxBuffer, MemoryHeader, HeaderSize)){
      return( -1);
   }
   char *p = &RxBuffer[ HeaderSize];
   p = strchr( p, ',');                // skip <memory type> field
   if( !p){
      return( -1);
   }
   p++;                                // skip ','
   // SMS count :
   int Count = GetDecNumber( p);
   return( Count);
} // SmsCount

//------------------------------------------------------------------------------

//******************************************************************************
// Send string
//******************************************************************************

bool TGsmModem::TxString( TString Data)
// Send <Data> string
{
   FPort->Flush();                            // flush previous data
   char *TxData  = Data.c_str();
   int  TxLength = strlen( TxData);
   if( FPort->Write( TxData, TxLength) != TxLength){
      return( false);
   }
   RwdTx( TxData, TxLength);
   return( true);
} // TxString

//******************************************************************************
// Receive string
//******************************************************************************

bool TGsmModem::RxString( int Size)
// Receive <Data> string into <RxBuffer>
{
   if( !Size){
      Size = sizeof( RxBuffer);  // read maximum - up to timeout
   }
   int Length = FPort->Read( RxBuffer, Size);
   if( Length == 0){
      return( false);            // no response - timeout
   }
   RxBuffer[ Length] = 0;        // terminate string
   RwdRx( RxBuffer, Length);
   return( true);
} // RxString

//******************************************************************************
// To GSM
//******************************************************************************

void TGsmModem::AsciiToGsm( char *Text)
// Convert to GSM alphabet
{
   int Length = strlen( Text);
   for( int i = 0; i < Length; i++){
      if( Text[ i] == '@'){
         Text[ i] = '\0';      // @ == GSM 0
      }
   }
} // AsciiToGsm

//******************************************************************************
// To ASCII
//******************************************************************************

void TGsmModem::GsmToAscii( char *Text, int Length)
// Convert GSM alphabet to text
{
   for( int i = 0; i < Length; i++){
      if( Text[ i] == '\0'){
         Text[ i] = '@';      // GSM 0 == @
      }
   }
   Text[ Length] = '\0';      // zero terminator
} // GsmToAscii

//******************************************************************************
// Hex conversion
//******************************************************************************

byte TGsmModem::GetHexNumber( char *Text)
// Converts two characters into number
{

   byte Value = 0;
   char ch    = Text[ 0];
   if( isdigit( ch)){
      Value = (ch - '0') << 4;
   } else {
      Value = (ch - 'A' + 10) << 4;
   }
   ch = Text[ 1];
   if( isdigit( ch)){
      Value |= (ch - '0');
   } else {
      Value |= (ch - 'A' + 10);
   }
   return( Value);
} // GetHexNumber

//******************************************************************************
// Decadic conversion
//******************************************************************************

byte TGsmModem::GetDecNumber( char *Text)
// Converts numeric characters into number
{
byte Value;
char ch;

   Value = 0;
   while( 1){
      ch = *Text++;
      if( !ch){
         return( Value);        // end of string
      }
      if( ch < '0' || ch > '9'){
         return( Value);
      }
      Value *= 10;
      Value += char2dec( ch);
   }
} // GetDecNumber

