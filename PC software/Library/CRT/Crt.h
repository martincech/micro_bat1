//******************************************************************************
//
//   Crt.h        CRT emulator
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef CrtH
   #define CrtH

//******************************************************************************
// Class TCrt
//******************************************************************************

class TCrt {
public:
  TCrt( TMemo *Memo);
  // Constructor
  void Clear();
  // Clear window
  int puts( char *String);
  // Write string
  int printf( char *format, ...);
  // Write string by format
  int vprintf( char *format, va_list);
  // Write string by format & list
  void Resize( TCanvas *Canvas);
  // Memo resize callback

  __property int Rows = {read=FRows, write=FRows};
  __property int Cols = {read=FCols, write=FCols};

//------------------------------------------------------------------------------
protected :
   TMemo *Memo;
   int   FRows;
   int   FCols;
   int   LastLine;
   bool  LastLinefeed;
}; // TCrt

#endif
