//******************************************************************************
//
//   ObjectMemory.h     Persistent memory for objects
//   Version 1.0        (c) VymOs
//
//******************************************************************************

#ifndef ObjectMemoryH
   #define ObjectMemoryH

#ifndef SysDefH
   #include "../Unisys/SysDef.h"
#endif

#include <IniFiles.hpp>

//******************************************************************************
// TObjectMemory
//******************************************************************************

class TObjectMemory {
public :

   //---------------- Functions

   TObjectMemory();
   // Constructor
   ~TObjectMemory();
   // Destructor
   void Save();
   // Save data
   bool Locate( TName Name);
   // Locate object by <Name>
   void Create( TName Name);
   // Create object by <Name>

   void      SaveInteger( TName Name, int Value);
   int       LoadInteger( TName Name);

   void      SaveHexadecimal( TName Name, dword Value);
   dword     LoadHexadecimal( TName Name);

   void      SaveBool( TName Name, bool Value);
   bool      LoadBool( TName Name);

   void      SaveFloat( TName Name, double Value);
   double    LoadFloat( TName Name);

   void      SaveString( TName Name, TString Value);
   TString   LoadString( TName Name);

   void      SaveDateTime( TName Name, TDateTime Value);
   TDateTime LoadDateTime( TName Name);

   __property bool IsEmpty  = {read=FEmpty};
//------------------------------------------------------------------------------
protected :
   TIniFile   *File;           // persistent storage
   AnsiString  CurrentObject;  // active object name
   bool        FEmpty;         // empty storage
}; // TObjectMemory

#endif
