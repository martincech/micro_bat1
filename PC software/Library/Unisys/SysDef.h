//******************************************************************************
//
//   SysDef.h      System Definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef SysDefH
   #define SysDefH

#ifndef UniH
   #include "Uni.h"
#endif

typedef int        TIdentifier;
typedef AnsiString TName;
typedef AnsiString TString;    

#define INVALID_IDENTIFIER (-1)
#define EMPTY_NAME         AnsiString("")
#define UNKNOWN_NAME       AnsiString("?")

#endif