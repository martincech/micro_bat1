//******************************************************************************
//
//   UcUtil.c     uC Date / Time computing
//   Version 0.0  (c) Vymos
//
//******************************************************************************

#include "UcUtil.h"

// globalni data :

// dny v mesici :
byte _UcMDay[12] = {
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

// dny v roce :
word _UcYDay[13] = {
  0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365
};

// interni konstanty :

#define DOW_1_1_2000     UC_SATURDAY   // den v tydnu pro 1.1.2000
#define FOUR_YEAR_DAYS   1461          // pocet dnu ctyrroku
// Nastaveni prechodu :
#define DST_START_MONTH  UC_MARCH
#define DST_START_HOUR   2
#define DST_END_MONTH    UC_OCTOBER
#define DST_END_HOUR     2

// Lokani funkce :

static byte GetTransitionDay( byte Year, byte Month);
// vrati datum (den v mesici), na ktery pripada prechod

//******************************************************************************
// Na Interni
//******************************************************************************

TUcDate UcDate( byte Year, byte Month, byte Day)
// Prepocita datum na interni reprezentaci
{
byte Leaps;                    // pocet prechodnych dnu

   Leaps = (Year + 4) / 4;     // 2000 (0) je 1.prechodny
   if( UcIsLeapYear( Year) && Month < UC_MARCH){
      Leaps--;                 // tento je prechodny, ale je nizke datum
   }
   return((TUcDate)Year * 365 +  Leaps + UcYearDay( Month) + Day - 1); // den v mesici od 1
} // UcDate

//******************************************************************************
// Na Datum
//******************************************************************************

void UcSplit( TUcDate Date, byte *Year, byte *Month, byte *Day)
// Prevede interni reprezentaci na datum
{
byte Tmp;
byte Y, M, D;
word YDay;
word YDays; // pocet dnu v roce

   Tmp  = Date / FOUR_YEAR_DAYS;     // pocet ctyrroku
   YDay = Date % FOUR_YEAR_DAYS;     // ofset od zacatku ctyrroku
   Y    = Tmp * 4;                   // nejblize nizsi start ctyrroku
   // uprav rok :
   while( 1){
      YDays = UcIsLeapYear( Y) ? 366 : 365;
      if( YDay < YDays){
         break;                      // ofset je uvnitr roku
      }
      Y++;                           // nasledujici rok
      YDay -= YDays;
   }
   *Year = Y;
   // YDay je den v roce, prevest na MM:DD
   YDay++;                     // datum zacina od 1
   if( UcIsLeapYear( Y)){
      if( YDay > (31 + 29)){
         YDay--;               // zkrat o prestupny den
      } else if( YDay == (31 + 29)){
         *Day   = 29;          // prave prestupny den
         *Month = UC_FEBRUARY;
         return;
      }
   }
   M = UC_JANUARY;
   while( 1){
      D = UcMonthDay( M);      // dnu v mesici
      if( D >= YDay){
         break;
      }
      YDay -= D;               // odecti dny mesice
      M++;                     // dalsi mesic
   }
   *Month = M;
   *Day   = (byte)YDay;
} // UcSplit

//******************************************************************************
// Den v tydnu
//******************************************************************************

byte UcDow( TUcDate Date)
// Vrati den v tydnu
{
   return( (Date + DOW_1_1_2000) % 7);
} // UcDow

//******************************************************************************
// Letni cas
//******************************************************************************

bool UcIsDst( byte Year, byte Month, byte Day, byte Hour)
// Vrati YES pro letni cas. Vstupni hodnoty jsou zimni cas
{
byte StartDay;

   if( Month < DST_START_MONTH || Month > DST_END_MONTH){
      return( NO);       // mimo letni cas
   }
   if( Month > DST_START_MONTH && Month < DST_END_MONTH){
      return( YES);      // zcela jiste letni cas
   }
   if( Month == DST_START_MONTH){
      // zahajeni letniho casu
      StartDay = GetTransitionDay( Year, DST_START_MONTH);
      if( Day > StartDay){
         return( YES);   // den po zahajeni
      }
      if( Day < StartDay){
         return( NO);    // den pred zahajenim
      }
      // v den prechodu :
      if( Hour >= DST_START_HOUR){
         return( YES);   // hodina po prechodu
      } else {
         return( NO);    // hodina pred prechodem
      }
   } else { // Month == DST_END_MONTH
      // konec letniho casu
      StartDay = GetTransitionDay( Year, DST_END_MONTH);
      if( Day > StartDay){
         return( NO);    // den po ukonceni
      }
      if( Day < StartDay){
         return( YES);   // den pred ukoncenim
      }
      // v den prechodu :
      if( Hour >= DST_END_HOUR){
         return( NO);    // hodina po prechodu
      } else {
         return( YES);   // hodina pred prechodem
      }
   }
} // UcIsDst

//******************************************************************************
// Prechodnovy den
//******************************************************************************

static byte GetTransitionDay( byte Year, byte Month)
// vrati datum (den v mesici), na ktery pripada prechod
{
// POZOR, tvrde predpoklada posledni nedeli v mesici
byte    Day;
TUcDate Date;
byte    Dow;

   Day   = UcMonthDay( Month);          // posledni den v mesici
   Date  = UcDate( Year, Month, Day);   // vnitrni reprezentace
   Dow   = UcDow( Date);                // den v tydnu
   if( Dow == UC_SUNDAY){
      return( Day);                     // posledni den je nedele
   }
   Day  -= Dow + 1;                     // pondeli (0) je o jeden den zpet,...
   return( Day);
} // GetTransitionDay

