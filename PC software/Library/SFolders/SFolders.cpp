//******************************************************************************
//
//   SFolders.cpp       Special folders in Windows
//   Version 1.4
//
//******************************************************************************

/*

Verze 1.1:
  - 26.9.2006: Predelano na dynamicke otevirani shfolders.dll, aby to slo pouzivat i na Win95/98, kde tato DLL neni.

Verze 1.2:
  - 7.3.2007: Pokud uz soubor v prac. adresari existuje, zkontroluje se, zda obsahuje vsechny sloupce. Pokud ne, soubor se
              zkopiruje znovu

Verze 1.3:
  - 24.6.2007: Kontrola sloupcu v souborech DB nefungovala pod Windows Vista. Kontrola tabulky (tj. prace s BDE) se provadela
               driv, nez se nastavil soubor Pdoxusrs.net, tj. kontrola sloupcu skoncila vzdy negativne. Nyni nastavim nejprve
               Paradox a az pak kontroluju pracovni soubory. Zmena ve fci PrepareData().
  - 24.6.2007: Zmena pracovniho adresare z CSIDL_COMMON_APPDATA na CSIDL_LOCAL_APPDATA, soubor vytvoreny v jednom uctu
               ve Vista stejne nemuze byt modifikovany jinym uzivatelem, takze by to asi nefungovalo vubec.
               Takto ma kazdy user svoje nastaveni. Zmena v GetFolders().

Verze 1.4:
  - 13.12.2007: Pracovni adresar lze zvolit rucne               


*/

#define ShlObjHPP       1       // Eliminace duplicitnich definic ve VCL
#include <shlobj.h>

#include <vcl.h>
#pragma hdrstop

#include "SFolders.h"
#include <Filectrl.hpp>         // DirectoryExists

#pragma package(smart_init)

//---------------------------------------------------------------------------
// Constructor
//---------------------------------------------------------------------------

TSpecialFolders::TSpecialFolders(String ApplicationName) {
  // Contructor
  Init(ApplicationName);
  ManualWorkingDir = "";
}

TSpecialFolders::TSpecialFolders(String ApplicationName, String ManualWorkingDirectory) {
  // Contructor s manualni volbou pracovniho adresare <ManualWorkingDirectory>
  Init(ApplicationName);
  ManualWorkingDir = ManualWorkingDirectory;
}

void TSpecialFolders::Init(String ApplicationName) {
  FApplicationName = ApplicationName;
  FDocuments       = "";
  FWorking         = "";
  FRunning         = "";
}

//---------------------------------------------------------------------------
// Nacteni cesty
//---------------------------------------------------------------------------

// Nazev funkce i jeji parametry se lisi v zavislosti na pouziti Unicode (viz definice v shlobj.h)
#ifdef UNICODE
  typedef HRESULT (WINAPI * TFunctionPtr)(HWND, int, HANDLE, DWORD, LPWSTR);
  #define FunctionName  "SHGetFolderPathW"
#else
  typedef HRESULT (WINAPI * TFunctionPtr)(HWND, int, HANDLE, DWORD, LPSTR);
  #define FunctionName  "SHGetFolderPathA"
#endif // !UNICODE

String TSpecialFolders::GetFolder(int FolderNumber) {
  // Nacte cestu k adresari <FolderNumber>. V pripade chyby vrati prazdny string.
  char Folder[_MAX_PATH];

  HMODULE Dll = LoadLibrary("shfolder.dll");
  if (!Dll) {
    return "";                  // Nelze najit DLL (Win95 nebo Win98orig). DLL neni treba uvolnovat, zatim se nenahrala.
  }
  try {
    TFunctionPtr Function = (TFunctionPtr)GetProcAddress(Dll, FunctionName);
    if (!Function) {
      throw 1;                  // Nelze najit funkci v DLL
    }
    Function(NULL, FolderNumber, NULL, SHGFP_TYPE_CURRENT, Folder);     // Pokud skonci chybou, vrati rovnou prazdny string
  } catch(...) {
    Folder[0] = 0;              // Doslo k nejake chybe, vratim prazdny string
    // Jdu dal, musim uvolnit DLL
  }
  FreeLibrary(Dll);             // Uvolnim DLL
  return String(Folder);
}

//---------------------------------------------------------------------------
// Nacteni cest do promennych
//---------------------------------------------------------------------------

void TSpecialFolders::GetFolders() {
  // Nacte adresare do promennych
  String Str;
  char Folder[_MAX_PATH];

  // Umisteni programu
  FRunning = ExtractFilePath(Application->ExeName);

  // Dokumenty
  if ((Str = GetFolder(CSIDL_PERSONAL)) != "") {
    // Neco nacetl
    FDocuments = Str + "\\";
  } else {
    // Doslo k chybe, adresar se nenacetl
    FDocuments = FRunning;      // Melo by fungovat i ve Win95, v pripade chyby nabidnu adresar programu
  }

  // Pracovni adresar
  if (ManualWorkingDir == "") {
    // Pracovni adresar zvolim automaticky
    if ((Str = GetFolder(CSIDL_LOCAL_APPDATA)) != "") {
      // Neco nacetl
      FWorking = Str + "\\" + FApplicationName + "\\";
    } else {
      // Doslo k chybe, adresar se nenacetl
      FWorking = FRunning + "Data\\";     // Na Win95 nefunguje => dam podadresar Data v adresari programu
    }
  } else {
    // Zvolil pracovni adresar rucne
    FWorking = ManualWorkingDir;
  }

  // Docasne soubory
  if (GetTempPath(_MAX_PATH - 1, Folder)) {
    // Neco nacetl
    FTemporary = String (Folder);
  } else {
    // Doslo k chybe, adresar se nenacetl
    FTemporary = FWorking + "Temp\\";   // Melo by fungovat vsude, ale co kdyby - kazdopadne tam musi moci zapisovat, nelze Program Files
  }
}

//---------------------------------------------------------------------------
// Porovnani dvou souboru
//---------------------------------------------------------------------------

static bool CheckColumns(TTable *SourceTable, TTable *DestinationTable) {
  // Porovna sloupce v <SourceTable> a <DestinationTable> a vrati true, pokud jsou stejne
  if (SourceTable->FieldCount != DestinationTable->FieldCount) {
    return false;               // Pocet sloupcu neodpovida
  }
  // Porovnam jednotlive sloupce
  for (int i = 0; i < SourceTable->FieldCount; i++) {
    if (SourceTable->Fields->Fields[i]->FieldName != DestinationTable->Fields->Fields[i]->FieldName) {
      return false;             // Nazev sloupce neodpovida
    }
    if (SourceTable->Fields->Fields[i]->DataType != DestinationTable->Fields->Fields[i]->DataType) {
      return false;             // Typ sloupce neodpovida
    }
    if (SourceTable->Fields->Fields[i]->DataSize != DestinationTable->Fields->Fields[i]->DataSize) {
      return false;             // Velikost sloupce neodpovida (dulezite u stringu)
    }
  }
  return true;
}

bool TSpecialFolders::SameColumns(AnsiString File) {
  // Pokud jde o soubor s priponou DB, porovna vsechny sloupce v tabulce <File> a odpovidajici tabulce v pracovnim adresari.
  // Vrati true, pokud jsou sloupce stejne.
  TTable *SourceTable, *DestinationTable;
  bool Result = false;

  if (ExtractFileExt(File).LowerCase() != ".db") {
    return true;                // Nejde o tabulku Paradox, tvarim se, jako by byly soubory shodne
  }

  SourceTable      = new TTable(NULL);
  DestinationTable = new TTable(NULL);

  try {
    // Otevru tabulky
    SourceTable->TableName = File;                      // Tabulka v Program Files (zdrojova)
    SourceTable->Open();
    DestinationTable->TableName = FWorking + File;      // Tabulka v pracovnim adresari (cilova)
    DestinationTable->Open();
    // Porovnam sloupce
    Result = CheckColumns(SourceTable, DestinationTable);
  } catch(...) {}

  SourceTable->Close();
  DestinationTable->Close();
  delete SourceTable;
  delete DestinationTable;

  return Result;
}

//---------------------------------------------------------------------------
// Zkopirovani souboru do pracovniho adresare
//---------------------------------------------------------------------------

void TSpecialFolders::CopyWorkingFile(String File) {
  // Zkopiruje soubor <File> do pracovniho adresare a nastavi jeho atributy tak, aby se dal soubor upravovat
  // Pokud jde o soubor s priponou DB,
  String Destination = FWorking + File;
  if (!FileExists(Destination) || !SameColumns(File)) {
    // Soubor neexistuje nebo ma jinou definici sloupcu nez by mel mit, prekopiruju ho
    if (!CopyFile(File.c_str(), Destination.c_str(), false)) {
      throw Exception("Cannot copy file: " + Destination);              // Melo by jit zkopirovat, ukoncim aplikaci
    }
  }
  // Nastavim atributy, aby sel soubor upravovat
  if (FileSetAttr(Destination, 0) != 0) {
    throw Exception("Cannot set file attributes: " + Destination);      // Melo by jit nastavit, ukoncim aplikaci
  }
}

//---------------------------------------------------------------------------
// Priprava datovych souboru
//---------------------------------------------------------------------------

void TSpecialFolders::PrepareData(TStringList *Files, TSession *Session, TDatabase *Database) {
  // Vytvori pracovni adresar, zkopiruje do nej datove soubory v seznamu <Files> a nastavi databazi <Database> na tento adresar
  // Nactu umisteni specialnich adresaru ve Windows
  GetFolders();
  // Zkontroluju, zda existuje pracovni adresar
  if (!DirectoryExists(FWorking)) {
    if (!CreateDir(FWorking)) {
      // Toto by se nemelo stat, bez toho nelze pokracovat
      throw Exception("Cannot create working directory: " + FWorking);
    }
  }
  // Presmeruju do pracovniho adresare Pdoxusrs.net (default je na C: a read-only pro obycejne uzivatele)
  Session->NetFileDir = FWorking;
  // Presmeruju do docasneho adresare soubory .lck, mezivysledky SQL Paradoxu, temp soubory QuickReportu atd.
  Session->PrivateDir = FTemporary;
  // Nastavim cestu k databazim - vsechny pouzivane tabulky musi byt pripojene na tuto databazi
  Database->Connected = false;
  Database->Params->Clear();
  Database->Params->Add("PATH=" + FWorking);
  Database->Params->Add("DEFAULT DRIVER=PARADOX");
  Database->Params->Add("ENABLE BCD=FALSE");
  Database->Connected = true;
  // Nakopiruju do pracovniho adresare potrebne soubory (ktere se nevytvareji, ale je treba je kopirovat)
  // 24.6.2007: Musim az nakonec, pote, co nakonfiguruji Paradox. Behem kopirovani se kontroluje obsah
  // tabulek, tj. pracuje se s tabulkami a bez korektne nastaveneho Paradoxu by to nefungovalo.
  for (int i = 0; i < Files->Count; i++) {
    CopyWorkingFile(Files->Strings[i]);
  }
}

