//******************************************************************************
//
//   Localize.cpp       Functions for localization with resource DLL
//   Version 1.0
//
//******************************************************************************

/*

POUZITI:

1) Musi existovat resource DLL pro vsechny jazyky, kazda DLL musi obsahovat
   alespon LOCALIZE_ID_LANG_NAME a LOCALIZE_ID_LANG_CODE z Id.h, ktere definuji
   nazev a kod jazyka
2) Jmena souboru DLL jsou ve formatu ApplicationName + kod.dll, napr. Bat2CZE.dll
3) BuildLanguageList() nacte seznam jazyku
4) LoadLanguage() nahraje do pameti jazyk
5) Pomoci LoadString() je mozny pristup ke stringum

*/

/*


*/

#include "Localize.h"
#include <SysUtils.hpp>             // TSearchRec

#define MAX_STRING_LENGTH   255     // Maximalni delka stringu v RC

//---------------------------------------------------------------------------
// Constructor
//---------------------------------------------------------------------------

TLocalize::TLocalize(AnsiString AppName) {
  // Contructor
  ApplicationName = AppName;
  hDll = NULL;
}

//---------------------------------------------------------------------------
// Desktruktor
//---------------------------------------------------------------------------

TLocalize::~TLocalize() {
  // Destructor

  // Smazu vsechny jazyky v seznamu
  for (TLanguageListIterator Iterator = LanguageList.begin(); Iterator != LanguageList.end(); Iterator++) {
    delete (*Iterator);
  }

  // Smazu samotny seznam
  LanguageList.clear();

  // Uvolnim nactenou knihovnu
  if (hDll) {
    FreeLibrary(hDll);
  }
}

//---------------------------------------------------------------------------
// Detekce jazyku
//---------------------------------------------------------------------------

void TLocalize::BuildLanguageList() {
  // Prohleda aktualni adresar a sestavi seznam jazyku, ktere jsou k dispozici
  TSearchRec SearchRec;
  int Result;
  HMODULE h;
  TCHAR s[MAX_STRING_LENGTH];
  AnsiString Name, DllName;
  int Code;
  TLocalizeLanguage *Language;

  // Projedu vsechny soubory s predponou FApplicationName a priponou DLL
  for (Result = FindFirst(ApplicationName + "*.dll", 0xFFFFFFFF, SearchRec); Result == 0; Result = FindNext(SearchRec)) {

	// Nahraju DLL
	if ((h = LoadLibrary(SearchRec.Name.c_str())) == NULL) {
	  continue;                         // DLL se nepodarilo otevrit
	}

	try {

      // Jmeno knihovny
      DllName = SearchRec.Name;

      // Najdu nazev jazyka
	  if (::LoadString(h, LOCALIZE_ID_LANG_NAME, s, MAX_STRING_LENGTH - 1) == 0) {
		throw 1;                        // Nazev jazyka se nepodarilo nalezt
	  }
      Name = s;                         // Ulozim

      // Najdu kod jazyka
	  if (::LoadString(h, LOCALIZE_ID_LANG_CODE, s, MAX_STRING_LENGTH - 1) == 0) {
		throw 1;                        // Kod jazyka se nepodarilo nalezt
	  }
      Code = AnsiString(s).ToInt();     // Ulozim, spatny format cisla hodi vyjimku

      // Ulozim jazyk do seznamu jazyku
      Language = new TLocalizeLanguage;
      Language->Code    = Code;
      Language->Name    = Name;
      Language->DllName = DllName;
      LanguageList.push_back(Language); // Pridam ukazatel do kontejneru

	} catch (...) {}

	// Uvolnim DLL
	FreeLibrary(h);
  }//for
  SortLanguages();          // Setridim
  FindClose(SearchRec);
}

//---------------------------------------------------------------------------
// Trideni seznamu jazyku podle nazvu
//---------------------------------------------------------------------------

void TLocalize::SortLanguages() {
  // Setridi seznam jazyku podle nazvu
  TLanguageListIterator Iterator1, Iterator2;
  TLocalizeLanguage *Language;

  for (Iterator1 = LanguageList.begin(); Iterator1 < LanguageList.end() - 1; Iterator1++) {
    for (Iterator2 = Iterator1 + 1; Iterator2 < LanguageList.end(); Iterator2++) {
      if ((*Iterator1)->Name > (*Iterator2)->Name) {
        // Prohodim prvky
        Language = (*Iterator1);
        (*Iterator1) = (*Iterator2);
        (*Iterator2) = Language;
      }
    }
  }
}

//---------------------------------------------------------------------------
// Vraceni poctu jazyku
//---------------------------------------------------------------------------

int TLocalize::GetLanguageCount() {
  // Vrati pocet jazku v seznamu
  return LanguageList.size();
}

//---------------------------------------------------------------------------
// Vraceni nazvu jazyka
//---------------------------------------------------------------------------

AnsiString TLocalize::GetLanguageName(int Index) {
  // Vrati nazev jazyka na pozici <Index>
  if (!CheckLanguageIndex(Index)) {
    return NULL;
  }
  return (*GetIterator(Index))->Name;
}

//---------------------------------------------------------------------------
// Vraceni jmena DLL
//---------------------------------------------------------------------------

AnsiString TLocalize::GetLanguageDllName(int Index) {
  // Vrati jmeno DLL jazyka na pozici <Index>
  if (!CheckLanguageIndex(Index)) {
    return NULL;
  }
  return (*GetIterator(Index))->DllName;
}

//---------------------------------------------------------------------------
// Vraceni kodu jazyka
//---------------------------------------------------------------------------

int TLocalize::GetLanguageCode(int Index) {
  // Vrati kod jazyka na pozici <Index>
  if (!CheckLanguageIndex(Index)) {
    return NULL;
  }
  return (*GetIterator(Index))->Code;
}

//---------------------------------------------------------------------------
// Kontrola indexu
//---------------------------------------------------------------------------

bool TLocalize::CheckLanguageIndex(int Index) {
  // Pokud je index <Index> platny v ramci seznamu jazyku, vrati true
  return (bool)(Index >= 0 && (unsigned)Index < LanguageList.size());
}

//---------------------------------------------------------------------------
// Najeti na urcity jazyk v seznamu
//---------------------------------------------------------------------------

vector<TLocalizeLanguage *>::iterator TLocalize::GetIterator(int Index) {
  // Vrati iterator na jazyk s indexem <Index>. Neosetruje prazdny seznam.
  TLanguageListIterator Iterator;

  Iterator = LanguageList.begin();
  while (Index) {
    Iterator++;
    Index--;
  }
  return Iterator;
}

//---------------------------------------------------------------------------
// Nahrani jazyka
//---------------------------------------------------------------------------

bool TLocalize::LoadLanguage(int Code) {
  // Nahraje do pameti DLL jazyka s kodem <Code>
  for (int i = 0; i < GetLanguageCount(); i++) {
    if (GetLanguageCode(i) == Code) {
      // Nasel jsem jazyk
      hDll = LoadLibrary(GetLanguageDllName(i).c_str());
      if (hDll == NULL) {
        return false;       // Nepodarilo se nahrat
      } else {
        return true;        // Vse v poradku
      }
    }//if
  }//for

  // Nenasel jsem jazyk
  hDll = NULL;
  return false;
}

//---------------------------------------------------------------------------
// Vraceni stringu
//---------------------------------------------------------------------------

AnsiString TLocalize::LoadString(int Id) {
  // Vrati string s id <Id>
  TCHAR s[MAX_STRING_LENGTH];
  if (::LoadString(hDll, Id, s, MAX_STRING_LENGTH - 1) == 0) {
    return "";              // String se zadanym Id neexistuje
  }
  return AnsiString(s);
}

