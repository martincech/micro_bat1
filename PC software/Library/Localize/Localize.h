//******************************************************************************
//
//   Localize.cpp       Functions for localization with resource DLL
//   Version 1.0
//
//******************************************************************************

#ifndef LocalizeH
   #define LocalizeH

#include "Id.h"	    			// Konstanty
#include <System.hpp>			// AnsiString
#include <vector.h>

struct TLocalizeLanguage {
  int        Code;                      // Kod jazyka
  AnsiString Name;                      // Nazev jazyka
  AnsiString DllName;                   // Jmeno DLL
};

class TLocalize {

public:

  TLocalize(AnsiString AppName);
  // Constructor

  ~TLocalize();
  // Destructor

  void BuildLanguageList();
  // Prohleda aktualni adresar a sestavi seznam jazyku, ktere jsou k dispozici

  int GetLanguageCount();
  // Vrati pocet jazku v seznamu

  AnsiString GetLanguageName(int Index);
  // Vrati nazev jazyka na pozici <Index>

  AnsiString GetLanguageDllName(int Index);
  // Vrati jmeno DLL jazyka na pozici <Index>

  int GetLanguageCode(int Index);
  // Vrati kod jazyka na pozici <Index>

  bool LoadLanguage(int Code);
  // Nahraje do pameti DLL jazyka s kodem <Code>

  AnsiString LoadString(int Id);
  // Vrati string s id <Id>

private:

  AnsiString ApplicationName;  	                // Jmeno aplikace

  vector<TLocalizeLanguage *> LanguageList;     // Seznam podporovanych jazku
  typedef vector<TLocalizeLanguage *>::iterator TLanguageListIterator;

  HMODULE hDll;                                 // Handle na nahranou DLL knihovnu

  bool CheckLanguageIndex(int Index);
  // Pokud je index <Index> platny v ramci seznamu jazyku, vrati true

  vector<TLocalizeLanguage *>::iterator TLocalize::GetIterator(int Index);
  // Vrati iterator na jazyk s indexem <Index>. Neosetruje prazdny seznam.

  void SortLanguages();
  // Setridi seznam jazyku podle nazvu

}; // TLocalize

#endif // LocalizeH
