object UartSetupDialog: TUartSetupDialog
  Left = 543
  Top = 247
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Setup Parameters'
  ClientHeight = 286
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 16
    Top = 16
    Width = 337
    Height = 217
  end
  object Label1: TLabel
    Left = 40
    Top = 56
    Width = 52
    Height = 13
    Caption = 'Baud rate :'
  end
  object Label2: TLabel
    Left = 40
    Top = 88
    Width = 48
    Height = 13
    Caption = 'Data bits :'
  end
  object Label3: TLabel
    Left = 40
    Top = 120
    Width = 32
    Height = 13
    Caption = 'Parity :'
  end
  object Label4: TLabel
    Left = 40
    Top = 152
    Width = 47
    Height = 13
    Caption = 'Stop bits :'
  end
  object Label5: TLabel
    Left = 40
    Top = 184
    Width = 64
    Height = 13
    Caption = 'Flow Control :'
  end
  object UartSetupOK: TBitBtn
    Left = 16
    Top = 248
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 6
    OnClick = UartSetupOKClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object UartSetupCancel: TBitBtn
    Left = 280
    Top = 248
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    Default = True
    ModalResult = 2
    TabOrder = 5
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object UartSetupBaudRate: TComboBox
    Left = 120
    Top = 48
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      '300'
      '600'
      '1200'
      '2400'
      '4800'
      '9600'
      '19200'
      '38400'
      '57600'
      '115200')
  end
  object UartSetupDataBits: TComboBox
    Left = 120
    Top = 80
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items.Strings = (
      '5'
      '6'
      '7'
      '8')
  end
  object UartSetupParity: TComboBox
    Left = 120
    Top = 112
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    Items.Strings = (
      'none'
      'odd'
      'even'
      'mark'
      'space')
  end
  object UartSetupStopBits: TComboBox
    Left = 120
    Top = 144
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    Items.Strings = (
      '1'
      '1.5'
      '2')
  end
  object UartSetupHandshake: TComboBox
    Left = 120
    Top = 176
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    Items.Strings = (
      'none'
      'hardware'
      'Xon/Xoff'
      'Half Duplex (by RTS)')
  end
end
