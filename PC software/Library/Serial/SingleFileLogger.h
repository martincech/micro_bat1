//******************************************************************************
//
//   SingleSingleFileLogger.h  Raw data file logger
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef SingleFileLoggerH
   #define SingleFileLoggerH

#ifndef RawDataH
   #include "Logger.h"
#endif

#include <stdio.h>

//******************************************************************************
// Class TSingleFileLogger
//******************************************************************************

class TSingleFileLogger : public TLogger
{
public:
   TSingleFileLogger(String Name, String Number);
   // Constructor
   ~TSingleFileLogger();
   // Destruktor
   virtual void Write( TDataType Type, void *Buffer, int Length);
   // Write data
   virtual void Report( char *Format, ...);
   // Write report

//------------------------------------------------------------------------------
protected :
   FILE         *File;          // file handle
   LARGE_INTEGER Frequency;     // counter frequency
   LARGE_INTEGER Counter;       // last operation time

   void BinaryWrite( char *Buffer, int Length);
   // Write data
   void NewFileName(String Name, String Number);
   // Check and create new file name
   void Timestamp();
   // Write timestamp
}; // TSingleFileLogger

#endif

