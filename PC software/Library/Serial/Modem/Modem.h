//******************************************************************************
//
//   Modem.h       Asynchronous communication via modem connection (TAPI)
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef ModemH
   #define ModemH

#ifndef UartH
   #include "../Uart.h"
#endif

//******************************************************************************
// TModem
//******************************************************************************

class TModem : public TUart {
public :
   //--- class functions :
   static bool Locate( TName Name, TIdentifier &Identifier);
   // Find device by <Name>, returns <Identifier>
   static bool Access( TIdentifier Identifier);
   // Check device access by <Identifier>

   TModem();
   // Constructor
   virtual ~TModem();
   // Destructor

   //--- inherited from TSerial :

   virtual bool Open( TIdentifier Identifier);
   // Open device by <Identifier>
   virtual void Close();
   // Close device
   virtual int Write( void *Data, int Length);
   // Write <Data> of size <Length>, returns written length (or 0 if error)
   virtual int Read( void *Data, int Length);
   // Read data <Data> of size <Length>, returns true length (or 0 if error)
   virtual void Flush();
   // Make input/output queue empty
#ifdef __PERSISTENT__
   virtual bool Load( TObjectMemory *Memory);
   // Load setup from <Memory>
   virtual void Save( TObjectMemory *Memory);
   // Save setup to <Memory>
#endif

   //--- UART functionality :

   virtual void SetRxNowait();
   // Set timing of receiver - returns collected data immediately
   virtual void SetRxWait( int ReplyTime, int IntercharacterTime);
   // Set timing of receiver :
   // If <IntercharacterTime> = 0 waits <ReplyTime> for whole message
   // If <IntercharacterTime> > 0 waits <ReplyTime> for first
   // character, next waits <IntercharacterTime> * count of characters
   virtual bool SetParameters( const TParameters &Parameters);
   // Set parameters of communication
   virtual void GetParameters( TParameters &Parameters);
   // Get parameters of communication

   //--- TAPI access :

   bool Dial( TString Number);
   // Dial by <Number>
   bool HangUp();
   // HangUp

   //--- TAPI properties :
   __property TStringList *DeviceList             = {read=GetDeviceList};
   __property int          MinRate                = {read=FMinRate,write=FMinRate};
   __property int          MaxRate                = {read=FMaxRate,write=FMaxRate};
   __property int          ConnectionRate         = {read=FConnectionRate};
//---------------------------------------------------------------------------
protected :
   TIdentifier FIdentifier;          // index in the DeviceList
   TName       FName;                // device name
   //---------------- Uart implementation
   HANDLE      FHandle;
   bool        ImmediateRead;        // RxNowait
   bool        FTotalTime;           // remember for delayed setup
   bool        FIntercharacterTime;  // remember for delayed setup

   //--- Inherited from TSerial :
   virtual TName GetName();
   virtual bool  GetIsOpen();
   //--- Own property setters/getters :
   virtual bool  GetCTS();
   virtual bool  GetDSR();
   virtual bool  GetRI();
   virtual bool  GetDCD();
   virtual void  SetDTR(bool Status);
   virtual void  SetRTS(bool Status);
   virtual void  SetTxD(bool Status);

   void  SafeTxTiming();
   // Set safe timing of transmitter
   DWORD GetModemStatus();
   // Read modem status
   void  BuildDCB( DCB &Dcb);
   // Set Dcb by <FParameters>

   //--- TAPI properties :
   OVERLAPPED   Overlapped;         // Async. data
   int          FMinRate;           // Requested min. connection rate
   int          FMaxRate;           // Requested max. connection rate
   int          FConnectionRate;    // Real connection rate

   TStringList *GetDeviceList();
   // property DeviceList
}; // TModem
//---------------------------------------------------------------------------
#endif
