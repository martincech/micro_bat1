//******************************************************************************
//
//   Modem.cpp    Asynchronous communication via modem connection (TAPI)
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Modem.h"
#include <tapi.h>

#pragma package(smart_init)

#define ClrOverlapped() Overlapped.Internal = 0;Overlapped.InternalHigh = 0;\
                        Overlapped.Offset   = 0;Overlapped.OffsetHigh   = 0;\
                        ResetEvent( Overlapped.hEvent)

#define DEFAULT_BAUD 9600
#define DATAMODEM    "comm/datamodem"  // device class

// Simplified call states :
typedef enum {
   CALL_IDLE,                     // no call placed
   CALL_DIALING,                  // start dialing
   CALL_CONNECTED,                // connection success
   CALL_DISCONNECTED,             // disconnected (busy, no carrier)
   CALL_ERROR,                    // call error (mode already in use)
} TCallState;

// Hide TAPI shits :
typedef struct {
   DWORD          Version;        // TAPI version
   TStringList   *DeviceList;     // names of devices
   HLINEAPP       LineApp;        // TAPI interface
   HLINE          Line;           // Device
   HCALL          Call;           // Connection
   LINECALLPARAMS CallParams;     // Connection parameters
   TCallState     CallState;      // connection status
   DWORD          DeviceId;       // device id
   HANDLE         Comm;           // Comm handle
   DWORD          ConnectionRate; // Connection baud rate
   DWORD          Error;          // Last error code
} TTapiData;

// Global in the module :
static TTapiData Tapi;

// comm/datamodem Device Configuration :

typedef struct device_config_tag {
   VARSTRING  vs;
   DWORD      dwSize;
   DWORD      dwVersion;
   WORD       fwOptions;
   WORD       wWaitBong;
   COMMCONFIG commconfig;
} DEVICECONFIG, FAR *LPDEVICECONFIG;

// comm/datamodem Modem Info :

typedef struct modem_info_tag {
   VARSTRING vs;
   HANDLE    hComm;
   char      szDeviceName[ 255];
} MODEM_INFO, FAR *LPMODEM_INFO;

// Local functions :

static bool Initialize();
// Initialize TAPI

void _USERENTRY Shutdown();
// Release TAPI

void CALLBACK LineCallback( DWORD hDevice, DWORD dwMessage, DWORD_PTR dwInstance,
                            DWORD_PTR dwParam1, DWORD_PTR dwParam2, DWORD_PTR dwParam3);
// TAPI callback


//******************************************************************************
// Locator
//******************************************************************************

bool TModem::Locate( TName Name, TIdentifier &Identifier)
// Find device by <Name>, returns <Identifier>
{
   if( !Tapi.LineApp){
      Initialize();
   }
   // search for device :
   DWORD Count   = Tapi.DeviceList->Count;
   for( DWORD i = 0; i < Count; i++){
      if( Tapi.DeviceList->Strings[ i] == Name){
         Identifier = i;
         return( true);
      }
   }
   return( false);
} // Locate

//******************************************************************************
// Access rights
//******************************************************************************

bool TModem::Access( TIdentifier Identifier)
// Check device access by <Identifier>
{
   DWORD Count = Tapi.DeviceList->Count;
   if( (DWORD)Identifier >= Count){
      return( false);                  // identifier out of range
   }
   return( true);
} // Access

//******************************************************************************
// Constructor
//******************************************************************************

TModem::TModem()
// Constructor
{
   if( !Tapi.LineApp){
      Initialize();                    // TAPI init
   }
   FIdentifier = INVALID_IDENTIFIER;
   FHandle     = INVALID_HANDLE_VALUE;
   FName       = UNKNOWN_NAME;
   FDtr = 0;
   FRts = 0;
   FTxD = 0;
   FParameters.BaudRate  = DEFAULT_BAUD;
   FParameters.DataBits  = 8;
   FParameters.StopBits  = 10;
   FParameters.Parity    = NO_PARITY;
   FParameters.Handshake = NO_HANDSHAKE;
   ImmediateRead         = true;

   FMinRate              = DEFAULT_BAUD;
   FMaxRate              = DEFAULT_BAUD;
   FConnectionRate       = 0;

   Overlapped.hEvent     = CreateEvent( NULL, TRUE, FALSE, NULL);
   ClrOverlapped();
} // TModem

//******************************************************************************
// Destructor
//******************************************************************************

TModem::~TModem()
// Destructor
{
   Close();
   if_okh( Overlapped.hEvent){
      CloseHandle( Overlapped.hEvent);
   }
} // ~TModem

//******************************************************************************
// Open
//******************************************************************************

bool TModem::Open( TIdentifier Identifier)
// Open device by <Name>
{
   Close();
   // search for device :
   DWORD Count   = DeviceList->Count;
   if( (DWORD)Identifier >= Count){
      return( false);                  // identifier out of range
   }
   Tapi.DeviceId = (DWORD)DeviceList->Objects[ Identifier];
   FName         = DeviceList->Strings[ Identifier];     // remember device name
   // prepare Call Parameters :
   memset( &Tapi.CallParams, 0, sizeof( LINECALLPARAMS));
   Tapi.CallParams.dwTotalSize  = sizeof( LINECALLPARAMS);
   Tapi.CallParams.dwBearerMode = LINEBEARERMODE_VOICE;
   Tapi.CallParams.dwMediaMode  = LINEMEDIAMODE_DATAMODEM;
   Tapi.CallParams.dwMaxRate    = FMaxRate;
   Tapi.CallParams.dwMinRate    = FMinRate;
   // open line :
   if__err( lineOpen( Tapi.LineApp, Tapi.DeviceId, &Tapi.Line, Tapi.Version, 0, 0,
                     LINECALLPRIVILEGE_NONE, LINEMEDIAMODE_DATAMODEM, &Tapi.CallParams)){
      return( false);
   }
   Tapi.CallState    = CALL_IDLE;
   Tapi.Comm         = INVALID_HANDLE_VALUE;
   FIdentifier       = Identifier;
   return( true);
} // Open

//******************************************************************************
// Close
//******************************************************************************

void TModem::Close()
// Close device
{
   // close UART :
   FHandle     = INVALID_HANDLE_VALUE;
   FIdentifier = INVALID_IDENTIFIER;
   FName       = UNKNOWN_NAME;
   // close COM :
   if_okh( Tapi.Comm){
      CloseHandle( Tapi.Comm);
      Tapi.Comm = INVALID_HANDLE_VALUE;
   }
   // close TAPI Line :
   if( Tapi.Call){
      lineDrop( Tapi.Call, NULL, 0);        // hang up
      lineDeallocateCall( Tapi.Call);       // return call resources
      Tapi.Call = 0;
   }
   // close Modem :
   if( Tapi.Line){
      lineClose( Tapi.Line);                // return modem resources
      Tapi.Line = 0;
   }
} // Close

//******************************************************************************
// Write
//******************************************************************************

int TModem::Write( void *Data, int Length)
// Write <Data> of size <Length>, returns written length (or 0 if error)
{
   if( !IsOpen){
      return( 0);
   }
   DWORD w;
   ClrOverlapped();
   if( WriteFile( FHandle, Data, Length, &w, &Overlapped)){
      return( w);
   }
   if( GetLastError() != ERROR_IO_PENDING){
      return( 0);         // something is wrong
   }
   if( !GetOverlappedResult( FHandle, &Overlapped, &w, TRUE)){
      return( 0);
   }
   return( w);
} // Write

//******************************************************************************
// Read
//******************************************************************************

int TModem::Read( void *Data, int Length)
// Read data <Data> of size <Length>, returns true length (or 0 if error)
{
   if( !IsOpen){
      return( 0);
   }
   if( Length == 0){
      IERROR;
   }
   DWORD r;
   // TotalTimeout emulation :
   if( ImmediateRead){
      // RxNowait
      ClrOverlapped();
      if( ReadFile( FHandle, Data, Length, &r, &Overlapped)){
         return( r);
      }
      if( GetLastError() != ERROR_IO_PENDING){
         return( 0);         // something is wrong
      }
      if( !GetOverlappedResult( FHandle, &Overlapped, &r, TRUE)){
         return( 0);
      }
      return( r);
   }
   // RxWait :
   ClrOverlapped();
   if( ReadFile( FHandle, Data, Length, &r, &Overlapped)){
      return( r);
   }
   if( GetLastError() != ERROR_IO_PENDING){
      return( 0);         // something is wrong
   }
   if( !GetOverlappedResult( FHandle, &Overlapped, &r, TRUE)){
      return( 0);
   }
   return( r);
} // Read

//******************************************************************************
// Set Parameters
//******************************************************************************

bool  TModem::SetParameters( const TParameters &Parameters)
// Set parameters of communication
{
   FParameters   = Parameters;         // remember
   ImmediateRead = true;               // safe Rx Timing
   if( !Tapi.Line){
      return( false);
   }
   // Get Device configuration size :
   VARSTRING DeviceHdr;
   memset( &DeviceHdr, 0, sizeof( DeviceHdr));
   DeviceHdr.dwTotalSize = sizeof( DeviceHdr);
   if__err( lineGetDevConfig( Tapi.DeviceId, &DeviceHdr, DATAMODEM)){
      return( false);                  // unable get DeviceConfig size
   }
   DWORD Needed = DeviceHdr.dwNeededSize;
   if( !Needed){
      return( false);                  // requested DeviceConfig size is zero
   }
   // Get Device configuration data :
   DEVICECONFIG *DeviceConfig;
   DeviceConfig = (DEVICECONFIG *)new byte[ Needed];
   memset( DeviceConfig, 0, Needed);
   DeviceConfig->vs.dwTotalSize = Needed;
   if__err( lineGetDevConfig( Tapi.DeviceId, (LPVARSTRING)DeviceConfig, DATAMODEM)){
      delete[] DeviceConfig;
      return( false);                  // unable get Device config
   }
   memset( &DeviceConfig->commconfig.dcb, 0, sizeof( DeviceConfig->commconfig.dcb));
   BuildDCB( DeviceConfig->commconfig.dcb);
   if__err( lineSetDevConfig( Tapi.DeviceId, (LPVOID)&DeviceConfig->dwSize, DeviceConfig->dwSize, DATAMODEM)){
      delete[] DeviceConfig;
      return( false);                  // unable set Device config
   }
   delete[] DeviceConfig;
   return( true);
} // SetParameters

//******************************************************************************
// Get Parameters
//******************************************************************************

void TModem::GetParameters( TParameters &Parameters)
// Get parameters of communication
{
   Parameters = FParameters;
} // GetParameters

//******************************************************************************
// Set Rx Timing
//******************************************************************************

void TModem::SetRxNowait()
// Set timing of receiver - returns collected data immediately
{
   ImmediateRead = true;         // remember setting
   if( !IsOpen){
      return;                    // after dial
   }
   COMMTIMEOUTS to;
   GetCommTimeouts( FHandle, &to);

   // return immediately if no chars present :
   to.ReadIntervalTimeout         =  MAXDWORD;
   to.ReadTotalTimeoutMultiplier  =  0;
   to.ReadTotalTimeoutConstant    =  0;

   SetCommTimeouts( FHandle, &to);
} // SetRxNowait

void TModem::SetRxWait( int TotalTime, int IntercharacterTime)
// Set timing of receiver :
// If <IntercharacterTime> = 0 waits <ReplyTime> for whole message
// If <IntercharacterTime> > 0 waits <ReplyTime> for first
// character, next waits <IntercharacterTime> * count of characters
{
   FTotalTime          = TotalTime;          // remember setting
   FIntercharacterTime = IntercharacterTime;
   if( !IsOpen){
      return;                                // after dial
   }
   if( TotalTime == 0 && IntercharacterTime == 0){
      IERROR;
   }
   if( TotalTime <= IntercharacterTime){
      // too short for Windows OS
      TotalTime = 10 * IntercharacterTime;   // patch it
   }
   ImmediateRead = false;                    // remember setting
   COMMTIMEOUTS to;
   GetCommTimeouts( FHandle, &to);

   // intercharacter doesn't work properly, emulate :
   to.ReadIntervalTimeout         =  0;
   to.ReadTotalTimeoutMultiplier  =  IntercharacterTime;
   to.ReadTotalTimeoutConstant    =  TotalTime;

   SetCommTimeouts( FHandle, &to);
} // SetRxWait

//******************************************************************************
// Flush
//******************************************************************************

void TModem::Flush()
// Make input/output queue empty
{
   if( !IsOpen){
      return;
   }
   PurgeComm( FHandle, PURGE_RXCLEAR | PURGE_TXCLEAR);
} // Flush

#ifdef __PERSISTENT__
//******************************************************************************
// Load
//******************************************************************************

#define PLoadInteger( name)   FParameters.name = Memory->LoadInteger(#name);
#define PLoadIntegerCast( name, cast) \
                              FParameters.name = (cast)Memory->LoadInteger(#name);

bool TModem::Load( TObjectMemory *Memory)
// Load setup from <Memory>
{
   if( !Memory->Locate( FName)){
      return( false);  // unable to find
   }
   PLoadInteger( BaudRate);
   PLoadInteger( DataBits);
   PLoadInteger( StopBits);
   PLoadIntegerCast( Parity, TParity);
   PLoadIntegerCast( Handshake, THandshake);
   SetParameters( FParameters);
   return( true);
} // Load

//******************************************************************************
// Save
//******************************************************************************

#define PSaveInteger( name)  Memory->SaveInteger(#name,FParameters.name)

void TModem::Save( TObjectMemory *Memory)
// Save setup to <Memory>
{
   Memory->Create( FName);
   PSaveInteger( BaudRate);
   PSaveInteger( DataBits);
   PSaveInteger( StopBits);
   PSaveInteger( Parity);
   PSaveInteger( Handshake);
} // Save
#endif

//******************************************************************************
// Dial
//******************************************************************************

bool TModem::Dial( TString Number)
// Dial by <Number>
{
   if( Tapi.Call){
      return( false);                            // already connected
   }
   if_okh( Tapi.Comm){
      IERROR;                                    // handle of closed call
   }
   Number = "T" + Number;                        // tone dialing
   Tapi.CallState    = CALL_DIALING;             // wait for dial
   Tapi.Error        = 0;
   long Result;
   Result = lineMakeCall( Tapi.Line, &Tapi.Call, Number.c_str(), 0, &Tapi.CallParams);
   if( Result < 0){
      return( false);                            // synchronous error
   }
   // wait for connection
   while( Tapi.CallState == CALL_DIALING){
      Application->ProcessMessages();            // waiting messages
   }
   switch( Tapi.CallState){
      case CALL_CONNECTED :
         break;                                  // connected ok
      case CALL_DISCONNECTED :
      case CALL_ERROR :
      default :
         Tapi.CallState = CALL_IDLE;             // no activity
         return( false);                         // unable connect
   }
   FHandle         = Tapi.Comm;                  // UART handle
   FConnectionRate = Tapi.ConnectionRate;
   // delayed settings :
   SafeTxTiming();
   if( ImmediateRead){
      SetRxNowait();
   } else {
      SetRxWait( FTotalTime, FIntercharacterTime);
   }
   Flush();
   return( true);
} // Dial

//******************************************************************************
// HangUp
//******************************************************************************

bool TModem::HangUp()
// HangUp
{
   if( !Tapi.Call){
      return( false);
   }
   FHandle = INVALID_HANDLE_VALUE;
   lineDrop( Tapi.Call, NULL, 0);
   return( true);
} // HangUp

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Name
//******************************************************************************

TName TModem::GetName()
// Get device name by <Identifier>
{
   if( FIdentifier == INVALID_IDENTIFIER){
      return( UNKNOWN_NAME);
   }
   return( FName);
} // GetName

//******************************************************************************
// Is Open
//******************************************************************************

bool TModem::GetIsOpen()
// Check if device is opened
{
  return( FHandle != INVALID_HANDLE_VALUE);
} // GetIsOpen

//******************************************************************************
// Property CTS
//******************************************************************************

bool TModem::GetCTS()
{
   return( GetModemStatus() & MS_CTS_ON);
} // GetCTS

//******************************************************************************
// Property DSR
//******************************************************************************

bool TModem::GetDSR()
{
   return( GetModemStatus() & MS_DSR_ON);
} // GetDSR

//******************************************************************************
// Property RI
//******************************************************************************

bool TModem::GetRI()
{
   return( GetModemStatus() & MS_RING_ON);
} // GetRING

//******************************************************************************
// Property RLSD
//******************************************************************************

bool TModem::GetDCD()
{
   return( GetModemStatus() & MS_RLSD_ON);
} // GetDCD

//******************************************************************************
// Property DTR
//******************************************************************************

void TModem::SetDTR( bool Status)
{

   if( !IsOpen){
      IERROR;
   }
   EscapeCommFunction( FHandle,
                       Status ? SETDTR : CLRDTR);
   FDtr = Status;
} // SetDTR

//******************************************************************************
// Property RTS
//******************************************************************************

void TModem::SetRTS( bool Status)
{
   if( !IsOpen){
      IERROR;
   }
   EscapeCommFunction( FHandle,
                       Status ? SETRTS : CLRRTS);
   FRts = Status;
} // SetRTS

//******************************************************************************
// Property TxD
//******************************************************************************

void TModem::SetTxD( bool Status)
{
   if( !IsOpen){
      IERROR;
   }
   EscapeCommFunction( FHandle,
                       Status ? SETBREAK : CLRBREAK);
   FTxD = Status;
} // SetTxD

//******************************************************************************
// Tx timing
//******************************************************************************

void  TModem::SafeTxTiming()
// Set safe timing of transmitter
{
   if( !IsOpen){
      IERROR;
   }
   COMMTIMEOUTS to;
   GetCommTimeouts( FHandle, &to);
   // Calculate via us :
   int CharTime;
   if( FParameters.BaudRate > 0 && FParameters.DataBits > 0){
      CharTime = (1000000 / FParameters.BaudRate) * FParameters.DataBits + 3;
      if( CharTime < 1){
         CharTime = 1;
      }
   } else {
      CharTime = 1;
   }
   // convert to ms :
   CharTime /= 1000;
   if( CharTime <= 0){
      CharTime = 1;
   }
   CharTime *= 2;   // security multiplier

   // maximum time for send :
   to.WriteTotalTimeoutMultiplier = CharTime;
   to.WriteTotalTimeoutConstant   = 100;
   SetCommTimeouts( FHandle, &to);
} // SafeTxTiming

//******************************************************************************
// Modem Status
//******************************************************************************

DWORD TModem::GetModemStatus()
// Read modem status
{
   if( !IsOpen){
      return( 0);
   }
   DWORD d = 0;
   if( !GetCommModemStatus( FHandle, &d)){
      IERROR;
   }
   return( d);
} // GetModemStatus

//******************************************************************************
// Build DCB
//******************************************************************************

void  TModem::BuildDCB( DCB &Dcb)
// Set Dcb by <FParameters>
{
   memset( &Dcb, 0, sizeof( DCB));
   Dcb.DCBlength     = sizeof( DCB);
   Dcb.fBinary       = true;   // binary mode
   Dcb.fAbortOnError = false;  // ignore invalid characters
   Dcb.XonLim        = 2048;
   Dcb.XoffLim       = 512;
   Dcb.XonChar       = 0x11;
   Dcb.XoffChar      = 0x13;
   // Translate baudrate :
   switch( FParameters.BaudRate){
      case 300 :
         Dcb.BaudRate = CBR_300;
         break;
      case 600 :
         Dcb.BaudRate = CBR_600;
         break;
      case 1200 :
         Dcb.BaudRate = CBR_1200;
         break;
      case 2400 :
         Dcb.BaudRate = CBR_2400;
         break;
      case 4800 :
         Dcb.BaudRate = CBR_4800;
         break;
      case 9600 :
         Dcb.BaudRate = CBR_9600;
         break;
      case 19200 :
         Dcb.BaudRate = CBR_19200;
         break;
      case 38400 :
         Dcb.BaudRate = CBR_38400;
         break;
      case 57600 :
         Dcb.BaudRate = CBR_57600;
         break;
      case 115200 :
         Dcb.BaudRate = CBR_115200;
         break;
      default :
         return; // invalid baudrate
   }
   // Translate Byte size :
   if( FParameters.DataBits < 5 ||
       FParameters.DataBits > 8){
      return;    // invalid size
   }
   Dcb.ByteSize = (BYTE)FParameters.DataBits;
   switch( FParameters.Parity){
      case NO_PARITY :
         Dcb.Parity = NOPARITY;
         break;
      case ODD_PARITY :
         Dcb.Parity = ODDPARITY;
         break;
      case EVEN_PARITY :
         Dcb.Parity = EVENPARITY;
         break;
      case MARK_PARITY :
         Dcb.Parity = MARKPARITY;
         break;
      case SPACE_PARITY :
         Dcb.Parity = SPACEPARITY;
         break;
      IDEFAULT
   }
   switch( FParameters.StopBits){
      case 10 :
         Dcb.StopBits = ONESTOPBIT;
         break;
      case 15 :
         Dcb.StopBits = ONE5STOPBITS;
         break;
      case 20 :
         Dcb.StopBits = TWOSTOPBITS;
         break;
      default :
         return;  // invalid stop bits count
   }
   // Translate Handshake :
   switch( FParameters.Handshake){
      case NO_HANDSHAKE :
         Dcb.fInX         = false;
         Dcb.fOutX        = false;
         Dcb.fOutxCtsFlow = false;
         Dcb.fOutxDsrFlow = false;
         Dcb.fDtrControl  = DTR_CONTROL_ENABLE;
         Dcb.fRtsControl  = RTS_CONTROL_ENABLE;
         Dcb.fDsrSensitivity =  false;   // ignore DSR
         break;
      case HARDWARE_HANDSHAKE :
         Dcb.fInX         = false;
         Dcb.fOutX        = false;
         Dcb.fOutxCtsFlow = true;
         Dcb.fOutxDsrFlow = true;
         Dcb.fDtrControl  = DTR_CONTROL_HANDSHAKE;
         Dcb.fRtsControl  = RTS_CONTROL_HANDSHAKE;
         Dcb.fDsrSensitivity =  true;   // trace DSR
         break;
      case XON_XOFF_HANDSHAKE :
         Dcb.fInX         = true;
         Dcb.fOutX        = true;
         Dcb.fOutxCtsFlow = false;
         Dcb.fOutxDsrFlow = false;
         Dcb.fDtrControl  = DTR_CONTROL_ENABLE;
         Dcb.fRtsControl  = RTS_CONTROL_ENABLE;
         Dcb.fDsrSensitivity =  false;   // ignore DSR
         break;
      case HALF_DUPLEX_HANDSHAKE :
         Dcb.fInX         = false;
         Dcb.fOutX        = false;
         Dcb.fOutxCtsFlow = false;
         Dcb.fOutxDsrFlow = false;
         Dcb.fDtrControl  = DTR_CONTROL_ENABLE;
         Dcb.fRtsControl  = RTS_CONTROL_TOGGLE; // via RTS
         Dcb.fDsrSensitivity =  false;          // ignore DSR
         break;
      IDEFAULT
   }
   // Other flags :
} // BuildDCB

//******************************************************************************
// Property DeviceList
//******************************************************************************

TStringList *TModem::GetDeviceList()
// property DeviceList
{
   return( Tapi.DeviceList);
} // GetDeviceList

//******************************************************************************
// Initialize
//******************************************************************************

static bool Initialize()
// Initialize TAPI
{
   // default values :
   Tapi.Version        = 0;
   Tapi.DeviceList     = new TStringList;
   Tapi.LineApp        = 0;
   Tapi.Call           = 0;
   Tapi.Line           = 0;
   Tapi.CallState      = CALL_IDLE;
   Tapi.DeviceId       = 0xFFFFFFFF;
   Tapi.Comm           = INVALID_HANDLE_VALUE;
   Tapi.ConnectionRate = 0;
   Tapi.Error          = 0;
   // initialize TAPI :
   DWORD  DeviceCount;
   char  *ApplicationName = Application->ExeName.c_str();
   if__err( lineInitialize( &Tapi.LineApp, HInstance, LineCallback, ApplicationName, &DeviceCount)){
      return( false);
   }
   if( !DeviceCount){
      lineShutdown( Tapi.LineApp);       // no device
      return( false);
   }
   atexit( Shutdown);                    // plan TAPI shutdown
   // explore devices - fill modem list :
   LINEEXTENSIONID ExtId;
   LINEDEVCAPS    *DevCaps;
   long            Result;
   for( DWORD i = 0; i < DeviceCount; i++){
      if__err( lineNegotiateAPIVersion( Tapi.LineApp, i, 0x00010000, 0x00090000, &Tapi.Version, &ExtId)){
         continue;
      }
      // try get Device caps size
      DevCaps = (LINEDEVCAPS *)new byte[ sizeof( LINEDEVCAPS)];
      memset( DevCaps, 0, sizeof( LINEDEVCAPS));
      DevCaps->dwTotalSize = sizeof( LINEDEVCAPS);
      Result = lineGetDevCaps( Tapi.LineApp, i, Tapi.Version, 0, DevCaps);
      DWORD Needed = DevCaps->dwNeededSize;
      delete[] DevCaps;
      if( (Result < 0) && (Result != (long)LINEERR_STRUCTURETOOSMALL)){
         continue;       // unable get data, skip
      }
      // new Device caps size :
      DevCaps = (LINEDEVCAPS *)new byte[ Needed];
      memset( DevCaps, 0, Needed);
      DevCaps->dwTotalSize = Needed;
      if__err( lineGetDevCaps( Tapi.LineApp, i, Tapi.Version, 0, DevCaps)){
         delete[] DevCaps;
         continue;
      }
      if( DevCaps->dwMediaModes  & LINEMEDIAMODE_DATAMODEM){
         // add modem
         int Index = Tapi.DeviceList->Add( (char *)DevCaps + DevCaps->dwLineNameOffset); // name
         Tapi.DeviceList->Objects[ Index] = (TObject *)i;     // internal identification
      }
      delete[] DevCaps;
   }
   return( true);
} // Initialize

//******************************************************************************
// Shutdown
//******************************************************************************

void _USERENTRY Shutdown()
// Release TAPI
{
   if_okh( Tapi.Comm){
      CloseHandle( Tapi.Comm);
      Tapi.Comm = INVALID_HANDLE_VALUE;
   }
   // close Call :
   if( Tapi.Call){
      lineDeallocateCall( Tapi.Call);
      Tapi.Call = 0;
   }
   // close Line :
   if( Tapi.Line){
      lineClose( Tapi.Line);
      Tapi.Line = 0;
   }
   // TAPI shutdown :
   if( Tapi.LineApp){
      lineShutdown( Tapi.LineApp);
      Tapi.LineApp = 0;
   }
   if( Tapi.DeviceList){
      delete Tapi.DeviceList;
   }
} // Shutdown

//******************************************************************************
// TAPI Callback
//******************************************************************************

void CALLBACK LineCallback( DWORD hDevice, DWORD dwMessage, DWORD_PTR dwInstance,
                            DWORD_PTR dwParam1, DWORD_PTR dwParam2, DWORD_PTR dwParam3)
{
static byte   LcBuffer[ 2 * sizeof( LINECALLINFO)];    // variable size
LINECALLINFO *LineCallInfo = (LINECALLINFO *)LcBuffer; // pointer of right type

   switch( dwMessage){
      case LINE_REPLY :
         // MakeCall reply code
         Tapi.Error = dwParam2;             // remember error code
         if( dwParam2 != 0){
            Tapi.CallState = CALL_ERROR;    // eg. modem in use
            break;
         }
         break;                             // call OK

      case LINE_CLOSE :
         Tapi.Line     = 0;
         Tapi.DeviceId = 0xFFFFFFFF;
         break;

      case LINE_CALLSTATE :
         switch( dwParam1){
            case LINECALLSTATE_IDLE :
               // call losts physical connection
               if_okh( Tapi.Comm){
                  CloseHandle( Tapi.Comm);
                  Tapi.Comm = INVALID_HANDLE_VALUE;
               }
               if( Tapi.Call){
                  lineDeallocateCall( Tapi.Call);
                  Tapi.Call = 0;
               }
               Tapi.CallState = CALL_IDLE;            // call disposed
               break;

            case LINECALLSTATE_CONNECTED :
               // connection established
               // get connection speed :
               LineCallInfo->dwTotalSize = sizeof( LcBuffer);
               if__err( lineGetCallInfo( Tapi.Call, LineCallInfo)){
                  Tapi.ConnectionRate = 0;
               } else {
                  Tapi.ConnectionRate = LineCallInfo->dwRate;
               }
               // get COM port handle :
               MODEM_INFO ModemInfo;
               ModemInfo.vs.dwTotalSize    = sizeof( MODEM_INFO);
               ModemInfo.vs.dwStringFormat = STRINGFORMAT_BINARY;
               ModemInfo.hComm             = INVALID_HANDLE_VALUE;
               lineGetID( Tapi.Line, 0, Tapi.Call, LINECALLSELECT_LINE, (LPVARSTRING)&ModemInfo, DATAMODEM);
               if_errh( ModemInfo.hComm){
                  // something is wrong, unable get port handle
                  if( Tapi.Call){
                     lineDrop( Tapi.Call, NULL, 0);   // hang up
                     lineDeallocateCall( Tapi.Call);  // return resources
                     Tapi.Call = 0;
                  }
                  Tapi.CallState = CALL_IDLE;         // wait for next call
                  break;
               }
               Tapi.CallState = CALL_CONNECTED;
               Tapi.Comm      = ModemInfo.hComm;
               break;

            case LINECALLSTATE_DISCONNECTED :
               // connection dropped
               if_okh( Tapi.Comm){
                  CloseHandle( Tapi.Comm);
                  Tapi.Comm = INVALID_HANDLE_VALUE;
               }
               if( Tapi.Call){
                  lineDrop( Tapi.Call, NULL, 0);
               }
               Tapi.CallState = CALL_DISCONNECTED;
               break;

            default :
               break;
         }
         break;

      default :
         break;
   }
} // LineCallback


