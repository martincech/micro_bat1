//******************************************************************************
//
//   UartSetup.h    UART parameters dialog
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef UartSetupH
#define UartSetupH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>

#include "Uart.h"

//---------------------------------------------------------------------------

class TUartSetupDialog : public TForm
{
__published:	// IDE-managed Components
   TComboBox *UartSetupBaudRate;
   TComboBox *UartSetupDataBits;
   TComboBox *UartSetupParity;
   TComboBox *UartSetupStopBits;
   TComboBox *UartSetupHandshake;
   TBevel *Bevel1;
   TLabel *Label1;
   TLabel *Label2;
   TLabel *Label3;
   TLabel *Label4;
   TLabel *Label5;
   TBitBtn *UartSetupCancel;
   TBitBtn *UartSetupOK;
   void __fastcall UartSetupOKClick(TObject *Sender);

private:	// User declarations
   void __fastcall SetupByParameters( TUart::TParameters &Parameters);
   // Nastaveni dialogu podle parameteru
   void __fastcall ConvertToParameters( TUart::TParameters &Parameters);
   // Nastaveni parametru podle dialogu

public:		// User declarations
   __fastcall TUartSetupDialog(TComponent* Owner);
   // Constructor
   bool __fastcall Execute( TUart::TParameters &Parameters, TString Caption);
   // Run setup dialog modal
};

//---------------------------------------------------------------------------
extern PACKAGE TUartSetupDialog *UartSetupDialog;
//---------------------------------------------------------------------------
#endif
