//******************************************************************************
//
//   SingleFileLogger.h   Raw data file logger
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include "..\Unisys\uni.h"

#include "SingleFileLogger.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

TSingleFileLogger::TSingleFileLogger(String Name, String Number)
// Konstruktor
{
   File  = 0;
   NewFileName(Name, Number);
   if( !QueryPerformanceFrequency( &Frequency)){
      Frequency.QuadPart = 1;
   }
} // TSingleFileLogger

//******************************************************************************
// Destructor
//******************************************************************************

TSingleFileLogger::~TSingleFileLogger()
// Destruktor
{
   if_null( File){
      return;
   }
   fclose(File);
} // ~TSingleFileLogger

//******************************************************************************
// Zapis dat
//******************************************************************************

void TSingleFileLogger::Write( TDataType Type, void *Buffer, int Length)
// Zapis dat
{
   if_null( File){
      return;
   }
   if( Length <= 0){
      return;
   }
   if_null( Buffer){
      IERROR;
   }
   fprintf( File, "\n");        // Novy radek
   Timestamp();
   switch( Type){
      case UNKNOWN :
         // neznama data
         fprintf( File, "UNKNOWN :\n");
         break;
      case RX_DATA :
         // prijata data
         fprintf( File, "RX :\n");
         break;
      case TX_DATA :
         // vyslana data
         fprintf( File, "TX :\n");
         break;
      case RX_GARBAGE :
         // prijate smeti
         fprintf( File, "RX GARBAGE :\n");
         break;
   }
   BinaryWrite( (char *)Buffer, Length);
} // Write

//******************************************************************************
// Zapis zpravy
//******************************************************************************

void TSingleFileLogger::Report( char *Format, ...)
// Zapis textu
{
va_list Arg;

   if_null( File){
      return;
   }
   fprintf( File, "\n");        // Novy radek
   Timestamp();
   va_start( Arg, Format);
   vfprintf( File, Format, Arg);
} // Report

//******************************************************************************
// Binarni zapis dat
//******************************************************************************

void  TSingleFileLogger::BinaryWrite( char *Buffer, int Length)
// Binarni zapis dat
{
   int i = 0;
   while( 1){
      for( int j = 0; j < 16; j++){
         fprintf( File, "%02X ", (unsigned char)Buffer[ i++]);
         if( i >= Length){
            goto next;
         }
      }
      fprintf( File, "\n");
   }
   next :
   fprintf( File, "\n");
} // BinaryWrite

//******************************************************************************
// Novy nazev souboru
//******************************************************************************

void TSingleFileLogger::NewFileName(String Name, String Number)
// zkontroluj a uprav nazev souboru
{
  unsigned short Year, Month, Day, Hour, Min, Sec, Msec;

  TDateTime dt = TDateTime::CurrentDateTime();
  dt.DecodeDate( &Year, &Month, &Day);
  dt.DecodeTime( &Hour, &Min, &Sec, &Msec);

  // zaloz novy soubor
  if (FileExists(Name)) {
    DeleteFile(Name);          // Smazu dosavadni soubor
  }
  File = fopen(Name.c_str(), "a");
  if( !File){
     return;
  }
  fprintf( File, "%02d.%02d.%02d %02d:%02d:%02d.%03d\n",
           Day, Month, Year %100, Hour, Min, Sec, Msec);
  fprintf( File, String("Number: " + Number + "\n\n").c_str());
  QueryPerformanceCounter( &Counter);
} // NewFileName

//******************************************************************************
// Cas
//******************************************************************************

void TSingleFileLogger::Timestamp()
// zapise cas
{
   LARGE_INTEGER NewCounter;
   QueryPerformanceCounter( &NewCounter);

   unsigned short Hour, Min, Sec, Msec;
   TDateTime dt = TDateTime::CurrentDateTime();
   dt.DecodeTime( &Hour, &Min, &Sec, &Msec);

   fprintf( File, "%02d:%02d:%02d.%03d %07Ld ", Hour, Min, Sec, Msec, ((NewCounter.QuadPart - Counter.QuadPart) * 1000000) / Frequency.QuadPart);
   Counter.QuadPart = NewCounter.QuadPart;
} // Timestamp

