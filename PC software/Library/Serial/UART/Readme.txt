Nastaveni timeoutu COM

Pro Rx :

Celkovy timeout se sklada z RTTC a z Length * RTTM


RIT       MAX  MAX 0   t   0   t
RTTM      MAX  0   0   0   0   0
RTTC      T    0   T   T   0   0
Length    N    N   N   N   N   N
-----------------------------------
Pozn.:    1    2   3   4   5   6


1.Vraci se po 1. prijatem znaku (ceka na nej T). Znaky z bufferu
  vraci ihned, i je-li jich mene nez N

2. Vrati znaky z bufferu, bez cekani

3. Ceka na N znaku, po dobu T. Vrati znaky prijate za tuto dobu,
   nebo se vraci po prijeti N znaku (za < T).
   Analogicky pro 0 < RTTM < MAX

4. Je-li mezera > t vrati se i po < N znacich. Konci nejpozdeji za T ( < N).
   Analogicky pro 0 < RTTM < MAX

5. Ceka na N znaku nekonecne dlouho

6. Ceka na N znaku nekonecne. Je-li mezera > t, vrati < N znaku.

