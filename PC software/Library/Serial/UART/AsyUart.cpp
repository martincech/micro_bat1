//******************************************************************************
//
//   AsyUart.cpp  Asynchronous serial device RS232/RS422/RS485
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "AsyUart.h"

#include <string.h>
#include <stdio.h>

#pragma package(smart_init)

#define ClrOverlapped() Overlapped.Internal = 0;Overlapped.InternalHigh = 0;\
                        Overlapped.Offset   = 0;Overlapped.OffsetHigh   = 0;\
                        ResetEvent( Overlapped.hEvent)


// Implementation constants :

#define MIN_COM_NUMBER   1
#define MAX_COM_NUMBER  99

//******************************************************************************
// Locator
//******************************************************************************

bool TAsyUart::Locate( TName Name, TIdentifier &Identifier)
// Find device by <Name>, returns <Identifier>
{
   char *CName = Name.c_str();
   if( strlen( CName) < 4){
      return( false);
   }
   // prefix :
   if( CName[ 0] != 'C' ||
       CName[ 1] != 'O' ||
       CName[ 2] != 'M'){
      return( false);
   }
   int Number = atoi( &CName[ 3]);
   if( Number < MIN_COM_NUMBER ||
       Number > MAX_COM_NUMBER){
      return( false);
   }
   Identifier = Number;
   return( true);
} // Locate

//******************************************************************************
// Access rights
//******************************************************************************

bool TAsyUart::Access( TIdentifier Identifier)
// Check device access by <Identifier>
{
  char *Name = ConvertComName( Identifier);
  HANDLE TmpHandle = CreateFile( Name,GENERIC_READ|GENERIC_WRITE,
                                 0,NULL,OPEN_EXISTING,
                                 FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
  if_errh( TmpHandle){
     return( false);  // unable open
  }
  CloseHandle( TmpHandle);
  return( true);
} // Access

//******************************************************************************
// Constructor
//******************************************************************************

TAsyUart::TAsyUart():TUart()
// Constructor
{
   FIdentifier = INVALID_IDENTIFIER;
   FHandle     = INVALID_HANDLE_VALUE;
   FDtr = 0;
   FRts = 0;
   FTxD = 0;
   FParameters.BaudRate  = 9600;
   FParameters.DataBits  = 8;
   FParameters.StopBits  = 10;
   FParameters.Parity    = NO_PARITY;
   FParameters.Handshake = NO_HANDSHAKE;
   ImmediateRead         = YES;
   Overlapped.hEvent     = CreateEvent( NULL, TRUE, FALSE, NULL);
   ClrOverlapped();
} // TAsyUart

//******************************************************************************
// Destructor
//******************************************************************************

TAsyUart::~TAsyUart()
// Destructor
{
   Close();
   if_okh( Overlapped.hEvent){
      CloseHandle( Overlapped.hEvent);
   }
} // ~TAsyUart

//******************************************************************************
// Open
//******************************************************************************

bool TAsyUart::Open( TIdentifier Identifier)
// Open device by <Identifier>
{
   if( Identifier < MIN_COM_NUMBER ||
       Identifier > MAX_COM_NUMBER){
      IERROR;     // out of range
   }
   if( IsOpen){
      Close();    // close previous
   }
   char *Name = ConvertComName( Identifier);
   if_errh( FHandle = CreateFile( Name,GENERIC_READ|GENERIC_WRITE,
                                  0,NULL,OPEN_EXISTING,
                                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL)){
      return( false);
   }
   FIdentifier = Identifier;
   return( true);
} // Open

//******************************************************************************
// Clone
//******************************************************************************

bool TAsyUart::Clone( HANDLE Handle)
// "Open" by <Handle>
{
   if( IsOpen){
      Close();    // close previous
   }
   if_errh( Handle){
      return( false);
   }
   FHandle     = Handle;
   FIdentifier = MAX_COM_NUMBER;
   return( true);
} // Clone

//******************************************************************************
// Close
//******************************************************************************

void TAsyUart::Close()
// Close device
{
   if( IsOpen){
      CloseHandle( FHandle);
   }
   FHandle     = INVALID_HANDLE_VALUE;
   FIdentifier = INVALID_IDENTIFIER;
} // Close

//******************************************************************************
// Write
//******************************************************************************

int TAsyUart::Write( void *Data, int Length)
// Write <Data> of size <Length>, returns written length (or 0 if error)
{
   if( !IsOpen){
      IERROR;
   }
   DWORD w;
   ClrOverlapped();
   if( WriteFile( FHandle, Data, Length, &w, &Overlapped)){
      return( w);
   }
   if( GetLastError() != ERROR_IO_PENDING){
      return( 0);         // something is wrong
   }
   if( !GetOverlappedResult( FHandle, &Overlapped, &w, TRUE)){
      return( 0);
   }
   return( w);
} // Write

//******************************************************************************
// Read
//******************************************************************************

int TAsyUart::Read( void *Data, int Length)
// Read data <Data> of size <Length>, returns true length (or 0 if error)
{
   if( !IsOpen){
      IERROR;
   }
   if( Length == 0){
      IERROR;
   }
   DWORD r;
   // TotalTimeout emulation :
   if( ImmediateRead){
      // RxNowait
      ClrOverlapped();
      if( ReadFile( FHandle, Data, Length, &r, &Overlapped)){
         return( r);
      }
      if( GetLastError() != ERROR_IO_PENDING){
         return( 0);         // something is wrong
      }
      if( !GetOverlappedResult( FHandle, &Overlapped, &r, TRUE)){
         return( 0);
      }
      return( r);
   }
   // RxWait :
   ClrOverlapped();
   if( ReadFile( FHandle, Data, Length, &r, &Overlapped)){
      return( r);
   }
   if( GetLastError() != ERROR_IO_PENDING){
      return( 0);         // something is wrong
   }
   if( !GetOverlappedResult( FHandle, &Overlapped, &r, TRUE)){
      return( 0);
   }
   return( r);
} // Read

//******************************************************************************
// Set Parameters
//******************************************************************************

bool  TAsyUart::SetParameters( const TParameters &Parameters)
// Set parameters of communication
{
DCB dcb;

   if( !IsOpen){
      IERROR;
   }
   // Get original settings :
   if( !GetCommState( FHandle, &dcb)){
      return( false);
   }
   // Translate baudrate :
   switch( Parameters.BaudRate){
      case 300 :
         dcb.BaudRate = CBR_300;
         break;
      case 600 :
         dcb.BaudRate = CBR_600;
         break;
      case 1200 :
         dcb.BaudRate = CBR_1200;
         break;
      case 2400 :
         dcb.BaudRate = CBR_2400;
         break;
      case 4800 :
         dcb.BaudRate = CBR_4800;
         break;
      case 9600 :
         dcb.BaudRate = CBR_9600;
         break;
      case 19200 :
         dcb.BaudRate = CBR_19200;
         break;
      case 38400 :
         dcb.BaudRate = CBR_38400;
         break;
      case 57600 :
         dcb.BaudRate = CBR_57600;
         break;
      case 115200 :
         dcb.BaudRate = CBR_115200;
         break;
      default :
         return( false); // invalid baudrate
   }
   // Translate Byte size :
   if( Parameters.DataBits < 5 ||
       Parameters.DataBits > 8){
      return( false);    // invalid size
   }
   dcb.ByteSize = (BYTE)Parameters.DataBits;
   switch( Parameters.Parity){
      case NO_PARITY :
         dcb.Parity = NOPARITY;
         break;
      case ODD_PARITY :
         dcb.Parity = ODDPARITY;
         break;
      case EVEN_PARITY :
         dcb.Parity = EVENPARITY;
         break;
      case MARK_PARITY :
         dcb.Parity = MARKPARITY;
         break;
      case SPACE_PARITY :
         dcb.Parity = SPACEPARITY;
         break;
      IDEFAULT
   }
   switch( Parameters.StopBits){
      case 10 :
         dcb.StopBits = ONESTOPBIT;
         break;
      case 15 :
         dcb.StopBits = ONE5STOPBITS;
         break;
      case 20 :
         dcb.StopBits = TWOSTOPBITS;
         break;
      default :
         return( false);  // invalid stop bits count
   }
   // Translate Handshake :
   switch( Parameters.Handshake){
      case NO_HANDSHAKE :
         dcb.fInX         = false;
         dcb.fOutX        = false;
         dcb.fOutxCtsFlow = false;
         dcb.fOutxDsrFlow = false;
         dcb.fDtrControl  = DTR_CONTROL_ENABLE;
         dcb.fRtsControl  = RTS_CONTROL_ENABLE;
         dcb.fDsrSensitivity =  false;   // ignore DSR
         break;
      case HARDWARE_HANDSHAKE :
         dcb.fInX         = false;
         dcb.fOutX        = false;
         dcb.fOutxCtsFlow = true;
         dcb.fOutxDsrFlow = true;
         dcb.fDtrControl  = DTR_CONTROL_HANDSHAKE;
         dcb.fRtsControl  = RTS_CONTROL_HANDSHAKE;
         dcb.fDsrSensitivity =  true;   // trace DSR
         break;
      case XON_XOFF_HANDSHAKE :
         dcb.fInX         = true;
         dcb.fOutX        = true;
         dcb.fOutxCtsFlow = false;
         dcb.fOutxDsrFlow = false;
         dcb.fDtrControl  = DTR_CONTROL_ENABLE;
         dcb.fRtsControl  = RTS_CONTROL_ENABLE;
         dcb.fDsrSensitivity =  false;   // ignore DSR
         break;
      case HALF_DUPLEX_HANDSHAKE :
         dcb.fInX         = false;
         dcb.fOutX        = false;
         dcb.fOutxCtsFlow = false;
         dcb.fOutxDsrFlow = false;
         dcb.fDtrControl  = DTR_CONTROL_ENABLE;
         dcb.fRtsControl  = RTS_CONTROL_TOGGLE; // via RTS
         dcb.fDsrSensitivity =  false;          // ignore DSR
         break;
      IDEFAULT
   }
   // Other flags :
   dcb.fAbortOnError=FALSE;  // ignore invalid characters
   // New setup :
   if( !SetCommState( FHandle, &dcb)){
      return( false);
   }
   FParameters = Parameters;   // remember
   // Safe timimg :
   SafeTxTiming();
   SetRxNowait();
   return( true);
} // SetParameters

//******************************************************************************
// Get Parameters
//******************************************************************************

void TAsyUart::GetParameters( TParameters &Parameters)
// Get parameters of communication
{
   Parameters = FParameters;
} // GetParameters

//******************************************************************************
// Set Rx Timing
//******************************************************************************

void TAsyUart::SetRxNowait()
// Set timing of receiver - returns collected data immediately
{
   if( !IsOpen){
      IERROR;
   }
   ImmediateRead = true;         // remember setting
   COMMTIMEOUTS to;
   GetCommTimeouts( FHandle, &to);

   // return immediately if no chars present :
   to.ReadIntervalTimeout         =  MAXDWORD;
   to.ReadTotalTimeoutMultiplier  =  0;
   to.ReadTotalTimeoutConstant    =  0;

   SetCommTimeouts( FHandle, &to);
} // SetRxNowait

void TAsyUart::SetRxWait( int TotalTime, int IntercharacterTime)
// Set timing of receiver :
// If <IntercharacterTime> = 0 waits <ReplyTime> for whole message
// If <IntercharacterTime> > 0 waits <ReplyTime> for first
// character, next waits <IntercharacterTime> * count of characters
{
   if( !IsOpen){
      IERROR;
   }
   if( TotalTime == 0 && IntercharacterTime == 0){
      IERROR;
   }
   if( TotalTime <= IntercharacterTime){
      // too short for Windows OS
      TotalTime = 10 * IntercharacterTime;   // patch it
   }
   ImmediateRead = false;                    // remember setting
   COMMTIMEOUTS to;
   GetCommTimeouts( FHandle, &to);

   // intercharacter doesn't work properly, emulate :
   to.ReadIntervalTimeout         =  0;
   to.ReadTotalTimeoutMultiplier  =  IntercharacterTime;
   to.ReadTotalTimeoutConstant    =  TotalTime;

   SetCommTimeouts( FHandle, &to);
} // SetRxWait

//******************************************************************************
// Flush
//******************************************************************************

void TAsyUart::Flush()
// Make input/output queue empty
{
   if( !IsOpen){
      IERROR;
   }
   PurgeComm( FHandle,PURGE_RXCLEAR | PURGE_TXCLEAR);
} // Flush

#ifdef __PERSISTENT__
//******************************************************************************
// Load
//******************************************************************************

#define PLoadInteger( name)   FParameters.name = Memory->LoadInteger(#name);
#define PLoadIntegerCast( name, cast) \
                              FParameters.name = (cast)Memory->LoadInteger(#name);

bool TAsyUart::Load( TObjectMemory *Memory)
// Load setup from <Memory>
{
   TName FullName = GetFullName();
   if( !Memory->Locate( FullName)){
      return( false);  // unable to find
   }
   PLoadInteger( BaudRate);
   PLoadInteger( DataBits);
   PLoadInteger( StopBits);
   PLoadIntegerCast( Parity, TParity);
   PLoadIntegerCast( Handshake, THandshake);
   SetParameters( FParameters);
   return( true);
} // Load

//******************************************************************************
// Save
//******************************************************************************

#define PSaveInteger( name)  Memory->SaveInteger(#name,FParameters.name)

void TAsyUart::Save( TObjectMemory *Memory)
// Save setup to <Memory>
{
   TName FullName = GetFullName();
   Memory->Create( FullName);
   PSaveInteger( BaudRate);
   PSaveInteger( DataBits);
   PSaveInteger( StopBits);
   PSaveInteger( Parity);
   PSaveInteger( Handshake);
} // Save

//******************************************************************************
// Get Full Name
//******************************************************************************

TName TAsyUart::GetFullName()
// Get full device name
{
   TName FullName = SERIAL_NAME + "/" + Name;
   return( FullName);
} // GetFullName
#endif

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Name
//******************************************************************************

TName TAsyUart::GetName()
// Get device name by <Identifier>
{
   if( !IsOpen){
      return( UNKNOWN_NAME);
   }
   return( "COM" + TName( FIdentifier));
} // GetName

//******************************************************************************
// Property Is Open
//******************************************************************************

bool TAsyUart::GetIsOpen()
// Check if device is opened
{
   return( FIdentifier != INVALID_IDENTIFIER);
} // GetIsOpen

//******************************************************************************
// Property CTS
//******************************************************************************

bool TAsyUart::GetCTS()
{
   return( GetModemStatus() & MS_CTS_ON);
} // GetCTS

//******************************************************************************
// Property DSR
//******************************************************************************

bool TAsyUart::GetDSR()
{
   return( GetModemStatus() & MS_DSR_ON);
} // GetDSR

//******************************************************************************
// Property RI
//******************************************************************************

bool TAsyUart::GetRI()
{
   return( GetModemStatus() & MS_RING_ON);
} // GetRING

//******************************************************************************
// Property RLSD
//******************************************************************************

bool TAsyUart::GetDCD()
{
   return( GetModemStatus() & MS_RLSD_ON);
} // GetDCD

//******************************************************************************
// Property DTR
//******************************************************************************

void TAsyUart::SetDTR( bool Status)
{

   if( !IsOpen){
      IERROR;
   }
   EscapeCommFunction( FHandle,
                       Status ? SETDTR : CLRDTR);
   FDtr = Status;
} // SetDTR

//******************************************************************************
// Property RTS
//******************************************************************************

void TAsyUart::SetRTS( bool Status)
{
   if( !IsOpen){
      IERROR;
   }
   EscapeCommFunction( FHandle,
                       Status ? SETRTS : CLRRTS);
   FRts = Status;
} // SetRTS

//******************************************************************************
// Property TxD
//******************************************************************************

void TAsyUart::SetTxD( bool Status)
{
   if( !IsOpen){
      IERROR;
   }
   EscapeCommFunction( FHandle,
                       Status ? SETBREAK : CLRBREAK);
   FTxD = Status;
} // SetTxD

//******************************************************************************
// Tx timing
//******************************************************************************

void  TAsyUart::SafeTxTiming()
// Set safe timing of transmitter
{
   if( !IsOpen){
      IERROR;
   }
   COMMTIMEOUTS to;
   GetCommTimeouts( FHandle, &to);
   // Calculate via us :
   int CharTime;
   if( FParameters.BaudRate > 0 && FParameters.DataBits > 0){
      CharTime = (1000000 / FParameters.BaudRate) * FParameters.DataBits + 3;
      if( CharTime < 1){
         CharTime = 1;
      }
   } else {
      CharTime = 1;
   }
   // convert to ms :
   CharTime /= 1000;
   if( CharTime <= 0){
      CharTime = 1;
   }
   CharTime *= 2;   // security multiplier

   // maximum time for send :
   to.WriteTotalTimeoutMultiplier = CharTime;
   to.WriteTotalTimeoutConstant   = 100;
   SetCommTimeouts( FHandle, &to);
} // SafeTxTiming

//******************************************************************************
// Modem Status
//******************************************************************************

DWORD TAsyUart::GetModemStatus()
// Read modem status
{
   if( !IsOpen){
      return( 0);
   }
   DWORD d = 0;
   if( !GetCommModemStatus( FHandle, &d)){
      IERROR;
   }
   return( d);
} // GetModemStatus

//******************************************************************************
// Encrypt Name
//******************************************************************************

char *TAsyUart::ConvertComName( TIdentifier Identifier)
// Encrypt com name for OS
{
static char Name[ 8];

   if( Identifier < MIN_COM_NUMBER ||
       Identifier > MAX_COM_NUMBER){
      strcpy( Name, "COM?");
   } else if( Identifier <= 9){
      sprintf( Name, "COM%d", Identifier);
   } else {
      sprintf( Name, "\\\\.\\COM%d", Identifier);
   }
   return( Name);
} // ConvertComName

