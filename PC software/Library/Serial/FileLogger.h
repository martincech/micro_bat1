//******************************************************************************
//
//   FileLogger.h  Raw data file logger
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef FileLoggerH
   #define FileLoggerH

#ifndef RawDataH
   #include "Logger.h"
#endif

#include <stdio.h>

//******************************************************************************
// Class TFileLogger
//******************************************************************************

class TFileLogger : public TLogger
{
public:
   TFileLogger( char *Prefix);
   // Constructor
   virtual void Write( TDataType Type, void *Buffer, int Length);
   // Write data
   virtual void Report( char *Format, ...);
   // Write report

//------------------------------------------------------------------------------
protected :
   FILE         *File;          // file handle
   AnsiString    FPrefix;       // filename prefix
   int           FileHour;      // current hour of active file
   LARGE_INTEGER Frequency;     // counter frequency
   LARGE_INTEGER Counter;       // last operation time

   void BinaryWrite( char *Buffer, int Length);
   // Write data
   void NewFileName();
   // Check and create new file name
   void Timestamp();
   // Write timestamp
}; // TFileLogger

#endif

