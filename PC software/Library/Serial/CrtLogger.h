//******************************************************************************
//
//   CrtLogger.h     Raw data viewer
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef CrtLoggerH
   #define CrtLoggerH

#ifndef RawDataH
   #include "Logger.h"
#endif

#ifndef CrtH
   #include "../Crt/Crt.h"
#endif

//******************************************************************************
// Class TCrtLogger
//******************************************************************************

class TCrtLogger : public TLogger
{
public:
   // Display mode :
   typedef enum {
      BINARY, // binary dump
      TEXT,   // text dump
      MIXED,  // binary and text dump 
   } TDisplayMode;

   TCrtLogger( TCrt *Crt);
   // Constructor
   virtual void Write( TDataType Type, void *Buffer, int Length);
   // Write data
   virtual void Report( char *Format, ...);
   // Write report
   __property TDisplayMode Mode = {read=FMode,write=FMode};
   // Display mode

//------------------------------------------------------------------------------
protected :
   TCrt *Crt;          // Crt module
   TDisplayMode FMode;

   void BinaryWrite( char *Buffer, int Length);
   // Binary dump
   void TextWrite( char *Buffer, int Length);
   // Text dump
   void MixedWrite( char *Buffer, int Length);
   // Mixed dump
   void TextLineWrite( char *Line);
   // Write text line
   void MixedLineWrite( char *Line, int LineLength);
   // Write mixed line
}; // TRawData


extern TCrtLogger *Logger;    // global pointer

#endif

