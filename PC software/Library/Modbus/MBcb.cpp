//******************************************************************************
//
//   MBCb.c       Modbus callbacks
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MBCb.h"
#include "MBPacket.h"
#include "Workplace.h"
#include "MBPdu.h"

#include "Main.h"

//******************************************************************************
// Com Name
//******************************************************************************

TString MBCGetComName( void)
// Vrati nazev COMu
{
   return( Workplace->PortName);
} // MBCGetComName

//******************************************************************************
// Baud
//******************************************************************************

word MBCGetBaud( void)
// Vrati baudovou rychlost
{
   return( Workplace->Parameters->BaudRate);
} // MBCGetBaud

//-----------------------------------------------------------------------------
// Parity
//-----------------------------------------------------------------------------

byte MBCGetParity( void)
// Vrati paritu (viz MBCom.h)
{
   //>>> nacti konfiguraci
   if( Workplace->Parameters->DataBits == 8){
      switch( Workplace->Parameters->Parity){
         case TUart::NO_PARITY :
            return( COM_PARITY_NONE);
         case TUart::EVEN_PARITY :
            return( COM_PARITY_EVEN);
         case TUart::ODD_PARITY :
            return( COM_PARITY_ODD);
         case TUart::MARK_PARITY :
            return( COM_PARITY_MARK);
         default :
            return( COM_PARITY_NONE);
      }
   } else if( Workplace->Parameters->DataBits == 7){
      switch( Workplace->Parameters->Parity){
         case TUart::EVEN_PARITY :
            return( COM_PARITY_7BITS_EVEN);
         case TUart::ODD_PARITY :
            return( COM_PARITY_7BITS_ODD);
         case TUart::MARK_PARITY :
            return( COM_PARITY_7BITS_MARK);
         IDEFAULT
      }
   } else {
      return( COM_PARITY_NONE);
   }
} // MBCGetParity

//-----------------------------------------------------------------------------
// Celkovy timeout
//-----------------------------------------------------------------------------

word MBCGetTotalTimeout( void)
// Vrati timeout odpovedi v [ms]
{
   //>>> nacti konfiguraci
   return( Workplace->ReceiveTimeout);
} // MBCGetTimeout

//-----------------------------------------------------------------------------
// Meziznakovy timeout
//-----------------------------------------------------------------------------

word MBCGetIntercharacterTimeout( void)
// Vrati meziznakovy timeout v [ms]
{
   //>>> nacti konfiguraci
   return( Workplace->IntercharacterTimeout);
} // MBCGetTimeout

//-----------------------------------------------------------------------------
// Mode
//-----------------------------------------------------------------------------

byte MBCGetMode( void)
// Vrati mod komunikace (viz MBPacket.h)
{
   //>>> nacti konfiguraci
   return( MB_RTU_MODE);
//   return( MB_ASCII_MODE);
} // MBCGetMode

//-----------------------------------------------------------------------------
// Address
//-----------------------------------------------------------------------------

byte MBCGetSlaveAddress( void)
// Vrati adresu obsluhovaneho zarizeni
{
   //>>> nacti konfiguraci
   return( 0x01);
} // MBCGetOwnAddress

//******************************************************************************
// Error
//******************************************************************************

void MBCError( byte Code)
// Chybove hlaseni
{
   TString Message;
   switch( Code){
      case MBE_OK :
         Message = "OK";
         break;
      case MBE_PARITY :
         Message = "Parity error";
         break;
      case MBE_OVERRUN :
         Message = "Overrun error";
         break;
      case MBE_TIMEOUT :
         Message = "Timeout";
         break;
      case MBE_FRAME :
         Message = "Frame error";
         break;
      case MBE_WRONG_ADDRESS :
         Message = "Wrong reply address";
         break;
      case MBE_WRONG_FUNCTION :
         Message = "Wrong reply function";
         break;
      case MBE_WRONG_ITEM :
         Message = "Reply data doesn't match send data";
         break;
      case MBE_IMPLEMENTATION :
         Message = "Implementation error";
         break;
      IDEFAULT
   }
   MainForm->SetStatus( Message);  // zapis do stavove radky programu
} // MBCError

//******************************************************************************
// Exception
//******************************************************************************

void MBCException( byte Code)
// Nastala vyjimka
{
   TString Message;
   Message.printf( "Exception %x", Code);
//   Application->MessageBox( Message.c_str(), "Exception", MB_OK);
   MainForm->SetStatus( Message);  // zapis do stavove radky programu
} // MBCException

//******************************************************************************
// Read Discrete
//******************************************************************************

void MBCReadDiscrete( word Address, word Count, byte *Data)
// Precteny binarni hodnoty
{
   MainForm->SetStatus( "Read discrete OK");
} // MBCReadDiscrete

//******************************************************************************
// Read Coils
//******************************************************************************

void MBCReadCoils( word Address, word Count, byte *Data)
// Precteny binarni hodnoty
{
   MainForm->SetStatus( "Read coils OK");
} // MBCReadCoils

//******************************************************************************
// Read Input Registers
//******************************************************************************

void MBCReadInputRegisters( word Address, word Count, word *Data)
// Precteny 16bit hodnoty
{
   MainForm->SetStatus( "Read Input Registers OK");
} // MBCReadInputRegisters

//******************************************************************************
// Read Registers
//******************************************************************************

void MBCReadRegisters( word Address, word Count, word *Data)
// Precteny 16bit hodnoty
{
   MainForm->SetStatus( "Read registers OK");
} // MBCReadRegisters

//******************************************************************************
// Diagnostic Data
//******************************************************************************

void MBCDiagnosticData( word Data)
// Data vracena zarizenim
{
   TString Message;
   Message.printf( "Diagnostic data=%04hXh", Data);
   MainForm->SetStatus( Message);
} // MBCDiagnosticData

//******************************************************************************
// Diagnostic Counter
//******************************************************************************

void MBCDiagnosticCounter( word CounterCode, word CounterData)
// Vrati hodnotu diagnostickeho citace <CounterCode>, viz MBD_... kostanty
// hodnota citace je <CounterData>
{
   TString Message;
   Message.printf( "%hd", CounterData);
   switch( CounterCode){
      case MBD_TOTAL_COUNT :
         MainForm->EdtTotalCount->Text = Message;
         break;
      case MBD_ERROR_COUNT :
         MainForm->EdtErrorCount->Text = Message;
         break;
      case MBD_EXCEPTION_COUNT :
         MainForm->EdtExceptionCount->Text = Message;
         break;
      case MBD_MESSAGE_COUNT :
         MainForm->EdtMessageCount->Text = Message;
         break;
      case MBD_NO_RESPONSE_COUNT :
         MainForm->EdtNoResponseCount->Text = Message;
         break;
      case MBD_NAK_COUNT :
         Message.printf( "NAK Count=%hd", CounterData);
         break;
      case MBD_BUSY_COUNT :
         Message.printf( "Busy Count=%hd", CounterData);
         break;
      case MBD_OVERRUN_COUNT :
         MainForm->EdtOverrunCount->Text = Message;
         break;
      default :
         Message.printf( "Unknown counter %hd", CounterCode);
         break;
   }
   MainForm->SetStatus( Message);
} // MBDDiagnosticCounter

//******************************************************************************
// Slave Id
//******************************************************************************

void MBCSlaveId( word SlaveId, byte RunIndicator, byte *Data)
// Identifikace slave
{
   Data[ SID_ADDITIONAL_DATA_SIZE] = 0; // sorry, ComBuffer je dost dlouhy
   TString Message;
   Message.printf( "ID=%04hXh Run=%c Data=%s", SlaveId, RunIndicator ? 'Y' : 'N', Data);
   MainForm->SetStatus( Message);
} // MBCSlaveId

//******************************************************************************
// Vyrobce
//******************************************************************************

void MBCVendorId( byte *Data, byte Size)
// Identifikace vyrobce
{
   char Text[ 80];
   memcpy( Text, Data, Size);
   Text[ Size] = 0;
   MainForm->EdtVendor->Text = Text;
} // MBCVendorId

//******************************************************************************
// Produkt
//******************************************************************************

void MBCProductCode( byte *Data, byte Size)
// Identifikace produktu
{
   char Text[ 80];
   memcpy( Text, Data, Size);
   Text[ Size] = 0;
   MainForm->EdtProduct->Text = Text;
} // MBCProductCode

//******************************************************************************
// Verze
//******************************************************************************

void MBCRevisionString( byte *Data, byte Size)
// Cislo verze
{
   char Text[ 80];
   memcpy( Text, Data, Size);
   Text[ Size] = 0;
   MainForm->EdtRevision->Text = Text;
} // MBCRevision

