//******************************************************************************
//
//   MB.c         Modbus main functions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MB.h"
#include "MBPdu.h"
#include "MBPacket.h"
#include "MBCb.h"
#include "MBCom.h"

// kontextova pamet :
static byte LastFunction;
static word LastAddress;
static word LastCount;

// Lokalni funkce :
static void RxPacket( void);
// Zpracuje prijaty paket

#ifdef MB_ENABLE_DIAGNOSTIC
static void ReceiveDiagnostic( word SubFunction, word Data);
// Dekuduje diagnostiku
#endif // MB_ENABLE_DIAGNOSTIC
#ifdef MB_ENABLE_IDENTIFICATION
void ReceiveIdentification( TIdentificationReply *Identification, word Size);
// Dekoduje identifikaci
#endif // MB_ENABLE_IDENTIFICATION

//******************************************************************************
// Initialization
//******************************************************************************

void MBInitialize( void)
// Inicializace
{
word Baud;
byte Parity;
word TotalTimeout;
word IntercharacterTimeout;
byte Mode;
TString ComName;

   ComName               = MBCGetComName();
   Baud                  = MBCGetBaud();
   Parity                = MBCGetParity();
   TotalTimeout          = MBCGetTotalTimeout();
   IntercharacterTimeout = MBCGetIntercharacterTimeout();
   Mode                  = MBCGetMode();
   MBSetMode( ComName, Baud, Parity, TotalTimeout, IntercharacterTimeout, Mode);
   LastFunction = 0;
} // MBInitialize

//-----------------------------------------------------------------------------
// Exec
//-----------------------------------------------------------------------------

void MBExecute( void)
// Automat protokolu, volat co nejcasteji
{
   switch( ComStatus){
      case COM_STATUS_STOPPED :
         // zastaveno - stav po inicializaci
         break;
      case COM_STATUS_RX_RUNNING :
         // ceka se na data
         break;                        // ceka na Rx data
      case COM_STATUS_RX_PARITY :
         // paket s chybou parity
         ComStatus = COM_STATUS_STOPPED;
         MBCError( MBE_PARITY);
         break;
      case COM_STATUS_RX_OVERRUN :
         // paket s chybou prebehu
         ComStatus = COM_STATUS_STOPPED;
         MBCError( MBE_OVERRUN);
         break;
      case COM_STATUS_RX_DONE :
         // dokoncen ASCII paket
         ComStatus = COM_STATUS_STOPPED;
         RxPacket();                   // zpracovani paketu
         break;
      case COM_STATUS_RX_TIMEOUT :
         // ASCII timeout nebo RTU dokoncen paket
         ComStatus = COM_STATUS_STOPPED;
         if( ComDataSize == 0){
            MBCError( MBE_TIMEOUT);
            break;
         }
         if( MBIsAscii()){
            MBCError( MBE_TIMEOUT);
            break;                     // timeout
         }
         RxPacket();                   // zpracovani paketu
         break;
      case COM_STATUS_TX_RUNNING :
         break;                        // ceka na dokonceni Tx
      case COM_STATUS_TX_DONE :
         ComRxStart();                 // zahajit prijem
         break;
      default :
         break;
   }
} // MBExecute

//******************************************************************************
// Zapis funkcniho kodu
//******************************************************************************

void MBWriteFunction( byte Code)
// Zapise funkcni kod
{
   MBWriteByte( Code);
   LastFunction = Code;      // uschovej jako kontext
} // MBWriteFunction

//******************************************************************************
// Zapis adresy
//******************************************************************************

void MBWriteItemAddress( word Address)
// Zapise adresu veliciny
{
   MBWriteWord( Address);
   LastAddress = Address;
} // MBWriteItemAddress

//******************************************************************************
// Zapis poctu
//******************************************************************************

void MBWriteCount( word Count)
// Zapise pocet velicin
{
   MBWriteWord( Count);
   LastCount = Count;
} // MBWriteCount

//-----------------------------------------------------------------------------
// Rx paket
//-----------------------------------------------------------------------------

static void RxPacket( void)
// Zpracuje prijaty paket
{
TMBPduReply *Pdu;
word         Size;
byte         Address;
byte         FunctionCode;

   if( !MBReceive( (void **)&Pdu, &Size)){
      MBCError( MBE_FRAME);
      return;
   }
   Address = MBGetAddress();
   if( Address != MBCGetSlaveAddress()){
      MBCError( MBE_WRONG_ADDRESS);
      return;                          // cizi paket
   }
   FunctionCode = Pdu->Function;
   if( FunctionCode & 0x80){
      // exception
      MBCException( Pdu->Exception.Code);
      return;
   }
   if( FunctionCode != LastFunction){
      MBCError( MBE_WRONG_FUNCTION);
      return;                          // kod funkce odlisny od pozadavku
   }
//------------------------------------------------------------------------------
   switch( FunctionCode){
#ifdef MB_ENABLE_DISCRETE
      case MBF_READ_DISCRETE :
         if( Pdu->ReadDiscrete.ByteCount != MBBitsToBytes( LastCount)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( Size != (word)SizeofTReadDiscreteReply() + Pdu->ReadDiscrete.ByteCount){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         MBCReadDiscrete( LastAddress, LastCount, Pdu->ReadDiscrete.Data);
         break;
#endif // MB_ENABLE_DISCRETE
#ifdef MB_ENABLE_COILS
      case MBF_READ_COILS :
         if( Pdu->ReadCoils.ByteCount != MBBitsToBytes( LastCount)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( Size != (word)SizeofTReadCoilsReply() + Pdu->ReadCoils.ByteCount){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         MBCReadCoils( LastAddress, LastCount, Pdu->ReadCoils.Data);
         break;

      case MBF_WRITE_SINGLE_COIL :
         if( Size != sizeof( TWriteSingleCoilReply)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( LastAddress != MBGetWord( Pdu->WriteSingleCoil.Address)){
            MBCError( MBE_WRONG_ITEM);
            break;
         }
         MBCOk();
         break;

      case MBF_WRITE_MULTIPLE_COILS :
         if( Size != sizeof( TWriteCoilsReply)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( LastAddress != MBGetWord( Pdu->WriteCoils.Address)){
            MBCError( MBE_WRONG_ITEM);
            break;
         }
         if( LastCount != MBGetWord( Pdu->WriteCoils.Count)){
            MBCError( MBE_WRONG_ITEM);
            break;
         }
         MBCOk();
         break;
#endif // MB_ENABLE_COILS
#ifdef MB_ENABLE_INPUT_REGISTERS
      case MBF_READ_INPUT_REGISTERS :
         if( Pdu->ReadInputRegisters.ByteCount != 2 * LastCount){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( Size != (word)SizeofTReadInputRegistersReply() + Pdu->ReadInputRegisters.ByteCount){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         MBCReadInputRegisters( LastAddress, LastCount, Pdu->ReadInputRegisters.Data);
         break;
#endif // MB_ENABLE_INPUT_REGISTERS
#ifdef MB_ENABLE_REGISTERS
      case MBF_READ_HOLDING_REGISTERS :
         if( Pdu->ReadRegisters.ByteCount != 2 * LastCount){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( Size != (word)SizeofTReadRegistersReply() + Pdu->ReadRegisters.ByteCount){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         MBCReadRegisters( LastAddress, LastCount, Pdu->ReadRegisters.Data);
         break;

      case MBF_WRITE_SINGLE_REGISTER :
         if( Size != sizeof( TWriteSingleRegisterReply)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( LastAddress != MBGetWord( Pdu->WriteSingleRegister.Address)){
            MBCError( MBE_WRONG_ITEM);
            break;
         }
         MBCOk();
         break;

      case MBF_WRITE_MULTIPLE_REGISTERS :
         if( Size != sizeof( TWriteRegistersReply)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( LastAddress != MBGetWord( Pdu->WriteRegisters.Address)){
            MBCError( MBE_WRONG_ITEM);
            break;
         }
         if( LastCount != MBGetWord( Pdu->WriteRegisters.Count)){
            MBCError( MBE_WRONG_ITEM);
            break;
         }
         MBCOk();
         break;

      case MBF_MASK_WRITE_REGISTER :
         if( Size != sizeof( TMaskWriteRegisterReply)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( LastAddress != MBGetWord( Pdu->MaskWriteRegister.Address)){
            MBCError( MBE_WRONG_ITEM);
            break;
         }
         MBCOk();
         break;

      case MBF_RW_MULTIPLE_REGISTERS :
         if( Pdu->ReadWriteRegisters.ByteCount != 2 * LastCount){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         if( Size != (word)SizeofTReadWriteRegistersReply() + Pdu->ReadWriteRegisters.ByteCount){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         MBCReadRegisters( LastAddress, LastCount, Pdu->ReadWriteRegisters.Data);
         break;
#endif // MB_ENABLE_REGISTERS
#ifdef MB_ENABLE_DIAGNOSTIC
      case MBF_DIAGNOSTIC :
         if( Size != sizeof( TDiagnosticReply)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         ReceiveDiagnostic( MBGetWord( Pdu->Diagnostic.SubFunction), MBGetWord( Pdu->Diagnostic.Data));
         break;
#endif // MB_ENABLE_DIAGNOSTIC
#ifdef MB_ENABLE_SLAVE_ID
      case MBF_REPORT_SLAVE_ID :
         if( Size != sizeof( TSlaveIdReply)){
            MBCError( MBE_IMPLEMENTATION);
            break;
         }
         MBCSlaveId( MBGetWord( Pdu->SlaveId.SlaveId), Pdu->SlaveId.RunIndicator,
                     Pdu->SlaveId.Data);
         break;

#endif // MB_ENABLE_SLAVE_ID
#ifdef MB_ENABLE_IDENTIFICATION
      case MBF_READ_DEVICE_ID :
         ReceiveIdentification( &Pdu->Identification, Size);
         break;
#endif // MB_ENABLE_IDENTIFICATION

//------------------------------------------------------------------------------
      default :
         MBCError( MBE_IMPLEMENTATION);
         break;
   }
} // RxPacket

#ifdef MB_ENABLE_DIAGNOSTIC
static void ReceiveDiagnostic( word SubFunction, word Data)
// Dekuduje diagnostiku
{
   switch( SubFunction){
      case MBD_RETURN_QUERY_DATA :
         MBCDiagnosticData( Data);
         break;
      case MBD_RESTART_COMMUNICATION :
      case MBD_CLEAR_STATUS :
      case MBD_CLEAR_OVERRUN_COUNT :
         MBCOk();
         break;
      case MBD_TOTAL_COUNT :
      case MBD_ERROR_COUNT :
      case MBD_EXCEPTION_COUNT :
      case MBD_MESSAGE_COUNT :
      case MBD_NO_RESPONSE_COUNT :
      case MBD_NAK_COUNT :
      case MBD_BUSY_COUNT :
      case MBD_OVERRUN_COUNT :
         MBCDiagnosticCounter( SubFunction, Data);
         break;
      default :
         MBCError( MBE_IMPLEMENTATION);
         break;
   }
} // ReceiveDiagnostic
#endif // MB_ENABLE_DIAGNOSTIC

#ifdef MB_ENABLE_IDENTIFICATION
//******************************************************************************
// Identification
//******************************************************************************

void ReceiveIdentification( TIdentificationReply *Identification, word Size)
// Dekoduje identifikaci
{
byte        i;
TObjectData *Data;
byte        *p;

   if( Identification->MEIType != MB_MEI_DEVICE_IDENTIFICATION){
      MBCError( MBE_IMPLEMENTATION);
      return;
   }
   p = (byte *)Identification->Object;
   for( i = 0; i < Identification->NumberOfObjects; i++){
      Data = (TObjectData *)p;
      switch( Data->Id){
         case MEI_OBJECT_VENDOR :
            MBCVendorId( Data->Data, Data->Length);
            break;
         case MEI_OBJECT_PRODUCT_CODE :
            MBCProductCode( Data->Data, Data->Length);
            break;
         case MEI_OBJECT_REVISION :
            MBCRevisionString( Data->Data, Data->Length);
            break;
         default :
            break;      // ostatni kody nekdy jindy
      }
      p += SizeofTObjectData() + Data->Length;
   }
} // ReceiveIdentification
#endif // MB_ENABLE_IDENTIFICATION

