//******************************************************************************
//
//   MBSIdent.c   Modbus send Identification request
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MB.h"
#include "MBPdu.h"
#include "MBPacket.h"
#include "MBCb.h"
#include "MBCom.h"

//******************************************************************************
// Slave ID
//******************************************************************************

void MBGetIdentification( void)
// Vysle dotaz na identifikaci
{
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_READ_DEVICE_ID);
   MBWriteByte( MB_MEI_DEVICE_IDENTIFICATION); // MEI
   MBWriteByte( MEI_BASIC_IDENTIFICATION);     // Read device Id Code
   MBWriteByte( MEI_OBJECT_VENDOR);            // Object Id
   MBWriteCrc();
   ComTxStart();
} // MBGetIdentification

 