//******************************************************************************
//
//   MBSSid.c    Modbus send Slave ID request
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MB.h"
#include "MBPdu.h"
#include "MBPacket.h"
#include "MBCb.h"
#include "MBCom.h"

//******************************************************************************
// Slave ID
//******************************************************************************

void MBGetSlaveId( void)
// Vysle dotaz na identifikaci
{
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_REPORT_SLAVE_ID);
   MBWriteCrc();
   ComTxStart();
} // MBGetSlaveId

