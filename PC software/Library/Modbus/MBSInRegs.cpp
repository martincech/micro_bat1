//******************************************************************************
//
//   MBSInRegs.c   Modbus send input register request
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MB.h"
#include "MBPdu.h"
#include "MBPacket.h"
#include "MBCb.h"
#include "MBCom.h"

//******************************************************************************
// Read Registers
//******************************************************************************

void MBReadInputRegisters( word Address, word Count)
// Cteni stavu
{
   if( Count > MB_MAX_READ_REGISTERS_COUNT){
      IERROR;
   }
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_READ_INPUT_REGISTERS);
   MBWriteItemAddress( Address);
   MBWriteCount( Count);
   MBWriteCrc();
   ComTxStart();
} // MBReadInputRegisters

 