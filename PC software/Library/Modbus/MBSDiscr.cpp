//******************************************************************************
//
//   MBSDiscr.c   Modbus send discrete request
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MB.h"
#include "MBPdu.h"
#include "MBPacket.h"
#include "MBCb.h"
#include "MBCom.h"

//******************************************************************************
// Read Discrete
//******************************************************************************

void MBReadDiscrete( word Address, word Count)
// Cteni stavu
{
   if( Count > MB_MAX_READ_DISCRETE_COUNT){
      IERROR;
   }
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_READ_DISCRETE);
   MBWriteItemAddress( Address);
   MBWriteCount( Count);
   MBWriteCrc();
   ComTxStart();
} // MBReadDiscrete

