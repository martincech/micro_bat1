//******************************************************************************
//
//   MBSRegs.c    Modbus send register request
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MB.h"
#include "MBPdu.h"
#include "MBPacket.h"
#include "MBCb.h"
#include "MBCom.h"

//******************************************************************************
// Read Registers
//******************************************************************************

void MBReadRegisters( word Address, word Count)
// Cteni stavu
{
   if( Count > MB_MAX_READ_REGISTERS_COUNT){
      IERROR;
   }
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_READ_HOLDING_REGISTERS);
   MBWriteItemAddress( Address);
   MBWriteCount( Count);
   MBWriteCrc();
   ComTxStart();
} // MBReadRegisters

//******************************************************************************
// Write Single Register
//******************************************************************************

void MBWriteSingleRegister( word Address, word Value)
// Vysle zapis
{
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_WRITE_SINGLE_REGISTER);
   MBWriteItemAddress( Address);
   MBWriteWord( Value);
   MBWriteCrc();
   ComTxStart();
} // MBWriteSingleRegister

//******************************************************************************
// Write Registers
//******************************************************************************

void MBWriteRegisters( word Address, word Count, word *Data)
// Zapis pole hodnot
{
byte i;

   if( Count > MB_MAX_WRITE_REGISTERS_COUNT){
      IERROR;
   }
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_WRITE_MULTIPLE_REGISTERS);
   MBWriteItemAddress( Address);
   MBWriteCount( Count);
   MBWriteByte( 2 * Count);            // ByteCount
   for( i = 0; i < Count; i++){
      MBWriteWord( Data[ i]);
   }
   MBWriteCrc();
   ComTxStart();
} // MBWriteRegisters

//******************************************************************************
// Mask Write Single Register
//******************************************************************************

void MBMaskWriteRegister( word Address, word AndMask, word OrMask)
// Vysle zapis masky
{
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_MASK_WRITE_REGISTER);
   MBWriteItemAddress( Address);
   MBWriteWord( AndMask);
   MBWriteWord( OrMask);
   MBWriteCrc();
   ComTxStart();
} // MBMaskWriteRegister

//******************************************************************************
// Read/Write Registers
//******************************************************************************

void MBReadWriteRegisters( word ReadAddress,  word ReadCount,
                           word WriteAddress, word WriteCount, word *Data)
// Zapis pole hodnot, cteni pole hodnot
{
byte ByteCount;
byte i;

   if( ReadCount > MB_MAX_RW_READ_REGISTERS_COUNT){
      IERROR;
   }
   if( WriteCount > MB_MAX_RW_WRITE_REGISTERS_COUNT){
      IERROR;
   }
   //!!! spocitat skutecnou delku
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_RW_MULTIPLE_REGISTERS);
   MBWriteItemAddress( ReadAddress);
   MBWriteCount( ReadCount);
   MBWriteWord( WriteAddress);
   MBWriteWord( WriteCount);
   MBWriteByte( 2 * WriteCount);            // WriteByteCount
   for( i = 0; i < WriteCount; i++){
      MBWriteWord( Data[ i]);
   }
   MBWriteCrc();
   ComTxStart();
} // MBReadWriteRegisters
