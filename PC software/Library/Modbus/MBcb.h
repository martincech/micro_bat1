//******************************************************************************
//
//   MBCb.h       Modbus callbacks
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __MBCb_H__
   #define __MBCb_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Chybove kody :
typedef enum {
   MBE_OK,                // bez chyby
   MBE_PARITY,            // chyba parity
   MBE_OVERRUN,           // chyba prebehu
   MBE_TIMEOUT,           // timeout odpovedi
   MBE_FRAME,             // chyba ramce (CRC)
   MBE_WRONG_ADDRESS,     // odpoved s odlisnou adresou
   MBE_WRONG_FUNCTION,    // odpoved s odlisnym kodem funkce
   MBE_WRONG_ITEM,        // chyba polozky odpovedi
   MBE_IMPLEMENTATION,    // implementacni chyba
   _MBE_LAST
} TMBError;

// Parametry komunikace --------------------------------------------------------

TString MBCGetComName( void);
// Vrati nazev COMu

word MBCGetBaud( void);
// Vrati baudovou rychlost

byte MBCGetParity( void);
// Vrati paritu (viz MBCom.h)

word MBCGetTotalTimeout( void);
// Vrati timeout odpovedi v [ms]

word MBCGetIntercharacterTimeout( void);
// Vrati meziznakovy timeout v [ms]

byte MBCGetMode( void);
// Vrati mod komunikace (viz MBPacket.h)

// Adresa obsluhovaneho zarizeni -----------------------------------------------

byte MBCGetSlaveAddress( void);
// Vrati adresu obsluhovaneho zarizeni

// Chybove hlaseni -------------------------------------------------------------

void MBCError( byte Code);
// Chybove hlaseni

#define MBCOk() MBCError( MBE_OK);
// Uspesne ukonceni operace

// Exception -------------------------------------------------------------------

void MBCException( byte Code);
// Nastala vyjimka


#ifdef MB_ENABLE_DISCRETE
// DIscrete --------------------------------------------------------------------

void MBCReadDiscrete( word Address, word Count, byte *Data);
// Precteny binarni hodnoty

#endif // MB_ENABLE_DISCRETE
#ifdef MB_ENABLE_COILS
// Coils -----------------------------------------------------------------------

void MBCReadCoils( word Address, word Count, byte *Data);
// Precteny binarni hodnoty

#endif // MB_ENABLE_COILS
#ifdef MB_ENABLE_INPUT_REGISTERS
// Input Registers -------------------------------------------------------------

void MBCReadInputRegisters( word Address, word Count, word *Data);
// Precteny 16bit hodnoty

#endif // MB_ENABLE_INPUT_REGISTERS
#ifdef MB_ENABLE_REGISTERS
// Holding Registers -----------------------------------------------------------

void MBCReadRegisters( word Address, word Count, word *Data);
// Precteny 16bit hodnoty

#endif // MB_ENABLE_REGISTERS
#ifdef MB_ENABLE_DIAGNOSTIC
// Diagnostic ------------------------------------------------------------------

void MBCDiagnosticData( word Data);
// Data vracena zarizenim

void MBCDiagnosticCounter( word CounterCode, word CounterData);
// Vrati hodnotu diagnostickeho citace <CounterCode>, viz MBD_... kostanty,
// hodnota citace je <CounterData>

#endif // MB_ENABLE_DIAGNOSTIC
#ifdef MB_ENABLE_SLAVE_ID
// Slave ID --------------------------------------------------------------------

void MBCSlaveId( word SlaveId, byte RunIndicator, byte *Data);
// Identifikace slave

#endif // MB_ENABLE_SLAVE_ID
#ifdef MB_ENABLE_IDENTIFICATION
// Idenfication ----------------------------------------------------------------

void MBCVendorId( byte *Data, byte Size);
// Identifikace vyrobce

void MBCProductCode( byte *Data, byte Size);
// Identifikace produktu

void MBCRevisionString( byte *Data, byte Size);
// Cislo verze

#endif // MB_ENABLE_IDENTIFICATION

#endif

 