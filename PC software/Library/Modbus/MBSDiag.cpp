//******************************************************************************
//
//   MBSDiag.c    Modbus send diagnostic request
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MB.h"
#include "MBPdu.h"
#include "MBPacket.h"
#include "MBCb.h"
#include "MBCom.h"

static void SendDiagnostic( word SubFunction, word Data);
// Vysle diagnosticky paket

//******************************************************************************
// Ping
//******************************************************************************

void MBReturnData( word Data)
// Vysle diagnosticka data
{
   SendDiagnostic( MBD_RETURN_QUERY_DATA, Data);
} // MBReturnData

//******************************************************************************
// Restart
//******************************************************************************

void MBRestartCommunication( void)
// Restart komunikace
{
   SendDiagnostic( MBD_RESTART_COMMUNICATION, 0);
} // MBRestartCommunication

//******************************************************************************
// Clear
//******************************************************************************

void MBClearStatus( void)
// Nulovani citacu paketu
{
   SendDiagnostic( MBD_CLEAR_STATUS, 0);
} // MBClearStatus

//******************************************************************************
// Total Count
//******************************************************************************

void MBReadTotalCount( void)
// Vrati celkovy pocet paketu
{
   SendDiagnostic( MBD_TOTAL_COUNT, 0);
} // MBReadTotalCount

//******************************************************************************
// Error Count
//******************************************************************************

void MBReadErrorCount( void)
// Vrati pocet chybonych paketu
{
   SendDiagnostic( MBD_ERROR_COUNT, 0);
} // MBReadErrorCount

//******************************************************************************
// Exception Count
//******************************************************************************

void MBReadExceptionCount( void)
// Vrati pocet vyjimek
{
   SendDiagnostic( MBD_EXCEPTION_COUNT, 0);
} // MBReadExceptionCount

//******************************************************************************
// Pocet zprav
//******************************************************************************

void MBReadMessageCount( void)
// Vrati pocet zpracovanych paketu
{
   SendDiagnostic( MBD_MESSAGE_COUNT, 0);
} // MBReadMessageCount

//******************************************************************************
// Zpravy bez odezvy
//******************************************************************************

void MBReadNoResponseCount( void)
// Vrati pocet paketu bez odpovedi
{
   SendDiagnostic( MBD_NO_RESPONSE_COUNT, 0);
} // MBReadNoResponseCount

//******************************************************************************
// NAK Count
//******************************************************************************

void MBReadNakCount( void)
// Vrati pocet prikazu s negativni odpovedi
{
   SendDiagnostic( MBD_NAK_COUNT, 0);
} // MBReadNakCount

//******************************************************************************
// NAK Count
//******************************************************************************

void MBReadBusyCount( void)
// Vrati pocet busy prikazu
{
   SendDiagnostic( MBD_BUSY_COUNT, 0);
} // MBReadBusyCount

//******************************************************************************
// Total Count
//******************************************************************************

void MBReadOverrunCount( void)
// Vrati pocet prebehu
{
   SendDiagnostic( MBD_OVERRUN_COUNT, 0);
} // MBReadOverrunCount

//******************************************************************************
// Total Count
//******************************************************************************

void MBClearOverrunCount( void)
// Nuluje pocet prebehu
{
   SendDiagnostic( MBD_CLEAR_OVERRUN_COUNT, 0);
} // MBClearOverrunCount

//******************************************************************************
// Send Diagnostic
//******************************************************************************

static void SendDiagnostic( word SubFunction, word Data)
// Vysle diagnosticky paket
{
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_DIAGNOSTIC);
   MBWriteWord( SubFunction);
   MBWriteWord( Data);
   MBWriteCrc();
   ComTxStart();
} // SendDiagnostic

 