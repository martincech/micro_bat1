//******************************************************************************
//
//   MB.h         Modbus main functions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __MB_H__
   #define __MB_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//******************************************************************************
// Hlavni funkce
//******************************************************************************

void MBInitialize( void);
// Inicializace

void MBExecute( void);
// Automat protokolu, volat co nejcasteji

// Pomocne funkce --------------------------------------------------------------

void MBWriteFunction( byte Code);
// Zapise funkcni kod

void MBWriteItemAddress( word Address);
// Zapise adresu veliciny

void MBWriteCount( word Count);
// Zapise pocet velicin

#ifdef MB_ENABLE_DISCRETE
//******************************************************************************
// Discrete
//******************************************************************************

void MBReadDiscrete( word Address, word Count);
// Cteni stavu

#endif // MB_ENABLE_DISCRETE
#ifdef MB_ENABLE_COILS
//******************************************************************************
// Coils
//******************************************************************************

void MBReadCoils( word Address, word Count);
// Cteni stavu

void MBWriteSingleCoil( word Address, byte Value);
// Vysle zapis

void MBWriteCoils( word Address, word Count, byte *Data);
// Zapis pole hodnot

#endif // MB_ENABLE_COILS
#ifdef MB_ENABLE_INPUT_REGISTERS
//******************************************************************************
// Input Registers
//******************************************************************************

void MBReadInputRegisters( word Address, word Count);
// Cteni stavu

#endif // MB_ENABLE_INPUT_REGISTERS
#ifdef MB_ENABLE_REGISTERS
//******************************************************************************
// Registers
//******************************************************************************

void MBReadRegisters( word Address, word Count);
// Cteni 16bit hodnot

void MBWriteSingleRegister( word Address, word Value);
// Vysle zapis

void MBWriteRegisters( word Address, word Count, word *Data);
// Zapis pole hodnot

void MBMaskWriteRegister( word Address, word AndMask, word OrMask);
// Vysle zapis masky

void MBReadWriteRegisters( word ReadAddress,  word ReadCount,
                           word WriteAddress, word WriteCount, word *Data);
// Zapis pole hodnot, cteni pole hodnot

#endif // MB_ENABLE_REGISTERS
#ifdef MB_ENABLE_DIAGNOSTIC
//******************************************************************************
// Diagnostic
//******************************************************************************

void MBReturnData( word Data);
// Vysle diagnosticka data

void MBRestartCommunication( void);
// Restart komunikace

void MBClearStatus( void);
// Nulovani citacu paketu

void MBReadTotalCount( void);
// Vrati celkovy pocet paketu

void MBReadErrorCount( void);
// Vrati pocet chybonych paketu

void MBReadExceptionCount( void);
// Vrati pocet vyjimek

void MBReadMessageCount( void);
// Vrati pocet zpracovanych paketu

void MBReadNoResponseCount( void);
// Vrati pocet paketu bez odpovedi

void MBReadNakCount( void);
// Vrati pocet prikazu s negativni odpovedi

void MBReadBusyCount( void);
// Vrati pocet busy prikazu

void MBReadOverrunCount( void);
// Vrati pocet prebehu

void MBClearOverrunCount( void);
// Nuluje pocet prebehu

#endif // MB_ENABLE_DIAGNOSTIC

#ifdef MB_ENABLE_SLAVE_ID
//******************************************************************************
// Slave ID
//******************************************************************************

void MBGetSlaveId( void);
// Vysle dotaz na identifikaci

#endif // MB_ENABLE_SLAVE_ID
#ifdef MB_ENABLE_IDENTIFICATION
//******************************************************************************
// Identification
//******************************************************************************

void MBGetIdentification( void);
// Vysle dotaz na identifikaci
#endif // MB_ENABLE_IDENTIFICATION

#endif
