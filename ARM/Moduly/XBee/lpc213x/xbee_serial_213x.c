#include "xbee/platform.h"
#include "xbee/serial.h"
#include "Hardware.h"

#ifdef UCONIO_COM0
   #define __COM0__
      #include "../inc/Com.h"
   #define COM_BAUD    UART0_BAUD
   #define COM_FORMAT  UART0_FORMAT
   #define COM_TIMEOUT UART0_TIMEOUT
   #define COM_NAME    "0"
#else // UCONIO_COM1
   #define __COM1__
      #include "../inc/Com.h"
   #define COM_BAUD    UART1_BAUD
   #define COM_FORMAT  UART1_FORMAT
   #define COM_TIMEOUT UART1_TIMEOUT
   #define COM_NAME    "1"
#endif

bool_t xbee_ser_invalid( xbee_serial_t *serial)
{
  return 0;
}

const char *xbee_ser_portname( xbee_serial_t *serial)
{
   return COM_NAME;
}

int xbee_ser_write( 
  xbee_serial_t *serial, 
  const void FAR *buffer, 
  int length)
{
int i;
  
   for(i = 0; i < length; i++){
      xbee_ser_putchar(serial, ((const uint8_t*)buffer)[i]);
   }
   return length;
}

int xbee_ser_read( xbee_serial_t *serial, void FAR *buffer, int bufsize)
{
int i;
byte Ch;  

   for(i = 0; i < bufsize; i++){
      if(!ComRxChar( &Ch)){break;}
      ((byte*)buffer)[i] = Ch;
   }
   return i; 
}


int xbee_ser_putchar( xbee_serial_t *serial, uint8_t ch)
{
   ComTxChar(ch);
   return 0;
}

int xbee_ser_getchar( xbee_serial_t *serial)
{
   byte Ch;
   ComRxChar( &Ch);
   return Ch;
}

int xbee_ser_tx_free( xbee_serial_t *serial)
{
  return INT_MAX;
}

int xbee_ser_tx_used( xbee_serial_t *serial)
{
  return 0;
}

int xbee_ser_tx_flush( xbee_serial_t *serial)
{
   return 0;
}

int xbee_ser_rx_free( xbee_serial_t *serial)
{
  return INT_MAX;
}

int xbee_ser_rx_used( xbee_serial_t *serial)
{
  return 0;
}

int xbee_ser_rx_flush( xbee_serial_t *serial)
{
   return 0;
}

int xbee_ser_open( xbee_serial_t *serial, uint32_t baudrate)
{
   ComSetup( baudrate, serial->communicationFormat, COM_TIMEOUT);
   serial->baudrate = baudrate;
   //ComSetup( 9600, COM_8BITS, COM_TIMEOUT);
   //serial->baudrate = 9600;
   return 0;
}

int xbee_ser_baudrate( xbee_serial_t *serial, uint32_t baudrate)
{
   return xbee_ser_open(serial, baudrate);
}

int xbee_ser_close( xbee_serial_t *serial)
{
   ComDisconnect();
   return 0;
}

int xbee_ser_break( xbee_serial_t *serial, bool_t enabled)
{     
   return 0;
}

int xbee_ser_flowcontrol( xbee_serial_t *serial, bool_t enabled)
{
   return 0;
}

int xbee_ser_set_rts( xbee_serial_t *serial, bool_t asserted)
{
   return 0;
}

int xbee_ser_get_cts( xbee_serial_t *serial)
{
   if(XBeeCts()) {
      return 0;
   }
   return 1;
}