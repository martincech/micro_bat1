#include "xbee/platform.h"
#include "../../inc/System.h"
#include "../../inc/uconio.h"



uint32_t (xbee_seconds_timer)( void) {
   return SysTime() / 1000;
}

uint32_t (xbee_millisecond_timer)( void) {
   return SysTime();
}