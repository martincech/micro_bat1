//*****************************************************************************
//
//    XBee.c     XBee
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Hardware.h"           // podminena kompilace
#include "XBee.h"
#include "xbee/device.h"
#include "xbee/wpan.h"
#include "wpan/aps.h"
#include "xbee/transparent_serial.h"
#include "xbee/discovery.h"
#include "zigbee/zdo.h"
#include "xbee/atcmd.h"

typedef enum {
   XBEE        = 0x19,
   XBEE_PRO    = 0x1A
} EXBeeVersion;

static xbee_dev_t my_xbee;
static xbee_serial_t xbee_serport;
static rxhandler handler;
static int State;
static int Rssi;
static TYesNo UpdateRssi = YES;
static EXBeeVersion XBeeVersion = XBEE;

#define RSSI_MAX (XBeeVersion == XBEE_PRO? 0x58 : 0x5C)
#define RSSI_MIN 0x1A

static int VrCallback( const xbee_cmd_response_t FAR *response);
// Firmware version callback

static int HvCallback( const xbee_cmd_response_t FAR *response);
// Hardware version callback

static int DbCallback( const xbee_cmd_response_t FAR *response);
// Rssi callback

static int TxFrameHandler( xbee_dev_t *xbee, const void FAR *raw, uint16_t length, void FAR *context);
// TX status handler

static int RxFrameHandler( xbee_dev_t *xbee, const void FAR *raw, uint16_t length, void FAR *context);
// Rx frame handler

// function receiving data on transparent serial cluster when ATAO != 0
static int transparent_rx( const wpan_envelope_t FAR *envelope, void FAR *context)
{
    return 0;
}

// must be sorted by cluster ID
const wpan_cluster_table_entry_t digi_data_clusters[] =
{
    // transparent serial goes here (cluster 0x0011)
    { DIGI_CLUST_SERIAL, transparent_rx, NULL,
        WPAN_CLUST_FLAG_INOUT | WPAN_CLUST_FLAG_NOT_ZCL },

    // handle join notifications (cluster 0x0095) when ATAO is not 0
    XBEE_DISC_DIGI_DATA_CLUSTER_ENTRY,

    WPAN_CLUST_ENTRY_LIST_END
};

/// Used to track ZDO transactions in order to match responses to requests
/// (#ZDO_MATCH_DESC_RESP).
wpan_ep_state_t zdo_ep_state = { 0 };

const wpan_endpoint_table_entry_t sample_endpoints[] = {
    ZDO_ENDPOINT(zdo_ep_state),

    // Endpoint/cluster for transparent serial and OTA command cluster
    {	WPAN_ENDPOINT_DIGI_DATA,		// endpoint
        WPAN_PROFILE_DIGI,				// profile ID
        NULL,									// endpoint handler
        NULL,									// ep_state
        0x0000,								// device ID
        0x00,									// version
        digi_data_clusters				// clusters
    },
    { WPAN_ENDPOINT_END_OF_LIST }
};

const xbee_dispatch_table_entry_t xbee_frame_handlers[] =
{
    XBEE_FRAME_HANDLE_LOCAL_AT,

    XBEE_FRAME_HANDLE_REMOTE_AT,

    XBEE_FRAME_HANDLE_ATND_RESPONSE,		// for processing ATND responses

    // this entry is for when ATAO is not 0
    XBEE_FRAME_HANDLE_RX_EXPLICIT,		// rx messages via endpoint table

    // next two entries are used when ATAO is 0
    XBEE_FRAME_HANDLE_AO0_NODEID,			// for processing NODEID messages
    { XBEE_FRAME_RECEIVE, 0, RxFrameHandler, NULL },		// rx messages direct
    { XBEE_FRAME_TRANSMIT_STATUS, 0, TxFrameHandler, NULL },		// rx messages direct
    XBEE_FRAME_TABLE_END
};

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void XBeeInit( void)
// Init XBEE
{
   handler = NULL;
   State = XBEE_INIT;
   XBeePortInit();
   XBeeWake();

   xbee_serport.baudrate = Config.Printer.CommunicationSpeed;
   xbee_serport.communicationFormat = Config.Printer.CommunicationFormat;
   xbee_dev_init(&my_xbee, &xbee_serport, NULL, NULL);
   xbee_wpan_init( &my_xbee, sample_endpoints);
   //XBeeSleep();
} // XBeeInit

//-----------------------------------------------------------------------------
// Append
//-----------------------------------------------------------------------------
void XBeeSend(byte *buf, unsigned int length)
// send data over XBEE network to network coordinator
{
   if(State != XBEE_CONNECTED) {
      return;
   }
   XBeeWake();
   if(buf == NULL || length == 0){ return; }
   wpan_envelope_t env;
   wpan_envelope_create( &env, &my_xbee.wpan_dev, WPAN_IEEE_ADDR_COORDINATOR, WPAN_NET_ADDR_COORDINATOR);
   env.payload = buf;
   env.length = length;
   xbee_transparent_serial( &env);
} // XBeeSend

//-----------------------------------------------------------------------------
// State
//-----------------------------------------------------------------------------

int XBeeState( void)
// State
{
   return State;
} // XBeeState

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void XBeeExecute( void)
// Execute
{
int16_t h1;
   xbee_dev_tick( &my_xbee);
   xbee_cmd_tick();

   switch(State) {
      case XBEE_INIT:
         h1=xbee_cmd_create(&my_xbee,"HV");
         xbee_cmd_set_callback(h1, HvCallback, 0);
         if(xbee_cmd_send(h1) != 0) {
            break;
         }
         State = XBEE_WAITING_INIT;
         break;
      case XBEE_CONNECT:
         Rssi = 0;
         h1=xbee_cmd_create(&my_xbee,"VR");
         xbee_cmd_set_target(h1,WPAN_IEEE_ADDR_COORDINATOR,WPAN_NET_ADDR_COORDINATOR);
         xbee_cmd_set_callback(h1, VrCallback, 0);
         if(xbee_cmd_send(h1) != 0) {
            break;
         }

         State = XBEE_WAITING_CONNECTION;
         break;

      case XBEE_WAITING_CONNECTION:
      case XBEE_WAITING_INIT:
         break;

      case XBEE_CONNECTED:
         if(UpdateRssi) {
            UpdateRssi = NO;
            h1=xbee_cmd_create(&my_xbee,"DB");
            xbee_cmd_set_callback(h1, DbCallback, 0);
            if(xbee_cmd_send(h1) != 0) {
               break;
            }
         }
         break;
   }
} // XBeeExecute

//-----------------------------------------------------------------------------
// Rssi
//-----------------------------------------------------------------------------

int XBeeRssiGet( void)
// Rssi
{
   UpdateRssi = YES;
   return Rssi;
} // XBeeRssiGet

int XBeeRssiGetPercentual( void)
// Rssi 0-100%
{
   return (128 * 100/(RSSI_MAX-RSSI_MIN)) * (RSSI_MAX - XBeeRssiGet()) / 128;
}

TYesNo XBeeRegisterRxEvent(rxhandler h)
// register event which will be invoked after some data received from XBee network
{
   if(h == NULL){
      return NO;
   }
   handler = h;
   return YES;
}

TYesNo XBeeUnregisterRxEvent(rxhandler h)
// unregister event to be invoked after some data arrived
{
   if(handler == h){
      handler = NULL;
      return YES;
   }
   return handler == NULL? YES : NO;
}



static int VrCallback( const xbee_cmd_response_t FAR *response)
// Firmware version callback
{
   if(response->flags & XBEE_CMD_RESP_FLAG_TIMEOUT) {
      State = XBEE_CONNECT;
      return XBEE_ATCMD_DONE;
   }
   if(response->value_length != 2) {
      State = XBEE_CONNECT;
      return XBEE_ATCMD_DONE;
   }

   State = XBEE_CONNECTED;
   return XBEE_ATCMD_DONE;
} // VrCallback


static int HvCallback( const xbee_cmd_response_t FAR *response)
// Hardware version callback
{
   if(response->flags & XBEE_CMD_RESP_FLAG_TIMEOUT) {
      State = XBEE_INIT;
      return XBEE_ATCMD_DONE;
   }
   if(response->value_length != 2) {
      State = XBEE_INIT;
      return XBEE_ATCMD_DONE;
   }

   XBeeVersion = response->value_bytes[0] == XBEE ? XBEE : XBEE_PRO;
   State = XBEE_CONNECT;
   return XBEE_ATCMD_DONE;
} // HvCallback

static int DbCallback( const xbee_cmd_response_t FAR *response)
// Rssi callback
{
   if(response->flags & XBEE_CMD_RESP_FLAG_TIMEOUT) {
      State = XBEE_CONNECT;
      return XBEE_ATCMD_DONE;
   }
   if(response->value_length != 1) {
      return XBEE_ATCMD_DONE;
   }

   Rssi = response->value_bytes[0];

   return XBEE_ATCMD_DONE;
} // DbCallback

static int TxFrameHandler( xbee_dev_t *xbee, const void FAR *raw, uint16_t length, void FAR *context)
// TX status handler
{
   const xbee_frame_transmit_status_t FAR *TxStatusFrame = raw;

   if(TxStatusFrame->frame_type != XBEE_FRAME_TRANSMIT_STATUS) {
      return 0;
   }

   if(TxStatusFrame->delivery != XBEE_TX_DELIVERY_SUCCESS) {
      State = XBEE_CONNECT;
      return 0;
   }

   return 0;
} // TxFrameHandler 

static int RxFrameHandler( xbee_dev_t *xbee, const void FAR *raw, uint16_t length, void FAR *context)
// Rx frame handler
{
const xbee_frame_receive_t FAR *rx_frame = raw;
int DataLength = length - offsetof( xbee_frame_receive_t, payload); // for checksum
   
   if (handler == NULL || DataLength < 0) {
     return 0;
   }  
   handler(*(uint64_t*) rx_frame->ieee_address.l, rx_frame->payload, DataLength);
   return DataLength;
} // RxFrameHandler