//*****************************************************************************
//
//    platform_lpc123x.h   LPC213x platform specific header for XBee
//    Version 1.0  (c) Veit electronics
//
//*****************************************************************************
#ifndef __XBEE_PLATFORM_LPC213x
  #define __XBEE_PLATFORM_LPC213x
#include <stdint.h>
#include "Hardware.h"
#include <limits.h>

// This type isn't in stdint.h
typedef uint8_t			bool_t;

// the "FAR" modifier is not used
#define FAR
#define INT16_C(x)      (x)

#define PACKED_UNION		union __attribute__ ((__packed__))
#define PACKED_STRUCT		struct __attribute__ ((__packed__))
#define ZCL_TIME_EPOCH_DELTA	ZCL_TIME_EPOCH_DELTA_1970
#define XBEE_MS_TIMER_RESOLUTION TIMER_PERIOD

// Elements needed to keep track of serial port settings.  Must have a
// baudrate memember, other fields are platform-specific.
typedef struct xbee_serial_t {
   uint32_t		baudrate;
   uint8_t              communicationFormat;
} xbee_serial_t;

#define _f_memcpy       memcpy
#define _f_memset       memset
#endif		// __XBEE_PLATFORM_LPC213x


