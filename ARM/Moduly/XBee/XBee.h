//*****************************************************************************
//
//    XBee.h        XBee
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __XBee_H__
   #define __XBee_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef enum {
   XBEE_INIT,
   XBEE_WAITING_INIT,
   XBEE_CONNECT,
   XBEE_WAITING_CONNECTION,
   XBEE_CONNECTED
} EXbeeState;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void XBeeInit( void);
// Init XBEE

void XBeeSend(byte *buf, unsigned int length);
// send data over XBEE network to network coordinator

int XBeeState( void);
// State

void XBeeExecute( void);
// Execute

int XBeeRssiGet( void);
// Rssi

int XBeeRssiGetPercentual( void);
// Rssi 0-100%

typedef void (*rxhandler)(uint64 from, const byte *frame, uint16 length);

TYesNo XBeeRegisterRxEvent(rxhandler handler);
// register event which will be invoked after some data received from XBee network

TYesNo XBeeUnregisterRxEvent(rxhandler handler);
// unregister event to be invoked after some data arrived

#endif
