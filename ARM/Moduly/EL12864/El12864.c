//*****************************************************************************
//
//    El12864.c  -  El12864/KS108 graphic controller services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/El12864.h"
#include "../../inc/System.h"


#define GPU_BUS_MASK      (0xFF << GPU_DATA_OFFSET)        // D0..D7 bus mask

// operace na sbernici :
#define BusIn()      GPU_DATA_DIR &= ~GPU_BUS_MASK         // bus input
#define BusOut()     GPU_DATA_DIR |=  GPU_BUS_MASK         // bus output
#define StrobeIdle() GpuClrE()                             // inactive strobe

// sdilena sbernice (postaci E=0, vychozi smer vystup, CS1,2 je nesdilene) :
#define AttachBus()  StrobeIdle();  BusOut()               // obsazeni sbernice
#define ReleaseBus() GpuDeselect(); BusIn()                // uvolneni sbernice

// Prikazy radice :
#define GPU_ON          0x3F           // display on
#define GPU_OFF         0x3E           // display off
#define GPU_START_LINE  0xC0           // start line
#define GPU_SET_PAGE    0xB8           // set page
#define GPU_SET_ADDRESS 0x40           // byte address
// status :
#define GPU_STATUS_BUSY  (1 << 7)      // status busy
#define GPU_STATUS_OFF   (1 << 5)      // status off
#define GPU_STATUS_RESET (1 << 4)      // status reset

// Konstanty :
#define GPU_RESET_DURATION   15                            // trvani reset [ms]
#define GPU_PAGE_COUNT       8                             // pocet stranek
#define GPU_ADDRESS_COUNT    64                            // pocet bytu na stranku    
#define GPU_MAX_PAGE         (GPU_PAGE_COUNT - 1)          // pocet stranek
#define GPU_MAX_ADDRESS      (GPU_ADDRESS_COUNT - 1)
#define GPU_CONTROLLER       GPU_ADDRESS_COUNT             // rozlisujici bit radice (ze sloupce)

// prepocet Y -> Row
#define GetRow( y)       ((y) >> 3)                        // adresa bytu

#ifndef GPU_DISABLE_BUFFER
   // Kreslici buffer :
   TGraphicBuffer GBuffer;
#endif

// Lokalni promenne :
static word ColumnAddress;          // posledni zapisova adresa
static word RowAddress;             // posledni zapisova stranka

//---- Lokalni funkce :

static void InitController( void);
// Inicializace radice

static void Clear( unsigned Pattern);
// Smaze pamet

static void WriteCommand( unsigned Command);
// Zapis prikazu

static void WriteData( unsigned Value);
// Zapis dat

static void WaitForReady( void);
// Cekani na pripravenost radice

static void Strobe( void);
// Strobovaci impuls

static void SetData( unsigned Value);
// Zapise data na sbernici D0..D7

static unsigned GetData( void);
// Precte data ze sbernice D0..D7

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void GpuInit( void)
// Inicializace
{
   GpuClearAll();                      // all controls to L
   GpuInitPorts();                     // port directions
   BusOut();                           // default direction
   // reset :
   GpuSelectBoth();                    // selektuj oba
   SysDelay( GPU_RESET_DURATION);      // trvani resetu
   GpuSetRST();                        // konec resetu
   SysDelay( GPU_RESET_DURATION);      // ustaleni po resetu
   GpuDeselect();                      // konec selectu   
   // incializace radicu :
   GpuSelect1();
   InitController();
   GpuSelect2();
   InitController();
   ReleaseBus();
#ifndef GPU_DISABLE_BUFFER
   GpuInitBuffer();
#endif   
} // GpuInit

//-----------------------------------------------------------------------------
// On
//-----------------------------------------------------------------------------

void GpuOn( void)
// Zapne display
{
   AttachBus();
   GpuSelect1();
   WriteCommand( GPU_ON);
   GpuSelect2();
   WriteCommand( GPU_ON);
   ReleaseBus();
} // GpuOn

//-----------------------------------------------------------------------------
// Off
//-----------------------------------------------------------------------------

void GpuOff( void)
// Vypne display
{
   AttachBus();
   GpuSelect1();
   WriteCommand( GPU_OFF);
   GpuSelect2();
   WriteCommand( GPU_OFF);
   ReleaseBus();
} // GpuOff

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void GpuClear( void)
// Smaze display
{
   AttachBus();
   GpuSelect1();
   Clear( 0);
   GpuSelect2();
   Clear( 0);
   ReleaseBus();
} // GpuClear

//-----------------------------------------------------------------------------
// Coordinate
//-----------------------------------------------------------------------------

void GpuGoto( int Row, int Column)
// Nastavi souradnici zapisu na radek <Row>, sloupec <Column>
{
   AttachBus();
   if( Column & GPU_CONTROLLER){
      // prava polovina
      GpuSelect2();
   } else {
      // leva polovina
      GpuSelect1();
   }
   RowAddress    = Row    & GPU_MAX_PAGE;
   ColumnAddress = Column & GPU_MAX_ADDRESS;
   WriteCommand( GPU_SET_PAGE    | RowAddress);
   WriteCommand( GPU_SET_ADDRESS | ColumnAddress);   
} // GpuGoto

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void GpuWrite( unsigned Pattern)
// Zapise byte <Pattern> na nastavenou souradnici (inkrementuje <Column>)
{
   WriteData( Pattern);
   ColumnAddress++;
   if( ColumnAddress & GPU_CONTROLLER){
      // prava polovina
      GpuSelect2();
      ColumnAddress = 0;                 // adresa 0 v pravem radici
      WriteCommand( GPU_SET_PAGE    | RowAddress);
      WriteCommand( GPU_SET_ADDRESS | ColumnAddress);
   }
} // GpuWrite

//-----------------------------------------------------------------------------
// Done
//-----------------------------------------------------------------------------

void GpuDone( void)
// Ukonci zapis
{
   ReleaseBus();
} // GpuDone

#ifndef GPU_DISABLE_BUFFER

#ifndef GPU_EXTERNAL_FLUSH
//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void GpuFlush( void)
// Zkopiruje aktivni oblast do radice
{
int x, y;
int minY, maxY;
int minX, maxX;

   minY = GetRow( GBuffer.MinY);
   maxY = GetRow( GBuffer.MaxY);
   minX = GBuffer.MinX;
   maxX = GBuffer.MaxX;
   for( y = minY; y <= maxY; y++){
      GpuGoto( y, minX);
      for( x = minX; x <= maxX; x++){
         GpuWrite( GBuffer.Buffer[y][x]);
      }
   }
   GpuDone();
   GpuInitBuffer();
} // GpuFlush
#endif // GPU_EXTERNAL_FLUSH

//-----------------------------------------------------------------------------
// Inicializace Bufferu
//-----------------------------------------------------------------------------

void GpuInitBuffer( void)
// Inicializace bufferu
{
   GBuffer.MinX = G_WIDTH;
   GBuffer.MaxX = 0;
   GBuffer.MinY = G_HEIGHT;
   GBuffer.MaxY = 0;   
} // GpuInitBuffer
#endif // GPU_DISABLE_BUFFER

//-----------------------------------------------------------------------------
// Inicializace radice
//-----------------------------------------------------------------------------

static void InitController( void)
// Inicializace radice
{
   WriteCommand( GPU_START_LINE);     // line 0
   Clear( 0);                         // clear RAM
   WriteCommand( GPU_ON);             // on   
} // InitController

//-----------------------------------------------------------------------------
// Mazani
//-----------------------------------------------------------------------------

static void Clear( unsigned Pattern)
// Smaze pamet
{
int Page, Address;

   for( Page = 0; Page < GPU_PAGE_COUNT; Page++){
      WriteCommand( GPU_SET_PAGE | Page);
      WriteCommand( GPU_SET_ADDRESS);
      for( Address = 0; Address < GPU_ADDRESS_COUNT; Address++){
         WriteData( Pattern);
      }
   }
} // Clear

//-----------------------------------------------------------------------------
// Zapis prikazu
//-----------------------------------------------------------------------------

static void WriteCommand( unsigned Command)
// Zapis prikazu
{
   WaitForReady();
   GpuClrRW();                         // write
   GpuClrDI();                         // command
   SetData( Command);                  // set data bus
   Strobe();                           // write strobe
   StrobeIdle();                       // inactive strobe
} // WriteCommand

//-----------------------------------------------------------------------------
// Zapis dat
//-----------------------------------------------------------------------------

static void WriteData( unsigned Value)
// Zapis dat
{
   WaitForReady();
   GpuClrRW();                         // write
   GpuSetDI();                         // data
   SetData( Value);                    // set data bus
   Strobe();                           // write strobe
   StrobeIdle();                       // inactive strobe
} // WriteData

//-----------------------------------------------------------------------------
// Pripravenost
//-----------------------------------------------------------------------------

static void WaitForReady( void)
// Cekani na pripravenost radice
{
unsigned Status;

   BusIn();                            // direction for read
   GpuSetRW();                         // read
   GpuClrDI();                         // command (status)
   do {
      Strobe();                        // read strobe
      Status = GetData();              // read D0..D7
      StrobeIdle();                    // inactive strobe
   } while( Status & GPU_STATUS_BUSY);
   BusOut();                           // default direction
} // WaitForReady

//-----------------------------------------------------------------------------
// Strobovani
//-----------------------------------------------------------------------------

static void Strobe( void)
// Strobovaci impuls
{
   SysUDelay( 1);                      // setup duration
   GpuSetE();                          // strobe edge
   SysUDelay( 1);                      // strobe hold duration   
} // Strobe

//-----------------------------------------------------------------------------
// zapis dat
//-----------------------------------------------------------------------------

static void SetData( unsigned Value)
// Zapise data na sbernici D0..D7
{
unsigned Mask;

   Mask = Value << GPU_DATA_OFFSET;
   GPU_DATA_CLR = ~Mask & GPU_BUS_MASK;
   GPU_DATA_SET =  Mask & GPU_BUS_MASK;
} // SetData

//-----------------------------------------------------------------------------
// Cteni dat
//-----------------------------------------------------------------------------

static unsigned GetData( void)
// Precte data ze sbernice D0..D7
{
   return((GPU_DATA_PIN & GPU_BUS_MASK) >> GPU_DATA_OFFSET);
} // GetData
