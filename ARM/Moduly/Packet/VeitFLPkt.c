//******************************************************************************
//
//   VeitFLPkt.c   Implementation of VeitFL protocol
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
#include "VeitFLPkt.h"
#include "../Inc/uconio.h"             // UART conio
#include "../Inc/File/Fd.h"            // files
#include "xWeight.h"                   // format weight
#include "Sdb.h"                       // Samples database
#include "Cal.h"                       // Calibration
#include "Calc.h"                      // Calculation
#include "Weighing.h"
#include "Flagging.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

typedef enum {
   BAT1 = 1,
   NET_COORDINATOR = 3
} EVeitFLDevices;

typedef enum{
   WEIGHT_REPORT = 1,
   WEIGHING_REPORT = 2,
   FILES_REPORT = 3
} EDataTypeDev1;

typedef enum{
   WEIGHING_PARAMETERS = 1,
   CATEGORIES_NAMES = 2,
   REPORT_FILES = 3,
   DELETE_FILES = 4,
} EDataTypeDev3;

#define LF  '\n'   // NL new line
#define RS  '\x1E'
#define RS_STRING "\x1E"

#define DEVICE_ID 1

#if PROJECT_XBEE == 1
#include "XBee.h"
#endif

const char rs = RS;
static TYesNo ParseDeleteFilesParameters(char *data_loc);
static TYesNo ParseCategoryName(int *category_id, char *data_loc, char *category_name);
static TYesNo ParseWeighingParameters(float *data_low_limit, float *data_high_limit, char *data_loc, int *category_id);
static TYesNo IsValidFileName(char *filename);
static void CloseConfigFile(void);
static void OpenConfigFile(void);
static TFdirHandle CreateFileAndOpenIfNotExists(char *data_loc);
static void SaveConfigAndActivateFile(TFdirHandle file);
static TYesNo ParseString(char *from, char **result);
static TYesNo ParseInt(char *from, int *result);
static void ParseDev3(int device_version);
// parse message from device <device_id> == 3

void VeitFLPktSetup( void)
// Set communication parameters and init
{
#if PROJECT_XBEE == 1
  XBeeRegisterRxEvent(&VeitFlReceiveFrame);
  // send greetings on setup
  VeitFlPktSendWeighingReport();
#endif
}

void VeitFlPktSendWeight(TWeight Weight)
// Send single <Weight>
{
#if PROJECT_PRINTER_VEITFLPROTO_VERSION == 1
   uprintf("%d"RS_STRING"%d"RS_STRING"%d"RS_STRING"%s"RS_STRING"%s"RS_STRING, 
      DEVICE_ID,       //<device_id>
      PROJECT_PRINTER_VEITFLPROTO_VERSION,       //<device_version> 
      WEIGHT_REPORT,       //<data_type>
      Config.ScaleName,       //<data_id>
      SdbInfo()->Name);      //<data_loc>
   Weight = CalRawWeight(Weight);
   Weight = CalWeightByUnits(Weight, UNITS_G);
   xWeightLeftDecimals( uputchar, Weight, 0);      //<data_val>
   uputchar(LF);
   uflush();
#endif
}
/*
void VeitFlPktSendFileReport( void)
{
#if PROJECT_PRINTER_VEITFLPROTO_VERSION == 1
   uprintf("%d"RS_STRING"%d"RS_STRING"%d"RS_STRING"%s"RS_STRING"%s", 
      DEVICE_ID,       //<device_id>
      PROJECT_PRINTER_VEITFLPROTO_VERSION,      //<device_version> 
      FILE_REPORT,       //<data_type>
      Config.ScaleName,       //<data_id>
      SdbInfo()->Name);      //<data_loc>
   uputchar(LF);
   uflush();
#endif   
}*/

void VeitFlPktSendWeighingReport( void)
{
int CategoryCount;
int i;
   uprintf("%d"RS_STRING"%d"RS_STRING"%d"RS_STRING"%s"RS_STRING"%s", 
      DEVICE_ID,       //<device_id>
      PROJECT_PRINTER_VEITFLPROTO_VERSION,      //<device_version> 
      WEIGHING_REPORT,       //<data_type>
      Config.ScaleName,       //<data_id>
      SdbInfo()->Name);      //<data_loc>
   uprintf("%d", SdbConfig()->WeightSorting.Mode);
   switch(SdbConfig()->WeightSorting.Mode) {
      case WEIGHT_SORTING_NONE:
         break;

      case WEIGHT_SORTING_LIGHT_HEAVY:
         uprintf(RS_STRING"%d", SdbConfig()->WeightSorting.LowLimit);
         break;

      case WEIGHT_SORTING_LIGHT_OK_HEAVY:
         uprintf(RS_STRING"%d"RS_STRING"%d", SdbConfig()->WeightSorting.LowLimit, SdbConfig()->WeightSorting.HighLimit);
         break;

      case WEIGHT_SORTING_CATEGORY:
         CategoryCount = WeighingSortingCategoryCount(SdbConfig()->WeightSorting.Categories);
         uprintf(RS_STRING"%s", SdbConfig()->WeightSorting.Categories[0].Name);
         uflush();
         for(i = 1 ; i < CategoryCount ; i++) {
            uprintf(RS_STRING"%d"RS_STRING"%d"RS_STRING"%d"RS_STRING"%s", i, SdbConfig()->WeightSorting.Categories[i].LowLimit, SdbConfig()->WeightSorting.Categories[i].HighLimit, SdbConfig()->WeightSorting.Categories[i].Name);
            uflush();
         }
         break;
   }
   uputchar(LF);
   uflush();
}


#define FILES_PER_PACKET   3

void VeitFlPktFilesReport( void)
// Send files report
{
   //<data_type_specific> :=<file-name><file-name-or-empty>
#if PROJECT_PRINTER_VEITFLPROTO_VERSION == 1
TFdirHandle Fd;
int Count;
char        Name[ FDIR_NAME_LENGTH + 1];
   uprintf("%d"RS_STRING"%d"RS_STRING"%d"RS_STRING"%s"RS_STRING"%s", 
      DEVICE_ID,       //<device_id>
      PROJECT_PRINTER_VEITFLPROTO_VERSION,      //<device_version> 
      FILES_REPORT);       //<data_type>
   FdMoveAt( 0);
   Fd = FdFindNext();
   if(Fd != FDIR_INVALID) {
      forever{
         Count++;
         FdGetName( Fd, Name);
         uputs(Name);
         Fd = FdFindNext();
         if(Fd == FDIR_INVALID) {
            break;
         }
         uputs(RS_STRING);
         if(Count == FILES_PER_PACKET) {
            uflush();
            Count = 0;
         }
      }
   }
   uputchar(LF);
   uflush();
#endif
} // VeitFlPktFilesReport

void VeitFlReceiveFrame(uint64 from, const byte *frame, uint16 length)
{
int device_id;
int device_version;
char data[length + 1];
  
   memcpy(data, frame, length);
   // check LF at the end of the stream
   if(data[length-1] != LF){
      return;
   }
   data[length-1] = RS;
   data[length] = '\0';

   // <device_id><RS><device_version><RS>
   if( !ParseInt(data, &device_id)){
      return;
   }
   if( !ParseInt(NULL, &device_version)){
      return;
   }

   switch(device_id){
      case NET_COORDINATOR:
         WeighingSuspend();
         ParseDev3(device_version);
         WeighingRelease();
         break;
      default: break;
   }   
}

static void ParseDev3(int device_version)
// parse message from device <device_id> == 3
{
char data_loc[FDIR_NAME_LENGTH + 1] = {'\0'};
char category_name[WEIGHING_SORTING_CATEGORY_NAME_SIZE + 1] = {'\0'};
float data_low_limit, data_high_limit;
int category_id;
TFdirHandle file;

   switch(device_version){
      case 1: 
      {
         int data_type;
         if( !ParseInt(NULL, &data_type)){
            return;
         }
         //<device_specific_data> := <data_type><RS><data_type_specific>
         switch(data_type){
            case WEIGHING_PARAMETERS:
            {
               if(!ParseWeighingParameters(&data_low_limit, &data_high_limit, data_loc, &category_id)){
                  return;
               }
               if(!IsValidFileName(data_loc)){
                  return;
               }
               // set weighing parameters according to received data
               // active file according to data_loc
               file = CreateFileAndOpenIfNotExists(data_loc);
               if(file == FDIR_INVALID){
                  return;
               }
               TWeightSorting sortingBackup;
               memcpy(&sortingBackup, &SdbConfig()->WeightSorting, sizeof(sortingBackup));
               // set limits
               if(category_id != -1) {
                  Config.WeighingParameters.WeightSorting.Mode = SdbConfig()->WeightSorting.Mode = WEIGHT_SORTING_CATEGORY;
                  Config.WeighingParameters.WeightSorting.Categories[category_id].LowLimit = SdbConfig()->WeightSorting.Categories[category_id].HighLimit = data_low_limit;
                  Config.WeighingParameters.WeightSorting.Categories[category_id].HighLimit = SdbConfig()->WeightSorting.Categories[category_id].HighLimit = data_high_limit;
               } else {
                  if((int)data_low_limit != -1){
                     if((int)data_high_limit == -1){
                        Config.WeighingParameters.WeightSorting.Mode = SdbConfig()->WeightSorting.Mode = WEIGHT_SORTING_LIGHT_HEAVY; // 2 groups
                     }else{
                        Config.WeighingParameters.WeightSorting.Mode = SdbConfig()->WeightSorting.Mode = WEIGHT_SORTING_LIGHT_OK_HEAVY; // 3 groups   
                        if(data_high_limit < data_low_limit){
                           //swap
                           data_high_limit = (int)data_high_limit ^ (int)data_low_limit;
                           data_low_limit  = (int)data_high_limit ^ (int)data_low_limit;
                           data_high_limit = (int)data_high_limit ^ (int)data_low_limit;
                        }
                     }
                  }else{
                     Config.WeighingParameters.WeightSorting.Mode = SdbConfig()->WeightSorting.Mode = WEIGHT_SORTING_NONE; // no sorting
                  }               
                  Config.WeighingParameters.WeightSorting.LowLimit = SdbConfig()->WeightSorting.LowLimit = (int)data_low_limit;
                  Config.WeighingParameters.WeightSorting.HighLimit = SdbConfig()->WeightSorting.HighLimit = (int)data_high_limit;
               }
               if(!memequ(&sortingBackup, &SdbConfig()->WeightSorting, sizeof(sortingBackup))){
                  SaveConfigAndActivateFile(file);               
                  if(!Config.EnableFileParameters){
                     ReflagAllFiles();
                  } else{
                     ReflagFile(file);
                  }
               }               
               //  response with file report
               VeitFlPktSendWeighingReport();
            }
            break;

            case CATEGORIES_NAMES:
               if(!ParseCategoryName(&category_id, data_loc, category_name)){
                  return;
               }
               if(category_id < 1 || category_id > WEIGHING_SORTING_CATEGORIES_COUNT) {
                  return;
               }
               // set weighing parameters according to received data
               // active file according to data_loc
               file = CreateFileAndOpenIfNotExists(data_loc);
               if(file == FDIR_INVALID){
                  return;
               } 
               strcpy((char*)Config.WeighingParameters.WeightSorting.Categories[category_id].Name, category_name);
               strcpy((char*)SdbConfig()->WeightSorting.Categories[category_id].Name, category_name);
               SaveConfigAndActivateFile(file);
               break;

            case REPORT_FILES:
               VeitFlPktFilesReport();
               break;
            
            case DELETE_FILES:
               CloseConfigFile();
               while(YES) {
                  if(!ParseDeleteFilesParameters(data_loc)){
                     break;
                  }
                  if(!IsValidFileName(data_loc)){
                     continue;
                  }
                  // set weighing parameters according to received data
                  // active file according to data_loc
                  file = FdSearch( data_loc);
                  if(file == FDIR_INVALID){
                     continue;
                  } 
                  SdbOpen( file, NO);
                  SdbDelete();
                  if(file == Config.LastFile){
                     Config.LastFile = FDIR_INVALID;
                  }
               }
               OpenConfigFile();
               break;

            default: break;
         }
      }
      break;


      default: break;
   }
}

static TYesNo ParseString(char *from, char **result){
   *result = strtok(from, &rs);
   return *result != NULL ? YES : NO;
}

static TYesNo ParseInt(char *from, int *result){
char *item;
char *endptr;
   if(!ParseString(from, &item)){
      return NO;
   }
   *result = (int) strtol(item, &endptr, 10);
   return *result == 0 && item == endptr? NO : YES;
}


static TYesNo ParseDeleteFilesParameters(char *data_loc)
{
char *item1;
   
   //<data_type_specific> :=<file-name><file-name-or-empty>

   item1 = strtok(NULL, &rs);

   if(item1 == NULL) {
      return NO;
   }
   strncpy(data_loc, item1, FDIR_NAME_LENGTH);
   return YES;

}

static TYesNo ParseCategoryName(int *category_id, char *data_loc, char *category_name)
{
char *item1, *item2, *item3;

   //<data_type_specific> :=<data_loc><RS><category_id><RS><category_name>
   item1 = strtok(NULL, &rs);
   item2 = strtok(NULL, &rs);
   item3 = strtok(NULL, &rs);
   if(item1 == NULL || item2 == NULL || item3 == NULL){
      return NO;
   }
   strncpy(data_loc, item1, FDIR_NAME_LENGTH);
   sscanf(item2, "%d", category_id);
   strncpy(category_name, item3, WEIGHING_SORTING_CATEGORY_NAME_SIZE);
   return YES;
}

static TYesNo ParseWeighingParameters(float *data_low_limit, float *data_high_limit, char *data_loc, int *category_id)
{
char *item1, *item2, *item3, *item4;

   //<data_type_specific> :=<data_low_limit><RS><data_high_limit><RS><data_loc>
   item1 = strtok(NULL, &rs);
   item2 = strtok(NULL, &rs);
   item3 = strtok(NULL, &rs);
   item4 = strtok(NULL, &rs);
   if(item4 != NULL){
      strncpy(data_loc, item4, FDIR_NAME_LENGTH);
      sscanf(item3, "%d", category_id);
      sscanf(item2, "%f", data_high_limit);
      sscanf(item1, "%f", data_low_limit);
      if(*category_id < 1 || *category_id > WEIGHING_SORTING_CATEGORIES_COUNT) {
         return NO;
      }
      if(*data_high_limit <= *data_low_limit) {
         return NO;
      }
   } else if(item3 != NULL){
      strncpy(data_loc, item3, FDIR_NAME_LENGTH);
      sscanf(item2, "%f", data_high_limit);
      sscanf(item1, "%f", data_low_limit);
      if(*data_high_limit <= *data_low_limit) {
         return NO;
      }
      *category_id = -1;
   } else if( item2 != NULL){
      strncpy(data_loc, item2, FDIR_NAME_LENGTH);
      sscanf(item1, "%f", data_low_limit);
      *data_high_limit = -1;
      *category_id = -1;
   } else if( item1 != NULL){
      strncpy(data_loc, item1, FDIR_NAME_LENGTH);
      *data_low_limit = -1;
      *data_high_limit = -1;
      *category_id = -1;
   }
   return YES;
}

static TYesNo IsValidFileName(char *filename){
int parsed, consumed;

   if(filename[0] != '\0'){
      // check file name validity
      consumed = strlen(filename);
      for(parsed = 0; parsed < consumed; parsed++){
         if(!isprint(filename[parsed])){
            return NO;
         }
      }
   } else {
      return NO; // invalid file name => invalid packet
   }
   return YES;
}

static void CloseConfigFile( void)
{
   if(Config.LastFile != FDIR_INVALID){
      SdbClose();
   }
}

static void OpenConfigFile( void)
{
   if(Config.LastFile == FDIR_INVALID){
      // find first file in system
      if(FdCount() != 0){
         FdFindBegin();
         Config.LastFile = FdFindNext();
      }
   }
   SdbOpen(Config.LastFile, NO);
}

static TFdirHandle CreateFileAndOpenIfNotExists(char *data_loc)
{
TFdirHandle file = FdSearch( data_loc);

   if(file == FDIR_INVALID){
      // non existing file, create it
      *SdbConfig() = Config.WeighingParameters;      // fill with defaults
      if( !SdbCreate( data_loc)){
         return FDIR_INVALID;
      }
      file = FdSearch( data_loc);
   }
   if(Config.LastFile != file){
      CloseConfigFile();
      SdbOpen( file, NO);
      Config.LastFile = file;
   }    
   return file;
}

static void SaveConfigAndActivateFile(TFdirHandle file)
{
   if(Config.LastFile != file){
      // switch to this file
      Config.LastFile = file;
      ConfigSaveSection( LastFile);
      CalcStatistics();
   }
}
