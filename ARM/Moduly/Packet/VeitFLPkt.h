//******************************************************************************
//
//   VeitFLPkt.h   Implementation of VeitFL protocol
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __VeitFLPkt_H__
   #define __VeitFLPkt_H__

#include "WeightDef.h"
#include "../Inc/Uni.h"

void VeitFLPktSetup( void);
// Set communication parameters and init

void VeitFlPktSendWeight(TWeight Weight);
// Send single <Weight>

void VeitFlPktSendFileReport( void);
// Send file report (current bat name and current file name)

void VeitFlPktFilesReport( void);
// Send files report

void VeitFlReceiveFrame(uint64 from, const byte *frame, uint16 length);
// receive single frame from network

void VeitFlPktSendWeighingReport( void);
#endif