//*****************************************************************************
//
//    Histogram.c  -  Histogram calculations
//    Version 1.0, (c) VymOs & P.Veit
//
//*****************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
   #include "Histogram.h"
#endif

#ifdef __GNUC__
   // GNU ARM only
   #include "../Inc/Histogram.h"
#endif

// Local functions :

static void UpdateCenter( THistogram *Histogram);
// Update center position by step

#ifdef HISTOGRAM_NORMALIZE_STEP
   static THistogramValue NormalizeStep( THistogramValue Step);
   // Returns normalized step
#endif

//-----------------------------------------------------------------------------
// Mazani
//-----------------------------------------------------------------------------

void HistogramClear( THistogram *Histogram)
// Clear data
{
int i;

   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Histogram->Slot[ i] = 0;
   }
} // HistogramClear

//-----------------------------------------------------------------------------
// Pridani hodnoty
//-----------------------------------------------------------------------------

void HistogramAdd( THistogram *Histogram, THistogramValue Value)
// Add <Value>
{
int Index;

   Index = HistogramGetSlot( Histogram, Value);
   Histogram->Slot[ Index] += 1;
} // HistogramAdd

//-----------------------------------------------------------------------------
// Pridani hodnoty
//-----------------------------------------------------------------------------

int HistogramGetSlot( THistogram *Histogram, THistogramValue Value)
// Find slot index by <Value>
{
int Index;

   Value -= Histogram->Center;
#if (HISTOGRAM_SLOTS % 2 == 0)
   // Even number of slots
   if( Value >= 0){
      if( Value >= (HISTOGRAM_SLOTS / 2 * Histogram->Step)){
         return( HISTOGRAM_SLOTS - 1);      // above range
      }
      Index  = Value / Histogram->Step;     // distance by center
      Index += HISTOGRAM_SLOTS / 2;
   } else {
      // Value < 0
      Value = -Value;
      if( Value >= (HISTOGRAM_SLOTS / 2 * Histogram->Step)){
         return( 0);                       // under range
      }
      Index  = Value / Histogram->Step;    // distance by center
      Index  = HISTOGRAM_SLOTS / 2 - 1 - Index;
   }
#else
   // Odd number of slots
   if( Value >= 0){
      if (Value >= (HISTOGRAM_SLOTS * Histogram->Step / 2)) {
         return( HISTOGRAM_SLOTS - 1);      // above range
      }
      Index  = (Value + Histogram->Step / 2) / Histogram->Step; // distance by center
      Index += HISTOGRAM_SLOTS / 2;
   } else {
      Value = -Value;
      if (Value >= (HISTOGRAM_SLOTS * Histogram->Step / 2)) {
         return( 0);                       // under range
      }
      Index  = (Value + Histogram->Step / 2) / Histogram->Step; // distance by center
      Index  = HISTOGRAM_SLOTS / 2 - Index;
   }
#endif
   return( Index);
} // HistogramGetSlot

//-----------------------------------------------------------------------------
// Center
//-----------------------------------------------------------------------------

void HistogramSetCenter( THistogram *Histogram, THistogramValue Center)
// Set histogram <Center>
{
   Histogram->Average = Center;
   Histogram->Center  = Center;
} // HistogramSetCenter

//-----------------------------------------------------------------------------
// Range
//-----------------------------------------------------------------------------

void HistogramSetRange( THistogram *Histogram, int Range) 
// Set histogram step by <Range> [%]
{
   Histogram->Step = (Histogram->Center * 2 * Range) / (100 * HISTOGRAM_SLOTS);
   if( Histogram->Step < HISTOGRAM_MIN_STEP){
      Histogram->Step = HISTOGRAM_MIN_STEP;                // step under limit
   }
#ifdef HISTOGRAM_NORMALIZE_STEP
   Histogram->Step = NormalizeStep( Histogram->Step);      // normalize step size
#endif
   UpdateCenter( Histogram);                               // update center position
} // HistogramSetRange

//-----------------------------------------------------------------------------
// Step
//-----------------------------------------------------------------------------

void HistogramSetStep( THistogram *Histogram, THistogramValue Step)
// Set histogram <Step>
{
   Histogram->Step = Step;
   UpdateCenter( Histogram);                     // update center position
} // HistogramSetStep

//-----------------------------------------------------------------------------
// Empty
//-----------------------------------------------------------------------------

TYesNo HistogramEmpty( THistogram *Histogram) 
// Returns YES if empty
{
int i;

   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      if( Histogram->Slot[ i] > 0){
         return( NO);
      }
   }
   return( YES);
} // HistogramEmpty

//-----------------------------------------------------------------------------
// Maximum
//-----------------------------------------------------------------------------

THistogramValue HistogramMaximum( THistogram *Histogram)
// Get maximal slot value
{
THistogramValue Max;
int             i;

   Max = 0;
   // get column maximum :   
   for( i = 0; i < HISTOGRAM_SLOTS; i++) {
      if( Histogram->Slot[i] > Max){
         Max = Histogram->Slot[ i];
      }
   }
   if( Max == 0){
      Max++;                           // at least 1
   }
   return( Max);
} // HistogramMaximum

//-----------------------------------------------------------------------------
// Normalize
//-----------------------------------------------------------------------------

int HistogramNormalize( THistogram *Histogram, THistogramValue MaxValue, int SlotIndex)
// Returns histogram value normalized by <MaxValue>
{
   return( (Histogram->Slot[ SlotIndex] * HISTOGRAM_NORM) / MaxValue);
} // HistogramNormalize

//-----------------------------------------------------------------------------
// Get Value
//-----------------------------------------------------------------------------

THistogramValue HistogramGetValue( THistogram *Histogram, int SlotIndex) 
// Returns column value by <SlotIndex>
{
  return( Histogram->Center + (SlotIndex - (HISTOGRAM_SLOTS / 2)) * Histogram->Step);
} // HistogramGetValue

//*****************************************************************************

//-----------------------------------------------------------------------------
// Update center
//-----------------------------------------------------------------------------

static void UpdateCenter( THistogram *Histogram)
// Update center position by step
{
THistogramValue Left;

#ifdef HISTOGRAM_NORMALIZE_STEP
   // round to step size
   Histogram->Center += Histogram->Step / 2;
   Histogram->Center /= Histogram->Step;
   Histogram->Center *= Histogram->Step;
#endif
   Left = Histogram->Step * HISTOGRAM_SLOTS / 2;  // left boundary
   if( Histogram->Center < Left){
      // negative value, shift center
      Histogram->Center = Left;
   }
} // UpdateCenter

#ifdef HISTOGRAM_NORMALIZE_STEP
//-----------------------------------------------------------------------------
// Normalize Step
//-----------------------------------------------------------------------------

static const THistogramValue _NStep[] = {
   1, 2, 5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 
   150, 200, 250, 300, 400, 500, 600, 700, 800, 900, 1000
};

static THistogramValue NormalizeStep( THistogramValue Step)
// Returns normalized step
{
int i;
int Count;

   Count = sizeof( _NStep) / sizeof( THistogramValue);
   for( i = 0; i < Count; i++){
      if( Step <= _NStep[ i]){
         return(  _NStep[ i]);
      }
   }
   return( Step);                      // out of range
} // NormalizeStep
#endif
