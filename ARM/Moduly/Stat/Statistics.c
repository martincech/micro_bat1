//*****************************************************************************
//
//    Statistics.c  -  Statistics utility
//    Version 1.0, (c) Vymos & P.Veit
//
//*****************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
   #include "Statistics.h"
   #include <math.h>
#endif

#ifdef __GNUC__
   // GNU ARM only
   #include "../Inc/Statistics.h"
   #include <math.h>
#endif

#define MIN_AVERAGE        1e-03         // mensi hodnota povazovana za 0
#define MIN_SIGMA          1e-03         // mensi hodnota povazovana za 0
#define DEFAULT_UNIFORMITY 100           // implicitni uniformita (pri chybe)

//******************************************************************************
// Mazani
//******************************************************************************

void StatClear( TStatistic *statistic)
// smaze statistiku
{
   statistic->XSuma    = 0;
   statistic->X2Suma   = 0;
   statistic->UniMin   = 0;
   statistic->UniMax   = 0;
   statistic->Count    = 0;
   statistic->CountIn  = 0;
   statistic->CountOut = 0;
} // StatClear

//******************************************************************************
// Pridani
//******************************************************************************

void StatAdd( TStatistic *statistic, TNumber x)
// prida do statistiky
{
   statistic->XSuma  += x;
   statistic->X2Suma += x * x;
   statistic->Count++;
} // StatAdd

void StatRemove( TStatistic *statistic, TNumber x)
{
   statistic->XSuma  -= x;
   statistic->X2Suma -= x * x;
   statistic->Count--;
}
//******************************************************************************
// Prumer
//******************************************************************************

TNumber StatAverage( TStatistic *statistic)
// vrati stredni hodnotu
{
   if( statistic->Count == 0){
      return( 0);
   }
   return( statistic->XSuma / statistic->Count);
} // StatAverage

//******************************************************************************
// Odchylka
//******************************************************************************

TNumber StatSigma( TStatistic *statistic)
// vrati smerodatnou odchylku
{
TNumber tmp;

   if (statistic->Count <= 1) {
      return( 0);
   }
   tmp = statistic->XSuma * statistic->XSuma / statistic->Count;
   tmp = statistic->X2Suma - tmp;
   if( tmp <= MIN_SIGMA){
      return( 0);
   }
   return( sqrt( 1/(TNumber)(statistic->Count - 1) * tmp));
} // StatSigma

//******************************************************************************
// Variace
//******************************************************************************

TNumber StatVariation( TNumber average, TNumber sigma)
// vrati variaci (procentni smerodatna odchylka)
{
   if( average < MIN_AVERAGE){
      return( 0);         // "deleni nulou"
   }
   return( sigma / average * 100);
} // StatVariation

//******************************************************************************
// Uniformity
//******************************************************************************

void StatUniformityInit( TStatistic *statistic, TNumber Min, TNumber Max)
// Inicializuje uniformitu
{
   statistic->UniMin   = Min;
   statistic->UniMax   = Max;
   statistic->CountIn  = 0;
   statistic->CountOut = 0;
}  // StatUniformityInit

void StatUniformityAdd( TStatistic *statistic, TNumber Number)
// Prida novy vzorek do uniformity
{
   // Prida novy vzorek do uniformity
   if( Number < statistic->UniMin || Number > statistic->UniMax) {
     // Vzorek je mimo pasmo
     statistic->CountOut++;
     return;
   }
   // Vzorek je v pasmu
   statistic->CountIn++;
} // StatUniformityAdd

TNumber StatUniformityGet( TStatistic *statistic)
// Vypocte uniformitu
{
   if( statistic->CountIn == 0 && statistic->CountOut == 0) {
      return( DEFAULT_UNIFORMITY);
   } else {
      return( (TNumber)(100 * statistic->CountIn) / (TNumber)(statistic->CountIn + statistic->CountOut));
   }
} // StatUniformityGet
