//*****************************************************************************
//
//    EepUtil.c    Common SPI EEPROM utilities
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../Inc/Eep.h"

//-----------------------------------------------------------------------------
// Ulozeni dat
//-----------------------------------------------------------------------------

TYesNo EepSaveData( unsigned Address, const void *Data, int Size)
// Zapise <Data> o velikosti <Size> na <Address>
{
TYesNo     NewPage;
const byte *p;

   p = Data;
   NewPage = YES;                      // prvni blok
   while( Size-- > 0){
      if( NewPage){
         // Zacatek nove stranky
         NewPage = NO;
         if (!EepPageWriteStart(Address)){
            return( NO);
         }
      }
      EepPageWriteData( *p);
      p++;
      Address++;
      if( Address % EEP_PAGE_SIZE == 0){
         // Nova adresa uz lezi v nove strance, provedu zapis stranky
         EepPageWritePerform();        // predchozi stranka
         NewPage = YES;                // start zapisu nove stranky
      }
   }
   if (!NewPage) {
      EepPageWritePerform();           // zbytek posledni stranky
   }
   return( YES);
} // EepSaveData

//-----------------------------------------------------------------------------
// Nacteni dat
//-----------------------------------------------------------------------------

void EepLoadData( unsigned Address, void *Data, int Size)
// Nacte <Data> o velikosti <Size> z <Address>
{
byte *p;

   p = Data;
   EepBlockReadStart( Address);
   while( Size-- > 0){
      *p = EepBlockReadData();
      p++;
   }
   EepBlockReadStop();
} // EepLoadData

//-----------------------------------------------------------------------------
// Vyplneni pameti hodnotou
//-----------------------------------------------------------------------------

TYesNo EepFillData( unsigned Address, byte Pattern, int Size)
// Vyplni prostor od <Address> v delce <Size> pomoci <Pattern>
{
TYesNo NewPage;

   NewPage = YES;                 // prvni blok
   while( Size-- > 0) {
      if (NewPage) {
         // Zacatek nove stranky
         NewPage = NO;
         if( !EepPageWriteStart( Address)){
            return( NO);
         }
      }
      EepPageWriteData( Pattern);
      Address++;
      if( Address % EEP_PAGE_SIZE == 0) {
         // Nova adresa uz lezi v nove strance, provedu zapis stranky
         EepPageWritePerform();        // predchozi stranka
         NewPage = YES;                // start zapisu nove stranky
      }
   }
   if( !NewPage) {
     EepPageWritePerform();            // zbytek posledni stranky
   }
   return( YES);
} // EepFillData

//-----------------------------------------------------------------------------
// Komparace
//-----------------------------------------------------------------------------

TYesNo EepMatchData( unsigned Address, void *Data, int Size)
// Komparuje <Data> o velikosti <Size> na <Address>
{
byte *p;

   p = Data;
   EepBlockReadStart( Address);
   while( Size-- > 0){
      if( *p != EepBlockReadData()){
         EepBlockReadStop();
         return( NO);
      }
      p++;
   }
   EepBlockReadStop();
   return( YES);
} // EepMatchData
