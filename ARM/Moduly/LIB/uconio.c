//*****************************************************************************
//
//    uconio.c     UART simple formatted output
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"           // podminena kompilace
#include "../../inc/uconio.h"
#include "../../inc/xprint.h"
#ifdef PROJECT_XBEE
  #include "XBee.h"
  #include "wpan/aps.h"
#endif

#include <stdarg.h>

#ifdef UCONIO_COM0
   #define __COM0__
#endif
#ifdef UCONIO_COM1
   #define __COM1__
#endif
#include "../inc/Com.h"

// Local functions :
#ifdef PROJECT_PRINTER

static void SendChar( int ch);
// sends character
#else 

#define BUFFER_SIZE  64

static byte Buffer[BUFFER_SIZE];
static byte BufferPtr = 0;

static void XBeeAppend( byte Ch);
// append to Tx buffer
#endif

//-----------------------------------------------------------------------------
// Character
//-----------------------------------------------------------------------------

void uputch( char ch)
// character output
{
  uputchar( ch);
} // uputch

//-----------------------------------------------------------------------------
// String
//-----------------------------------------------------------------------------

void uputs( const char *string)
// string output
{
   xputs( uputchar, string);
} // uputs

//-----------------------------------------------------------------------------
// Hexadecimal
//-----------------------------------------------------------------------------

void uprinthex( dword x, dword flags)
// hexadecimal output
{
   xprinthex( uputchar, x, flags);
} // uprinthex

//-----------------------------------------------------------------------------
// Decadic
//-----------------------------------------------------------------------------

void uprintdec( int32 x, dword flags)
// decadic number output
{
   xprintdec( uputchar, x, flags);
} // uprintdec

//-----------------------------------------------------------------------------
// Float
//-----------------------------------------------------------------------------

void ufloat( int x, int w, int d, dword flags)
// floating number output
{
   xfloat( uputchar, x, w, d, flags);
} // ufloat

//-----------------------------------------------------------------------------
// Formatted
//-----------------------------------------------------------------------------

void uprintf( const char *Format, ...)
// simple formatted output
{
va_list arg;

   va_start( arg, Format);
   xvprintf( uputchar, Format, arg);
} // uprintf

//------------------------------------------------------------------------------
// uputchar
//------------------------------------------------------------------------------

int uputchar( int ch)
// output character
{
#ifdef PROJECT_PRINTER
   if( ch == '\n'){
      SendChar( '\r');                 // add carriage return
   }
   SendChar( ch);
#endif

#ifdef PROJECT_XBEE
   XBeeAppend( ch);
#endif
   return( ch);   
} // uputchar

#ifdef PROJECT_PRINTER
//------------------------------------------------------------------------------
// send
//------------------------------------------------------------------------------

static void SendChar( int ch)
// sends character
{
   while( ComTxBusy());
   ComTxChar( ch);
} // SendChar

#endif

#ifdef PROJECT_XBEE
//-----------------------------------------------------------------------------
// Append
//-----------------------------------------------------------------------------

static void XBeeAppend( byte Ch)
// append to Tx buffer
{
   Buffer[BufferPtr] = Ch;
   BufferPtr++;
   
   if(BufferPtr == BUFFER_SIZE) {
      uflush();
   }
} // XBeeAppend

//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void uflush( void)
// flush Tx buffer
{
   XBeeSend(Buffer, BufferPtr);
   BufferPtr = 0;
}

#endif