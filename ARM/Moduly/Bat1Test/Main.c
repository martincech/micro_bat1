//******************************************************************************
//
//  Main.c         Bat1 test
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/conio.h"     // Display
#include "../Inc/St7259.h"    // Display
#include "../Inc/IAdc.h"      // Internal A/D convertor
#include "../inc/Ads123x.h"   // A/D convertor
#include "../Inc/uconio.h"    // UART1 conio
#include "../inc/Uart0.h"     // USB RS232
#include "../inc/Uart1.h"     // Printer RS232
#include "../inc/Eep.h"       // EEPROM
#include "../inc/Sound.h"     // Sound
#include "../inc/Rtc.h"       // RTC
#include "../inc/Dt.h"        // Date Time calculations
#include "Fonts.h"            // fonts list
#include "Backlight.h"        // Backlight
#include "TestDef.h"


#define BACKLIGHT_MAX 9

#define IADC_SHIFT  2
#define IADC_READS  (1 << IADC_SHIFT)

// start counter 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Local variables :
byte EnableCharger;

volatile unsigned Counter1s   = 0;         // odpocitavani 1 s
volatile unsigned Timer1s     = 0;         // priznak odpocitani 1 s

// Local functions :

static void UsbTest( void);
// Run USB test

static TYesNo RtcTest( void);
// Run RTC test

static TYesNo MemoryTest( void);
// Run memory test

static TYesNo MemoryPageTest( void);
// Run memory paging test

static TYesNo MemoryPatternTest( byte Pattern);
// Run memory pattern test

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
byte  ch;
byte  r;
int   i;
dword Value;

   CpuInit();
   PManInitPorts();
   PManSetVYP();

   KbdInit();
   BacklightInit();
   GInit();

   BacklightSet( BACKLIGHT_MAX);
   BacklightOn();
   GSetFont( FONT_8x8);
   cputs( "Display : ON\n");

   // USB UART0 :
   Uart0Init();
   Uart0Setup( UART0_BAUD, UART0_FORMAT, UART0_TIMEOUT);
   cputs( "UART0 : ON\n");
   // Master/printer UART1 :
   Uart1Init();
   Uart1Setup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   cputs( "UART1 : ON\n");
   // Internal ADC :
   IAdcInit();
   cputs( "IADC : ON\n");
   // External ADC :
   AdcInit();
   AdcStart();
   cputs( "ADC : ON\n");
   // EEPROM :
   EepInit();
   cputs( "EEPROM : ON\n");
   // RTC :
   RtcInit();
   cputs( "RTC : ON\n");
   // Sound :
   SndInit();
   cputs( "Sound : ON\n");
   // Charger :
   EnableCharger = YES;
   PManClrENNAB();
   cputs( "Charger : ON\n");

   // system run :
   EnableInts();                       // enable interrupts
   SetTimer1s();                       // start 1 s countdown
   SysStartTimer();                    // system timer
   StartWatchDog();                    // start watchdog

   // wait for power on key release :
   cputs( "Wait for key release...\n");
   KbdPowerUpRelease();                // wait for key release
   cputs( "Wait for commands...\n");

   // main loop :
   forever {
      /*
      if( kbhit()){
         if( getch() != K_ESC){
            continue;
         }
         // manual power off
         GpuShutdown();             // display off
         PManClrVYP();              // power off
         DisableInts();
         forever {
            SysDelay( 100);         // watchdog space
            WatchDog();             // wait for power failure
         }
      }
      */
      if( !Uart1RxChar( &ch)){
         continue;
      }
      r = TEST_OK;
      switch( ch){
         case TEST_DISPLAY :
            GSetColor( COLOR_BLACK);
            GBox( 0, 0, G_WIDTH, G_HEIGHT);
            GFlush();
            SysDelay( 500);
            cclear();
            cputs( "Display : OK\n");
            break;

         case TEST_SOUND :
            cputs( "SOUND : ");
            SndBeep( NOTE_C4, 1, 100);           // async. beep
            SysDelay( 500);
            SndBeep( NOTE_C1, VOL_MAX, 100);     // async. beep
            SysDelay( 100);
            cputs( "OK\n");
            break;

         case TEST_ADC :
            cputs( "ADC : ");
            if( !AdcDataReady()){
               cputs( "ERR\n");
               r = TEST_ERR;
               break;
            }
            Value = AdcRead();
            cputs( "OK\n");
            uprintf( "%08x", Value);
            break;

         case TEST_IADC :
            cputs( "IADC : ");
            // read A/D with averaging :
            Value = 0;
            for( i = 0; i < IADC_READS; i++){
               Value += IAdcRead( ACCU_ADC_VOLTAGE);
            }
            Value >>= IADC_SHIFT;
            cputs( "OK\n");
            uprintf( "%08x", Value);
            break;

         case TEST_MEMORY :
            cputs( "EEPROM : ");
            if( !MemoryTest()){
               cputs( "\nERR\n");
               r = TEST_ERR;
               break;
            }
            cputs( "OK\n");
            break;

         case TEST_RTC :
            cputs( "RTC : ");
            if( !RtcTest()){
               cputs( "ERR\n");
               r = TEST_ERR;
               break;
            }
            cputs( "OK\n");
            break;

         case TEST_USB :
            cputs( "USB : ");
            UsbTest();
            cputs( "OK\n");
            break;

         case TEST_POWER :
            cputs( "PWR : ");
            // PPR status :
            if( PManGetPPR()){
               Uart1TxChar( 'L');
            } else {
               Uart1TxChar( 'H');
            }
            // CHG status :
            if( PManGetCHG()){
               Uart1TxChar( 'H');
            } else {
               Uart1TxChar( 'L');
            }
            // USB status :
            if( PManGetUSBON()){
               Uart1TxChar( 'H');
            } else {
               Uart1TxChar( 'L');
            }
            // ENNAB status :
            if( EnableCharger){
               Uart1TxChar( 'L');
            } else {
               Uart1TxChar( 'H');
            }
            cputs( "OK\n");
            break;

         case TEST_CHARGER_ON :
            cputs( "Charger : ON\n");
            EnableCharger = YES;
            PManClrENNAB();
            break;

         case TEST_CHARGER_OFF :
            cputs( "Charger : OFF\n");
            EnableCharger = NO;
            PManSetENNAB();
            break;

         case TEST_OFF :
            cputs( "Power : OFF\n");
            Uart1TxChar( TEST_OK);     // send reply
            GpuShutdown();             // display off
            PManClrVYP();              // power off
            DisableInts();
            forever {
               SysDelay( 100);         // watchdog space
               WatchDog();             // wait for power failure
            }
            break;

         case TEST_BACKLIGHT_ON :
            cputs( "Backlight : ON\n");
            BacklightOn();
            break;

         case TEST_BACKLIGHT_OFF :
            cputs( "Backlight : OFF\n");
            BacklightOff();
            break;

          case TEST_KEYBOARD :
            cputs( "KBD : ");
            Value = KBD_PIN;
            // key ON :
            if( Value & KBD_ON){
               Uart1TxChar( 'H');
            } else {
               Uart1TxChar( 'L');
            }
            // key K0 :
            if( Value & KBD_K0){
               Uart1TxChar( 'H');
            } else {
               Uart1TxChar( 'L');
            }
            // key K1 :
            if( Value & KBD_K1){
               Uart1TxChar( 'H');
            } else {
               Uart1TxChar( 'L');
            }
            // key K2 :
            if( Value & KBD_K2){
               Uart1TxChar( 'H');
            } else {
               Uart1TxChar( 'L');
            }
            cputs( "OK\n");
            break;

         default :
            r = ch;                    // echo only
            break;
      }
      Uart1TxChar( r);                 // send reply
   }
} // main

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   SndTrigger();
// KbdTrigger();



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   USB Test
//------------------------------------------------------------------------------

static void UsbTest( void)
// Run USB test
{
byte ch;

   // echo characters, terminate on 0xFF
   forever {
      if( !Uart0RxChar( &ch)){
         continue;
      }
      Uart0TxChar( ch);
      if( ch == 0xFF){
         return;
      }
   }
} // UsbTest

//------------------------------------------------------------------------------
//   RTC test
//------------------------------------------------------------------------------

#define RTC_TEST_TIME  3

static TYesNo RtcTest( void)
// Run RTC test
{
TLocalTime Local;
TTimestamp Start, Now, Delta;

   Local.Day   = 11;
   Local.Month = 12;
   Local.Year  = 13;
   Local.Hour  = 14;
   Local.Min   = 15;
   Local.Sec   = 16;
   Start = DtEncode( &Local);
   RtcSave( Start);
   SysDelay( RTC_TEST_TIME * 1000);
   Now = RtcLoad();
   Delta = Now - Start;
   if( Delta < RTC_TEST_TIME - 1){
      return( NO);
   }
   if( Delta > RTC_TEST_TIME + 1){
      return( NO);
   }
   return( YES);
} // RtcTest

//------------------------------------------------------------------------------
//   Memory test
//------------------------------------------------------------------------------

static TYesNo MemoryTest( void)
// Run memory test
{
   cputs( "00,");
   if( !MemoryPatternTest( 0x00)){
      return( NO);
   }
   cputs( "55,");
   if( !MemoryPatternTest( 0x55)){
      return( NO);
   }
   cputs( "AA,");
   if( !MemoryPatternTest( 0xAA)){
      return( NO);
   }
   cputs( "pp,");
   if( !MemoryPageTest()){
      return( NO);
   }
   cputs( "FF\n");
   return( MemoryPatternTest( 0xFF));
} // MemoryTest

//------------------------------------------------------------------------------
//   Memory page test
//------------------------------------------------------------------------------

#define MAGIC_OFFSET 2

static TYesNo MemoryPageTest( void)
// Run memory paging test
{
int i, j;
byte Value;

   // write pattern :
   for( i = 0; i < EEP_SIZE; i += EEP_PAGE_SIZE){
      EepPageWriteStart( i);
      for( j = 0; j < EEP_PAGE_SIZE; j++){
         EepPageWriteData( (i + MAGIC_OFFSET) & 0xFF);
      }
      EepPageWritePerform();
   }
   // read pattern :
   for( i = 0; i < EEP_SIZE; i += EEP_PAGE_SIZE){
      EepBlockReadStart( i);
      for( j = 0; j < EEP_PAGE_SIZE; j++){
         Value = EepBlockReadData();
         if( Value != ((i + MAGIC_OFFSET) & 0xFF)){
            EepBlockReadStop();
            cprintf( "\nEEPROM error @%04x : %02x\n", i, Value);
            return( NO);
         }
      }
      EepBlockReadStop();
   }
   return( YES);
} // MemoryPageTest

//------------------------------------------------------------------------------
//   Memory pattern test
//------------------------------------------------------------------------------

static TYesNo MemoryPatternTest( byte Pattern)
// Run memory pattern test
{
int i, j;
byte Value;

   // write pattern :
   for( i = 0; i < EEP_SIZE; i += EEP_PAGE_SIZE){
      EepPageWriteStart( i);
      for( j = 0; j < EEP_PAGE_SIZE; j++){
         EepPageWriteData( Pattern);
      }
      EepPageWritePerform();
   }
   // read pattern :
   for( i = 0; i < EEP_SIZE; i += EEP_PAGE_SIZE){
      EepBlockReadStart( i);
      for( j = 0; j < EEP_PAGE_SIZE; j++){
         Value = EepBlockReadData();
         if( Value != Pattern){
            EepBlockReadStop();
            cprintf( "\nEEPROM error @%04x : %02x\n", i, Value);
            return( NO);
         }
      }
      EepBlockReadStop();
   }
   return( YES);
} // MemoryPatternTest
