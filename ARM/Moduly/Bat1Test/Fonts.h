//*****************************************************************************
//
//    Fonts.h      Project fonts
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __Fonts_H__
   #define __Fonts_H__

#ifndef __Font_H__   
   #include "../inc/Font.h"
#endif   

// Fonts enumeration :
typedef enum {
   FONT_8x8,
   TAHOMA8,
   LUCIDA6,
   _FONT_LAST
} TProjectFonts;

// fonts descriptors :
extern const TFontDescriptor const *Fonts[];

#endif
