//******************************************************************************
//                                                                            
//   MenuMaintenance.c  Maintenance menu
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "MenuMaintenance.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/File/Fd.h"       // File directory
#include "../Inc/Wgt/DLabel.h"    // Display label

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DEdit.h"            // Display edit value
#include "Wgt/DMsg.h"             // Display message

#include "Str.h"                  // Strings
#include "Password.h"             // Password checking
#include "MenuMemory.h"           // Memory functions
#include "MenuWUnits.h"           // Weighing units menu
#ifdef PROJECT_XBEE
#include "MenuXBee.h"          // Printer configuration menu
#else
#include "MenuPrinter.h"          // Printer configuration menu
#endif
#include "MenuCountry.h"          // Country configuration menu
#include "Config.h"               // Configuration utility
#include "Password.h"             // Password checking
#include "Sdb.h"                  // Samples database
#include "Bat1.h"                 // Default file only
#include "Contrast.h"             // Contrast control
#include "Backlight.h"            // Backlight
#include <string.h>

static DefMenu( MaintenanceMenu)
   STR_SCALE_NAME,
   STR_COUNTRY,
   STR_WEIGHING,
   STR_MEMORY,
#ifdef PROJECT_XBEE
   "XBee",
#else
   STR_PRINTER,
#endif
   STR_AUTO_POWER_OFF,
   STR_PASSWORD,
   STR_RESTORE_FACTORY_DEFAULTS,
EndMenu()

typedef enum {
   MM_SCALE_NAME,
   MM_COUNTRY,
   MM_WEIGHING,
   MM_MEMORY,
#ifdef PROJECT_XBEE
   MM_XBEE,
#else
   MM_PRINTER,
#endif
   MM_AUTO_POWER_OFF,
   MM_PASSWORD,
   MM_RESTORE_FACTORY_DEFAULTS,
} TMaintenanceMenuEnum;

// Local functions :

static void MenuMaintenanceParameters( int Index, int y, void *Parameters);
// Maintenance menu parameters

static void SetLastFile( TFdirHandle Handle);
// Set last working file

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuMaintenance( void)
// Maintenance menu
{
TMenuData   MData;
int         i;
char        Name[ SCALE_NAME_LENGTH + 1];
TFdirHandle Handle;

   // check for password :
   if( !PasswordCheck()){
      return;
   }
   // run menu :
   DMenuClear( MData);
   forever {
      if( !DMenu( STR_MAINTENANCE, MaintenanceMenu, (TMenuItemCb *)MenuMaintenanceParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MM_SCALE_NAME :
            strcpy( Name, Config.ScaleName);
            if( !DInputText( STR_SCALE_NAME, STR_ENTER_SCALE_NAME, Name, SCALE_NAME_LENGTH, NO)){
               break;
            }
            strcpy( Config.ScaleName, Name);
            ConfigSaveSection( ScaleName);
            break;

         case MM_COUNTRY :
            MenuCountry();
            break;

         case MM_WEIGHING :
            MenuWeighingUnits();
            break;

         case MM_MEMORY :
            MenuMemory();
            break;
#ifdef PROJECT_XBEE
         case MM_XBEE :
            MenuXBee();
            break;
#else
         case MM_PRINTER :
            MenuPrinter();
            break;
#endif
         case MM_AUTO_POWER_OFF :
            i = Config.PowerOffTimeout / 60;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, 0, POWER_OFF_TIMEOUT_MAX / 60, STR_MINUTES)){
               break;
            }
            Config.PowerOffTimeout  = i * 60;
            ConfigSaveSection( PowerOffTimeout);
            break;

         case MM_PASSWORD :
            PasswordEdit();
            break;

         case MM_RESTORE_FACTORY_DEFAULTS :
            if( !DMsgYesNo( STR_RESTORE_FACTORY_DEFAULTS, STR_REALLY_RESTORE_FACTORY_DEFAULTS, 0)){
               break;
            }
            Handle = Config.LastFile;  // Remember active file
            ConfigFactoryDefault();
            SetLastFile( Handle);
            // update immediately :
            ContrastSet();
            BacklightNormal();
            break;
      }
   }
} // MenuMaintenance

//******************************************************************************

//------------------------------------------------------------------------------
//  Menu Parameters
//------------------------------------------------------------------------------

static void MenuMaintenanceParameters( int Index, int y, void *Parameters)
// Maintenance menu parameters
{
   switch( Index){
      case MM_SCALE_NAME :
         DLabelNarrow( Config.ScaleName, DMENU_PARAMETERS_X, y);
         break;         

      case MM_AUTO_POWER_OFF :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Config.PowerOffTimeout / 60, STR_MINUTES);
         break;

      case MM_COUNTRY :
         DLabelEnum( Config.Country.Country, STR_COUNTRY_INTERNATIONAL, DMENU_PARAMETERS_X, y);
         break;

      case MM_PASSWORD :
         DLabelEnum( PasswordInactive() ? 0 : 1, STR_NO, DMENU_PARAMETERS_X, y);
         break;

      case MM_RESTORE_FACTORY_DEFAULTS :
         break;
   }
} // MenuMaintenanceParameters

//------------------------------------------------------------------------------
//  Last File
//------------------------------------------------------------------------------

static void SetLastFile( TFdirHandle Handle)
// Set last working file
{
   FdSetClass( SDB_CLASS);
   if( FdValid( Handle)){
      Config.LastFile = Handle;        // restore last file
      ConfigSaveSection( LastFile);
      return;
   }
   // last file is invalid
   if( FdCount() != 0){
      // there are some files
      FdFindBegin();
      if( (Handle = FdFindNext()) != FDIR_INVALID){
         Config.LastFile = Handle;     // set first file
         ConfigSaveSection( LastFile);
         return;
      }
   }
   // directory empty, create some file
   *SdbConfig() = Config.WeighingParameters;          // fill with defaults
   SdbCreate( BAT1_DEFAULT_FILE);                     // create default file name
   SdbClose();                                        // save file
   Config.LastFile = FdSearch( BAT1_DEFAULT_FILE);    // set this file active
   ConfigSaveSection( LastFile);
} // SetLastFile
