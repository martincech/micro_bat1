//******************************************************************************
//
//   Flagging.c    Flagging the weights
//   Version 1.0  (c) Veit 
//
//******************************************************************************

#include "Flagging.h"
#include "PWeighingDef.h"
#include "../Inc/File/Fd.h"       // File directory list
#include "Sdb.h"

TSampleFlag GetFlagWithGender(TSdbConfig *WeighingParameters, TWeight Weight, TYesNo Male)
// get flag by parameters for current weight and gender
{
TSampleFlag Flag = FLAG_INVALID;
int CategoryCount;

   switch(WeighingParameters->Saving.Mode){
      case SAVING_MODE_AUTOMATIC:
      case SAVING_MODE_MANUAL:
         switch( WeighingParameters->WeightSorting.Mode){
            case WEIGHT_SORTING_NONE:
               Flag = FLAG_NONE;
               break;

            case WEIGHT_SORTING_LIGHT_HEAVY :
               // check for limit
               if( Weight < WeighingParameters->WeightSorting.LowLimit){
                  Flag = FLAG_LIGHT;
               } else {
                  Flag = FLAG_HEAVY;
               }
               break;

            case WEIGHT_SORTING_LIGHT_OK_HEAVY :
               // check for both limits
               if( Weight < WeighingParameters->WeightSorting.LowLimit){
                  Flag = FLAG_LIGHT;
               } else if( Weight > WeighingParameters->WeightSorting.HighLimit){
                  Flag = FLAG_HEAVY;
               } else {
                  Flag = FLAG_OK;
               }
               break;
         
            case WEIGHT_SORTING_CATEGORY:
               Flag = FLAG_SORTING_CATEGORY1;
               CategoryCount = WeighingSortingCategoryCount( WeighingParameters->WeightSorting.Categories);
               for(int i = 1; i < CategoryCount ; i++) {
                  if( Weight >= WeighingParameters->WeightSorting.Categories[i].LowLimit && Weight <= WeighingParameters->WeightSorting.Categories[i].HighLimit){
                     Flag = FLAG_SORTING_CATEGORY1 + i;
                     break;
                  }
               }
               break;
         }
         break;
     case SAVING_MODE_MANUAL_BY_SEX:
         if(Male){
            Flag = FLAG_MALE;
         }else{
            Flag = FLAG_FEMALE;
         }
         break;
   }

   return Flag;
}

TSampleFlag GetFlag(TSdbConfig *WeighingParameters, TWeight Weight)
// get flag by parameters for current weight
{
   return GetFlagWithGender(WeighingParameters, Weight, YES);
}

void ReflagAllFiles( void)
// reflag all files according to file parameters
{
return;
int count;
int i;
TFdirHandle fd;
   count = FdCount();
   for(i = 0; i < count; i++){
      fd = FdGet(i);
      ReflagFile(fd);
   }
}

void ReflagFile(TFdirHandle File)
// reflag whole file according to file parameters

{
return;
TSdbRecord Record;
TSdbConfig *WConfig;
TSampleFlag Flag;
int count;
int i;

   if(!FdValid(File) || File == FDIR_INVALID){return;}
   SdbOpen(File, NO);
   if(Config.EnableFileParameters){
      WConfig = SdbConfig();
   }else{
      WConfig = &Config.WeighingParameters;
   }
   SdbBegin();
   count = SdbCount();
   for(i = 0; i < count; i++){
      SdbNext(&Record);
      if(Record.Flag == FLAG_MALE || Record.Flag == FLAG_FEMALE){
         // do not reflag manualy weighted genders
         continue;
      }
      Flag = GetFlag(WConfig, Record.Weight);
      if(Record.Flag != Flag){
         Record.Flag = Flag;
         //replace record in db
         SdbReplaceAt(i, &Record);
      }
   }
   SdbClose();
}

