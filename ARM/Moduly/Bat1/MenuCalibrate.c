//******************************************************************************
//                                                                            
//   MenuCalibrate.c  Calibration menu
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include "MenuCalibrate.h"

#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DMsg.h"             // Display message

#include "Str.h"                  // Strings
#include "Cal.h"                  // Calibration
#include "ConfigDef.h"            // Configuration
#include "Weighing.h"             // Weighing
#include "Wgt/DWeight.h"          // Display weight
#include "Beep.h"                 // Sound
#include "Screen.h"               // Screen

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuCalibrate( void)
// Process calibration
{
int        Range;
TRawWeight RawZero;
TRawWeight RawRange;

   // get physical range :
   Range = CalGetRange();
   // get released weight :
   if( !DMsgOkCancel( STR_CALIBRATION, STR_RELEASE_WEIGHT, 0)){
      return;
   }
   ScreenWait();
   RawZero = WeighingRaw();
   BeepCalibration();
   // get charged weight :
   if( !DInputWeight( STR_CALIBRATION, STR_ENTER_WEIGHT, &Range, 1, Config.Units.Range)){
      return;
   }
   ScreenWait();
   RawRange = WeighingRaw();
   // update calibration :
   CalUpdate( RawZero, RawRange, Range);
   CalSave();  
   WeighingRawTara( RawZero);          // set released weight as Tara
   BeepCalibration();
} // MenuCalibrate
