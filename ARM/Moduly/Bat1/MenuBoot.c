//******************************************************************************
//                                                                            
//   MenuBoot.c    Boot menu
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include "MenuBoot.h"
#include "../Inc/System.h"        // Operating system
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DEvent.h"    // Display event
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "Wgt/DInputSpin.h"       // Enter number
#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Wgt/DMsg.h"             // Display message
#include "Str.h"                  // Strings
#include "ConfigDef.h"            // Configuration data
#include "PMan.h"                 // Power management
#include "Accu.h"                 // Accumulator maitenance
#include "Backlight.h"            // Backlight
#include "ScreenAccu.h"           // Accumulator data
#include "Contrast.h"             // Display contrast
#include "Password.h"             // Password checking
#include "Fonts.h"                // Project fonts

//-----------------------------------------------------------------------------
// Strings
//-----------------------------------------------------------------------------
// boot menu title
static const char LSTR_BOOT_MENU[]           = "Service menu";

// boot menu :
static const char LSTR_CONTRAST[]            = "Contrast";
static const char LSTR_LANGUAGE[]            = "Language";
static const char LSTR_CLEAR_PASSWORD[]      = "Clear password";
static const char LSTR_BATTERY_CALIBRATION[] = "Battery calibration";
static const char LSTR_BATTERY_DATA[]        = "Battery data";

// boot menu messages :
static const char LSTR_REALLY_CLEAR_PASSWORD[] =  "Really clear password ?";
static const char LSTR_ENTER_NUMBER_OF_CYCLES[] = "Enter number of cycles";

static DefMenu( BootMenu)
   LSTR_CONTRAST,
   LSTR_LANGUAGE,
   LSTR_CLEAR_PASSWORD,
   LSTR_BATTERY_CALIBRATION,
   LSTR_BATTERY_DATA,
EndMenu()

typedef enum {
   BM_CONTRAST,
   BM_LANGUAGE,
   BM_CLEAR_PASSWORD,
   BM_BATTERY_CALIBRATION,
   BM_BATTERY_DATA,
} TBootMenuEnum;

// Local functions :
static void BootParameters( int Index, int y, void *UserData);
// Menu boot parameters

//------------------------------------------------------------------------------
//   Boot
//------------------------------------------------------------------------------

void MenuBoot( void)
// Display boot menu
{
TMenuData MData;
int       i;
unsigned  OldLanguage;
int       Cycles;
TYesNo    ShowResult;

   SetFont( TAHOMA16);
   DMenuClear( MData);
   forever {
      if( !DMenu( LSTR_BOOT_MENU, BootMenu, BootParameters, 0, &MData)){
         if( PManShutdown()){
            return;                    // power off
         }
         DEventDiscard();              // stop timeout chain
         DMenuClear( MData);           // go to first item
         continue;                     // no return on ESC
      }
      switch( MData.Item){
         case BM_CONTRAST :
            i = Config.Display.Contrast; 
            if( !DEditSpin( DMENU_EDIT_X, MData.y, &i, 0, CONTRAST_MAX, ContrastTest)){
               ContrastTest( CONTRAST_MAX / 2);    // default contrast
               break;
            }
            Config.Display.Contrast = i;
            ConfigSaveSection( Display.Contrast);
            break;

         case BM_LANGUAGE :
            OldLanguage = Config.Country.Language;         // save old language
            i = Config.Country.Language;                   // show old language
            CountrySetLanguage( LNG_ENGLISH);              // set english as current
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_LNG_CZECH, _LNG_COUNT)){
               CountrySetLanguage( OldLanguage);           // restore old language
               break;
            }
            CountrySetLanguage( i);
            ConfigSaveSection( Country);
            break;

         case BM_CLEAR_PASSWORD :
            if( !DMsgYesNo( LSTR_CLEAR_PASSWORD, LSTR_REALLY_CLEAR_PASSWORD, 0)){
               break;
            }
            PasswordInactivate();
            break;

         case BM_BATTERY_CALIBRATION :
            Cycles = 1;
            if( !DInputSpin( LSTR_BATTERY_CALIBRATION, LSTR_ENTER_NUMBER_OF_CYCLES, &Cycles, 1, 5, 0)){
               break;
            }
            BacklightBoot();           // backlight direct steering
            SysDisableTimeout();       // disable all timeouts
            ShowResult = YES;
            // run maintenance :
            do {
               if( !AccuMaitenance()){
                  ShowResult = NO;
                  break;
               }
            } while( --Cycles);
            if( ShowResult){
               ScreenAccumulatorData();// show accumulator data screen
               DEventWaitForEnterEsc();// wait for a key
            }
            SysEnableTimeout();        // enable all timeouts
            BacklightCharger();        // restore power save backlight
            break;

         case BM_BATTERY_DATA :
            ScreenAccumulatorData();   // show screen
            DEventWaitForEnterEsc();   // wait for a key
            break;
      }
   }
} // MenuBoot

//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

static void BootParameters( int Index, int y, void *UserData)
// Menu boot parameters
{
   switch( Index){
      case BM_CONTRAST :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Display.Contrast);
         break;

      case BM_LANGUAGE :
         DLabelEnum( Config.Country.Language, STR_LNG_CZECH, DMENU_PARAMETERS_X, y);
         break;

      case BM_CLEAR_PASSWORD :
         break;

      case BM_BATTERY_CALIBRATION :
         break;

      case BM_BATTERY_DATA :
         break;
   }
} // BootParameters
