//******************************************************************************
//
//   MenuSettings.c  Settings menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuSettings.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/System.h"        // Operating system
#include "../Inc/Dt.h"            // Date Time
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DEdit.h"            // Display input value
#include "Wgt/DEditVolume.h"      // Display edit volume
#include "Wgt/DMsg.h"             // Display message

#include "Str.h"                  // Strings
#include "Backlight.h"            // Backlight
#include "Contrast.h"             // Contrast

#include <string.h>


static DefMenu( SettingsMenu)
   STR_DISPLAY_BACKLIGHT,
   STR_DISPLAY_CONTRAST,
   STR_SAVING_VOLUME,
   STR_DATE_AND_TIME,
EndMenu()

typedef enum {
   ST_DISPLAY_BACKLIGHT,
   ST_DISPLAY_CONTRAST,
   ST_SAVING_VOLUME,
   ST_DATE_AND_TIME,
} TSettingsMenuEnum;

// Local functions :

static void SettingsParameters( int Index, int y, void *UserData);
// Menu Settings parameters

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuSettings( void)
// Settings menu
{
TMenuData  MData;
TLocalTime Local;
int        i;
int        OldMode;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_SETTINGS, SettingsMenu, SettingsParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case ST_DISPLAY_BACKLIGHT :
            // set backlight mode always on for testing :
            OldMode = Config.Display.Backlight.Mode;             // save old mode
            Config.Display.Backlight.Mode = BACKLIGHT_MODE_ON;   // set always on
            BacklightNormal();                                   // process mode
            // test intensity :
            i = Config.Display.Backlight.Intensity;
            if( !DEditSpin( DMENU_EDIT_X, MData.y, &i, 0, BACKLIGHT_MAX, BacklightTest)){
               Config.Display.Backlight.Mode = OldMode;          // restore old mode
               BacklightNormal();                                // process mode
               break;
            }
            Config.Display.Backlight.Mode = OldMode;             // restore mode
            Config.Display.Backlight.Intensity = i;
            ConfigSaveSection( Display.Backlight);
            BacklightNormal();                                   // process mode
            break;

         case ST_DISPLAY_CONTRAST :
            i = Config.Display.Contrast;
            if( !DEditSpin( DMENU_EDIT_X, MData.y, &i, 0, CONTRAST_MAX, ContrastTest)){
               ContrastSet();          // restore contrast
               break;
            }
            Config.Display.Contrast = i;
            ConfigSaveSection( Display.Contrast);
            break;

         case ST_SAVING_VOLUME :
            i = Config.Sounds.VolumeSaving;
            if( !DEditVolume( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Config.Sounds.VolumeSaving   = i;
            ConfigSaveSection( Sounds.VolumeSaving);
            break;

         case ST_DATE_AND_TIME :
            DtDecode( SysGetClock(), &Local);
            if( !DInputDate( STR_DATE_AND_TIME, STR_ENTER_DATE, &Local)){
               break;
            }
            if( !DInputTime( STR_DATE_AND_TIME, STR_ENTER_TIME, &Local)){
               break;
            }
            SysSetClock( DtEncode( &Local));
            break;
      }
   }
} // MenuSettings

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void SettingsParameters( int Index, int y, void *UserData)
// Menu Settings parameters
{
   switch( Index){
      case ST_DISPLAY_BACKLIGHT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Display.Backlight.Intensity);
         break;

      case ST_DISPLAY_CONTRAST :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Display.Contrast);
         break;

      case ST_SAVING_VOLUME :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Sounds.VolumeSaving);
         break;

      case ST_DATE_AND_TIME :
         break;
   }
} // SettingsParameters
