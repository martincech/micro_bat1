//******************************************************************************
//
//   Printer.h    Printer services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Printer_H__
   #define __Printer_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FdirDef_H__
   #include "../Inc/File/FdirDef.h"
#endif

#ifndef __GroupDef_H__
   #include "GroupDef.h"
#endif

#ifndef __Calc_H__
   #include "Calc.h"
#endif

typedef enum {
   PROTOCOL_STATISTICS_HISTOGRAM,          // print statistics & histogram
   PROTOCOL_STATISTICS,                    // print statistics
   PROTOCOL_TOTAL_STATISTICS,              // print total statistics only
   PROTOCOL_SAMPLES,                       // print samples
   _PROTOCOL_TYPE_COUNT,
} TPrinterProtocol;

typedef enum {
   PRINT_FILE,
   PRINT_GROUP,
   PRINT_MORE_FILES,
} TPrintSet;

void PrinterSetup( void);
// Set printer communication parameters

void PrinterPrint( const char *Name, TGroup Group, TPrintSet Set, TCalcMode Mode, TPrinterProtocol Protocol);
// Print <Protocol> of the <Group>/<Mode> with title <Name>

void PrinterWeight( int Index, TWeight Weight);
// Print single <Weight> with <Index>

void PrinterActualWeight( TWeight Weight);
// Print single <Weight>

#endif
