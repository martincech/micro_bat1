//******************************************************************************
//                                                                            
//   MenuBoot.h    Boot menu
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef __MenuBoot_H__
   #define __MenuBoot_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuBoot( void);
// Display boot menu

#endif
