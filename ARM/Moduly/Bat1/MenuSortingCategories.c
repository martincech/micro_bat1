//******************************************************************************
//                                                                            
//   MenuSortingCategories.c  Menu sorting categories
//   Version 1.0             (c) Veit Electronics
//
//******************************************************************************

#include "MenuSortingCategories.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/File/Fd.h"       // File directory
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "Wgt/DSortingCategories.h"   // sorting categories
#include "MenuSortingCategory.h"

#include "Wgt/DMsg.h"             // Display message
#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DWeight.h"          // Display weight value

#include "Str.h"                  // Strings

static DefMenu( SortingCategoriesMenu)
   STR_EDIT,
   STR_CREATE,
   STR_DELETE,
EndMenu()

typedef enum {
   MM_EDIT,
   MM_CREATE,
   MM_DELETE
} TSortingCategoriesMenuEnum;

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuSortingCategories( TWeightSortingCategory *Categories)
// Sorting categories menu
{
TMenuData   MData;
TWeightSortingCategory *Cat;
int CategoryCount;
TYesNo Default;

   // run menu :
   DMenuClear( MData);
   forever {
      CategoryCount = WeighingSortingCategoryCount(Categories);
      
      switch(CategoryCount) {
         case 0:
            MData.Mask = (1 << MM_EDIT) | (1 << MM_DELETE);
            MData.Item = MM_CREATE;
            break;

         case 1:
            MData.Mask = (1 << MM_DELETE);
            MData.Item = MM_CREATE;
            break;

         case WEIGHING_SORTING_CATEGORIES_COUNT:
            MData.Mask = (1 << MM_CREATE);
            if(MData.Item == MM_CREATE) {
               MData.Item = MM_EDIT;
            }
            break;

         default:
            MData.Mask = 0;
            break;
      }
      if( !DMenu( STR_SORTING_CATEGORIES, SortingCategoriesMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MM_CREATE :
            Cat = &Categories[CategoryCount];
            memset(Cat, 0, sizeof(TWeightSortingCategory));
            if(WeighingSortingCategoryIsDefault(Cat, Categories)) {
               Default = YES;
            } else {
               Default = NO;
            }
            MenuSortingCategory( Cat, Default);
            break;
            
        case MM_EDIT :
            if(!(Cat = DSortingCategories(Categories, CategoryCount))) {
               continue;
            }
            if(WeighingSortingCategoryIsDefault(Cat, Categories)) {
               Default = YES;
            } else {
               Default = NO;
            }
            MenuSortingCategory( Cat, Default);
            break;

        case MM_DELETE :
            if(!(Cat = DSortingCategories(Categories, CategoryCount))) {
               continue;
            }
            if(WeighingSortingCategoryIsDefault(Cat, Categories)) {
               DMsgOk( STR_SORTING_CATEGORY, STR_UNABLE_DELETE_DEFAULT_CATEGORY1, STR_UNABLE_DELETE_DEFAULT_CATEGORY2);
               continue;
            }
            if( !DMsgOkCancel( STR_SORTING_CATEGORY, STR_DELETE_CATEGORY, 0)){
               continue;
            }
            WeighingSortingCategoryInvalidate(Cat);
            void *To = Cat;
            void *From = (void *)(((int)Cat) + sizeof(TWeightSortingCategory));
            int Length = ((int)&Categories[WEIGHING_SORTING_CATEGORIES_COUNT - 1]) - ((int)Cat);
            memcpy(To, From, Length);
            memset(&Categories[WEIGHING_SORTING_CATEGORIES_COUNT - 1], 0, sizeof(TWeightSortingCategory));
            break;
      }
   }
} // MenuSortingCategories