//******************************************************************************
//                                                                            
//   MenuSortingCategories.h  Menu sorting categories
//   Version 1.0              (c) Veit Electronics
//
//******************************************************************************

#ifndef __MenuSortingCategories_H__
   #define __MenuSortingCategories_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"
#endif

void MenuSortingCategories( TWeightSortingCategory *Categories);
// Sorting categories menu

#endif
