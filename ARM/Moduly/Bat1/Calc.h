//*****************************************************************************
//
//    Calc.h - Statistic calculations
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __Calc_H__
   #define __Calc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Sample_H__
   #include "SdbDef.h"                 // sample flag only
#endif

#ifndef __CalcDef_H__
   #include "CalcDef.h"                // calculation record
#endif

// parameters
typedef enum {
   STATISTICS_TOTAL,                   // cumulative statistics
   STATISTICS_LIGHT,                   // light samples statistics
   STATISTICS_OK,                      // OK samples statistics
   STATISTICS_HEAVY,                   // heavy samples statistics
   STATISTICS_MAX,                     // max. number of statistics
} TStatisticsType;

#define STATISTICS_MALE   STATISTICS_LIGHT       // male samples
#define STATISTICS_FEMALE STATISTICS_OK          // female samples

#define STATISTICS_CATEGORY_ON_SCREEN1    STATISTICS_LIGHT
#define STATISTICS_CATEGORY_ON_SCREEN2    STATISTICS_OK
#define STATISTICS_CATEGORY_ON_SCREEN3    STATISTICS_HEAVY

#define STATISTICS_CATEGORY_ON_SCREEN_COUNT    3

typedef enum {
   CALC_TOTAL,                         // total statistics only
   CALC_BY_SEX,                        // group by sex
   CALC_LIGHT_HEAVY,                   // group by weight - light/heavy
   CALC_LIGHT_OK_HEAVY,                // group by weight - light/OK/heavy
   CALC_CATEGORY,
} TCalcMode;

extern TCalc Calc[ STATISTICS_MAX];    // global calculation data

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------
void CalcBackup( void);
// backup calculations

void CalcRestoreBackup( void);
// restore previous backup

void CalcModeCategoryOffsetSet( int Offset);
// Set calculation category offset

void CalcMode( TCalcMode Mode);
// Set calculation mode

void CalcStatistics( void);
// Calculate statistic of the dataset

void CalcAppend( TWeight Weight, TSampleFlag Flag);
// Append sample & recalculate

void CalcDelete(TWeight Weight, TSampleFlag Flag);
// Remove last added sample & recalculate
#endif
