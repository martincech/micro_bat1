//*****************************************************************************
//
//    PAccu.c      Accumulator parameters utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "PAccu.h"
#include "../Inc/Eep.h"             // EEPROM services
#include "MemoryDef.h"              // EEPROM layout
#include <string.h>

// EEPROM address :
#define PACCU_ADDRESS  offsetof( TEeprom, AccuData)

static TPAccuData _AccuData;           // global data

// Local functions :

static void PAccuDefault( void);
// Default values

static TPAccuCrc PAccuCrc( void);
// Calculate CRC

//-----------------------------------------------------------------------------
//   Load
//-----------------------------------------------------------------------------

void PAccuLoad( void)
// Load maitenance data from EEPROM
{
   EepLoadData( PACCU_ADDRESS, &_AccuData, sizeof( TPAccuData));
   if( PAccuCrc() == _AccuData.CheckSum){
      return;                          // load ok
   }
   // wrong checksum, set defaults
   PAccuDefault();
   PAccuSave();
} // PAccuLoad

//-----------------------------------------------------------------------------
//   Save
//-----------------------------------------------------------------------------

void PAccuSave( void)
// Save maintenance data to EEPROM
{
   _AccuData.CheckSum = PAccuCrc();
   EepSaveData( PACCU_ADDRESS, &_AccuData, sizeof( TPAccuData));
} // PAccuSave

//-----------------------------------------------------------------------------
//   Voltage
//-----------------------------------------------------------------------------

unsigned PAccuVoltage( unsigned RawValue)
// Calculates voltage [mV] by <RawValue>
{
   return( PAccuGetVoltage( RawValue, _AccuData.Range));
} // PAccuVoltage

//-----------------------------------------------------------------------------
//   Raw
//-----------------------------------------------------------------------------

unsigned PAccuRawVoltage( unsigned Value)
// Calculate raw voltage [LSB] by <Value> [mV]
{
   return( PAccuGetRawVoltage( Value, _AccuData.Range));
} // PAccuRawVoltage

//-----------------------------------------------------------------------------
//   Capacity
//-----------------------------------------------------------------------------

unsigned PAccuCapacity( unsigned RawValue)
// Calculate remaining capacity [%] by <RawValue>
{
   if( RawValue >= _AccuData.UMax){
      return( 100);                    // above limit
   }
   if( RawValue <= _AccuData.UMin){
      return( 0);                      // under limit
   }
   if( RawValue >= _AccuData.UMid){
      // range 100%..50%
      return( 50 + ((RawValue - _AccuData.UMid) * 50) / (_AccuData.UMax - _AccuData.UMid));
   }
   // range 50%..0%
   return( ((RawValue - _AccuData.UMin) * 50) / (_AccuData.UMid - _AccuData.UMin));
} // PAccuCapacity

//-----------------------------------------------------------------------------
//   Calibrate
//-----------------------------------------------------------------------------

void PAccuCalibrate( unsigned RawValue, unsigned Value)
// Calculate calibration data by <RawValue> [LSB] and <Value> [mV]
{
   _AccuData.Range = (word)((Value * PACCU_ADC_RANGE) / RawValue);
} // PAccuCalibrate

//-----------------------------------------------------------------------------
//   Range
//-----------------------------------------------------------------------------

unsigned PAccuGetRange( void)
// Get physical range [mV]
{
   return( _AccuData.Range);
} // PAccuGetRange

//-----------------------------------------------------------------------------
//   UMax
//-----------------------------------------------------------------------------

void PAccuSetUMax( unsigned RawValue)
// Set Umax voltage
{
   _AccuData.UMax = RawValue;
} // PAccuSetUMax

unsigned PAccuGetUMax( void)
// Get Umax voltage
{
   return( _AccuData.UMax);
} // PAccuGetUMax

//-----------------------------------------------------------------------------
//   UMid
//-----------------------------------------------------------------------------

void PAccuSetUMid( unsigned RawValue)
// Set Umid voltage
{
   _AccuData.UMid = RawValue;
} // PAccuSetUMid

unsigned PAccuGetUMid( void)
// Get Umid voltage
{
   return( _AccuData.UMid);
} // PAccuGetUMid

//-----------------------------------------------------------------------------
//   UMin
//-----------------------------------------------------------------------------

void PAccuSetUMin( unsigned RawValue)
// Set Umin voltage
{
   _AccuData.UMin = RawValue;
} // PAccuSetUMin

unsigned PAccuGetUMin( void)
// Get Umin voltage
{
   return( _AccuData.UMin);
} // PAccuGetUMin

//-----------------------------------------------------------------------------
//   UWarn
//-----------------------------------------------------------------------------

void PAccuSetUWarn( unsigned RawValue)
// Set Uwarn voltage
{
   _AccuData.UWarn = RawValue;
} // PAccuSetUWarn

unsigned PAccuGetUWarn( void)
// Get Uwarn voltage
{
   return( _AccuData.UWarn);
} // PAccuGetUWarn

//-----------------------------------------------------------------------------
//   Charge time
//-----------------------------------------------------------------------------

void PAccuSetChargeTime( unsigned Min)
// Set charge time to <Min>
{
   _AccuData.ChargeTime = Min;
} // PAccuSetChargeTime

unsigned PAccuGetChargeTime( void)
// Get charge time [Min]
{
   return( _AccuData.ChargeTime);
} // PAccuGetChargeTime

//-----------------------------------------------------------------------------
//   Discharge time
//-----------------------------------------------------------------------------

void PAccuSetDischargeTime( unsigned Min)
// Set discharge time to <Min>
{
   _AccuData.DischargeTime = Min;
} // PAccuSetDischargeTime

unsigned PAccuGetDischargeTime( void)
// Get discharge time [Min]
{
   return( _AccuData.DischargeTime);
} // PAccuGetDischargeTime

//-----------------------------------------------------------------------------
//   Default
//-----------------------------------------------------------------------------

static void PAccuDefault( void)
// Default values
{
   memset( &_AccuData, 0, sizeof( TPAccuData));
   _AccuData.Range = PACCU_RANGE;
} // PAccuDefault

//-----------------------------------------------------------------------------
//   CRC
//-----------------------------------------------------------------------------

#define PACCU_DATA_SIZE    (int)offsetof( TPAccuData, CheckSum)

static TPAccuCrc PAccuCrc( void)
// Calculate CRC
{
byte      *p;
TPAccuCrc Crc;
int       i;

   p   = (byte *)&_AccuData;
   Crc = 0;
   for( i = 0; i < PACCU_DATA_SIZE; i++){
      Crc += *p;
      p++;
   }
   return( ~Crc);
} // PAccuCrc
