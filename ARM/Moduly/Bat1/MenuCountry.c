//******************************************************************************
//                                                                            
//   MenuCountry.c   Country configuration menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuCountry.h"
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "Str.h"                  // Strings
#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DEdit.h"            // Display edit value

static DefMenu( CountryMenu)
   STR_COUNTRY,
   STR_LANGUAGE,
   STR_DATE_FORMAT,
   STR_TIME_FORMAT,
   STR_DAYLIGHT_SAVING_TIME,
EndMenu()

typedef enum {
   CM_COUNTRY,
   CM_LANGUAGE,
   CM_DATE_FORMAT,
   CM_TIME_FORMAT,
   CM_DAYLIGHT_SAVING_TIME,
} TCountryMenuEnum;

// Local functions :

static void CountryParameters(  int Index, int y, void *UserData);
// Menu country parameters

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuCountry( void)
// Country configuration menu
{
TMenuData MData;
int       i;
char      Buff[ 2];

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_COUNTRY, CountryMenu, CountryParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case CM_COUNTRY :
            i = Config.Country.Country;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_COUNTRY_INTERNATIONAL, _COUNTRY_COUNT)){
               break;
            }
            CountryLoad( i);
            ConfigSaveSection( Country);
            break;

         case CM_LANGUAGE :
            i = Config.Country.Language;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_LNG_CZECH, _LNG_COUNT)){
               break;
            }
            CountrySetLanguage( i);
            ConfigSaveSection( Country);
            break;

         case CM_DATE_FORMAT :
            i = Config.Country.Locale.DateFormat;
            if( !DInputEnum( STR_DATE_FORMAT, STR_ENTER_DATE_FORMAT, &i, STR_DATE_FORMAT_DDMMYYYY, _DATE_FORMAT_COUNT)){
               break;
            }
            Config.Country.Locale.DateFormat = i;
            Buff[ 0] = Config.Country.Locale.DateSeparator1;
            Buff[ 1] = '\0';
            if( !DInputText( STR_DATE_FORMAT, STR_ENTER_DATE_SEPARATOR1, Buff, 1, NO)){
               break;
            }
            Config.Country.Locale.DateSeparator1 = Buff[ 0];
            Buff[ 0] = Config.Country.Locale.DateSeparator2;
            Buff[ 1] = '\0';
            if( !DInputText( STR_DATE_FORMAT, STR_ENTER_DATE_SEPARATOR2, Buff, 1, NO)){
               break;
            }
            Config.Country.Locale.DateSeparator2 = Buff[ 0];
            ConfigSaveSection( Country.Locale);
            break;

         case CM_TIME_FORMAT :
            i = Config.Country.Locale.TimeFormat;
            if( !DInputEnum( STR_TIME_FORMAT, STR_ENTER_TIME_FORMAT, &i, STR_TIME_FORMAT_24, _TIME_FORMAT_COUNT)){
               break;
            }
            Config.Country.Locale.TimeFormat = i;
            Buff[ 0] = Config.Country.Locale.TimeSeparator;
            Buff[ 1] = '\0';
            if( !DInputText( STR_TIME_FORMAT, STR_ENTER_TIME_SEPARATOR, Buff, 1, NO)){
               break;
            }
            Config.Country.Locale.TimeSeparator = Buff[ 0];            
            ConfigSaveSection( Country.Locale);
            break;

         case CM_DAYLIGHT_SAVING_TIME :
            i = Config.Country.Locale.DstType;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_DST_TYPE_OFF, _DT_DST_TYPE_COUNT)){
               break;
            }
            Config.Country.Locale.DstType = i;
            ConfigSaveSection( Country.Locale);
            break;
      }
   }
} // MenuCountry

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void CountryParameters(  int Index, int y, void *UserData)
// Menu country parameters
{
   switch( Index){
      case CM_COUNTRY :
         DLabelEnum( Config.Country.Country, STR_COUNTRY_INTERNATIONAL, DMENU_PARAMETERS_X, y);
         break;

      case CM_LANGUAGE :
         DLabelEnum( Config.Country.Language, STR_LNG_CZECH, DMENU_PARAMETERS_X, y);
         break;

      case CM_DATE_FORMAT :
         DLabelEnum( Config.Country.Locale.DateFormat, STR_DATE_FORMAT_DDMMYYYY, DMENU_PARAMETERS_X, y);
         break;

      case CM_TIME_FORMAT :
         DLabelEnum( Config.Country.Locale.TimeFormat, STR_TIME_FORMAT_24, DMENU_PARAMETERS_X, y);
         break;

      case CM_DAYLIGHT_SAVING_TIME :
         DLabelEnum( Config.Country.Locale.DstType, STR_DST_TYPE_OFF, DMENU_PARAMETERS_X, y);
         break;
   }
} // CountryParameters
