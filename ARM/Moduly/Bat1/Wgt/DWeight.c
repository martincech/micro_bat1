//******************************************************************************
//
//   DWeight.c    Display weight
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "DWeight.h"
#include "../../Inc/conio.h"         // putchar
#include "../../Inc/Graphic.h"       // graphics
#include "../../Inc/sputchar.h"      // string putchar
#include "../Inc/Wgt/DLabel.h"       // Display label
#include "../xWeight.h"              // weight formatting

#include "../Str.h"                  // Strings
#include "DInput.h"                  // Display input value
#include "DEdit.h"                   // Display edit value

#include "../ConfigDef.h"            // Configuration

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

void DWeight( TWeight Weight)
// Display weight
{
   xWeight( putchar, Weight);
} // DWeight

//------------------------------------------------------------------------------
// Weight left
//------------------------------------------------------------------------------

void DWeightLeft( TWeight Weight)
// Display weight left aligned
{
   xWeightLeft( putchar, Weight);
} // DWeightLeft

//------------------------------------------------------------------------------
// Flag
//------------------------------------------------------------------------------

void DWeighingFlag( TSampleFlag Flag)
// Display weighing flag
{
   xWeighingFlag( putchar, Flag);
} // DWeighingFlag

//------------------------------------------------------------------------------
// Units
//------------------------------------------------------------------------------

void DWeighingUnits( void)
// Display current units
{
   xWeighingUnits( putchar);
} // DWeighingUnits

//------------------------------------------------------------------------------
// Weight & Units
//------------------------------------------------------------------------------

void DWeightWithUnits( TWeight Weight)
// Display weight with units
{
   xWeightWithUnits( putchar, Weight);
} // DWeightWithUnits

//------------------------------------------------------------------------------
// Weight & Units left
//------------------------------------------------------------------------------

void DWeightWithUnitsNarrow( int x, int y, TWeight Weight)
// Display weight with units left aligned
{
char Buffer[ DWEIGHT_MAX_LENGTH];

   DWeightWithUnitsFormat( Buffer, Weight);
#ifndef DALIGN_RIGHT
   DLabel( Buffer, x, y);
#else
   DLabelRight( Buffer, x, y);
#endif
} // DWeightWithUnitsNarrow

//------------------------------------------------------------------------------
// Weight narrow
//------------------------------------------------------------------------------

void DWeightNarrow( int x, int y, TWeight Weight)
// Display weight with narrow decimal dot
{
   GTextAt( x, y);
   GSetNumericPitch();
   xWeight( putchar, Weight);
   GSetNormalPitch();
} // DWeightNarrow

//------------------------------------------------------------------------------
// Weight & Units to buffer
//------------------------------------------------------------------------------

void DWeightWithUnitsFormat( char *Buffer, TWeight Weight)
// Format <Weight> with units to <Buffer>
{
   sputcharbuffer( Buffer);
   xWeightWithUnitsLeft( sputchar, Weight);
   sputchar( '\0');                    // string terminator
} // DWeightWithUnitsFormat

//------------------------------------------------------------------------------
// Input
//------------------------------------------------------------------------------

TYesNo DInputWeight( TUniStr Caption, TUniStr Text, int *Value, int LoLimit, int HiLimit)
// Input weight
{
   return( DInputNumber( Caption, Text, Value, Config.Units.Decimals, LoLimit, HiLimit, 
                         STR_UNITS_KG + Config.Units.Units));
} // DInputWeight

//------------------------------------------------------------------------------
// Edit
//------------------------------------------------------------------------------

TYesNo DEditWeight( int x, int y, int *Value, int LoLimit, int HiLimit)
// Edit weight
{
   return( DEditNumber( x, y, Value, Config.Units.Decimals, LoLimit, HiLimit, 
                        STR_UNITS_KG + Config.Units.Units));
} // DEditWeight

//------------------------------------------------------------------------------
// Weight width
//------------------------------------------------------------------------------

int DWeightWidth( void)
// Returns weight width (pixels)
{
   return( xWeightWidth() * GCharWidth());
} // DWeightWidth

//------------------------------------------------------------------------------
// Narrow Weight width
//------------------------------------------------------------------------------

int DWeightWidthNarrow( void)
// Returns weight width with narrow decimal dot (pixels)
{
   return( (xWeightWidth() - 1) * GCharWidth() + GTextWidth( "."));
} // DWeightWidthNarrow
