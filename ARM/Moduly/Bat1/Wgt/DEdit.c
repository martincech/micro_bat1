//******************************************************************************
//                                                                            
//   DEdit.c        Display edit box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DEdit.h"
#include "../../inc/wgt/DEnter.h"
#include "../../inc/wgt/DEnterList.h"
#include "../../inc/wgt/DMsg.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/Graphic.h"
#include "../../inc/conio.h"
#include "../../inc/Bcd.h"
#include "DFormat.h"                    // Display formats
#include "../Str.h"                     // strings from project directory
#include "../Bitmap.h"                  // project bitmaps
#include "../Fonts.h"                   // project fonts
#include <string.h>                     // strlen

#define DEDIT_HEIGHT  19                // edit field height

static void DrawFrame( int x, int y, int Width, int Height);
// Draw edit area frame

//------------------------------------------------------------------------------
//  Integer
//------------------------------------------------------------------------------

TYesNo DEditNumber( int x, int y, int *Value, 
                    int Decimals, int LoLimit, int HiLimit, TUniStr Units)
// Edit number
{
int  NWidth;
int  EditWidth;
int  NewValue;
char RangeTxt[ 32];
int  TxtWidth;

   SetFont( DEDIT_FONT);
   EditWidth = BcdGetWidth( HiLimit);
   NWidth    = EditWidth;
   if( NWidth <= Decimals){
      NWidth = Decimals + 1;           // fraction only, add 0.000
   }
   NewValue  = *Value;
   // units
#ifndef DALIGN_RIGHT
   TxtWidth = DEnterNumberWidth( NWidth, Decimals) + GLetterWidth( ' ');
   if( Units){
      DLabel( Units, x + TxtWidth, y);
   }
#else
   if( Units){
      TxtWidth = DEnterNumberWidth( NWidth, Decimals) + GLetterWidth( ' ') + DLabelWidth( Units);
      GSetColor( DCOLOR_ENTER_BG);
      GBox( x - TxtWidth, y, TxtWidth, DENTER_H);
      GSetColor( DCOLOR_ENTER);
      DLabelRight( Units, x, y);
      GSetColor( DCOLOR_DEFAULT);
      x -= GLetterWidth( ' ') + DLabelWidth( Units);
   }
   x -= DEnterNumberWidth( NWidth, Decimals);
#endif
   // edit value
   if( !DEnterNumber( &NewValue, NWidth, Decimals, EditWidth, x, y)){
      return( NO);                     // escape
   }
   // check for range
   if( NewValue >= LoLimit && NewValue <= HiLimit){
      *Value = NewValue;
      return( YES);                    // inside range
   }
   // format & show range
   DFormatRange( RangeTxt, Decimals, LoLimit, HiLimit, Units);
   DMsgOk( STR_ERROR, STR_OUT_OF_LIMITS, RangeTxt);
   return( NO);                        // number out of range
} // DEditNumber

//------------------------------------------------------------------------------
//  Enum
//------------------------------------------------------------------------------

TYesNo DEditEnum( int x, int y, int *Value, 
                  TUniStr Base, int EnumCount)
// Edit enum
{
   return( DEditEnumCallback( x, y, Value, Base, EnumCount, 0));
} // DEditEnum

//------------------------------------------------------------------------------
//  Enum with callback
//------------------------------------------------------------------------------

TYesNo DEditEnumCallback( int x, int y, int *Value, 
                          TUniStr Base, int EnumCount, TAction *OnChange)
// Edit enum with <OnChange> callback
{
int TxtWidth;

   SetFont( DEDIT_FONT);
   TxtWidth = DEnterEnumWidth( Base, EnumCount);
   DrawFrame( x, y, TxtWidth, DEDIT_HEIGHT);
#ifndef DALIGN_RIGHT
   // edit enum :
   return( DEnterEnum( Value, Base, EnumCount, OnChange, x, y, CENTER_LEFT));
#else
   x -= TxtWidth;
   // edit enum :
   return( DEnterEnum( Value, Base, EnumCount, OnChange, x, y, CENTER_RIGHT));
#endif
} // DEditEnum

#ifdef DEDIT_TEXT
//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DEditText( int x, int y, char *String, int CharCount)
// Edit text up to <Width> letters
{
   SetFont( DEDIT_FONT);
#ifdef DALIGN_RIGHT
   x -= DEnterTextWidth( CharCount);
#endif
   // edit text :
   if( !DEnterText( String, CharCount, x, y)){
      return( NO);                  // escape
   }
   StrTrimRight( String);
   if( strlen( String) > 0){
      return( YES);         
   }
   if( CharCount == 1){
      // single character - accept space
      String[ 0] = ' ';
      String[ 1] = '\0';
      return( YES);
   }
   DMsgOk( STR_ERROR, STR_STRING_EMPTY, 0);
   return( NO);
} // DEditText

#endif // DEDIT_TEXT

//------------------------------------------------------------------------------
//  List
//------------------------------------------------------------------------------

TYesNo DEditList( int x, int y, int *Value, const TUniStr *List)
// Edit list
{
int TxtWidth;

   SetFont( DEDIT_FONT);
   TxtWidth = DEnterListWidth( List);
   DrawFrame( x, y, TxtWidth, DEDIT_HEIGHT);
#ifndef DALIGN_RIGHT
   // edit list :
   return( DEnterList( Value, List, x, y, CENTER_LEFT));
#else
   x -= TxtWidth;
   // edit list :
   return( DEnterList( Value, List, x, y, CENTER_RIGHT));
#endif
} // DEditList

//------------------------------------------------------------------------------
//  Spin
//------------------------------------------------------------------------------

TYesNo DEditSpin( int x, int y, int *Value, 
                  int MinValue, int MaxValue, TAction *OnChange)
// Edit value by spinner
{
int TxtWidth;

   SetFont( DEDIT_FONT);
   TxtWidth = DEnterSpinWidth( MaxValue);
   DrawFrame( x, y, TxtWidth, DEDIT_HEIGHT);
#ifdef DALIGN_RIGHT
   x -= TxtWidth;
#endif
   // edit spin :
   return( DEnterSpin( Value, MinValue, MaxValue, OnChange, x, y));
} // DEditSpin

//------------------------------------------------------------------------------
//  Frame
//------------------------------------------------------------------------------

static void DrawFrame( int x, int y, int Width, int Height)
// Draw edit area frame
{
   // draw arrows
   GSetColor( DCOLOR_DEFAULT);
#ifndef DALIGN_RIGHT
   GBitmap( x + Width / 2 - 4, y - 5,          &BmpEditUp);
   GBitmap( x + Width / 2 - 4, y + Height + 1, &BmpEditDown);
#else
   GBitmap( x - Width / 2 - 4, y - 5,          &BmpEditUp);
   GBitmap( x - Width / 2 - 4, y + Height + 1, &BmpEditDown);
#endif
} // DrawFrame
