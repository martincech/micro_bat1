//******************************************************************************
//                                                                            
//  DSortingCategories.h     Display samples database
//  Version 1.0              (c) Veit Electronics
//
//******************************************************************************

#ifndef __DSortingCategories_H__
   #define __DSortingCategories_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"           // weighing parameters
#endif

TWeightSortingCategory *DSortingCategories( TWeightSortingCategory *Categories, int Count);
// Display sorting categories

#endif


