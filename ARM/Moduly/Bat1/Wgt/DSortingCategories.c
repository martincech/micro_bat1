//******************************************************************************
//                                                                            
//  DSortingCategories.c     Display samples database
//  Version 1.0              (c) Veit Electronics
//
//******************************************************************************

#include "DSortingCategories.h"
#include "../../Inc/Wgt/DEvent.h"
#include "../../Inc/Wgt/DMsg.h"
#include "../../Inc/Graphic.h"
#include "../../Inc/conio.h"

#include "DLayout.h"                      // Display layout
#include "DWeight.h"                      // Display weight
#include "../Beep.h"                      // Sounds
#include "../Str.h"                       // project directory strings
#include "../Screen.h"                    // Wait
#include "../Fonts.h"                     // Project fonts
#include "../Bitmap.h"                    // Bitmaps

// Constants :
#define DDB_TEXT_Y         23                    // top of the text area
#define DDB_LINE_HEIGHT    19
#define DDB_ITEM_HEIGHT   (2 * DDB_LINE_HEIGHT)  // database item height

#define DSAMPLES_HEIGHT    (G_HEIGHT - DDB_TEXT_Y - DLAYOUT_STATUS_H)  // items area height
#define DSAMPLES_COUNT     (DSAMPLES_HEIGHT / DDB_ITEM_HEIGHT)         // items per page

// Local functions :
static TYesNo NextPage( int *CurrentPage, int *RowCount, int Count);
// Move at next page

static TYesNo PreviousPage( int *CurrentPage, int *RowCount, int Count);
// Move at previous page

//static void UpdatePage( int *CurrentPage, int *CurrentItem, int *RowCount, int Count);
// Update page settings

static void ReadPage( int CurrentPage, int *RowCount, TWeightSortingCategory *Categories, int Count);
// Read page from database

static void DisplayPage( int CurrentPage, int LastCursorRow, int CursorRow, int RowCount);
// Display database page

static TWeightSortingCategory *PageRecord;

//------------------------------------------------------------------------------
//  Display
//------------------------------------------------------------------------------

TWeightSortingCategory *DSortingCategories( TWeightSortingCategory *Categories, int Count)
// Display sorting categories
{
int  RowCount;                // rows count
int  CurrentItem;             // active row
int  LastItem;                // last active row
int  CurrentPage;             // start of page offset
int  LastPage;                // last start of page offset

   GClear();                           // clear display
   DLayoutFileTitle( STR_SORTING_CATEGORIES); //STR_SORTING_CATEGORIES);            // display title
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);     // display status line
   CurrentItem =  0;                   // first row
   LastItem    = -1;                   // force redraw
   CurrentPage =  0;                   // first page
   LastPage    = -1;                   // force redraw
   forever {
      if( CurrentPage != LastPage){
         // redraw page
         ReadPage( CurrentPage, &RowCount, Categories, Count);
         LastPage = CurrentPage;
         LastItem = -1;             // redraw all page
      }
      if( CurrentItem != LastItem){
         DisplayPage( CurrentPage, LastItem, CurrentItem, RowCount);
         LastItem = CurrentItem;       // remember last
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CurrentItem > 0){
               BeepKey();
               CurrentItem--;
               break;
            }
            if( !PreviousPage( &CurrentPage, &RowCount, Count)){
               break;
            }
            CurrentItem = RowCount - 1;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CurrentItem < RowCount - 1){
               BeepKey();
               CurrentItem++;
               break;
            }
            if( !NextPage( &CurrentPage, &RowCount, Count)){
               break;
            }
            CurrentItem = 0;
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            if( !PreviousPage( &CurrentPage, &RowCount, Count)){
               break;
            }
            if( CurrentItem >= RowCount){
               CurrentItem = RowCount - 1;       // short page (at rollover) - move cursor at last item               
            }
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            if( !NextPage( &CurrentPage, &RowCount, Count)){
               break;
            }
            if( CurrentItem >= RowCount){
               CurrentItem = RowCount - 1;       // short page - move cursor at last item               
            }
            break;

         case K_ENTER :
            BeepKey();
            return &Categories[CurrentPage + CurrentItem];

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return 0;
      }     
   }
} // DSamplesDisplay

//------------------------------------------------------------------------------
//  Next page
//------------------------------------------------------------------------------

static TYesNo NextPage( int *CurrentPage, int *RowCount, int Count)
// Move at next page
{
int TotalCount;
int NextCount;

   TotalCount = Count;
   if( TotalCount <= DSAMPLES_COUNT){
      BeepError();
      return( NO);                     // one page only
   }
   if( *CurrentPage + DSAMPLES_COUNT >= TotalCount){
      // last visible page
      BeepRollover();
      *CurrentPage = 0;                // move to first page
      *RowCount    = DSAMPLES_COUNT;   // at least one page present
      return( YES);
   }
   BeepKey();
   *CurrentPage += DSAMPLES_COUNT;     // move to next page
   NextCount    = TotalCount - *CurrentPage;
   if( NextCount >= DSAMPLES_COUNT){
      *RowCount = DSAMPLES_COUNT;      // whole page
   } else {
      *RowCount = NextCount;           // short page
   }
   return( YES);
} // NextPage

//------------------------------------------------------------------------------
//  Previous page
//------------------------------------------------------------------------------

static TYesNo PreviousPage( int *CurrentPage, int *RowCount, int Count)
// Move at previous page
{
int TotalCount;
int LastPage;
int LastRows;

   TotalCount = Count;
   if( TotalCount <= DSAMPLES_COUNT){
      BeepError();
      *CurrentPage = 0;
      *RowCount    = TotalCount;
      return( NO);                     // one page only
   }
   if( *CurrentPage == 0){
      // at first page
      BeepRollover();
      LastPage  = TotalCount;
      LastRows  = LastPage % DSAMPLES_COUNT;
      LastPage /= DSAMPLES_COUNT;
      LastPage *= DSAMPLES_COUNT;
      if( !LastRows){
         LastRows  = DSAMPLES_COUNT;
         LastPage -= DSAMPLES_COUNT;
      }
      *RowCount    = LastRows;
      *CurrentPage = LastPage;
      return( YES);
   }
   BeepKey();
   *CurrentPage -= DSAMPLES_COUNT;  // previous page
   if( TotalCount < DSAMPLES_COUNT){
      *RowCount = TotalCount;
   } else {
      *RowCount = DSAMPLES_COUNT;
   }
   return( YES);
} // PreviousPage

//------------------------------------------------------------------------------
//  Update
//------------------------------------------------------------------------------

//static void UpdatePage( int *CurrentPage, int *CurrentItem, int *RowCount, int Count)
//// Update page settings
//{
//int TotalCount;
//int PageRows;
//
//   TotalCount = Count;
//   PageRows   = TotalCount - *CurrentPage;
//   if( PageRows >= DSAMPLES_COUNT){
//      // whole page visible
//      *RowCount = DSAMPLES_COUNT;
//      return;
//   }
//   if( PageRows <= 0){
//      // no item visible
//      PreviousPage( CurrentPage, RowCount, Count);
//      if( !*RowCount){
//         *CurrentItem = 0;             // empty database
//      } else {
//         *CurrentItem = *RowCount - 1; // at last item on page
//      }
//      return;
//   }
//   if( *CurrentItem > PageRows - 1){
//      *CurrentItem = PageRows - 1;     // move at last row
//   }
//   *RowCount = PageRows;
//} // UpdatePage

//------------------------------------------------------------------------------
//  Read page
//------------------------------------------------------------------------------

static void ReadPage( int CurrentPage, int *RowCount, TWeightSortingCategory *Categories, int Count)
// Read page from database
{
   PageRecord = &Categories[CurrentPage];
   if(CurrentPage + DSAMPLES_COUNT <= Count) {
      *RowCount = DSAMPLES_COUNT;
   } else {
      *RowCount = Count - CurrentPage;
   }
} // ReadPage

//------------------------------------------------------------------------------
//  Display page
//------------------------------------------------------------------------------

#define FLAG_ICON_X  (G_WIDTH - 15)

static void DisplayPage( int CurrentPage, int LastCursorRow, int CursorRow, int RowCount)
// Display database page
{
int RowY;
int i;

   SetFont( DSAMPLES_FONT);
   if( LastCursorRow == -1){
      // clear data area
      GSetColor( DCOLOR_BACKGROUND);
      GBox( 0, DLAYOUT_TITLE_H, G_WIDTH, G_HEIGHT - DLAYOUT_TITLE_H - DLAYOUT_STATUS_H);
   }
   // draw samples
   for( i = 0; i < RowCount; i++){
      RowY = DDB_TEXT_Y + i * DDB_ITEM_HEIGHT;
      if( LastCursorRow != -1){
         // draw partialy
         if( i != LastCursorRow && i != CursorRow){
            // dont't redraw other rows
            continue;
         }
         // erase row
         GSetColor( DCOLOR_BACKGROUND);
         GBox( 0, RowY, G_WIDTH, DDB_ITEM_HEIGHT);
         GSetColor( DCOLOR_DEFAULT);
      }
      // odd line shadow :
      if( i & 0x01){
         GSetColor( DCOLOR_SHADOW);
         GBox( 0, RowY, G_WIDTH, DDB_ITEM_HEIGHT);
      }
      // draw cursor box :
      if( i == CursorRow){
         GSetColor( DCOLOR_CURSOR);
         GBoxRound( 0, RowY, G_WIDTH, DDB_ITEM_HEIGHT, DCURSOR_R, DCURSOR_R);
         GSetColor( DCOLOR_BACKGROUND);
      } else {
         GSetColor( DCOLOR_DEFAULT);
      }
      // item number
      GTextAt( 3, RowY);
      cprintf( "%d", CurrentPage + i + 1);
      // weight
      GTextAt( 29, RowY);
      cputs((char*)PageRecord[ i].Name);
    
      if(i == 0 && CurrentPage == 0) {
         continue;
      }

      RowY += DDB_LINE_HEIGHT;

      GTextAt( 29, RowY);
      DWeight(PageRecord[ i].LowLimit);
      GTextAt( 85, RowY);
      cputs(" - ");
      GTextAt( 105, RowY);
      DWeightWithUnits(PageRecord[ i].HighLimit);
   }
   GFlush();                           // redraw rows
   // vertical separator line
   GSetColor( COLOR_LIGHTGRAY);
   GLine( 22, DLAYOUT_TITLE_H + 2, 22, G_HEIGHT - DLAYOUT_STATUS_H - 2);
   GSetMode( GMODE_REPLACE);
} // DisplayPage