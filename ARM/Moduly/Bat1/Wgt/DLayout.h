//******************************************************************************
//                                                                            
//  DGrid.h        Display grid
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DLayout_H__
   #define __DLayout_H__

#ifndef __StrDef_H__
   #include "../../Inc/Wgt/StrDef.h"
#endif

#ifndef __Graphic_H__
   #include "../../Inc/Graphic.h"
#endif

#define DLAYOUT_TITLE_H   20
#define DLAYOUT_STATUS_H  21

void DLayoutTitle( TUniStr Caption);
// Display window <Title>

void DLayoutFileTitle( TUniStr FileName);
// Display window <FileName>

void DLayoutStatus( TUniStr Left, TUniStr Right, const TBitmap *RightIcon);
// Draw status line

void DLayoutStatusMove( void);
// Draw status line with all cursor keys

void DLayoutStatusMoveVertical( void);
// Draw status line with vertical cursor keys

#endif
