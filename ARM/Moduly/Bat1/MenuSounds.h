//******************************************************************************
//                                                                            
//   MenuSounds.h    Sounds configuration menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef __MenuSounds_H__
   #define __MenuSounds_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuSounds( void);
// Sounds configuration menu

#endif
