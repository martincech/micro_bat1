//******************************************************************************
//                                                                            
//   MenuXBee.h   XBee configuration menu
//   Version 1.0  (c) Veit electronics
//
//******************************************************************************

#ifndef __MenuXBee_H__
   #define __MenuXBee_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuXBee( void);
// XBee configuration menu

#endif
