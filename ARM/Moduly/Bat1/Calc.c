//*****************************************************************************
//
//    Calc.c - Statistic calculations
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
#endif

#include "Calc.h"
#include "../Inc/File/Ds.h"
#include "../Inc/Statistics.h"
#include "../Inc/Cpu.h"         // WatchDog
#include <string.h>

// Global data :
TCalc Calc[ STATISTICS_MAX];


// Local data :
static TStatistic _Statistics[ STATISTICS_MAX];
static TCalcMode  _Mode;
static int _CatOffset;

typedef struct {
   TCalc Calc[ STATISTICS_MAX];
   TStatistic Statistics[ STATISTICS_MAX];
   TCalcMode  Mode;

} TCalcBackup;
static TCalcBackup _Backup;
// Local functions :

static void CalcPrimary( void);
// Calculate primary statistics

static void CalcSecondary( void);
// Calculate secondary statistics

static void AddSample( TWeight Weight, TSampleFlag Flag);
// Add sample to primary
static void RemoveSample( TWeight Weight, TSampleFlag Flag);
// Remove sample from primary

static void UpdatePrimary( void);
// Recalculate primary statistics

static int IndexByFlag( TSampleFlag Flag);
// Returns statistics index by <Flag> or -1 invalid

static void CalcClear( void);
// Clear results

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

static void CalcClear(void)
// Clear results
{
int i;

   for( i = 0; i < STATISTICS_MAX; i++){  
      StatClear( &_Statistics[ i]);
      HistogramClear( &Calc[ i].Histogram);
      Calc[ i].Count      = 0;
      Calc[ i].Average    = 0;
      Calc[ i].Sigma      = 0;
      Calc[ i].Cv         = 0;
      Calc[ i].Uniformity = 0;
   }
} // CalcClear

//-----------------------------------------------------------------------------
// Mode
//-----------------------------------------------------------------------------

void CalcMode( TCalcMode Mode)
// Set calculation mode
{
   _Mode = Mode;
} // CalcMode

//-----------------------------------------------------------------------------
// Category offset
//-----------------------------------------------------------------------------

void CalcModeCategoryOffsetSet( int Offset)
// Set calculation category offset
{
   _CatOffset = Offset;
} // CalcModeCategoryOffsetSet

//-----------------------------------------------------------------------------
// Primary statistics
//-----------------------------------------------------------------------------

static void CalcPrimary( void)
// Calculate primary statistics
{
TSdbRecord Record;

   DsBegin();
   while( DsNext( &Record)){
      AddSample( Record.Weight, Record.Flag);
      WatchDog();
   }
   UpdatePrimary();
} // CalcPrimary

//-----------------------------------------------------------------------------
// Secondary statistics
//-----------------------------------------------------------------------------

static void CalcSecondary( void)
// Calculate secondary statistics
{
int        i;
int        Index;
TSdbRecord Record;

   // Uniformity & histogram initialisation :
   for( i = 0; i < STATISTICS_MAX; i++){
      StatUniformityInit( &_Statistics[ i], 
                          Calc[ i].Average * (TNumber)(100 - Config.StatisticParameters.UniformityRange) / 100.0,
                          Calc[ i].Average * (TNumber)(100 + Config.StatisticParameters.UniformityRange) / 100.0);
      HistogramClear( &Calc[ i].Histogram);
      HistogramSetCenter( &Calc[ i].Histogram, Calc[ i].Average);
      if( Config.StatisticParameters.Histogram.Mode == HISTOGRAM_MODE_RANGE) {      
         HistogramSetRange( &Calc[ i].Histogram, Config.StatisticParameters.Histogram.Range);  // by range
      } else {        
         HistogramSetStep( &Calc[ i].Histogram, Config.StatisticParameters.Histogram.Step);    // by step
      }
   }
   // scan samples :
   DsBegin();
   while( DsNext( &Record)){
      // update total :
      StatUniformityAdd( &_Statistics[ STATISTICS_TOTAL], Record.Weight);
      HistogramAdd( &Calc[ STATISTICS_TOTAL].Histogram, Record.Weight);
      WatchDog();
      // update by flag :
      Index = IndexByFlag( Record.Flag);
      if( Index < 0){
         continue;                     // invalid group
      }
      StatUniformityAdd( &_Statistics[ Index], Record.Weight);
      HistogramAdd( &Calc[ Index].Histogram, Record.Weight);
   }
   // uniformity results :
   for( i = 0; i < STATISTICS_MAX; i++){
      Calc[ i].Uniformity = (word)(10 * StatUniformityGet( &_Statistics[ i]));
   }
} // CalcSecondary

//-----------------------------------------------------------------------------
// Statistics backup
//-----------------------------------------------------------------------------
void CalcBackup( void)
// backup calculations
{
   memcpy(_Backup.Calc, Calc, sizeof(Calc));
   memcpy(_Backup.Statistics, _Statistics, sizeof(_Statistics));
   memcpy(&_Backup.Mode, &_Mode, sizeof(_Mode));
}

void CalcRestoreBackup( void)
// restore previous backup
{
   memcpy(Calc, _Backup.Calc, sizeof(Calc));
   memcpy(_Statistics, _Backup.Statistics, sizeof(_Statistics));
   memcpy(&_Mode, &_Backup.Mode, sizeof(_Mode));
}

//-----------------------------------------------------------------------------
// Statistics
//-----------------------------------------------------------------------------

void CalcStatistics( void)
// Calc statistic of the dataset
{
   CalcClear();
   CalcPrimary();
   CalcSecondary();
} // CalcStatistics

//-----------------------------------------------------------------------------
// Append
//-----------------------------------------------------------------------------

void CalcAppend( TWeight Weight, TSampleFlag Flag)
// Append sample & recalculate
{
   AddSample( Weight, Flag);
   UpdatePrimary();
   CalcSecondary();
} // CalcAppend

void CalcDelete(TWeight Weight, TSampleFlag Flag)
// Remove last added sample & recalculate
{
   RemoveSample( Weight, Flag);
   UpdatePrimary();
   CalcSecondary();   
}
//*****************************************************************************

//-----------------------------------------------------------------------------
// Add sample
//-----------------------------------------------------------------------------

static void AddSample( TWeight Weight, TSampleFlag Flag)
// Add sample to primary
{
int Index;

   // update total statistics :
   StatAdd(&_Statistics[ STATISTICS_TOTAL], Weight);
   // update by flag :
   Index = IndexByFlag( Flag);
   if( Index < 0){
      return;                          // invalid group
   }
   StatAdd(&_Statistics[ Index], Weight);
} // AddSample

static void RemoveSample( TWeight Weight, TSampleFlag Flag)
// Remove sample from primary
{
int Index;

// update total statistics :
   StatRemove(&_Statistics[ STATISTICS_TOTAL], Weight);
   // update by flag :
   Index = IndexByFlag( Flag);
   if( Index < 0){
      return;                          // invalid group
   }
   StatRemove(&_Statistics[ Index], Weight);
}
//-----------------------------------------------------------------------------
// Update primary
//-----------------------------------------------------------------------------

static void UpdatePrimary( void)
// Recalculate primary statistics
{
int i;

   for( i = 0; i < STATISTICS_MAX; i++){
      Calc[ i].Count   = _Statistics[ i].Count;
      Calc[ i].Average = StatAverage( &_Statistics[ i]);
      Calc[ i].Sigma   = StatSigma( &_Statistics[ i]);
      Calc[ i].Cv      = (word)(10 * StatVariation( Calc[ i].Average, Calc[ i].Sigma));
   }
} // UpdatePrimary

//-----------------------------------------------------------------------------
// Index by Flag
//-----------------------------------------------------------------------------

static int IndexByFlag( TSampleFlag Flag)
// Returns statistics index by <Flag> or -1 invalid
{
   if( _Mode == CALC_TOTAL){
      return( -1);                     // total only
   }
   if( _Mode == CALC_BY_SEX){
      switch( Flag){
         case FLAG_MALE :
            return( STATISTICS_MALE);

         case FLAG_FEMALE :
            return( STATISTICS_FEMALE);

         default :
            return( -1);
      }
   }
   if(_Mode == CALC_CATEGORY) {
      if((Flag >= FLAG_SORTING_CATEGORY1 + _CatOffset) && (Flag < FLAG_SORTING_CATEGORY1 + _CatOffset + STATISTICS_CATEGORY_ON_SCREEN_COUNT)) {
         return STATISTICS_CATEGORY_ON_SCREEN1 + Flag - (FLAG_SORTING_CATEGORY1 + _CatOffset);
      } else {
         return -1;
      }
   }
   // CALC_LIGHT_(OK)_HEAVY
   switch( Flag){
      case FLAG_LIGHT :
         return( STATISTICS_LIGHT);

      case FLAG_OK :
         if( _Mode == CALC_LIGHT_HEAVY){
            return( -1);               // sort without OK
         }
         return( STATISTICS_OK);

      case FLAG_HEAVY :
         return( STATISTICS_HEAVY);

      default :
         return( -1);
   }
} // IndexByFlag
