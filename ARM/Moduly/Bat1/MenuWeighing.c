//******************************************************************************
//                                                                            
//   MenuWeighing.c  Weighing menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuWeighing.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "../Inc/File/Fd.h"       // File directory list
#include "../Inc/Cpu.h"           // WatchDog

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Wgt/DMsg.h"             // Display message
#include "Wgt/DDir.h"             // Display file directory

#include "Str.h"                  // Strings
#include "Sdb.h"                  // Samples database
#include "Screen.h"               // ScreenWait
#include "Weighing.h"             // Weighing utility
#include "Wgt/DWeight.h"          // Display weight
#include "Wgt/DSamples.h"         // Display samples database
#include "Flagging.h"
#include "MenuSortingCategories.h"

static DefMenu( WeighingMenu)
   STR_ACTIVE_FILE,
   STR_VIEW_FILE,
   STR_NUMBER_OF_BIRDS,
   STR_LIMIT,
   STR_LOW_LIMIT,
   STR_HIGH_LIMIT,
   STR_SORTING_CATEGORIES,
   STR_CLEAR_FILE,
   STR_CLEAR_ALL_FILES,
EndMenu()

typedef enum {
   WM_ACTIVE_FILE,
   WM_VIEW_FILE,
   WM_NUMBER_OF_BIRDS,
   WM_LIMIT,
   WM_LOW_LIMIT,
   WM_HIGH_LIMIT,
   WM_SORTING_CATEGORIES_PARAMETERS,
   WM_CLEAR_FILE,
   WM_CLEAR_ALL_FILES,
} TWeighingMenuEnum;

static TWeightSorting SortingBackup;   
// Local functions :

static void MenuWeighingParameters( int Index, int y, TSdbConfig *Parameters);
// Display weighing parameters

static void ClearAll( void);
// Clear all files
static void ChangeFlagsInFiles(TWeightSorting *newParameters)
{
   if(newParameters == NULL || 
      memequ(&SortingBackup, newParameters, sizeof(SortingBackup))){
      return;
   }
   ScreenWait();
   if(!Config.EnableFileParameters){
      ReflagAllFiles();
   } else{
      ReflagFile(Config.LastFile);
   }
}

static void CloseFileAndSetParameters( TWeighingParameters *Parameters)
{
   if( Config.LastFile != FDIR_INVALID){
      SdbClose();
   }
   WeighingSetGlobalParameters(Config.LastFile);             // update global parameters
   ChangeFlagsInFiles(&Parameters->WeightSorting);
}

static TWeighingParameters *OpenFileAndGetParameters( void)
{
TWeighingParameters *Parameters;

   FdSetClass( SDB_CLASS);
   if( !FdValid( Config.LastFile)){
      Config.LastFile = FDIR_INVALID;            // set as invalid
   } 
   if( Config.LastFile != FDIR_INVALID){
      SdbOpen( Config.LastFile, NO);             // open active file
   }
   if(Config.EnableFileParameters){
      Parameters = SdbConfig();      
   }else{
      Parameters = &Config.WeighingParameters;
   }   
   memcpy(&SortingBackup, &Parameters->WeightSorting, sizeof(SortingBackup));
   return Parameters;
}
//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuWeighing( TYesNo ActiveFileOnly)
// Weighing menu
{
TMenuData   MData;
TFdirHandle Handle;
int         i;
TYesNo Terminate = NO;
TWeighingParameters *Parameters;
   
   DMenuClear( MData);
   Parameters = OpenFileAndGetParameters();
   forever {
      // create menu mask
      if( Config.LastFile == FDIR_INVALID){
         MData.Mask  = 0xFFFFFFFF;                     // disable all items
         MData.Mask &= ~(1 << WM_ACTIVE_FILE);         // enable file selection
         MData.Mask &= ~(1 << WM_CLEAR_ALL_FILES);     // enable delete all
      } else {
         // check for limits
         if( Parameters->Saving.Mode == SAVING_MODE_MANUAL_BY_SEX){
            // manual mode by sex (no weight sorting)
            MData.Mask = (1 << WM_LIMIT) | (1 << WM_LOW_LIMIT) | (1 << WM_HIGH_LIMIT) | (1 << WM_SORTING_CATEGORIES_PARAMETERS);       // hide all limits
         } else {
            // manual/automatic mode - check for weight sorting
            switch( Parameters->WeightSorting.Mode){
               case WEIGHT_SORTING_NONE :
                  MData.Mask = (1 << WM_LIMIT) | (1 << WM_LOW_LIMIT) | (1 << WM_HIGH_LIMIT) | (1 << WM_SORTING_CATEGORIES_PARAMETERS);// hide all limits
                  break;

               case WEIGHT_SORTING_LIGHT_HEAVY :
                  MData.Mask = (1 << WM_LOW_LIMIT) | (1 << WM_HIGH_LIMIT) | (1 << WM_SORTING_CATEGORIES_PARAMETERS);                  // hide low/high limits
                  break;

               case WEIGHT_SORTING_LIGHT_OK_HEAVY :
                  MData.Mask = (1 << WM_LIMIT) | (1 << WM_SORTING_CATEGORIES_PARAMETERS);                                             // hide single limit
                  break;

               case WEIGHT_SORTING_CATEGORY :
                  MData.Mask = (1 << WM_LIMIT) | (1 << WM_HIGH_LIMIT) | (1 << WM_LOW_LIMIT);                                             // mask single limit
                  break;
            }
         }
         // check for birds count
         if( !Parameters->EnableMoreBirds){
            MData.Mask |= (1 << WM_NUMBER_OF_BIRDS);
         }
      }
      if(ActiveFileOnly) {
         if(Terminate) {
            CloseFileAndSetParameters(Parameters);
            return;   
         }
         MData.Item = WM_ACTIVE_FILE;
      } else {
         // menu selection
         if( !DMenu( STR_WEIGHING, WeighingMenu, (TMenuItemCb *)MenuWeighingParameters, Parameters, &MData)){
            CloseFileAndSetParameters(Parameters);
            return;                                 // close menu
         }
      }
      switch( MData.Item){
         case WM_ACTIVE_FILE :
            Terminate = YES;
            Handle = DDirSelectFile( STR_SELECT_FILE, Config.LastFile);
            if( Handle == FDIR_INVALID){
               break;
            }
            CloseFileAndSetParameters(Parameters);
            Config.LastFile = Handle;
            ConfigSaveSection( LastFile)
            Parameters = OpenFileAndGetParameters();
            break;

         case WM_CLEAR_FILE :
            if( !DMsgYesNo( STR_CLEAR_FILE, STR_REALLY_CLEAR, SdbInfo()->Name)){
               break;
            }
            ScreenWait();
            SdbEmpty();
            break;

         case WM_VIEW_FILE :
            CloseFileAndSetParameters(Parameters);
            DSamplesDisplay( Config.LastFile);
            Parameters = OpenFileAndGetParameters();
            break;

         case WM_NUMBER_OF_BIRDS :
            i = Parameters->NumberOfBirds;
            if( !DEditSpin( DMENU_EDIT_X, MData.y, &i, 1, NUMBER_OF_BIRDS_MAX, 0)){
               break;
            }
            Parameters->NumberOfBirds = i;
            break;

         case WM_LIMIT :
         case WM_LOW_LIMIT :
            i = Parameters->WeightSorting.LowLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Config.Units.Range)){
               break;
            }
            Parameters->WeightSorting.LowLimit = i;
            break;

         case WM_HIGH_LIMIT :
            i = Parameters->WeightSorting.HighLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Config.Units.Range)){
               break;
            }
            Parameters->WeightSorting.HighLimit = i;
            break;

         case WM_SORTING_CATEGORIES_PARAMETERS :
            MenuSortingCategories(Parameters->WeightSorting.Categories);
            break;

         case WM_CLEAR_ALL_FILES :
            if( !DMsgYesNo( STR_CLEAR_ALL_FILES, STR_REALLY_CLEAR_ALL, 0)){
               break;
            }
            ScreenWait();
            CloseFileAndSetParameters(Parameters);
            ClearAll();
            Parameters = OpenFileAndGetParameters();
            break;
      }
   }
} // MenuWeighing

//******************************************************************************

//------------------------------------------------------------------------------
//  Menu Parameters
//------------------------------------------------------------------------------

static void MenuWeighingParameters( int Index, int y, TSdbConfig *Parameters)
// Display weighing parameters
{
   switch( Index){
      case WM_ACTIVE_FILE :
         if( Config.LastFile == FDIR_INVALID){
            DLabelNarrow( "?", DMENU_PARAMETERS_X, y);
            break;
         }
         DLabelNarrow( SdbInfo()->Name, DMENU_PARAMETERS_X, y);
         break;

      case WM_NUMBER_OF_BIRDS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->NumberOfBirds);
         break;

      case WM_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->WeightSorting.LowLimit);
         break;

      case WM_LOW_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->WeightSorting.LowLimit);
         break;

      case WM_HIGH_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->WeightSorting.HighLimit);
         break;
   }
} // LimitParameters

//------------------------------------------------------------------------------
//   Clear all
//------------------------------------------------------------------------------

static void ClearAll( void)
// Clear all files
{
TFdirHandle Handle;

   FdSetClass( SDB_CLASS);
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      SdbOpen( Handle, NO);
      SdbEmpty();
      SdbClose();
      WatchDog();
   }
} // ClearAll
