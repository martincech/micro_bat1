//******************************************************************************
//                                                                            
//   MenuSortingCategory.c  Menu sorting category
//   Version 1.0           (c) Veit Electronics
//
//******************************************************************************

#include "MenuSortingCategory.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/File/Fd.h"       // File directory
#include "../Inc/Wgt/DLabel.h"    // Display label

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DWeight.h"          // Display weight value

#include "Str.h"                  // Strings

static DefMenu( SortingCategoryMenu)
   STR_NAME,
   STR_HIGH_LIMIT,
   STR_LOW_LIMIT,
EndMenu()

typedef enum {
   MM_NAME,
   MM_HIGH_LIMIT,
   MM_LOW_LIMIT,
} TSortingCategoryMenuEnum;

// Local functions :

static void MenuSortingCategoryParameters( int Index, int y, TWeightSortingCategory *Category);
// Sorting category menu parameters

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuSortingCategory( TWeightSortingCategory *Category, TYesNo Default)
// Sorting category menu
{
TMenuData   MData;
int         i;
char        Name[ WEIGHING_SORTING_CATEGORY_NAME_SIZE + 1];

   // run menu :
   DMenuClear( MData);
   forever {
      if( Default){
         MData.Mask = (1 << MM_LOW_LIMIT) | (1 << MM_HIGH_LIMIT);
      }
      if( !DMenu( STR_SORTING_CATEGORY, SortingCategoryMenu, (TMenuItemCb *)MenuSortingCategoryParameters, Category, &MData)){
         return;
      }
      switch( MData.Item){
         case MM_NAME :
            strcpy( Name, (char*)Category->Name);
            if( !DInputText( STR_NAME, STR_ENTER_NAME, Name, WEIGHING_SORTING_CATEGORY_NAME_SIZE, NO)){
               break;
            }
            strcpy( (char*)Category->Name, Name);
            break;
            
        case MM_HIGH_LIMIT :
            i = Category->HighLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, Category->LowLimit, Config.Units.Range)){
               break;
            }
            Category->HighLimit = i;
            break;

        case MM_LOW_LIMIT :
            i = Category->LowLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Category->HighLimit)){
               break;
            }
            Category->LowLimit = i;
            break;
      }
   }
} // MenuSortingCategory

//******************************************************************************

//------------------------------------------------------------------------------
//  Menu Parameters
//------------------------------------------------------------------------------

static void MenuSortingCategoryParameters( int Index, int y, TWeightSortingCategory *Category)
// Sorting category menu parameters
{
   switch( Index){
      case MM_NAME :
         DLabelNarrow((TUniStr)Category->Name, DMENU_PARAMETERS_X, y);
         break;         

      case MM_LOW_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Category->LowLimit);
         break;

      case MM_HIGH_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Category->HighLimit);
         break;
   }
} // MenuSortingCategoryParameters