//******************************************************************************
//
//   Flagging.h    Flagging utilities
//   Version 1.0  (c) Veit
//
//******************************************************************************

#ifndef __Flagging_H__
   #define __Flagging_H__

#include "Hardware.h"
#include "Sdb.h"             // Samples database

TSampleFlag GetFlagWithGender(TSdbConfig *WeighingParameters, TWeight Weight, TYesNo Male);
// get flag by parameters for current weight and gender
TSampleFlag GetFlag(TSdbConfig *WeighingParameters, TWeight Weight);
// get flag by parameters for current weight
void ReflagFile(TFdirHandle File);
// reflag whole file according to file parameters
void ReflagAllFiles( void);
// reflag all files according to file parameters
//------------------------------------------------------------------------------
// Basic functions
//------------------------------------------------------------------------------

#endif
