//******************************************************************************
//                                                                            
//  Menu.h         Bat1 menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuCharger( void);
// Display charging

void MenuExecute( void);
// Main menu loop

#endif
