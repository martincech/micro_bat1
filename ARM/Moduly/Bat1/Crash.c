//******************************************************************************
//
//   Crash.c      Crash record utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "Crash.h"
#include "../Inc/System.h"          // Operating system
#include "../Inc/Eep.h"             // EEPROM services
#include "MemoryDef.h"

// EEPROM addresses
#define EXCEPTION_ADDRESS offsetof( TEeprom, Exception)
#define WATCHDOG_ADDRESS  offsetof( TEeprom, WatchDog)

// Local functions
static byte SignalStatus( void);
// Returns signal status

//-----------------------------------------------------------------------------
// Exception
//-----------------------------------------------------------------------------

void CrashException( int Type, dword Address)
// Record exception
{
TExceptionInfo Info;

   Info.Timestamp = SysGetClockNaked();
   Info.Address   = Address;
   Info.Type      = Type;
   Info.Status    = SignalStatus();
   EepSaveData( EXCEPTION_ADDRESS, &Info, sizeof( TExceptionInfo));
} // CrashException

//-----------------------------------------------------------------------------
// Watchdog
//-----------------------------------------------------------------------------

void CrashWatchDog( void)
// Record watchdog activity
{
TWatchDogInfo Info;

   Info.Timestamp = SysGetClock();
   Info.Status    = SignalStatus();
   EepSaveData( WATCHDOG_ADDRESS, &Info, sizeof( TWatchDogInfo));
} // CrashWatchDog

//*****************************************************************************

//-----------------------------------------------------------------------------
// Status
//-----------------------------------------------------------------------------

static byte SignalStatus( void)
// Returns signal status
{
byte Status;

   Status = 0;
   if( PManGetPPR()){
      Status |= CRASH_PPR;
   }
   if( PManGetCHG()){
      Status |= CRASH_CHG;
   }
   if( PManGetUSBON()){
      Status |= CRASH_USBON;
   }
   return( Status);
} // SignalStatus
