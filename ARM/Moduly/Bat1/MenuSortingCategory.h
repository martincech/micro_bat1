//******************************************************************************
//                                                                            
//   MenuSortingCategory.h  Menu sorting category
//   Version 1.0           (c) Veit Electronics
//
//******************************************************************************

#ifndef __MenuSortingCategory_H__
   #define __MenuSortingCategory_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"
#endif

void MenuSortingCategory( TWeightSortingCategory *Category, TYesNo Default);
// Sorting category menu

#endif
