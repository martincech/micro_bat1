//*****************************************************************************
//
//    WeightDef.h  -  Weight definitions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __WeightDef_H__
   #define __WeightDef_H__

#include "../inc/Uni.h"

typedef int32 TWeight;       // Physical representation of the weight
typedef int32 TRawWeight;    // ADC representation of the weight
typedef int32 TSumWeight;    // weight averaging sum
typedef int64 TMulWeight;    // weight multiplication

#endif
