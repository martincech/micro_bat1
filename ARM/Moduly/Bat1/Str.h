//******************************************************************************
//
//  Str.h          String codes
//  Version 1.0    (c) Robot
//
//******************************************************************************

#ifndef __Str_H__
   #define __Str_H__

#ifndef __StrDef_H__
   #include "../inc/wgt/StrDef.h"
#endif

#define STR_NULL (char *)0 // undefined string

//-----------------------------------------------------------------------------
// Buttons
//-----------------------------------------------------------------------------

#define STR_BTN_OK           (char *)1	
#define STR_BTN_YES          (char *)2	
#define STR_BTN_NO           (char *)3	
#define STR_BTN_CANCEL       (char *)4	
#define STR_BTN_HISTOGRAM    (char *)5	
#define STR_BTN_STATISTICS   (char *)6	
#define STR_BTN_DELETE       (char *)7	
#define STR_BTN_EXIT         (char *)8	
#define STR_BTN_SELECT       (char *)9	

//-----------------------------------------------------------------------------
// Standard title :
//-----------------------------------------------------------------------------

#define STR_ERROR            (char *)10	

//-----------------------------------------------------------------------------
// Input box :
//-----------------------------------------------------------------------------

#define STR_OUT_OF_LIMITS    (char *)11	

//-----------------------------------------------------------------------------
// Directory box :
//-----------------------------------------------------------------------------

#define STR_NO_FILES_DEFINED (char *)12	
#define STR_NO_GROUPS_DEFINED (char *)13	
#define STR_SELECT_FILE      (char *)14	
#define STR_SELECT_GROUP     (char *)15	
#define STR_SELECT_FILES     (char *)16	

//-----------------------------------------------------------------------------
// Text box :
//-----------------------------------------------------------------------------

#define STR_STRING_EMPTY     (char *)17	

//-----------------------------------------------------------------------------
// DSamples box :
//-----------------------------------------------------------------------------

#define STR_DELETE_SAMPLE    (char *)18	
#define STR_REALLY_DELETE_RECORD (char *)19	

//-----------------------------------------------------------------------------
// Physical units :
//-----------------------------------------------------------------------------

#define STR_MINUTES          (char *)20	
#define STR_SECONDS          (char *)21	

//-----------------------------------------------------------------------------
// Yes/No enum :
//-----------------------------------------------------------------------------

#define STR_NO               (char *)22	
#define STR_YES              (char *)23	

//-----------------------------------------------------------------------------
// Country enum :
//-----------------------------------------------------------------------------

#define STR_COUNTRY_INTERNATIONAL (char *)24	
#define STR_COUNTRY_ALBANIA  (char *)25	
#define STR_COUNTRY_ALGERIA  (char *)26	
#define STR_COUNTRY_ARGENTINA (char *)27	
#define STR_COUNTRY_AUSTRALIA (char *)28	
#define STR_COUNTRY_AUSTRIA  (char *)29	
#define STR_COUNTRY_BANGLADESH (char *)30	
#define STR_COUNTRY_BELARUS  (char *)31	
#define STR_COUNTRY_BELGIUM  (char *)32	
#define STR_COUNTRY_BOLIVIA  (char *)33	
#define STR_COUNTRY_BRAZIL   (char *)34	
#define STR_COUNTRY_BULGARIA (char *)35	
#define STR_COUNTRY_CANADA   (char *)36	
#define STR_COUNTRY_CHILE    (char *)37	
#define STR_COUNTRY_CHINA    (char *)38	
#define STR_COUNTRY_COLOMBIA (char *)39	
#define STR_COUNTRY_CYPRUS   (char *)40	
#define STR_COUNTRY_CZECH    (char *)41	
#define STR_COUNTRY_DENMARK  (char *)42	
#define STR_COUNTRY_ECUADOR  (char *)43	
#define STR_COUNTRY_EGYPT    (char *)44	
#define STR_COUNTRY_ESTONIA  (char *)45	
#define STR_COUNTRY_FINLAND  (char *)46	
#define STR_COUNTRY_FRANCE   (char *)47	
#define STR_COUNTRY_GERMANY  (char *)48	
#define STR_COUNTRY_GREECE   (char *)49	
#define STR_COUNTRY_HUNGARY  (char *)50	
#define STR_COUNTRY_INDIA    (char *)51	
#define STR_COUNTRY_INDONESIA (char *)52	
#define STR_COUNTRY_IRAN     (char *)53	
#define STR_COUNTRY_IRELAND  (char *)54	
#define STR_COUNTRY_ISRAEL   (char *)55	
#define STR_COUNTRY_ITALY    (char *)56	
#define STR_COUNTRY_JAPAN    (char *)57	
#define STR_COUNTRY_JORDAN   (char *)58	
#define STR_COUNTRY_LATVIA   (char *)59	
#define STR_COUNTRY_LEBANON  (char *)60	
#define STR_COUNTRY_LITHUANIA (char *)61	
#define STR_COUNTRY_LUXEMBOURG (char *)62	
#define STR_COUNTRY_MALAYSIA (char *)63	
#define STR_COUNTRY_MALTA    (char *)64	
#define STR_COUNTRY_MEXICO   (char *)65	
#define STR_COUNTRY_MONGOLIA (char *)66	
#define STR_COUNTRY_MOROCCO  (char *)67	
#define STR_COUNTRY_NEPAL    (char *)68	
#define STR_COUNTRY_NETHERLANDS (char *)69	
#define STR_COUNTRY_NEW_ZEALAND (char *)70	
#define STR_COUNTRY_NIGERIA  (char *)71	
#define STR_COUNTRY_NORWAY   (char *)72	
#define STR_COUNTRY_PAKISTAN (char *)73	
#define STR_COUNTRY_PARAGUAY (char *)74	
#define STR_COUNTRY_PERU     (char *)75	
#define STR_COUNTRY_PHILIPPINES (char *)76	
#define STR_COUNTRY_POLAND   (char *)77	
#define STR_COUNTRY_PORTUGAL (char *)78	
#define STR_COUNTRY_ROMANIA  (char *)79	
#define STR_COUNTRY_RUSSIA   (char *)80	
#define STR_COUNTRY_SLOVAKIA (char *)81	
#define STR_COUNTRY_SLOVENIA (char *)82	
#define STR_COUNTRY_SOUTH_AFRICA (char *)83	
#define STR_COUNTRY_SOUTH_KOREA (char *)84	
#define STR_COUNTRY_SPAIN    (char *)85	
#define STR_COUNTRY_SWEDEN   (char *)86	
#define STR_COUNTRY_SWITZERLAND (char *)87	
#define STR_COUNTRY_SYRIA    (char *)88	
#define STR_COUNTRY_THAILAND (char *)89	
#define STR_COUNTRY_TUNISIA  (char *)90	
#define STR_COUNTRY_TURKEY   (char *)91	
#define STR_COUNTRY_UKRAINE  (char *)92	
#define STR_COUNTRY_UK       (char *)93	
#define STR_COUNTRY_USA      (char *)94	
#define STR_COUNTRY_URUGUAY  (char *)95	
#define STR_COUNTRY_VENEZUELA (char *)96	
#define STR_COUNTRY_VIETNAM  (char *)97	

//-----------------------------------------------------------------------------
// Language enum :
//-----------------------------------------------------------------------------

#define STR_LNG_CZECH        (char *)98	
#define STR_LNG_DUTCH        (char *)99	
#define STR_LNG_ENGLISH      (char *)100	
#define STR_LNG_FINNISH      (char *)101	
#define STR_LNG_FRENCH       (char *)102	
#define STR_LNG_GERMAN       (char *)103	
#define STR_LNG_HUNGARIAN    (char *)104	
#define STR_LNG_ITALIAN      (char *)105	
#define STR_LNG_JAPANESE     (char *)106	
#define STR_LNG_POLISH       (char *)107	
#define STR_LNG_PORTUGUESE   (char *)108	
#define STR_LNG_RUSSIAN      (char *)109	
#define STR_LNG_SPANISH      (char *)110	
#define STR_LNG_TURKISH      (char *)111	

//-----------------------------------------------------------------------------
// Date format enum :
//-----------------------------------------------------------------------------

#define STR_DATE_FORMAT_DDMMYYYY (char *)112	
#define STR_DATE_FORMAT_MMDDYYYY (char *)113	
#define STR_DATE_FORMAT_YYYYMMDD (char *)114	
#define STR_DATE_FORMAT_YYYYDDMM (char *)115	
#define STR_DATE_FORMAT_DDMMMYYYY (char *)116	
#define STR_DATE_FORMAT_MMMDDYYYY (char *)117	
#define STR_DATE_FORMAT_YYYYMMMDD (char *)118	
#define STR_DATE_FORMAT_YYYYDDMMM (char *)119	

//-----------------------------------------------------------------------------
// Time format enum :
//-----------------------------------------------------------------------------

#define STR_TIME_FORMAT_24   (char *)120	
#define STR_TIME_FORMAT_12   (char *)121	

//-----------------------------------------------------------------------------
// Daylight saving time enum :
//-----------------------------------------------------------------------------

#define STR_DST_TYPE_OFF     (char *)122	
#define STR_DST_TYPE_EU      (char *)123	
#define STR_DST_TYPE_US      (char *)124	

//-----------------------------------------------------------------------------
// Units enum :
//-----------------------------------------------------------------------------

#define STR_UNITS_KG         (char *)125	
#define STR_UNITS_G          (char *)126	
#define STR_UNITS_LB         (char *)127	

//-----------------------------------------------------------------------------
// Saving mode enum :
//-----------------------------------------------------------------------------

#define STR_SAVING_MODE_AUTOMATIC (char *)128	
#define STR_SAVING_MODE_MANUAL (char *)129	
#define STR_SAVING_MODE_MANUAL_BY_SEX (char *)130	

//-----------------------------------------------------------------------------
// Weight sorting enum :
//-----------------------------------------------------------------------------

#define STR_WEIGHT_SORTING_NONE (char *)131	
#define STR_WEIGHT_SORTING_LIGHT_HEAVY (char *)132	
#define STR_WEIGHT_SORTING_LIGHT_OK_HEAVY (char *)133	
#define STR_WEIGHT_SORTING_CATEGORY (char *)134	

//-----------------------------------------------------------------------------
// Backlight mode enum :
//-----------------------------------------------------------------------------

#define STR_BACKLIGHT_MODE_AUTO (char *)135	
#define STR_BACKLIGHT_MODE_ON (char *)136	
#define STR_BACKLIGHT_MODE_OFF (char *)137	

//-----------------------------------------------------------------------------
// Histogram mode enum :
//-----------------------------------------------------------------------------

#define STR_HISTOGRAM_MODE_RANGE (char *)138	
#define STR_HISTOGRAM_MODE_STEP (char *)139	

//-----------------------------------------------------------------------------
// Display mode enum :
//-----------------------------------------------------------------------------

#define STR_DISPLAY_MODE_BASIC (char *)140	
#define STR_DISPLAY_MODE_ADVANCED (char *)141	
#define STR_DISPLAY_MODE_STRONG (char *)142	

//-----------------------------------------------------------------------------
// Printer protocol enum :
//-----------------------------------------------------------------------------

#define STR_PROTOCOL_STATISTICS_HISTOGRAM (char *)143	
#define STR_PROTOCOL_STATISTICS (char *)144	
#define STR_PROTOCOL_TOTAL_STATISTICS (char *)145	
#define STR_PROTOCOL_SAMPLES (char *)146	

//-----------------------------------------------------------------------------
// Beep tone enum :
//-----------------------------------------------------------------------------

#define STR_BEEP_TONE1       (char *)147	
#define STR_BEEP_TONE2       (char *)148	
#define STR_BEEP_TONE3       (char *)149	
#define STR_BEEP_TONE4       (char *)150	
#define STR_BEEP_TONE5       (char *)151	
#define STR_BEEP_TONE6       (char *)152	
#define STR_BEEP_TONE7       (char *)153	
#define STR_BEEP_TONE8       (char *)154	
#define STR_BEEP_MELODY1     (char *)155	
#define STR_BEEP_MELODY2     (char *)156	
#define STR_BEEP_MELODY3     (char *)157	
#define STR_BEEP_MELODY4     (char *)158	

//-----------------------------------------------------------------------------
// Capacity enum :
//-----------------------------------------------------------------------------

#define STR_CAPACITY_NORMAL  (char *)159	
#define STR_CAPACITY_EXTENDED (char *)160	

//-----------------------------------------------------------------------------
// Charger screen :
//-----------------------------------------------------------------------------

#define STR_CHARGING         (char *)161	
#define STR_CHARGED          (char *)162	

//-----------------------------------------------------------------------------
// Wait screen :
//-----------------------------------------------------------------------------

#define STR_WAIT             (char *)163	

//-----------------------------------------------------------------------------
// No memory screen :
//-----------------------------------------------------------------------------

#define STR_NO_MEMORY        (char *)164	

//-----------------------------------------------------------------------------
// Weighing screen :
//-----------------------------------------------------------------------------

#define STR_NO_FILE          (char *)165	

//-----------------------------------------------------------------------------
// Off screen :
//-----------------------------------------------------------------------------

#define STR_OFF              (char *)166	

//-----------------------------------------------------------------------------
// Main menu :
//-----------------------------------------------------------------------------

#define STR_MAIN_MENU        (char *)167	
#define STR_WEIGHING         (char *)168	
#define STR_STATISTICS       (char *)169	
#define STR_SETTINGS         (char *)170	
#define STR_CONFIGURATION    (char *)171	
#define STR_MAINTENANCE      (char *)172	

//STR_DELETE_SAMPLE �Delete sample�
#define STR_DELETE_LAST_SAMPLE (char *)173	

//-----------------------------------------------------------------------------
// Weighing menu :
//-----------------------------------------------------------------------------

#define STR_ACTIVE_FILE      (char *)174	
#define STR_VIEW_FILE        (char *)175	
#define STR_NUMBER_OF_BIRDS  (char *)176	
#define STR_LIMIT            (char *)177	
#define STR_LOW_LIMIT        (char *)178	
#define STR_HIGH_LIMIT       (char *)179	
#define STR_CLEAR_FILE       (char *)180	
#define STR_CLEAR_ALL_FILES  (char *)181	

// Weighing menu messages :
#define STR_REALLY_CLEAR     (char *)182	
#define STR_REALLY_CLEAR_ALL (char *)183	

//-----------------------------------------------------------------------------
// Statistics menu :
//-----------------------------------------------------------------------------

#define STR_TOTAL_STATISTICS (char *)184	
#define STR_COMPARE_FILES    (char *)185	
#define STR_PRINT            (char *)186	

// Total statistics selection menu :
#define STR_ONE_FILE         (char *)187	
#define STR_ONE_GROUP        (char *)188	
#define STR_MORE_FILES       (char *)189	
#define STR_ALL_FILES        (char *)190	

// Total statistics messages :
#define STR_NO_FILES_SELECTED (char *)191	

// Compare file messages :
#define STR_TOTAL            (char *)192	

// Print messages :
#define STR_ENTER_PROTOCOL_TYPE (char *)193	

//-----------------------------------------------------------------------------
// Settings menu :
//-----------------------------------------------------------------------------

#define STR_DISPLAY_BACKLIGHT (char *)194	
#define STR_DISPLAY_CONTRAST (char *)195	
#define STR_SAVING_VOLUME    (char *)196	
#define STR_DATE_AND_TIME    (char *)197	

// Settings menu messages :
#define STR_ENTER_DATE       (char *)198	
#define STR_ENTER_TIME       (char *)199	

//-----------------------------------------------------------------------------
// Configuration menu :
//-----------------------------------------------------------------------------

#define STR_FILES            (char *)200	
#define STR_FILE_GROUPS      (char *)201	
#define STR_SAVING           (char *)202	
#define STR_DISPLAY          (char *)203	
#define STR_SOUNDS           (char *)204	
//STR_STATISTICS        Statistics

//-----------------------------------------------------------------------------
// Configuration/Files menu :
//-----------------------------------------------------------------------------

#define STR_CREATE           (char *)205	
#define STR_EDIT_SAVING_OPTIONS (char *)206	
#define STR_EDIT_NOTE        (char *)207	
#define STR_RENAME           (char *)208	
#define STR_DELETE           (char *)209	

// Files menu messages :
#define STR_ENTER_NOTE       (char *)210	
#define STR_ENTER_NAME       (char *)211	
#define STR_FILE_EXISTS      (char *)212	
#define STR_NOT_ENOUGH_SPACE (char *)213	
#define STR_REALLY_DELETE_FILE (char *)214	

//-----------------------------------------------------------------------------
// Configuration/File groups menu :
//-----------------------------------------------------------------------------

//STR_CREATE         Create
#define STR_EDIT_FILE_LIST   (char *)215	
//STR_EDIT_NOTE      Edit note
//STR_RENAME         Rename
//STR_DELETE         Delete

//STR_ENTER_NOTE        Enter note
//STR_ENTER_NAME        Enter Name
#define STR_GROUP_EXISTS     (char *)216	
#define STR_REALLY_DELETE_GROUP (char *)217	
//STR_NO_FILES_SELECTED No files selected
//STR_NOT_ENOUGH_SPACE  Not enough space

//-----------------------------------------------------------------------------
// Configuration/Backlight menu :
//-----------------------------------------------------------------------------

#define STR_MODE             (char *)218	
#define STR_DURATION         (char *)219	
#define STR_INTENSITY        (char *)220	

//-----------------------------------------------------------------------------
// Configuration/Display menu :
//-----------------------------------------------------------------------------

#define STR_BACKLIGHT        (char *)221	
#define STR_CONTRAST         (char *)222	
#define STR_DISPLAY_MODE     (char *)223	

//-----------------------------------------------------------------------------
// Configuration/Sounds menu :
//-----------------------------------------------------------------------------

//STR_SAVING           Saving
#define STR_KEYBOARD         (char *)224	

// Sounds/Saving submenu :
#define STR_SAVING_SOUNDS    (char *)225	
#define STR_DEFAULT_TONE     (char *)226	
#define STR_TONE_LIGHT       (char *)227	
#define STR_TONE_OK          (char *)228	
#define STR_TONE_HEAVY       (char *)229	
#define STR_VOLUME           (char *)230	

// Sound/Keyboard submenu :
#define STR_KEYBOARD_SOUNDS  (char *)231	
#define STR_TONE             (char *)232	
//STR_VOLUME           Volume
#define STR_SPECIAL_SOUNDS   (char *)233	

//-----------------------------------------------------------------------------
// Configuration/Statistics menu :
//-----------------------------------------------------------------------------

#define STR_UNIFORMITY_RANGE (char *)234	
#define STR_HISTOGRAM_MODE   (char *)235	
#define STR_HISTOGRAM_RANGE  (char *)236	
#define STR_HISTOGRAM_STEP   (char *)237	

//-----------------------------------------------------------------------------
// Maintenance menu :
//-----------------------------------------------------------------------------

#define STR_SCALE_NAME       (char *)238	
#define STR_COUNTRY          (char *)239	
//STR_WEIGHING                Weighing
#define STR_MEMORY           (char *)240	
#define STR_PRINTER          (char *)241	
#define STR_AUTO_POWER_OFF   (char *)242	
#define STR_PASSWORD         (char *)243	
#define STR_RESTORE_FACTORY_DEFAULTS (char *)244	

// Service menu messages :
#define STR_ENTER_SCALE_NAME (char *)245	
#define STR_REALLY_RESTORE_FACTORY_DEFAULTS (char *)246	

//-----------------------------------------------------------------------------
// Maintenance/Weighing menu :
//-----------------------------------------------------------------------------

#define STR_UNITS            (char *)247	
#define STR_DIVISION         (char *)248	
#define STR_RANGE            (char *)249	
#define STR_CALIBRATION      (char *)250	

//-----------------------------------------------------------------------------
// Maintenance/Memory menu :
//-----------------------------------------------------------------------------

#define STR_CAPACITY         (char *)251	
#define STR_FORMAT           (char *)252	

// Memory menu messages :
#define STR_REALLY_FORMAT    (char *)253	

//-----------------------------------------------------------------------------
// Maintenance/Printer menu :
//-----------------------------------------------------------------------------

#define STR_PAPER_WIDTH      (char *)254	
#define STR_COMMUNICATION_SPEED (char *)255	
#define STR_COMMUNICATION_FORMAT (char *)256	

//-----------------------------------------------------------------------------
// Maintenance/Country menu :
//-----------------------------------------------------------------------------

//STR_COUNTRY             Country
#define STR_LANGUAGE         (char *)257	
#define STR_DATE_FORMAT      (char *)258	
#define STR_TIME_FORMAT      (char *)259	
#define STR_DAYLIGHT_SAVING_TIME (char *)260	

// Country menu messages :
#define STR_ENTER_DATE_FORMAT (char *)261	
#define STR_ENTER_DATE_SEPARATOR1 (char *)262	
#define STR_ENTER_DATE_SEPARATOR2 (char *)263	
#define STR_ENTER_TIME_FORMAT (char *)264	
#define STR_ENTER_TIME_SEPARATOR (char *)265	

//-----------------------------------------------------------------------------
// Saving options menu :
//-----------------------------------------------------------------------------

//STR_MODE                  Mode
#define STR_WEIGHING_MORE_BIRDS (char *)266	
//STR_NUMBER_OF_BIRDS       Number of birds
#define STR_WEIGHT_SORTING   (char *)267	
#define STR_SORTING          (char *)268	
#define STR_FILTER           (char *)269	
#define STR_STABILISATION_TIME (char *)270	
#define STR_MINIMUM_WEIGHT   (char *)271	
#define STR_STABILISATION_RANGE (char *)272	
#define STR_RESET_DEFAULTS   (char *)273	
#define STR_ENABLE_FILE_PARAMETERS (char *)274	
#define STR_COPY_TO_FILE     (char *)275	

// Saving options menu messages :
#define STR_REALLY_RESET_DEFAULTS (char *)276	
#define STR_REALLY_COPY_TO_FILES (char *)277	


//-----------------------------------------------------------------------------
// Calibration menu :
//-----------------------------------------------------------------------------

// calibration menu messages :
#define STR_ENTER_WEIGHT     (char *)278	
#define STR_RELEASE_WEIGHT   (char *)279	

//-----------------------------------------------------------------------------
// Password :
//-----------------------------------------------------------------------------

#define STR_PROTECT_BY_PASSWORD (char *)280	
#define STR_ENTER_PASSWORD   (char *)281	
#define STR_ENTER_NEW_PASSWORD (char *)282	
#define STR_CONFIRM_PASSWORD (char *)283	
#define STR_PASSWORDS_DONT_MATCH (char *)284	
#define STR_INVALID_PASSWORD (char *)285	

//-----------------------------------------------------------------------------
// Printer :
//-----------------------------------------------------------------------------

#define STR_PRINTING         (char *)286	
#define STR_START_PRINTING   (char *)287	

//-----------------------------------------------------------------------------
// Screen
//-----------------------------------------------------------------------------

#define STR_MEMORY_CAPACITY  (char *)288	
#define STR_FILES_COUNT      (char *)289	
#define STR_FILES_SIZE       (char *)290	
#define STR_GROUPS_COUNT     (char *)291	
#define STR_MEMORY_USED      (char *)292	

//-----------------------------------------------------------------------------
// Sorting categories
//-----------------------------------------------------------------------------

#define STR_SORTING_CATEGORIES (char *)293	
#define STR_SORTING_CATEGORY (char *)294	
#define STR_DELETE_CATEGORY  (char *)295	
#define STR_UNABLE_DELETE_DEFAULT_CATEGORY1 (char *)296	
#define STR_UNABLE_DELETE_DEFAULT_CATEGORY2 (char *)297	
#define STR_NAME             (char *)298	
#define STR_EDIT             (char *)299	

// system :
#define _STR_LAST (char *)300 // strings count

#endif
