//******************************************************************************
//
//   Weighing.c    Weighing utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "Weighing.h"
#include "Cal.h"             // Calibration
#include "ConfigDef.h"       // Configuration data
#include "../Inc/System.h"   // Operating system
#include "../Inc/Cpu.h"      // System
#include "../Inc/Ads123xf.h" // A/D convertor
#include "../Inc/File/Fd.h"  // File Directory
#include "../Inc/File/Fs.h"  // File System
#include "../Inc/File/Ds.h"  // Data Set
#include "Beep.h"            // Beep
#include "Calc.h"            // Statistic calculations
#include "Sdb.h"             // Samples database
#include "Bat1.h"            // Weighing limits
#include "Screen.h"          // No memory message
#include "Flagging.h"
#include <string.h>

#ifdef PROJECT_PRINTER_VEITFLPROTO_VERSION
#include "Packet/VeitFLPkt.h"
#endif
#ifdef PROJECT_PRINTER
#include "Printer.h"         // Printer
#endif

#define INERTIA_DELAY        2                 // intertia mutiplicator for inital delay
#define STABILISATION_DELAY  1000              // A/D start stabilisation
#define SDB_TRESHOLD_SIZE    18000             // SDB slow processing treshold size
#define TIME_HOLD_DISPLAY    2000              // hold display sorting symbol
#define WEIGHING_DRIFT       3                 // drift filter (units LSB)

// actual weighing (for calibration) :
#define AVG_POWER            6                   // averaging power
#define AVG_COUNT           (1 << AVG_POWER)     // averaging count

// Local data :

static TWeight         ActualWeight;      // currently measured weight
static TWeighingStatus ActualStatus;      // current weighing status
static TWeight         LastWeight;        // last saved weight
static TSampleFlag     LastFlag;          // last saved flag
static TWeight         Tara;              // tara weight
static TYesNo          NewRecord;         // New record/record loaded from database
static TSdbConfig     *WParameters;       // actual weighing parameters

typedef struct{
   byte Mode;
   TFdirHandle ActiveFile;
   int Count;
}TBackup;
static TBackup     StatsBackup;  // backup of weighing parameters

// Local functions :

static TWeight NettoWeight( TWeight Weight);
// Calculate weight by <Weight> tara and number of birds

static void SetupFilter( TSavingParameters *Parameters, TWeight Zero);
// Setup weight filering

static void SaveWithLimits( TWeight Weight);
// Check <Weight> limits & save

static TYesNo SaveRecord( TWeight Weight, TSampleFlag Flag);
// Save record

static void UpdateLastWeight( void);
// Update last weight by database

static TWeight RoundByDivision( TWeight Weight);
// Round <Weight> by Division

static void DisplayHold( void);
// Wait for frozen display

static void WeighingManual( TYesNo ForMale);
// execute manual weighing for gender
//------------------------------------------------------------------------------
// Initialisation
//------------------------------------------------------------------------------

void WeighingInit( void)
// Initialize weighing
{
   AdcInit();
   ActualWeight = 0;
   ActualStatus = WEIGHING_OK;
   LastWeight   = 0;
   LastFlag     = FLAG_INVALID;
   Tara         = 0;
   NewRecord    = NO;
} // WeighingInit

//------------------------------------------------------------------------------
// Global parameters
//------------------------------------------------------------------------------

void WeighingSetGlobalParameters( TFdirHandle Handle)
// Set/Update current file by global parameters
{
   if( Config.EnableFileParameters){
      return;                          // file specific parameters
   }
   // use global parameters
   WeighingCopyParameters(Handle);
} // WeighingSetGlobalParameters

//------------------------------------------------------------------------------
// Copy parameters
//------------------------------------------------------------------------------

void WeighingCopyParameters( TFdirHandle Handle)
// Unconditionaly Set/Update current file by global parameters
{
   if(Handle != FDIR_INVALID){
      return;
   }
   SdbOpen(Handle, NO);
   memcpy(SdbConfig(), &Config.WeighingParameters, sizeof(Config.WeighingParameters));
   SdbClose();
} // WeighingCopyParameters

//------------------------------------------------------------------------------
// Start
//------------------------------------------------------------------------------

void WeighingStart( void)
// Power up start weighing
{

   // setup filter :
   SetupFilter( &Config.WeighingParameters.Saving, 0);     // use defaults
   while( FilterRecord.Status < FILTER_WAIT_STEP){         // wait for filter
      WatchDog();
   }
   SysDelay( STABILISATION_DELAY);               // add some time
   Tara = CalWeight( AdcLowPassRead());          // use actual weight as tara
   memset(&StatsBackup, -1, sizeof(StatsBackup));
   WeighingRelease();                            // standard start
} // WeighingStart

//------------------------------------------------------------------------------
// Stop
//------------------------------------------------------------------------------

void WeighingStop( void)
// Power down stop weighing
{
   WeighingSuspend();
} // WeighingStop

//------------------------------------------------------------------------------
// Suspend
//------------------------------------------------------------------------------

void WeighingSuspend( void)
// Temporary stop weighing
{
   AdcStop();
   if(WParameters != 0){
      StatsBackup.Mode = WParameters->Saving.Mode;     
   }
   StatsBackup.ActiveFile = Config.LastFile;
   if( Config.LastFile != FDIR_INVALID){
      StatsBackup.Count = SdbCount();
      SdbClose();                      // valid database, close it
   }else{
      StatsBackup.Count = -1;
   }
   CalcBackup();
} // WeighingSuspend

//------------------------------------------------------------------------------
// Release
//------------------------------------------------------------------------------

void WeighingRelease( void)
// Start temporary stopped weighing
{
   DsSetCurrent();                     // set dataset to currently opened file
#ifdef PROJECT_PRINTER_VEITFLPROTO_VERSION
   VeitFLPktSetup();
#endif

#if PROJECT_PRINTER
   PrinterSetup();                     // set printer parameters
#endif
   WParameters  = 0;                   // no file
   LastWeight   = 0;                   // no last weight
   LastFlag     = FLAG_INVALID;
   FdSetClass( SDB_CLASS);
   if( !FdValid( Config.LastFile)){
      Config.LastFile = FDIR_INVALID;                           // set as invalid
      SetupFilter( &Config.WeighingParameters.Saving, Tara);    // use defaults
      CalcStatistics(); // no backup, calculate it
      return;                                                   // start without database
   }
   // start with the file
   WeighingSetGlobalParameters(Config.LastFile);                               // update parameters by global settings
   SdbOpen( Config.LastFile, NO);
   WParameters = SdbConfig();                                   // working parameters
   if( SdbInfo()->Size > SDB_TRESHOLD_SIZE){
      ScreenWait();                                             // big samples count - show hourglass
   }
   UpdateLastWeight();                                          // find last weight
   SetupFilter( &WParameters->Saving, Tara);                    // set filter parameters
   if(StatsBackup.Mode == WParameters->Saving.Mode && 
         StatsBackup.ActiveFile == Config.LastFile &&
         StatsBackup.Count == SdbCount()){
      CalcRestoreBackup();
   }else{
      if( WParameters->Saving.Mode == SAVING_MODE_MANUAL_BY_SEX){
         CalcMode( CALC_BY_SEX);                                   // group by sex
      } else { // automatic or manual mode
         switch( WParameters->WeightSorting.Mode){
            case WEIGHT_SORTING_NONE :
               CalcMode( CALC_TOTAL);
               break;
            case WEIGHT_SORTING_LIGHT_HEAVY :
               CalcMode( CALC_LIGHT_HEAVY);
               break;
            case WEIGHT_SORTING_LIGHT_OK_HEAVY :
               CalcMode( CALC_LIGHT_OK_HEAVY);
               break;
            default :
               CalcMode( CALC_TOTAL);
               break;
         }
      }
      CalcStatistics(); //recalculate statistics
   }
} // WeighingRelease

//------------------------------------------------------------------------------
// Manual MALE
//------------------------------------------------------------------------------

void WeighingManualMale( void)
// Save manual value button MALE
{
   WeighingManual(YES);
} // WeighingManualMale

//------------------------------------------------------------------------------
// Manual FEMALE
//------------------------------------------------------------------------------

void WeighingManualFemale( void)
// Save manual value button FEMALE
{
   WeighingManual(NO);
} // WeighingManualFemale

//------------------------------------------------------------------------------
// Auto
//------------------------------------------------------------------------------

void WeighingExecute( void)
// Execute automatic weighing
{
TRawWeight  RawWeight;
TWeight     Weight;

   // set actual value :
   RawWeight    = AdcLowPassRead();
   ActualWeight = CalWeight( RawWeight);
#ifdef PROJECT_WEIGHING_PRINT_ACTUAL
   PrinterActualWeight( NettoWeight( ActualWeight));
#endif
   // check for working file :
   if( !Config.LastFile){
      return;                               // invalid working file
   }
   // Automatic weighing ------------------------------------------------------
   if( WParameters->Saving.Mode != SAVING_MODE_AUTOMATIC){
      return;                               // manual mode
   }
#ifdef PROJECT_WEIGHING_PRINT_LAST
   // check for last saved valid and wait for release :
   if( LastFlag != FLAG_INVALID && FilterRecord.Status == FILTER_WAIT_EMPTY && NewRecord){
      PrinterActualWeight( LastWeight);     // send saved weight
   }
#endif
#ifdef PROJECT_WEIGHING_PRINT_ZERO
   if(FilterRecord.Status != FILTER_WAIT_EMPTY) {
      Weight = NettoWeight( ActualWeight);
      if( Weight == 0){
         PrinterActualWeight( Weight);
      }
   }
#endif
   // check for stable value :
   if( !AdcRead( &RawWeight)){
      return;                               // no data (unstable value)
   }
   ActualWeight = CalWeight( RawWeight);    // update actual weight for display
   Weight = NettoWeight( ActualWeight);     // get netto weight
   SysResetTimeout();                       // backlight on & clear inactivity counters
   SaveWithLimits( Weight);                 // check for limits & save
} // WeighingExecute

//------------------------------------------------------------------------------
// Delete
//------------------------------------------------------------------------------

void WeighingDelete( void)
// Delete last value
{
TSdbRecord Record;

   if( !Config.LastFile){
      return;                          // invalid working file
   }
   SdbMoveAt( SdbCount() - 1);         // get last record
   SdbNext( &Record);
   if( !SdbDeleteLastRecord()){
      BeepError();
      return;                          // nothing to delete
   }
   ScreenWait();                       // display Wait
   UpdateLastWeight();                 // find last valid weight
   CalcDelete(Record.Weight, Record.Flag); // recalculate statistics
   if( WParameters->Saving.Mode == SAVING_MODE_AUTOMATIC){
      AdcRestart();                    // restart filtering
   }
} // WeighingDelete

//------------------------------------------------------------------------------
// Tara
//------------------------------------------------------------------------------

void WeighingTara( void)
// Save tara
{
   Tara = ActualWeight;
   // actualize filter zero :
   if( !Config.LastFile){
      SetupFilter( &Config.WeighingParameters.Saving, Tara);
   } else {
      SetupFilter( &WParameters->Saving, Tara);
   }
} // WeighingTara

//------------------------------------------------------------------------------
// Raw weight
//------------------------------------------------------------------------------

TRawWeight WeighingRaw( void)
// Get actual value
{
TRawWeight RawWeight;
int        i;
TYesNo     Inversion;

   Inversion = FilterRecord.Inversion; // save inversion status
   FilterRecord.Inversion = NO;        // disable ADC data inversion
   AdcStart();
   SysDelay( STABILISATION_DELAY);
   RawWeight = 0;
   for( i = 0; i < AVG_COUNT; i++){
      RawWeight += AdcRawRead();
      SysDelay( ADC_SAMPLING_PERIOD);
   }
   AdcStop();
   FilterRecord.Inversion = Inversion; // restore inversion status
   return( RawWeight >> AVG_POWER);
} // WeighingRaw

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

void WeighingRawTara( TRawWeight RawWeight)
// Set <RawWeight> as tara
{
   if( CalGetInversion()){
      RawWeight = -RawWeight;          // reversed bridge polarity
   }
   Tara = CalWeight( RawWeight);
} // WeighingRawTara

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

TWeight WeighingWeight( void)
// Returns actual weight
{
   return( NettoWeight( ActualWeight));
} // WeighingWeight

//------------------------------------------------------------------------------
// Weighing status
//------------------------------------------------------------------------------

TWeighingStatus WeighingStatus( void)
// Returns actual weighing status
{
   return( ActualStatus);
} // WeighingStatus

//------------------------------------------------------------------------------
// Last Weight
//------------------------------------------------------------------------------

TWeight WeighingLastWeight( void)
// Returns last saved weight
{
   return( LastWeight);
} // WeighingLastWeight

//------------------------------------------------------------------------------
// Last Flag
//------------------------------------------------------------------------------

TSampleFlag WeighingLastFlag( void)
// Returns last saved flag
{
   return( LastFlag);
} // WeighingLastFlag

//------------------------------------------------------------------------------
// Number of birds
//------------------------------------------------------------------------------

int WeighingNumberOfBirds( void)
// Return number of birds (0 by disabled option)
{
   if( !WParameters->EnableMoreBirds){
      return( 0);
   }
   return( WParameters->NumberOfBirds);
} // WeighingNumberOfBirds

//******************************************************************************

//------------------------------------------------------------------------------
// Netto weight
//------------------------------------------------------------------------------

static TWeight NettoWeight( TWeight Weight)
// Calculate weight by <ActualWeight> tara and number of birds
{
   Weight -= Tara;                          // remove tara
   ActualStatus = WEIGHING_OK;
   // check for limits :
   if( Weight > (TWeight)Config.Units.Range){
      ActualStatus = WEIGHING_OVER;
      Weight       = Config.Units.Range;
      return( Weight);                      // return saturated weight
   }
   // fiter drift :
   if( (Weight >= -WEIGHING_DRIFT) && (Weight <= WEIGHING_DRIFT)){
      Weight = 0;
   }
   // check for number of birds :
   if( !WParameters->EnableMoreBirds){
      return( RoundByDivision( Weight));    // without more birds
   }
   if( !WParameters->NumberOfBirds){
      return( 0);                           // divide by zero
   }
   return( RoundByDivision( Weight / WParameters->NumberOfBirds));
} // NettoWeight

//------------------------------------------------------------------------------
// Setup filter
//------------------------------------------------------------------------------

static void SetupFilter( TSavingParameters *Parameters, TWeight Zero)
// Setup weight filering
{
   AdcStop();                          // stop measuring
   FilterRecord.Inversion       = CalGetInversion();
   FilterRecord.AveragingWindow = Parameters->Filter;
   FilterRecord.StableWindow    = Parameters->StabilisationTime;
   FilterRecord.ZeroWeight      = CalRawWeight( Zero);
   FilterRecord.StableRange     = Parameters->StabilisationRange;
   FilterRecord.TresholdWeight  = CalRawWeight( Parameters->MinimumWeight + Zero);
   FilterRecord.MaxWeight       = CalRawWeight( Config.Units.Range + Zero);
   AdcStart();
} // SetupFilter

//------------------------------------------------------------------------------
// Save with limits
//------------------------------------------------------------------------------

static void SaveWithLimits( TWeight Weight)
// Check <Weight> limits & save
{
TSampleFlag Flag;

   Flag = GetFlag(WParameters, Weight);
   // display sort symbol :
   ScreenSorting( Flag);
   // beep :
   switch( Flag){
      case FLAG_NONE :
         BeepWeighingDefault();
         break;
      case FLAG_OK :
         BeepWeighingOk();
         break;
      case FLAG_LIGHT :
         BeepWeighingLight();
         break;
      case FLAG_HEAVY :
         BeepWeighingHeavy();
         break;
   }
   // save record :
   if( !SaveRecord( Weight, Flag)){
      return;
   }
   DisplayHold();                      // show frozen weight
} // SaveWithLimits

//------------------------------------------------------------------------------
// Save
//------------------------------------------------------------------------------

static TYesNo SaveRecord( TWeight Weight, TSampleFlag Flag)
// Save record
{
TSdbRecord Record;

   Record.Timestamp = SysGetClock();
   Record.Weight    = Weight;
   Record.Flag      = Flag;
#ifdef PROJECT_PRINTER_VEITFLPROTO_VERSION
   VeitFlPktSendWeight( Weight);
#endif
   if( !SdbAppend( &Record)){
      BeepError();
      ScreenNoMemory();
      return( NO);
   }
   CalcAppend( Weight, Flag);
#ifdef PROJECT_WEIGHING_PRINT_SAVED
   PrinterWeight( Calc[ STATISTICS_TOTAL].Count, Weight);   // print index & weight
#endif
   LastWeight = Weight;
   LastFlag   = Flag;
   NewRecord = YES;
   return( YES);
} // SaveRecord

//------------------------------------------------------------------------------
// Update Last Weight
//------------------------------------------------------------------------------

static void UpdateLastWeight( void)
// Update last weight by database
{
TSdbRecord Record;

   if( !SdbCount()){
      LastWeight   = 0;                // no last weight
      LastFlag     = FLAG_INVALID;     // no last flag
      return;                          // database empty
   }
   SdbMoveAt( SdbCount() - 1);         // last record
   SdbNext( &Record);
   LastWeight = Record.Weight;
   LastFlag   = Record.Flag;
   NewRecord = NO;

} // UpdateLastWeight

//------------------------------------------------------------------------------
// Round Division
//------------------------------------------------------------------------------

static TWeight RoundByDivision( TWeight Weight)
// Round <Weight> by Division
{
   if( Config.Units.Division <= 1){
      return( Weight);                      // no division
   }
   // Add division/2 for round up :
   Weight <<= 1;                            // Weight * 2
   if( Weight >= 0){
      Weight  += Config.Units.Division;     // add division
   } else {
      Weight  -= Config.Units.Division;     // add division
   }
   // Make division :
   Weight  /= (Config.Units.Division << 1); // Weight / 2
   Weight  *=  Config.Units.Division;
   return( Weight);
} // RoundByDivision

//------------------------------------------------------------------------------
// Manual weighing
//------------------------------------------------------------------------------

static void WeighingManual( TYesNo ForMale)
// execute manual weighing for gender
{
TWeight     Weight;
TSampleFlag Flag;
   if( !Config.LastFile){
      return;                          // invalid working file
   }
   if( WParameters->Saving.Mode == SAVING_MODE_AUTOMATIC){
      return;                          // automatic mode
   }
   Weight = NettoWeight( ActualWeight);
   if( Weight < WParameters->Saving.MinimumWeight){
      BeepError();
      return;                          // under minimal weight
   }
   // check for manual weighing without flag
   if( WParameters->Saving.Mode == SAVING_MODE_MANUAL){
      SaveWithLimits( Weight);         // check for limits & save
      return;
   }
   // manual weighing - male sex
   Flag = GetFlagWithGender(WParameters, Weight, ForMale);
   ScreenSorting( Flag);          // display flag
   BeepWeighingHeavy();
   if( !SaveRecord( Weight, Flag)){
      return;
   }
   DisplayHold();  // show frozen weight
}

//------------------------------------------------------------------------------
// Initialisation
//------------------------------------------------------------------------------

#define TIMER_FLASH_PERIOD (TIMER_FLASH1 + TIMER_FLASH2)

static void DisplayHold( void)
// Wait for frozen display
{
#ifdef PROJECT_WEIGHING_PRINT_LAST
int Count;

   // send stabilized weight with flash period
   Count = TIME_HOLD_DISPLAY / TIMER_FLASH_PERIOD;
   do {
      PrinterActualWeight( LastWeight);// send saved weight
      SysDelay( TIMER_FLASH_PERIOD);
   } while( --Count);
   PrinterActualWeight( LastWeight);   // send saved weight
   SysResetFlash();                    // start flash again
#else
   SysDelay( TIME_HOLD_DISPLAY);       // hold display
   SysResetFlash();                    // start flash again
#endif
} // DisplayHold
