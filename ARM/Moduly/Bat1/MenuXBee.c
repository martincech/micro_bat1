//******************************************************************************
//
//   MenuXBee.c   XBee configuration menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuXBee.h"

#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "../Inc/Wgt/DEvent.h"    // Display events
#include "../Inc/Wgt/Beep.h"

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Wgt/DMsg.h"             // Display message
#include "Wgt/DLayout.h"          // Display layout

#include "Str.h"                  // Strings
#include "XBee.h"

#include <stdio.h>

// Baud rate enum :
typedef enum {
   BAUD_2400,
   BAUD_4800,
   BAUD_9600,
   BAUD_19200,
   BAUD_38400,
   BAUD_57600,
   _XBEE_BAUD_COUNT
} TBaud;

// Baud rate list :
static DefList( BaudList)
   "2400 Bd",
   "4800 Bd",
   "9600 Bd",
   "19200 Bd",
   "38400 Bd",
   "57600 Bd",
EndList()

// COM format list :
static DefList( FormatList)
   "8-N-1",
   "8-E-1",
   "8-O-1",
   "8-M-1",
   "8-S-1",
   "7-N-1",
   "7-E-1",
   "7-O-1",
   "7-M-1",
   "7-S-1",
EndList()

// XBee menu :

static DefMenu( XBeeMenu)
   STR_COMMUNICATION_SPEED,
   STR_COMMUNICATION_FORMAT,
   "Rssi",
EndMenu()

typedef enum {
   PM_COMMUNICATION_SPEED,
   PM_COMMUNICATION_FORMAT,
   PM_RSSI,
} TXBeeMenuEnum;

// Local functions :

static void XBeeParameters(  int Index, int y, void *UserData);
// Menu XBee parameters

static int EncodeCommunicationSpeed( int Baud);
// Encode <Baud> to enum

static int DecodeCommunicationSpeed( int BaudCode);
// Decode <BaudCode> enum to speed

static void MenuRssi( void);

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuXBee( void)
// XBee configuration menu
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( "XBee", XBeeMenu, XBeeParameters, 0, &MData)){
         ConfigSaveSection( Printer);
         return;
      }
      switch( MData.Item){
         case PM_COMMUNICATION_SPEED :
            i = EncodeCommunicationSpeed( Config.Printer.CommunicationSpeed);
            if( !DEditList( DMENU_EDIT_X, MData.y, &i, BaudList)){
               break;
            }
            Config.Printer.CommunicationSpeed = DecodeCommunicationSpeed( i);
            break;

         case PM_COMMUNICATION_FORMAT :
            i = Config.Printer.CommunicationFormat;
            if( !DEditList( DMENU_EDIT_X, MData.y, &i, FormatList)){
               break;
            }
            Config.Printer.CommunicationFormat = i;
            break;

         case PM_RSSI :
            MenuRssi();
            break;
      }
   }
} // MenuXBee

static void MenuRssi( void) {
char RssiText[32];
   forever {
      switch( DEventWait()){
         case K_FLASH1 :
            sprintf(RssiText, "Rssi: -%d dBm", XBeeRssiGet());
            GClear();
            // frame :
            DLayoutTitle( "XBee");
            DLayoutStatus( STR_BTN_CANCEL, 0, 0);
            DLabelCenter( RssiText,  0, G_HEIGHT/2, G_WIDTH, 0);
            GFlush();
            break;

         case K_ENTER :
         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }
   }
} 

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void XBeeParameters(  int Index, int y, void *UserData)
// Menu XBee parameters
{
   switch( Index){
      case PM_COMMUNICATION_SPEED :
         DLabelList( EncodeCommunicationSpeed( Config.Printer.CommunicationSpeed), BaudList, DMENU_PARAMETERS_X, y);
         break;

      case PM_COMMUNICATION_FORMAT :
         DLabelList( Config.Printer.CommunicationFormat, FormatList, DMENU_PARAMETERS_X, y);
         break;
   }
} // XBeeParameters

//------------------------------------------------------------------------------
//   Baud coding
//------------------------------------------------------------------------------

static const int _BaudTable[ _XBEE_BAUD_COUNT] = { 2400, 4800, 9600, 19200, 38400, 57600};

static int EncodeCommunicationSpeed( int Baud)
// Encode <Baud> to enum
{
int i;

   for( i = 0; i < _XBEE_BAUD_COUNT; i++){
      if( _BaudTable[ i] == Baud){
         return( i);
      }
   }
   return( BAUD_2400);
} // EncodeCommunicationSpeed


static int DecodeCommunicationSpeed( int BaudCode)
// Decode <BaudCode> enum to speed
{
   return( _BaudTable[ BaudCode]);
} // DecodeCommunicationSpeed
