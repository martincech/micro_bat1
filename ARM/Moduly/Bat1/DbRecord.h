//******************************************************************************
//
//   DbRecord.h   Database record definition
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __DbRecord_H__
   #define __DbRecord_H__

#ifndef __SdbDef_H__
   #include "SdbDef.h"
#endif

// config record :
typedef TSdbConfig TDbConfig;

// data record :
typedef TSdbRecord TDbRecord;


#define DB_CONFIG_SIZE        (int)sizeof( TDbConfig)
#define DB_RECORD_SIZE        (int)sizeof( TDbRecord)

// record flag :
#define DB_FLAG_OFFSET        offsetof( TDbRecord, Flag)
#define DbIsDeleted( Flag)    ((Flag)  &  FLAG_DELETED) // deleted record
#define DbSetDeleted( Flag)    (Flag)  |= FLAG_DELETED  // mark as deleted
#define DbFlag( Record)        (Record).Flag            // flag of the record

#endif
 
