//******************************************************************************
//
//   Config.c     Configuration utilities
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "Config.h"
#include "../Inc/Eep.h"
#include "Bat1.h"
#include "BeepDef.h"
#include "MemoryDef.h"              // EEPROM layout
#include <string.h>

#include "ConfigSet.c"       // default configuration definition

TConfig Config;              // global configuration data

#define CONFIG_ADDRESS  offsetof( TEeprom, Config)

// Local functions :

static TYesNo CheckIntegrity( void);
// Check config integrity

static void UpdateCrc( void);
// Write CRC by data

static TConfigCrc CalculateCrc( void);
// Calculate CRC

static void ConfigDefault( void);
// Set default values

//------------------------------------------------------------------------------
// Load
//------------------------------------------------------------------------------

void ConfigLoad( void)
// Load configuration from EEPROM
{
   if( !CheckIntegrity()){
      // wrong checksum, set defaults
      ConfigDefault();
      ConfigSave();
      return;
   }
   EepLoadData( CONFIG_ADDRESS, &Config, sizeof( TConfig));
   if( Config.Version != BAT1_VERSION){
      // version chage, set defaults
      ConfigDefault();
      ConfigSave();
      return;
   }
   if( Config.Build != BAT1_BUILD || Config.HwVersion != BAT1_HW_VERSION){
      // hardware change or new build
      Config.Build     = BAT1_BUILD;
      Config.HwVersion = BAT1_HW_VERSION;
      ConfigSave();                    // save new build & hw version
   }
   // fixup Country & Language range :
   if( Config.Country.Country >= _COUNTRY_COUNT){
      Config.Country.Country = COUNTRY_INTERNATIONAL;
      CountryLoad( COUNTRY_INTERNATIONAL);
      ConfigSaveSection( Country);
   }
   if( Config.Country.Language >= _LNG_COUNT){
      Config.Country.Language = LNG_ENGLISH;
      CountrySetLanguage( LNG_ENGLISH);
      ConfigSaveSection( Country);
   }
} // ConfigLoad

//------------------------------------------------------------------------------
// Save
//------------------------------------------------------------------------------

void ConfigSave( void)
// Save configuration to EEPROM
{
   if( EepMatchData( CONFIG_ADDRESS, &Config, sizeof( TConfig))){
      return;                          // match ok, already saved
   }
   EepSaveData( CONFIG_ADDRESS, &Config, sizeof( TConfig));
   EepFillData( CONFIG_ADDRESS + sizeof( TConfig), 0, CONFIG_DATA_SIZE - sizeof( TConfig));
   UpdateCrc();
} // ConfigSave

//------------------------------------------------------------------------------
// Factory default
//------------------------------------------------------------------------------

void ConfigFactoryDefault( void)
// Save factory default configuration to EEPROM
{
   ConfigDefault();
   ConfigSave();
} // ConfigFactoryDefault

//------------------------------------------------------------------------------
// Save Fragment
//------------------------------------------------------------------------------

void ConfigSaveFragment( int Offset, int Size)
// Save configuration fragment
{
   if( EepMatchData( CONFIG_ADDRESS + Offset, (byte *)&Config + Offset, Size)){
      return;                          // match ok, already saved
   }
   // save data fragment :
   EepSaveData( CONFIG_ADDRESS + Offset, (byte *)&Config + Offset, Size);
   UpdateCrc();
} // ConfigSaveFragment

//******************************************************************************

#define CONFIG_CRC_ADDRESS (CONFIG_ADDRESS + offsetof( TConfigSpare, CheckSum))

//------------------------------------------------------------------------------
// Check integrity
//------------------------------------------------------------------------------

static TYesNo CheckIntegrity( void)
// Check config integrity
{
TConfigCrc Crc;
TConfigCrc EepCrc;

   Crc = CalculateCrc();
   EepLoadData( CONFIG_CRC_ADDRESS, &EepCrc, sizeof( TConfigCrc));
   return( Crc == EepCrc);
} // CheckIntegrity

//------------------------------------------------------------------------------
// Update CRC
//------------------------------------------------------------------------------

static void UpdateCrc( void)
// Write CRC by data
{
TConfigCrc Crc;

   Crc = CalculateCrc();
   EepSaveData( CONFIG_CRC_ADDRESS, &Crc, sizeof( TConfigCrc));
} // UpdateCrc

//------------------------------------------------------------------------------
// Calculate CRC
//------------------------------------------------------------------------------

static TConfigCrc CalculateCrc( void)
// Calculate CRC
{
TConfigCrc Crc;
int        Size;

   Crc  = 0;
   Size = CONFIG_DATA_SIZE;
   EepBlockReadStart( CONFIG_ADDRESS);
   while( Size-- > 0){
      Crc += EepBlockReadData();
   }
   EepBlockReadStop();
   return( ~Crc);
} // ConfigCrc

//------------------------------------------------------------------------------
// Default
//------------------------------------------------------------------------------

static void ConfigDefault( void)
// Set default values
{
   memcpy( &Config, &_DefaultConfig, sizeof( TConfig));
} // ConfigDefault
