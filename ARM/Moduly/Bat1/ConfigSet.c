//******************************************************************************
//
//   ConfigSet.c  Configuration defaults
//   Version 0.0  (c) VymOs
//
//******************************************************************************

const TConfig _DefaultConfig = {
/* Version   */        BAT1_VERSION,
/* Build     */        BAT1_BUILD,
/* HwVersion */        BAT1_HW_VERSION,

/* ScaleName */        BAT1_SCALE_NAME,
/* Password  */       { 0, 0, 0, 0},

/* Country   */ {       
   /* Country  */      BAT1_COUNTRY,
   /* Language */      BAT1_LANGUAGE,
   /* _Spare   */      0,
   /* Locale   */      { CPG_LATIN, DATE_FORMAT_DDMMYYYY, '.', '.', TIME_FORMAT_24, ':', DT_DST_TYPE_OFF, 0},
},

/* Units     */ {
   /* Range       */   UNITS_KG_RANGE,
   /* Units       */   UNITS_KG,
   /* Decimals    */   UNITS_KG_DECIMALS,
   /* MaxDivision */   UNITS_KG_MAX_DIVISION,
   /* Division    */   UNITS_KG_DIVISION,
},

/* Sounds    */ {
   /* ToneDefault    */ BEEP_MELODY1,
   /* ToneLight      */ BEEP_MELODY2,
   /* ToneOk         */ BEEP_MELODY3,
   /* ToneHeavy      */ BEEP_MELODY4,
   /* ToneKeyboard   */ BEEP_TONE7,
   /* EnableSpecial  */ YES,
   /* VolumeKeyboard */ 2,
   /* VolumeSaving   */ VOLUME_MAX,
},

/* Display   */ {
   /* Mode      */     DISPLAY_MODE_BASIC,
   /* Contrast  */     CONTRAST_MAX / 2,
   /* _Spare    */     0,
   /* Backlight */ {
      /* Mode      */  BACKLIGHT_MODE_AUTO,
      /* Intensity */  BACKLIGHT_MAX,
      /* Duration  */  BACKLIGHT_DURATION,
   },
},

/* Printer */ {
   /* PaperWidth          */ PRINTER_PAPER_WIDTH,
   /* CommunicationFormat */ COM_8BITS,
   /* CommunicationSpeed  */ PRINTER_SPEED,
}, 

/* KeyboardTimeout */      KEYBOARD_TIMEOUT,
/* PowerOffTimeout */      POWER_OFF_TIMEOUT,

/* LastFile */             FDIR_INVALID,
/* EnableFileParameters */ NO,
/* PrintProtocol */        0,
/* _Spare        */        0,

/* WeighingParameters */ {
   /* EnableMoreBirds */ NO,
   /* NumberOfBirds */   NUMBER_OF_BIRDS_DEFAULT,
   /* WeightSorting */  {
      /* Mode      */  WEIGHT_SORTING_NONE,
      /* _Spare    */  {0, 0, 0},
      /* LowLimit  */  1000,
      /* HighLimit */  2000,
      {
         { 0,     1, "Default"}
      }
   },
   /* Saving */ {
      /* Mode   */     SAVING_MODE_AUTOMATIC,
      /* Filter             */  SAVING_FILTER,
      /* StabilisationTime  */  SAVING_STABILISATION,
      /* _Spare             */  0,
      /* MinimumWeight      */  SAVING_MINIMUM_KG,
      /* StabilisationRange */  SAVING_RANGE,
   },
}, // WeighingParameters

/* StatisticParameters */ {
   /* UniformityRange */ 10,
   /* _Spare    */       {0, 0, 0},
   /* Histogram       */ {
      /* Mode   */     HISTOGRAM_MODE_RANGE,
      /* Range  */     HISTOGRAM_RANGE,
      /* _Spare */     0,
      /* Step   */     HISTOGRAM_STEP,
   },
} // StatisticParameters
}; // TConfig
