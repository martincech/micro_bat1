//******************************************************************************
//                                                                            
//   xWeight.c      putchar based weight formatting
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "xWeight.h"
#include "../Inc/xprint.h"        // Display
#include "../Inc/Bcd.h"           // Display

#include "Bat1.h"                 // Max display weight
#include "Str.h"                  // Strings
#include "ConfigDef.h"            // Configuration

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

void xWeight( TPutchar *xPutchar, TWeight Weight)
// Display weight
{
int Width;

   Width = BcdGetWidth( BAT1_WEIGHT_MAX);
   if( !Config.Units.Decimals){
      xprintdec( xPutchar, Weight, Width);
      return;
   }
   xfloat( xPutchar, Weight, Width + 1, Config.Units.Decimals, 0);
} // xWeight

//------------------------------------------------------------------------------
// Weight width
//------------------------------------------------------------------------------

int xWeightWidth( void)
// Weight width (chars)
{
   return( BcdGetWidth( BAT1_WEIGHT_MAX) + 1);  // always with dot
} // xWeightWidth

//------------------------------------------------------------------------------
// Weight left
//------------------------------------------------------------------------------

void xWeightLeft( TPutchar *xPutchar, TWeight Weight)
// Display weight left aligned
{
  xWeightLeftDecimals(xPutchar, Weight, Config.Units.Decimals);
} // xWeightLeft

void xWeightLeftDecimals( TPutchar *xPutchar, TWeight Weight, byte decimals)
// Display weight left aligned
{
   if( !decimals){
      xprintdec( xPutchar, Weight, 0);
      return;
   }
   xfloat( xPutchar, Weight, 0, decimals, 0);
}

//------------------------------------------------------------------------------
// Flag
//------------------------------------------------------------------------------

void xWeighingFlag( TPutchar *xPutchar, TSampleFlag Flag)
// Display weighing flag
{
   switch( Flag){
      case FLAG_NONE :
         (*xPutchar)( 'N');
         break;
      case FLAG_OK :
         (*xPutchar)( 'O');
         break;
      case FLAG_LIGHT :
         (*xPutchar)( 'L');
         break;
      case FLAG_HEAVY :
         (*xPutchar)( 'H');
         break;
      case FLAG_MALE :
         (*xPutchar)( 'M');
         break;
      case FLAG_FEMALE :
         (*xPutchar)( 'F');
         break;
   }
} // xWeightingFlag

//------------------------------------------------------------------------------
// Units
//------------------------------------------------------------------------------

void xWeighingUnits( TPutchar *xPutchar)
// Display current units
{
   xputs( xPutchar, StrGet( STR_UNITS_KG + Config.Units.Units));
} // xWeighingUnits

//------------------------------------------------------------------------------
// Weight & Units
//------------------------------------------------------------------------------

void xWeightWithUnits( TPutchar *xPutchar, TWeight Weight)
// Display weight with units
{
   xWeight( xPutchar, Weight); 
   (*xPutchar)(' ');
   xWeighingUnits( xPutchar);
} // xWeightWithUnits

//------------------------------------------------------------------------------
// Weight & Units
//------------------------------------------------------------------------------

void xWeightWithUnitsLeft( TPutchar *xPutchar, TWeight Weight)
// Display weight with units left aligned
{
   xWeightLeft( xPutchar, Weight); 
   (*xPutchar)(' ');
   xWeighingUnits( xPutchar);
} // xWeightWithUnitsLeft
