//*****************************************************************************
//
//    Fonts.c      project fonts
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Fonts.h"
#include "../inc/Country.h"
#include "../inc/Graphic.h"
#include <string.h>
#include <ctype.h>
#include "Font/Tahoma16.c"
#include "Font/Tahoma16j.c"
#include "Font/ArialBold14.c"
#include "Font/ArialBold18.c"
#include "Font/ArialBold21.c"
//#include "Font/ArialBold27.c"
//#include "Font/ArialBold45.c"
//#include "Font/ArialBold55.c"
//#include "Font/ArialBold90.c"

#include "Font/ArialBold27n.c"
#include "Font/ArialBold45n.c"
#include "Font/ArialBold55n.c"
#include "Font/ArialBold90n.c"

const TFontDescriptor const * const Fonts[] = {
   /* Tahoma16      */   &FontTahoma16,
   /* Tahoma16j     */   &FontTahoma16j,
   /* ArialBold14   */   &FontArialBold14,
   /* ArialBold18   */   &FontArialBold18,
   /* ArialBold20   */   &FontArialBold21,
   ///* ArialBold27  */   &FontArialBold27,
   /* ArialBold27n  */   &FontArialBold27n,
   ///* ArialBold45  */   &FontArialBold45,
   /* ArialBold45n  */   &FontArialBold45n,
   ///* ArialBold55  */   &FontArialBold55,   
   /* ArialBold55n  */   &FontArialBold55n,
   ///* ArialBold90  */   &FontArialBold90,
   /* ArialBold90n  */   &FontArialBold90n,
   /* last          */   0
};   

//------------------------------------------------------------------------------
//  Set font
//------------------------------------------------------------------------------

void SetFont( int FontNumber)
// Set font
{
   if( (CountryGetCodePage() == CPG_JAPAN) && (FontNumber == TAHOMA16)){
      GSetFont( TAHOMA16J);           // translate font
      return;
   }
   if( (CountryGetCodePage() != CPG_JAPAN) && (FontNumber == TAHOMA16J)){
      GSetFont( TAHOMA16);           // translate font
      return;
   }
   GSetFont( FontNumber);              // direct font
} // SetFont

int SetFontAccordingToTextWidth(char *text, int maxWidth, int maxHeight)
// set the best font according to text width so the text is in the required area
// return length of the text which can fit
{
int i = _FONT_LAST; 
int width;  
int textLen; 
   
   textLen = strlen(text);
   while(i>0){
      i--;
      
      SetFont(i);
      if(GCharHeight() > maxHeight){
         continue;
      }
      width = GTextWidth(text);      
      if(width < maxWidth){
         if(Fonts[i]->Type == FONT_NUMERIC){
            TYesNo nodigit = NO;
            int j;
            for(j = 0; j < textLen; j++){
               if(!isdigit(text[j])){
                  nodigit = YES;
                  break;
               }
            }
            if(nodigit){
               continue;
            }
         }
         break;
      }else{
         width = maxWidth;
      }
      i--;
   }      
   return ((double)textLen)*(width/(double)GTextWidth(text));
}