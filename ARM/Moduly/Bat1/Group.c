//******************************************************************************
//
//   Group.c      Group utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "Group.h"
#include "../Inc/File/Fd.h"  // File directory
#include "../Inc/Cpu.h"      // WatchDog
#include <string.h>

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------

TYesNo GroupCreate( char *Name, TGroup Group)
// Create new group
{
   if( !FsCreate( Name, GRP_CLASS)){
      return( NO);
   }
   if( !FsWrite( Group, sizeof( TGroup))){
      FsDelete();                      // not enough space
      return( NO);
   }
   FsClose();
   return( YES);
} // GroupCreate

//------------------------------------------------------------------------------
// Load
//------------------------------------------------------------------------------

void GroupLoad( TFdirHandle Handle, TGroup Group)
// Load group data into <Group>
{
   FsOpen( Handle, YES);
   FsRead( Group, sizeof( TGroup));
   FsClose();
} // GroupLoad

//------------------------------------------------------------------------------
// Save
//------------------------------------------------------------------------------

void GroupSave( TFdirHandle Handle, TGroup Group)
// Save <Group> into group
{
   FsOpen( Handle, NO);
   FsWSeek( 0, FS_SEEK_SET);           // overwrite data
   FsWrite( Group, sizeof( TGroup));
   FsClose();
} // GroupSave

//------------------------------------------------------------------------------
// Load note
//------------------------------------------------------------------------------

void GroupLoadNote( TFdirHandle Handle, char *Note)
// Load group note
{
   FsOpen( Handle, YES);
   strcpy( Note, FsInfo()->Note);
   FsClose();
} // GroupLoadNote

//------------------------------------------------------------------------------
// Save note
//------------------------------------------------------------------------------

void GroupSaveNote( TFdirHandle Handle, char *Note)
// Save group note
{
   FsOpen( Handle, NO);
   strcpy( FsInfo()->Note, Note);
   FsClose();
} // GroupSaveNote

//------------------------------------------------------------------------------
// Delete
//------------------------------------------------------------------------------

void GroupDelete( TFdirHandle Handle)
// Delete group
{
   FsOpen( Handle, NO);
   FsDelete();
} // GroupDelete

//------------------------------------------------------------------------------
// Delete file
//------------------------------------------------------------------------------

void GroupDeleteFile( TFdirHandle Handle)
// Remove <Handle> from all groups
{
TGroup      Group;
TFdirHandle GHandle;

   FdSetClass( GRP_CLASS);             // group directory class
   FdFindBegin();
   while( (GHandle = FdFindNext()) != FDIR_INVALID){
      FsOpen( GHandle, NO);
      FsWSeek( 0, FS_SEEK_SET);        // overwrite data
      FsRead(  Group, sizeof( TGroup));
      GroupRemove( Group, Handle);
      FsWrite( Group, sizeof( TGroup));
      FsClose();
      WatchDog();
   }
} // GroupDeleteFile
