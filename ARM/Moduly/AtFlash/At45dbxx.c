//*****************************************************************************
//
//   At45dbxx.c     Flash memory AT45DBxxx
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#include "..\inc\At45dbxx.h"
#include "../../inc/System.h"

// Kody prikazu :

#define FLASH_ARRAY_READ      0xE8         // SPI mode 0 or 3
#define FLASH_PAGE_READ       0xD2
#define FLASH_BUFFER1_READ    0xD4
#define FLASH_BUFFER2_READ    0xD6
#define FLASH_STATUS_READ     0xD7

#define FLASH_BUFFER1_WRITE   0x84
#define FLASH_BUFFER2_WRITE   0x87
#define FLASH_BUFFER1_SAVE    0x83         // buffer 1 to main memory page with Erase
#define FLASH_BUFFER2_SAVE    0x86         // buffer 2 to main memory page with Erase
#define FLASH_PAGE_ERASE      0x81

#define FLASH_PAGE_TO_BUFFER1 0x53
#define FLASH_PAGE_TO_BUFFER2 0x55

// adresace pameti :

#if FLASH_PAGE_SIZE == 528
   // z linearni adresy :
   #define MkBufferAddress( a)     ((a) & 0x1FF)
   #define MkPageAddress( a)       ((word)((a) >> 9))

   #define BufferLowAddress( b)    ((b) & 0x00FF)
   #define BufferHighAddress( b)   (((b) >> 8) & 0x01)
   #define PageLowAddress( p)      ((p) << 2)
   #define PageHighAddress( p)     ((p) >> 6)
#elif FLASH_PAGE_SIZE == 264
   // z linearni adresy :
   #define MkBufferAddress( a)     ((a) & 0xFF)
   #define MkPageAddress( a)       ((word)((a) >> 8))

   #define BufferLowAddress( b)    ((b) & 0x00FF)
   #define BufferHighAddress( b)   0
   #define PageLowAddress( p)      ((p) << 1)
   #define PageHighAddress( p)     ((p) >> 7)
#else
   // AT45DB642 - 1056 buffer, 8192 pages
   #error "Unknown FLASH DATA device"
#endif

// popis stavoveho registru :

#define FLASH_STATUS_RDY   0x80 // ready bit
#define FLASH_STATUS_COMP  0x40 // priznak komparace
#define FLASH_STATUS_FIXED 0x2C // typove cislo
#define FLASH_STATUS_MASK  0xFC // platne bity stavu

//-----------------------------------------------------------------------------
// Stav
//-----------------------------------------------------------------------------

native FlashStatus( void)
// Vrati stavovy byte pameti
{
native value;

   SpiAttach();                        // SPI mode 0
   SpiWriteByte( FLASH_STATUS_READ);
   value = SpiReadByte();
   SpiRelease();                       // deselect
   return( value);
} // FlashStatus

//-----------------------------------------------------------------------------
// Cekani
//-----------------------------------------------------------------------------

TYesNo FlashWaitForReady( void)
// Ceka na dokonceni operace
{
native Now;

   Now = SysTime();
   do {
      if( FlashStatus() & FLASH_STATUS_RDY){
         return( YES);
      }
   } while( (SysTime() - Now) < FLASH_TIMEOUT);
   return( NO);
} // FlashWaitForReady

//-----------------------------------------------------------------------------
// Zapis do bufferu
//-----------------------------------------------------------------------------

void FlashWriteBufferStart( native Offset)
// Zahaji sekvencni zapis do bufferu
{
   SpiAttach();                        // SPI mode 0
   if( Offset & FLASH_BUFFER2){
      Offset &= ~FLASH_BUFFER2;
      SpiWriteByte( FLASH_BUFFER2_WRITE);
   } else {
      SpiWriteByte( FLASH_BUFFER1_WRITE);
   }
   SpiWriteByte( 0);                   // don't care
   SpiWriteByte( BufferHighAddress( Offset));
   SpiWriteByte( BufferLowAddress( Offset));
} // FlashWriteBufferStart

//-----------------------------------------------------------------------------
// Ulozeni bufferu
//-----------------------------------------------------------------------------

void FlashSaveBuffer( native Page)
// Zapis bufferu do flash
{
   SpiAttach();                        // SPI mode 0
   if( Page & FLASH_BUFFER2){
      Page &= ~FLASH_BUFFER2;
      SpiWriteByte( FLASH_BUFFER2_SAVE);
   } else {
      SpiWriteByte( FLASH_BUFFER1_SAVE);
   }
   SpiWriteByte( PageHighAddress( Page));
   SpiWriteByte( PageLowAddress( Page));
   SpiWriteByte( 0);                   // don't care
   SpiRelease();                       // deselect
} // FlashSaveBuffer

//-----------------------------------------------------------------------------
// Nacteni bufferu
//-----------------------------------------------------------------------------

void FlashLoadBuffer( native Page)
// Plneni bufferu z flash
{
   SpiAttach();                        // SPI mode 0
   if( Page & FLASH_BUFFER2){
      Page &= ~FLASH_BUFFER2;
      SpiWriteByte( FLASH_PAGE_TO_BUFFER2);
   } else {
      SpiWriteByte( FLASH_PAGE_TO_BUFFER1);
   }
   SpiWriteByte( PageHighAddress( Page));
   SpiWriteByte( PageLowAddress( Page));
   SpiWriteByte( 0);                   // don't care
   SpiRelease();                       // deselect
} // FlashLoadBuffer

//-----------------------------------------------------------------------------
// Cteni z pameti
//-----------------------------------------------------------------------------

void FlashBlockReadStart( native Page, native Offset)
// Zahaji sekvencni cteni dat primo z flash
{
   SpiAttach();                        // SPI mode 0
   SpiWriteByte( FLASH_ARRAY_READ);
   SpiWriteByte( PageHighAddress( Page));
   SpiWriteByte( PageLowAddress( Page) | BufferHighAddress( Offset));
   SpiWriteByte( BufferLowAddress( Offset));
   SpiWriteByte( 0);                   // additional don't care
   SpiWriteByte( 0);                   // additional don't care
   SpiWriteByte( 0);                   // additional don't care
   SpiWriteByte( 0);                   // additional don't care
} // FlashPageReadStart

//-----------------------------------------------------------------------------
// Cteni z bufferu
//-----------------------------------------------------------------------------

void FlashBufferReadStart( native Offset)
// Zahaji sekvencni cteni dat z bufferu
{
   SpiAttach();                        // SPI mode 0
   if( Offset & FLASH_BUFFER2){
      Offset &= ~FLASH_BUFFER2;
      SpiWriteByte( FLASH_BUFFER2_READ);
   } else {
      SpiWriteByte( FLASH_BUFFER1_READ);
   }
   SpiWriteByte( 0);                   // don't care
   SpiWriteByte( BufferHighAddress( Offset));
   SpiWriteByte( BufferLowAddress(  Offset));
   SpiWriteByte( 0);                   // additional don't care
} // FlashBufferReadStart
