//******************************************************************************
//                                                                            
//  SysTimer.c     System timer services                                                                 
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/System.h"

#define TIMER_IR      T0_IR            // interrupt flag register
#define TIMER_TCR     T0_TCR           // timer control register
#define TIMER_CTCR    T0_CTCR          // counter/timer control register
#define TIMER_PR      T0_PR            // prescaler 
#define TIMER_MCR     T0_MCR           // match control register
#define TIMER_VIC     VIC_TIMER0       // VIC channel

#define TIMER_CHANNEL 0                // match channel
#define TIMER_MR      T0_MR0           // match value register

#define TIMER_INCREMENT ((TIMER_PERIOD * FBUS) / 1000 - 1)

static volatile dword _TimerTick;

static void __irq SysTimerHandler( void);
// Zpracovani preruseni od casovace

//------------------------------------------------------------------------------
//   Start timer
//------------------------------------------------------------------------------

void SysStartTimer( void)
// Start systemoveho casovace
{
   CpuIrqAttach( SYS_TIMER_IRQ, TIMER_VIC, SysTimerHandler);
   TIMER_PR    = 0;                    // Count every PCLK edge
   TIMER_MCR  |= MCR_INTERRUPT( TIMER_CHANNEL) | 
                 MCR_RESET( TIMER_CHANNEL);  // enable match interrupt & reset
   TIMER_MR    = TIMER_INCREMENT;      // match register to interval
   TIMER_TCR   = TCR_RESET;            // Reset timer
   TIMER_TCR   = TCR_ENABLE;           // Run timer
} // SysStartTimer

//------------------------------------------------------------------------------
//   Timer handler
//------------------------------------------------------------------------------

static void __irq SysTimerHandler( void)
// Zpracovani preruseni od casovace
{
   TIMER_IR    = IR_MR( TIMER_CHANNEL);// clear flag
   _TimerTick += TIMER_PERIOD;         // system timer
   SysTimerExecute();                  // user actions
   CpuIrqDone();
} // SysTimerHandler

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

dword SysTime( void)
// Cte milisekundovy citac
{
   return( _TimerTick);
} // SysTime
