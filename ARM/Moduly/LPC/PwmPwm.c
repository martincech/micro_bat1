//*****************************************************************************
//
//    PwmPwm.c     PWM by hardware PWM
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/Pwm.h"
#include "../../inc/System.h"
#include "../../inc/Cpu.h"

#if   SPWM_CHANNEL == 1
   // P0.0
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 << 0)                // PINSEL0, P0.0 dibit
   #define PINSEL_PWM    (2 << 0)                // PINSEL0 function 10 on P0.0
   #define PWM_MR        PWM_MR1
#elif SPWM_CHANNEL == 2
   // P0.7
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 << 14)               // PINSEL0, P0.7 dibit
   #define PINSEL_PWM    (2 << 14)               // PINSEL0 function 10 on P0.7
   #define PWM_MR        PWM_MR2
#elif SPWM_CHANNEL == 3
   // P0.1
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 <<  2)               // PINSEL0, P0.1 dibit
   #define PINSEL_PWM    (2 <<  2)               // PINSEL0 function 10 on P0.1
   #define PWM_MR        PWM_MR3
#elif SPWM_CHANNEL == 4
   // P0.8
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 << 16)               // PINSEL0, P0.8 dibit
   #define PINSEL_PWM    (2 << 16)               // PINSEL0 function 10 on P0.8
   #define PWM_MR        PWM_MR4
#elif SPWM_CHANNEL == 5
   // P0.21
   #define PINSEL        PCB_PINSEL1             // index : 21 - 16 = 5
   #define PINSEL_MASK   (3 << 10)               // PINSEL1, P0.21 dibit
   #define PINSEL_PWM    (1 << 10)               // PINSEL1 function 01 on P0.21
   #define PWM_MR        PWM_MR5
#elif SPWM_CHANNEL == 6
   // P0.9
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 << 18)               // PINSEL0, P0.9 dibit
   #define PINSEL_PWM    (2 << 18)               // PINSEL0 function 10 on P0.9
   #define PWM_MR        PWM_MR6
#else
   #error "Invalid PWM_CHANNEL"
#endif

#define SPWM_INCREMENT ((SPWM_PERIOD * (FBUS / 1000)) / 1000)

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void PwmInit( void)
// Initialisation
{
   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_PWM;// select pin fuction
   PWM_PR  = 0;                                  // prescaler
   PWM_TCR = PWMTCR_RESET;                       // reset
   PWM_MCR = PWMMCR_RESET0;                      // clear by PWM0
   PWM_PCR = PWMPCR_PWMENA( SPWM_CHANNEL);       // enable PWM output
} // PwmInit

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void PwmStop( void)
// Stop PWM
{
   PWM_TCR = PWMTCR_RESET;                       // reset counter
} // PwmStop

//-----------------------------------------------------------------------------
// Run
//-----------------------------------------------------------------------------

void PwmStart( int Percent)
// Start PWM with <Percent> duty cycle
{
   PWM_MR0 =  SPWM_INCREMENT;                                    // period
#ifdef SPWM_INVERT
   PWM_MR  =  SPWM_INCREMENT - (SPWM_INCREMENT * Percent) / 100; // width
#else
   PWM_MR  = (SPWM_INCREMENT * Percent) / 100;                   // width
#endif
   PWM_LER =  PWMLER_ENABLE0 | PWMLER_ENABLE( SPWM_CHANNEL);     // enable latch
   PWM_TCR =  PWMTCR_ENABLE  | PWMTCR_PWM_ENABLE;                // enable count and PWM
} // PwmStart
