//*****************************************************************************
//
//    UartAsync.c - RS232 communication services template
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/TinyFifo.h"        // FIFO pro buffery
#include "../../inc/System.h"


//-- FIFO pro Rx -------------------------------------------------------------

static volatile word iread;            // cteci ukazatel do Rx bufferu
static volatile word iwrite;           // zapisovaci ukazatel do Rx bufferu
static byte ibuff[ UART_ISIZE];         // Rx fifo


#define UartRFifoInit()     TFifoInit(  iread, iwrite)                 // inicializace
#define UartRFifoEmpty()    TFifoEmpty( iread, iwrite)                 // zadne nactene znaky
#define UartRFifoRead()     TFifoRead(  ibuff, iread,  UART_ISIZE)     // cteni znaku
#define UartRFifoFlush()    TFifoFlush( iread);                        // preskok znaku
#define UartRFifoFull()     TFifoFull(  iread, iwrite, UART_ISIZE)     // zaplnene fifo
#define UartRFifoWrite( ch) TFifoWrite( ibuff, iwrite, UART_ISIZE, ch) // zapis znaku

//-- FIFO pro Tx -------------------------------------------------------------

static volatile bool _tx_running;      // probiha vysilani

static volatile word oread;            // cteci ukazatel do Tx bufferu
static volatile word owrite;           // zapisovaci ukazatel do Tx bufferu
static byte obuff[ UART_OSIZE];        // Tx fifo
static word _Timeout;                  // intercharacter timeout


#define UartTFifoInit()     TFifoInit(  oread, owrite)                 // inicializace
#define UartTFifoEmpty()    TFifoEmpty( oread, owrite)                 // zadne nactene znaky
#define UartTFifoRead()     TFifoRead(  obuff, oread,  UART_OSIZE)     // cteni znaku
#define UartTFifoFlush()    TFifoFlush( oread);                        // preskok znaku
#define UartTFifoFull()     TFifoFull(  oread, owrite, UART_OSIZE)     // zaplnene fifo
#define UartTFifoWrite( ch) TFifoWrite( obuff, owrite, UART_OSIZE, ch) // zapis znaku

// Lokalni funkce :

static void UartDisableInt( void);
static void UartEnableInt( void);

static void __irq UartHandler( void);
// Zpracovani preruseni od casovace

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void UARTInit( void)
// Inicializace komunikace
{
   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_UART; // select pin fuction
   CpuIrqAttach( UART_IRQ, VIC_UART, UartHandler);
} // UARTInit

//-----------------------------------------------------------------------------
// Odpojeni
//-----------------------------------------------------------------------------

void UARTDisconnect( void)
// Disconnect port pins
{
   PINSEL     = (PINSEL & ~PINSEL_MASK);          // back to GPIO
   GPIO_UART &= ~GPIO_UART_MASK;                  // direction to input
} // UARTDisconnect

//-----------------------------------------------------------------------------
// Setup
//-----------------------------------------------------------------------------

// format conversion table
static const byte _LcrFormat[ _COM_FORMAT_COUNT] = {
/* COM_8BITS       */  LCR_CHAR8 | LCR_STOP1 | LCR_NO_PARITY,
/* COM_8BITS_EVEN  */  LCR_CHAR8 | LCR_STOP1 | LCR_EVEN_PARITY,
/* COM_8BITS_ODD   */  LCR_CHAR8 | LCR_STOP1 | LCR_ODD_PARITY,
/* COM_8BITS_MARK  */  LCR_CHAR8 | LCR_STOP1 | LCR_MARK_PARITY,
/* COM_8BITS_SPACE */  LCR_CHAR8 | LCR_STOP1 | LCR_SPACE_PARITY,
/* COM_7BITS       */  LCR_CHAR7 | LCR_STOP1 | LCR_NO_PARITY,
/* COM_7BITS_EVEN  */  LCR_CHAR7 | LCR_STOP1 | LCR_EVEN_PARITY,
/* COM_7BITS_ODD   */  LCR_CHAR7 | LCR_STOP1 | LCR_ODD_PARITY,
/* COM_7BITS_MARK  */  LCR_CHAR7 | LCR_STOP1 | LCR_MARK_PARITY,
/* COM_7BITS_SPACE */  LCR_CHAR7 | LCR_STOP1 | LCR_SPACE_PARITY
};

void UARTSetup( unsigned Baud, unsigned Format, word Timeout)
// Set communication parameters
{
unsigned BaudDivisor;

   UartDisableInt();
   UART_IIR = 0;                       // clear interrupt ID register
   UART_LSR = 0;                       // clear line status register
   // set divisor :
   #ifndef UART_MULVAL
      BaudDivisor = (FBUS / 16) / Baud;
   #else
      BaudDivisor = ((FBUS / 16) * UART_MULVAL) / (UART_MULVAL + UART_DIVADDVAL) / Baud;
      UART_FDR = FDR_DIVADDVAL( UART_DIVADDVAL) | FDR_MULVAL( UART_MULVAL);
   #endif
   UART_LCR = LCR_DLAB;               // enable divisor latch access
   UART_DLL = BaudDivisor & 0xFF;
   UART_DLM = BaudDivisor >> 8;
   // set mode :
   UART_LCR = _LcrFormat[ Format];
   UART_FCR = FCR_RX_RESET | FCR_TX_RESET;   // clear & disable FIFO
   UartRFifoInit();
   UartTFifoInit();
   _tx_running = NO;
   _Timeout    = Timeout;             // rememeber value
   UartEnableInt();
} // UARTSetup

//-----------------------------------------------------------------------------
// Tx obsazeno
//-----------------------------------------------------------------------------

TYesNo UARTTxBusy( void)
// Vraci YES, probiha-li vysilani
{
   return( _tx_running);
} // UARTTxBusy

//-----------------------------------------------------------------------------
// Vysilani
//-----------------------------------------------------------------------------

void UARTTxChar( byte Char)
// Vyslani znaku
{
   if( UartTFifoFull()){
      return;                          // neni misto ve fronte
   }
   UartDisableInt();
   if( !_tx_running){
      // prazdne FIFO, vysilani je zastaveno
      _tx_running = YES;               // zahajeni vysilani
      UART_THR    = Char;              // vyslani prvniho znaku
   } else {
      // probiha vysilani
      UartTFifoWrite( Char);
   }
   UartEnableInt();
} // UARTTxChar

//-----------------------------------------------------------------------------
// Prijem
//-----------------------------------------------------------------------------

TYesNo UARTRxChar( byte *Char)
// Vraci prijaty znak, nebo NO pro timeout
{
native Now;

   Now = SysTime();
   do {
      if( !UartRFifoEmpty()){
         // prijat znak
         UartDisableInt();
         *Char = UartRFifoRead();
         UartEnableInt();
         return( YES);
      }
   } while( (SysTime() - Now) < _Timeout);
   return( NO);
} // UARTRxChar

//-----------------------------------------------------------------------------
// Cekani na Rx
//-----------------------------------------------------------------------------

TYesNo UARTRxWait( native Timeout)
// Ceka na znak az <Timeout> * 100 milisekund
{
native Now;

   Now = SysTime();
   do {
      WatchDog();
      if( !UartRFifoEmpty()){
         return( YES);                        // pripraven znak
      }
   } while( (SysTime() - Now) < Timeout);
   return( NO);
} // UARTRxWait

//-----------------------------------------------------------------------------
// Preskok prijmu
//-----------------------------------------------------------------------------

void UARTFlushChars( void)
// Vynecha vsechny znaky az do meziznakoveho timeoutu
{
native Now;

   Now = SysTime();
   do {
      WatchDog();
      if( !UartRFifoEmpty()){
         // prijat znak
         UartDisableInt();
         UartRFifoFlush();
         UartEnableInt();
         Now  = SysTime();             // timeout again
         continue;
      }
   } while( (SysTime() - Now) < _Timeout);
} // UARTFlushChars

#ifdef UART_BREAK_DETECTION
//-----------------------------------------------------------------------------
// Test Break
//-----------------------------------------------------------------------------

TYesNo UARTIsBreak( void)
// Testuje Tx break
{
   return( UART_LCR & LCR_BREAK);           // Tx break
} // UARTIsBreak
#endif

//-----------------------------------------------------------------------------
// Povoleni/zakaz preruseni
//-----------------------------------------------------------------------------

static void UartDisableInt( void)
{
   UART_IER = 0;
} // UartDisableInt

static void UartEnableInt( void)
{
   UART_IER = IER_RBR | IER_THRE;
} // UartEnableInt

//-----------------------------------------------------------------------------
// Prerusovaci rutina
//-----------------------------------------------------------------------------

static void __irq UartHandler( void)
// Zpracovani preruseni od casovace
{
#ifdef UART_BREAK_DETECTION
byte c;
#endif

   forever {
      switch( UART_IIR & (IIR_NO_INTERRUPT | IIR_MASK)){
         case IIR_NO_INTERRUPT :
            goto done;

         case IIR_THRE :
            // Preruseni od vysilace
            if( UartTFifoEmpty()){
               _tx_running = NO;                 // dovysilano
               goto done;                        // fronta je prazdna
            }
            UART_THR = UartTFifoRead();          // send data
            break;

         case IIR_RDA :
            // Preruseni od prijimace
#ifdef UART_BREAK_DETECTION
            if( UART_LSR & LSR_BI){
               // break aktivni
               c = UART_RBR;                     // cti preruseny znak
               UART_LCR |=  LCR_BREAK;           // Tx break
            } else {
               // prijem znaku
               UART_LCR &= ~LCR_BREAK;           // cancel Tx break
               if( UartRFifoFull()){
                  UartRFifoFlush();              // zahod nejstarsi znak
               }
               UartRFifoWrite( UART_RBR);        // uloz prijaty znak
            }
#else
            if( UartRFifoFull()){
               UartRFifoFlush();                 // zahod nejstarsi znak
            }
            UartRFifoWrite( UART_RBR);           // uloz prijaty znak
#endif
            break;
      }
   }
   //---------------------------------------------------------------------------
done :
   CpuIrqDone();
} // UartHandler
