//*****************************************************************************
//
//    Cpu.c        Philips LPC CPU functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/System.h"
#include "../../inc/Cpu.h"
#include <Hardware.h>

// PLL settings :
#if FXTAL < FCPU
   #define PLL_MULTIPLIER   (FCPU / FXTAL)            // M value
   #define PLL_DIVIDIER     (320000000 / 2 / FCPU)    // P value
#endif

// memory accelerator clock :
#if FCPU <= 20000000L                  // up to 20 MHz
   #define MAM_CLOCKS 1                // memory clock = cpu clock
#elif FCPU <= 40000000L                // up to 40 MHz
   #define MAM_CLOCKS 2                // memory clock = cpu clock / 2
#else                                  // up to 60 MHz
   #define MAM_CLOCKS 3                // memory clock = cpu clock / 3
#endif

// Peripheral clock :
#if   FBUS == FCPU
   #define VPB_CLOCK  VPBDIV_SAME
#elif FBUS == (FCPU / 2)
   #define VPB_CLOCK  VPBDIV_HALF
#elif FBUS == (FCPU / 4)
   #define VPB_CLOCK  VPBDIV_QUARTER
#else
   #error "Incorrect FBUS frequency"
#endif

// Local functions :

static void __irq NonvectoredIrqHandler( void);
// Non-vectored IRQ handler

//------------------------------------------------------------------------------
//   CPU initialisation
//------------------------------------------------------------------------------

void CpuInit( void)
// Cpu startup initialisation
{
   // memory accelerator :
   MAM_MAMCR  = MAMCR_DISABLED;        // turn off before timing setup
   MAM_MAMTIM = MAM_CLOCKS;            // memory access clocks
   MAM_MAMCR  = MAM_MODE;              // requested mode
   // PLL unit :
   #if FXTAL < FCPU
   {
   dword psel;

      psel = PLL_DIVIDIER;                           // raw P value
      // round P values at 1,2,4,8 - encode as PSEL :
      if( psel & 0x08){
         psel = 3;
      } else if( psel & 0x04){
         psel = 2;
      } else if( psel & 0x02){
         psel = 1;
      } else {
         psel = 0;
      }
      SCB_PLLCFG  = PLLCFG_SET( PLL_MULTIPLIER - 1, psel);
      SCB_PLLCON  = PLLCON_ENABLE;                   // enable PLL
      SCB_PLLFEED = PLLFEED_1;                       // launch sequence
      SCB_PLLFEED = PLLFEED_2;
      while( !(SCB_PLLSTAT & PLLSTAT_LOCK));         // wait for lock
      SCB_PLLCON  = PLLCON_ENABLE | PLLCON_CONNECT;  // connect PLL
      SCB_PLLFEED = PLLFEED_1;                       // launch sequence
      SCB_PLLFEED = PLLFEED_2;
   }
   #endif
   // peripheral bus clock :
   SCB_VPBDIV = VPB_CLOCK;
   // vectored interrupt controller :
   VICDefVectAddr = (unsigned)NonvectoredIrqHandler; // non-vectored handler
} // CpuInit

//------------------------------------------------------------------------------
//   CPU initialisation
//------------------------------------------------------------------------------

void CpuInitIap( void)
// Cpu IAP initialisation
{
   // memory mapping :
   SCB_MEMMAP = MEMMAP_FLASH;          // map vectors to flash
   // memory accelerator :
   MAM_MAMCR  = MAMCR_DISABLED;        // disable cache
   // PLL unit :
   SCB_PLLCON  = 0;                    // disable PLL
   SCB_PLLFEED = PLLFEED_1;            // launch sequence
   SCB_PLLFEED = PLLFEED_2;
   // peripheral bus clock :
   SCB_VPBDIV  = VPBDIV_QUARTER;       // default is 1/4
} // CpuInitIap

//------------------------------------------------------------------------------
//   FIQ
//------------------------------------------------------------------------------

void CpuFiqAttach( int channel)
// Enable FIQ for <channel>
{
   VICIntSelect |= 1 << channel;                      // enable as FIQ
   CpuIrqEnable( channel);                            // enable input
} // CpuIrqAttach

//------------------------------------------------------------------------------
//   IRQ
//------------------------------------------------------------------------------

void CpuIrqAttach( int irq, int channel, TIrqHandler *handler)
// Install IRQ handler
{
   VIC_VECTOR[  irq] = (unsigned)handler;             // set handler
   VIC_CONTROL[ irq] = VIC_SET_CHANNEL( channel);     // set channel
   CpuIrqEnable( channel);                            // enable input
} // CpuIrqAttach

//------------------------------------------------------------------------------
//   IRQ Enable
//------------------------------------------------------------------------------

void CpuIrqEnable( int channel)
// Enable IRQ on <channel>. Must not be used inside interrupt handler
{
dword Mask;

   Mask  = VICIntEnable;               // read mask
   Mask |= 1 << channel;               // enable channel
   VICIntEnable = Mask;                // write back
} // CpuIrqEnable

//------------------------------------------------------------------------------
//   IRQ Disable
//------------------------------------------------------------------------------

void CpuIrqDisable( int channel)
// Disable IRQ on <channel>
{
   VICIntEnClr = 1 << channel;
} // CpuIrqDisable

//------------------------------------------------------------------------------
//   IRQ Done
//------------------------------------------------------------------------------

void CpuIrqDone( void)
// Last operation in the interrupt handler
{
   VICVectAddr = 0;                    // VIC interrupt done
} // CpuIrqDone

//------------------------------------------------------------------------------
//   Enable iterrupts
//------------------------------------------------------------------------------

void EnableInts( void)
// Enable interrupts
{
register unsigned _cpsr;

  __asm__ __volatile__ ( " mrs  %0, cpsr" : "=r" (_cpsr) : /* no inputs */  );
  _cpsr &= ~(STATUS_IRQ | STATUS_FIQ);
  __asm__ __volatile__ (" msr  cpsr, %0" : /* no outputs */ : "r" (_cpsr)  );
} // EnableInts

//------------------------------------------------------------------------------
//   Disable iterrupts
//------------------------------------------------------------------------------

void DisableInts( void)
// Disable interrupts
{
register unsigned _cpsr;

  __asm__ __volatile__ (" mrs  %0, cpsr" : "=r" (_cpsr) : /* no inputs */  );
  _cpsr |= STATUS_IRQ | STATUS_FIQ;
  __asm__ __volatile__ (" msr  cpsr, %0" : /* no outputs */ : "r" (_cpsr)  );
} // DisableInts

//------------------------------------------------------------------------------
//   Watchdog
//------------------------------------------------------------------------------

void StartWatchDog( void)
// Start watchdog
{
#ifdef WATCHDOG_ENABLE
   WD_WDTC   = WATCHDOG_INTERVAL * (FBUS / 1000) / 4;
   WD_WDMOD |= WDMOD_ENABLE | WDMOD_RESET;
   WatchDog();                                  // initial feed sequence
#endif
} // StartWatchdog


void WatchDog( void)
// Refresh watchdog
{
#ifdef WATCHDOG_ENABLE
   DisableInts();
   WD_WDFEED = WDFEED_1;
   WD_WDFEED = WDFEED_2;
   EnableInts();
#endif
} // WatchDog

void WatchDogNaked( void)
// Refresh Watchdog from interrupt
{
#ifdef WATCHDOG_ENABLE
   WD_WDFEED = WDFEED_1;
   WD_WDFEED = WDFEED_2;
#endif
} // WatchDogNaked

TYesNo WatchDogActive( void)
// Returns YES by watchdog reset
{
#ifdef WATCHDOG_ENABLE
TYesNo Active;

   Active    = WD_WDMOD & WDMOD_FLAG;  // save reset reason
   WD_WDMOD &= ~WDMOD_FLAG;            // clear reset reason
   return( Active);
#else
   return( NO);
#endif
} // WatchDogActive

//------------------------------------------------------------------------------
//   Delay ms
//------------------------------------------------------------------------------

#ifdef WATCHDOG_ENABLE
   #define WD_CYCLES  (9 + 9 + 8)
#else
   #define WD_CYCLES  0
#endif

#define LOOP_CYCLES   4
#define MS_COUNT      ((FCPU / 1000 / LOOP_CYCLES) - WD_CYCLES)

#define EmptyLoop( ms)  __asm__ __volatile__ (                   \
                                     "L_LOOP_%=:           \n\t" \
                                     "subs   %0, %0, #1   \n\t"  \
                                     "bne   L_LOOP_%=   \n\t"    \
                                  :  /* no outputs */ : "r" (ms));

void SysDelay( dword ms)
// Delay <ms>
{
dword i;

   do {
      i = MS_COUNT;
      EmptyLoop( i);
#ifdef WATCHDOG_ENABLE
      DisableInts();
      WD_WDFEED = WDFEED_1;
      WD_WDFEED = WDFEED_2;
      EnableInts();
#endif
   } while( --ms);
} // SysDelay

//------------------------------------------------------------------------------
//   Delay us
//------------------------------------------------------------------------------

void SysUDelay( dword us)
// Delay <us>
{
dword i;

   i  = FCPU / 100;
   i *= (us + 1);
   i /= 10000 * LOOP_CYCLES;
   EmptyLoop( i);
} // SysUDelay

//------------------------------------------------------------------------------
//   Non-vectored interrupt
//------------------------------------------------------------------------------

static void __irq NonvectoredIrqHandler( void)
// Non-vectored IRQ handler
{
   // no functionality
   VICVectAddr = 0;                    // VIC interrupt done
} // NonvectoredIrqHandler
