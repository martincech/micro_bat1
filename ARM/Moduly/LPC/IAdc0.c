//*****************************************************************************
//
//    IAdc0.c      Internal A/D convertor 0 functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/IAdc.h"

#if  defined( __LPC213x__)
   #if defined( IADC_AD00)
      // P0.27 function 01
      #define AD00_MASK (3 << 22)
      #define AD00_SEL  (1 << 22)
   #else
      #define AD00_MASK 0
      #define AD00_SEL  0
   #endif
   #if defined( IADC_AD01)
      // P0.28 function 01
      #define AD01_MASK (3 << 24)
      #define AD01_SEL  (1 << 24)
   #else
      #define AD01_MASK 0
      #define AD01_SEL  0
   #endif
   #if defined( IADC_AD02)
      // P0.29 function 01
      #define AD02_MASK (3 << 26)
      #define AD02_SEL  (1 << 26)
   #else
      #define AD02_MASK 0
      #define AD02_SEL  0
   #endif
   #if defined( IADC_AD03)
      // P0.30 function 01
      #define AD03_MASK (3 << 28)
      #define AD03_SEL  (1 << 28)
   #else
      #define AD03_MASK 0
      #define AD03_SEL  0
   #endif
   #if defined( IADC_AD04)
      // P0.25 function 01
      #define AD04_MASK (3 << 18)
      #define AD04_SEL  (1 << 18)
   #else
      #define AD04_MASK 0
      #define AD04_SEL  0
   #endif
   #if defined( IADC_AD05)
      // P0.26 function 01
      #define AD05_MASK (3 << 20)
      #define AD05_SEL  (1 << 20)
   #else
      #define AD05_MASK 0
      #define AD05_SEL  0
   #endif
   #if defined( IADC_AD06)
      // P0.4 function 11
      #define AD06_MASK (3 << 8)
      #define AD06_SEL  (3 << 8)
   #else
      #define AD06_MASK 0
      #define AD06_SEL  0
   #endif
   #if defined( IADC_AD07)
      // P0.5 function 11
      #define AD07_MASK (3 << 10)
      #define AD07_SEL  (3 << 10)
   #else
      #define AD07_MASK 0
      #define AD07_SEL  0
   #endif

   #define PINSEL0_MASK   (AD06_MASK |AD07_MASK)
   #define PINSEL0_ADC    (AD06_SEL  |AD07_SEL)

   #define PINSEL1_MASK   (AD00_MASK |AD01_MASK |AD02_MASK |AD03_MASK |AD04_MASK |AD05_MASK)
   #define PINSEL1_ADC    (AD00_SEL  |AD01_SEL  |AD02_SEL  |AD03_SEL  |AD04_SEL  |AD05_SEL)
#else
   #error "Unknown processor model"
#endif

#define IADC_FREQUENCY 4500000

#if FBUS <= IADC_FREQUENCY
   #define CLOCK_DIV 0
#else
   #define CLOCK_DIV (((FBUS + IADC_FREQUENCY - 1) / IADC_FREQUENCY) - 1)
#endif

#define ADCR_DEFAULT ADCR_PDN | ADCR_10BITS | ADCR_CLKDIV( CLOCK_DIV)

//------------------------------------------------------------------------------
//   Initialisation
//------------------------------------------------------------------------------

void IAdcInit( void)
// Inicializace prevodniku
{
   PCB_PINSEL0 = (PCB_PINSEL0 & ~PINSEL0_MASK) | PINSEL0_ADC; // select pin fuction 0
   PCB_PINSEL1 = (PCB_PINSEL1 & ~PINSEL1_MASK) | PINSEL1_ADC; // select pin fuction 1
   AD0_ADCR = ADCR_DEFAULT;            // power on & clock
} //  IAdcInit

//------------------------------------------------------------------------------
//   Read
//------------------------------------------------------------------------------

word IAdcRead( int Channel)
// Cteni vstupu <Channel> prevodniku
{
dword DataRegister;

   Channel &= 0x07;
   AD0_ADCR = ADCR_DEFAULT | ADCR_START_NOW | ADCR_SEL( Channel); // start conversion
   forever {                           // wait for conversion
      DataRegister = AD0_ADGDR;
      if( DataRegister & ADGDR_DONE){
         return( (word)ADGDR_RESULT( DataRegister));
      }
   }
} // IAdcRead
