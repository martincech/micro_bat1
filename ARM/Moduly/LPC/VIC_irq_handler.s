/*****************************************************************************
  IRQ handler for a VIC located at 0xFFFFF000

  Copyright (c) 2005, 2007 Rowley Associates Limited.

  This file may be distributed under the terms of the License Agreement
  provided with this software. 
 
  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE
  WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *****************************************************************************/

  .code 32
  .global irq_handler

#ifdef PL192
#define VICVECTADDR 0xF00
#else
#define VICVECTADDR 0x30
#endif

irq_handler:
  // store the APCS registers
  stmfd sp!, {r0-r4, r12, lr}
  //((CTL_ISR_FN_t)VICVectAddr)(VICIRQStatus);
  ldr r0, =0xFFFFF000
  ldr r1, [r0, #VICVECTADDR]
  ldr r0, [r0]
  mov lr, pc
  bx r1
  // return from interrupt
  ldmfd sp!, {r0-r4, r12, lr}
  subs pc, lr, #4
