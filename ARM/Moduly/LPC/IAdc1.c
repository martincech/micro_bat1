//*****************************************************************************
//
//    IAdc1.c      Internal A/D convertor 1 functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/IAdc.h"

#if  defined( __LPC213x__)
   #if defined( IADC_AD10)
      // P0.6 function 11
      #define AD10_MASK (3 << 12)
      #define AD10_SEL  (3 << 12)
   #else
      #define AD10_MASK 0
      #define AD10_SEL  0
   #endif
   #if defined( IADC_AD11)
      // P0.8 function 11
      #define AD11_MASK (3 << 16)
      #define AD11_SEL  (3 << 16)
   #else
      #define AD11_MASK 0
      #define AD11_SEL  0
   #endif
   #if defined( IADC_AD12)
      // P0.10 function 11
      #define AD12_MASK (3 << 20)
      #define AD12_SEL  (3 << 20)
   #else
      #define AD12_MASK 0
      #define AD12_SEL  0
   #endif
   #if defined( IADC_AD13)
      // P0.12 function 11
      #define AD13_MASK (3 << 24)
      #define AD13_SEL  (3 << 24)
   #else
      #define AD13_MASK 0
      #define AD13_SEL  0
   #endif
   #if defined( IADC_AD14)
      // P0.13 function 11
      #define AD14_MASK (3 << 28)
      #define AD14_SEL  (3 << 28)
   #else
      #define AD14_MASK 0
      #define AD14_SEL  0
   #endif
   #if defined( IADC_AD15)
      // P0.15 function 11
      #define AD15_MASK (3 << 30)
      #define AD15_SEL  (3 << 30)
   #else
      #define AD15_MASK 0
      #define AD15_SEL  0
   #endif
   #if defined( IADC_AD16)
      // P0.21 function 10
      #define AD16_MASK (3 << 10)
      #define AD16_SEL  (2 << 10)
   #else
      #define AD16_MASK 0
      #define AD16_SEL  0
   #endif
   #if defined( IADC_AD17)
      // P0.22 function 01
      #define AD17_MASK (3 << 12)
      #define AD17_SEL  (1 << 12)
   #else
      #define AD17_MASK 0
      #define AD17_SEL  0
   #endif

   #define PINSEL0_MASK   (AD10_MASK |AD11_MASK |AD12_MASK |AD13_MASK |AD14_MASK |AD15_MASK)
   #define PINSEL0_ADC    (AD10_SEL  |AD11_SEL  |AD12_SEL  |AD13_SEL  |AD14_SEL  |AD15_SEL)

   #define PINSEL1_MASK   (AD16_MASK | AD17_MASK)
   #define PINSEL1_ADC    (AD16_SEL  | AD17_SEL)
#else
   #error "Unknown processor model"
#endif

#define IADC_FREQUENCY 4500000

#if FBUS <= IADC_FREQUENCY
   #define CLOCK_DIV 0
#else
   #define CLOCK_DIV (((FBUS + IADC_FREQUENCY - 1) / IADC_FREQUENCY) - 1)
#endif

#define ADCR_DEFAULT ADCR_PDN | ADCR_10BITS | ADCR_CLKDIV( CLOCK_DIV)

//------------------------------------------------------------------------------
//   Initialisation
//------------------------------------------------------------------------------

void IAdcInit( void)
// Inicializace prevodniku
{
   PCB_PINSEL0 = (PCB_PINSEL0 & ~PINSEL0_MASK) | PINSEL0_ADC; // select pin fuction 0
   PCB_PINSEL1 = (PCB_PINSEL1 & ~PINSEL1_MASK) | PINSEL1_ADC; // select pin fuction 1
   AD1_ADCR = ADCR_DEFAULT;            // power on & clock
} //  IAdcInit

//------------------------------------------------------------------------------
//   Read
//------------------------------------------------------------------------------

word IAdcRead( int Channel)
// Cteni vstupu <Channel> prevodniku
{
dword DataRegister;

   Channel &= 0x07;
   AD1_ADCR = ADCR_DEFAULT | ADCR_START_NOW | ADCR_SEL( Channel); // start conversion
   forever {                           // wait for conversion
      DataRegister = AD1_ADGDR;
      if( DataRegister & ADGDR_DONE){
         return( (word)ADGDR_RESULT( DataRegister));
      }
   }
} // IAdcRead
