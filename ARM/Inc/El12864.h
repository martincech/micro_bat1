//*****************************************************************************
//
//    El12864.h  -  El12864/KS108 graphic controller services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __El12864_H__
   #define __El12864_H__

#ifndef __Gpu_H__
   #include "Gpu.h"                    // zakladni funkce + buffer
#endif

// Rozsahy pro GpuGoto :
#define GPU_MAX_ROW         8          // pocet radku
#define GPU_MAX_COLUMN    128          // pocet sloupcu (bytu) na radku

//-----------------------------------------------------------------------------
// Rozsirujici funkce GPU
//-----------------------------------------------------------------------------

void GpuOn( void);
// Zapne display

void GpuOff( void);
// Vypne display

void GpuClear( void);
// Smaze display

void GpuGoto( int Row, int Column);
// Nastavi souradnici zapisu na radek <Row>, sloupec <Column>

void GpuWrite( unsigned Pattern);
// Zapise byte <Pattern> na nastavenou souradnici (inkrementuje <Column>)

void GpuDone( void);
// Ukonci zapis

#ifndef GPU_DISABLE_BUFFER
void GpuInitBuffer( void);
// Inicializace bufferu
#endif

#endif
