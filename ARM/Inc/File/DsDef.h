//******************************************************************************
//
//   DsDef.h      Data Set definition
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __DsDef_H__
   #define __DsDef_H__

#ifndef __Uni_H__
   #include "..\Uni.h"
#endif

#define DS_SIZE     (((FDIR_SIZE + 1) + 7) / 8)  // dataset bytes
typedef byte TFDataSet[ DS_SIZE];

#endif
