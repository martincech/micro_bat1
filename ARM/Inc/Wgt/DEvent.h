//******************************************************************************
//                                                                            
//  DEvent.h       Process events
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DEvent_H__
   #define __DEvent_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void DEventDiscard( void);
// Discard waiting events

int DEventWait( void);
// Wait for event

void DEventWaitForEnter( void);
// Wait for Enter key

TYesNo DEventWaitForEnterEsc( void);
// Wait for Enter/Esc key

#endif
