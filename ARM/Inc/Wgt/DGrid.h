//******************************************************************************
//                                                                            
//  DGrid.h        Display grid
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DGrid_H__
   #define __DGrid_H__

#ifndef __StrDef_H__
   #include "StrDef.h"
#endif

void DGridTitle( TUniStr Title);
// Display window <Title>

void DGridStatus( void);
// Clear status area

void DGridFrame( void);
// Display left & right line

void DGridButton( TUniStr Button);
// Display one <Button>

void DGridTwoButtons( TUniStr LButton, TUniStr RButton);
// Display two buttons

int DGridWidth( void);
// Returns width of main area

int DGridHeight( void);
// Returns height of main area

#endif
