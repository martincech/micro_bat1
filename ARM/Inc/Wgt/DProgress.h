//******************************************************************************
//                                                                            
//  DProgress.h    Display progress bar
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DProgress_H__
   #define __Progress_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void DProgress( int Percent, int x, int y, int Width, int Height);
// Display progress with <Percent>

void DProgressRight( int Percent, int x, int y, int Width, int Height);
// Display progress with <Percent> from right to left

void DProgressBar( int Percent, int x, int y, int Width, int Height);
// Display progress bar only with <Percent>

void DProgressBarRight( int Percent, int x, int y, int Width, int Height);
// Display progress bar only with <Percent> from right to left

#endif
