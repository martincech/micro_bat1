//******************************************************************************
//                                                                            
//  DLabel.h       Display text label
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DLabel_H__
   #define __DLabel_H__

#ifndef __StrDef_H__
   #include "StrDef.h"
#endif

// if <Width>  == 0 then uses <x> coordinate only
// if <Height> == 0 then uses <y> coordinate only

// Label centering mode :
typedef enum {
   CENTER_LEFT,              // center left corner
   CENTER_MIDDLE,            // center to middle
   CENTER_RIGHT              // center right corner
} TCenterType;

//------------------------------------------------------------------------------
//  Text Label
//------------------------------------------------------------------------------

void DLabel( TUniStr Text, int x, int y);
// Display <Text> at <x,y>

void DLabelCenter( TUniStr Text, int x, int y, int Width, int Height);
// Display <Text> centered at rectangle <x,y> with <Width,Height>

void DLabelRight( TUniStr Text, int x, int y);
// Display <Text> with upper right corner at <x,y>

void DLabelFormat( int x, int y, const char *Format, ...);
// Display label by <Format> with current alignment

#ifndef DALIGN_RIGHT
   #define DLabelNarrow( Text, x, y) DLabel( Text, x, y);
   // Display <Text> at <x,y> with current alignment
#else
   #define DLabelNarrow( Text, x, y) DLabelRight( Text, x, y);  
   // Display <Text> at <x,y> with current alignment
#endif

void DLabelFixed( TUniStr Text, int x, int y);
// Display <Text> at <x,y> with fixed pitch

int DLabelHeight( TUniStr Text);
// Returns pixel height of <Text>

int DLabelWidth( TUniStr Text);
// Returns pixel width of <Text>

//------------------------------------------------------------------------------
//  Enum Label
//------------------------------------------------------------------------------

void DLabelEnum( int Enum, TUniStr Base, int x, int y);
// Display <Enum> value by <Base> at <x,y> with current alignment

void DLabelEnumCenter( int Enum, TUniStr Base, int x, int y, int Width, TCenterType Center);
// Display <Enum> value at <x,y> with <Width> by <Center> mode

int DLabelEnumWidth( TUniStr Base, int EnumCount);
// Get enum pixel width

//------------------------------------------------------------------------------
//  List Label
//------------------------------------------------------------------------------

void DLabelList( int Index, const TUniStr *List, int x, int y);
// Display <Index> value by <List> at <x,y> with current alignment

void DLabelListCenter( int Index, const TUniStr *List, int x, int y, int Width, TCenterType Center);
// Display <List> value at <x,y> with <Width> by <Center> mode

int DLabelListWidth( const TUniStr *List);
// Get <List> pixel width

#endif
