//*****************************************************************************
//
//    Statistics.h  -  Statistics utility
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Statistics_H__
   #define __Statistics_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Popisovac statistiky :
typedef struct {
   TNumber    XSuma;
   TNumber    X2Suma;
   TNumber    UniMin;
   TNumber    UniMax;
   TStatCount Count;
   TStatCount CountIn;
   TStatCount CountOut;
} TStatistic;

//-----------------------------------------------------------------------------

void StatClear( TStatistic *statistic);
// smaze statistiku

void StatAdd( TStatistic *statistic, TNumber x);
// prida do statistiky

void StatRemove( TStatistic *statistic, TNumber x);
// odebere ze statistiky

TNumber StatAverage( TStatistic *statistic);
// vrati stredni hodnotu

TNumber StatSigma( TStatistic *statistic);
// vrati smerodatnou odchylku

TNumber StatVariation( TNumber average, TNumber sigma);
// vrati variaci (procentni smerodatna odchylka)

void StatUniformityInit( TStatistic *statistic, TNumber Min, TNumber Max);
// Inicializuje uniformitu

void StatUniformityAdd( TStatistic *statistic, TNumber Number);
// Prida novy vzorek do uniformity

TNumber StatUniformityGet( TStatistic *statistic);
// Vypocte uniformitu

#endif
