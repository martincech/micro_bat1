//*****************************************************************************
//
//    Sound.h      Sound creation
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef _Sound_H__
   #define __Sound_H__
   
#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Note :
typedef enum {
   NOTE_C1,
   NOTE_CIS1,
   NOTE_D1,
   NOTE_DIS1,
   NOTE_E1,
   NOTE_F1,
   NOTE_FIS1,
   NOTE_G1,
   NOTE_GIS1,
   NOTE_A1,
   NOTE_AIS1,
   NOTE_H1,

   NOTE_C2,
   NOTE_CIS2,
   NOTE_D2,
   NOTE_DIS2,
   NOTE_E2,
   NOTE_F2,
   NOTE_FIS2,
   NOTE_G2,
   NOTE_GIS2,
   NOTE_A2,
   NOTE_AIS2,
   NOTE_H2,

   NOTE_C3,
   NOTE_CIS3,
   NOTE_D3,
   NOTE_DIS3,
   NOTE_E3,
   NOTE_F3,
   NOTE_FIS3,
   NOTE_G3,
   NOTE_GIS3,
   NOTE_A3,
   NOTE_AIS3,
   NOTE_H3,

   NOTE_C4,
   NOTE_CIS4,
   NOTE_D4,
   NOTE_DIS4,
   NOTE_E4,
   NOTE_F4,
   NOTE_FIS4,
   NOTE_G4,
   NOTE_GIS4,
   NOTE_A4,
   NOTE_AIS4,
   NOTE_H4,

   NOTE_C5,
} TSndNote;

#define OCTAVE_MAX    3                // highest possible octave 0..OCTAVE_MAX

// Volume by 3 dB :
#define VOL_MAX       9                // volume range 0..VOL_MAX

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void SndInit( void);
// Initialisation

void SndBeep( int Note, int Volume, int Duration);
// Plays asynchronously <Note> with <Volume> and <Duration> [ms]

void SndSBeep( int Note, int Volume, int Duration);
// Plays synchronously <Note> with <Volume> and <Duration> [ms]

void SndSound( int Note, int Volume);
// Starts play <Note> with <Volume>

void SndNosound( void);
// Stops sound

//-----------------------------------------------------------------------------
// For internal use only
//-----------------------------------------------------------------------------

extern volatile short _snd_duration;             // duration coounter

#ifdef SOUND_PINSEL_INTERRUPT
   void SndNosoundInt( void);
   // Stops sound inside interrupt (WARNING - for internal use only)

   #define SndTrigger()              CheckTrigger( _snd_duration, SndNosoundInt())
   // Timer interrupt routine
   #define SndStop()                 _snd_duration = 0;
   // Stop timer
#else
   #define SndTrigger()              CheckTrigger( _snd_duration, SndNosound())
   // Timer interrupt routine
   #define SndStop()                 _snd_duration = 0;
   // Stop timer
#endif

#endif
