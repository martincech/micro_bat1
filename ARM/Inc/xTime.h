//******************************************************************************
//                                                                            
//  xTime.h        putchar based display date & time
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __xTime_H__
   #define __xTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __DtDef_H__
   #include "DtDef.h"
#endif

#ifndef __PutcharDef_H__
   #include "PutcharDef.h"
#endif

#define XDATE_MONTH_SIZE   3        // month shortcut size

void xTime( TPutchar *xputchar, TTimestamp Now);
// Display time only

int xTimeWidth( void);
// Returns character width of time

void xTimeShort( TPutchar *xputchar, TTimestamp Now);
// Display time without seconds

int xTimeShortWidth( void);
// Returns character width of time

const char *xTimeSuffix( TTimeSuffix Suffix);
// Returns suffix by <Suffix> enum

void xDate( TPutchar *xputchar, TTimestamp Now);
// Display date only

int xDateWidth( void);
// Returns character width of date

void xDateTime( TPutchar *xputchar, TTimestamp Now);
// Display date and time

int xDateTimeWidth( void);
// Returns character width of date time

void xDateTimeShort( TPutchar *xputchar, TTimestamp Now);
// Display date and time without seconds

int xDateTimeShortWidth( void);
// Returns character width of date time

const char *xDateMonth( TDtMonth Month);
// Returns <Month> shortcut

#endif
